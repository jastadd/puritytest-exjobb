IF %1=="-c" GOTO CHASE
IF %1=="-ef" GOTO EFFTP
IF %1=="-extj" GOTO PurityChecker
IF %1=="-igj" GOTO IGJ
IF %1=="-oigj" GOTO OIGJ
IF %1=="-jav" GOTO JAV
IF %1=="-jpure" GOTO JPURE

:CHASE
echo CHASE
"java" "-cp" "Tools/chase/antlr.jar;Tools/chase/ChAsE.jar;Tools/chase/jml-checker.jar;" "assignable.CheckModClause" "Tools/chase/Test.java" "Tools/chase/B.java" "Tools/chase/A.java"  
GOTO END

:REIM
echo REIM
GOTO END


:EFFTP
echo "EFFTP"
"scalac" "-P:effects:domains:purity:exceptions:io" "-Xplugin:Tools/efftp/effects-plugin_2.10-0.1-SNAPSHOT.jar" "-cp" "Tools/efftp/effects-plugin_2.10-0.1-SNAPSHOT.jar" "Tools/Efftp/test/efftp.java" "Tools/Efftp/test/neg.scala"
GOTO END

:PurityChecker
echo "PurityChecker"
"java" "-jar" "Tools/ExtendJPure/puritytest-exjobb-extendjextension.jar" "-cp" "beaver-rt.jar"  "Tools/ExtendJPure/testfiles/*.java"

:IGJ
echo "IGJ"
"java" "-Xbootclasspath/p:beaver-rt.jar:beaver-ant.jar:dist/*.jar" "-jar" "dist/checker.jar" "-cp" "dist/*.jar;beaver-rt.jar" "-processor" "org.checkerframework.checker.igj.IGJChecker" " AnnotatedTests_ReimJavari/small_GJ/*.java"
GOTO END

:JAVARI
echo "JAVARI"
"java" "-Xbootclasspath/p:beaver-rt.jar:beaver-ant.jar:dist/*.jar" "-jar" "dist/checker.jar" "-cp" "dist/*.jar;beaver-rt.jar" "-processor" "org.checkerframework.checker.javari.JavariChecker" " AnnotatedTests_ReimJavari/JavSmall/*.java"
GOTO END

echo "JAVARIFIER"

:JPURE
echo "JPURE"
"java" "-cp" "Tools/JPure/Jpurelib/antlr-3.5.2-complete.jar;Tools/JPure/Jpurelib/jkit.jar;Tools/JPure/jpureb.jar;Tools/JPure/Jpurelib/jrt.jar;beaver.rt.jar;beaver-ant.jar" "jpure.Main" "-d" "out/" "-classpath" "Tools/JPure/jpure.jar;Tools/JPure/Jpurelib/jrt.jar;beaver.rt.jar;beaver-ant.jar;javaTests/Jpure/F*.java"
GOTO END

:PURANO
echo "PURANO"
"java" "-cp" "Tools/Purano/purano.jar;Tools/Purano/lib/*;beaver.rt.jar;beaver-ant.jar;Tools/Purano/input/;" "jp.ac.osakau.farseerfc.purano.reflect.ClassFinder " "-output z.txt lang.ast -p 1 "
GOTO END
