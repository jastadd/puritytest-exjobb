/* Generated By:JJTree: Do not edit this line. ASTNormalAnnotation.java */

package org.jastadd.jrag.AST;

public class ASTNormalAnnotation extends SimpleNode {
  public ASTNormalAnnotation(int id) {
    super(id);
  }

  public ASTNormalAnnotation(JragParser p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(JragParserVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
}
