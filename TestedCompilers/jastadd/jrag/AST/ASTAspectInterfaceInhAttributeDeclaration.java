/* Generated By:JJTree: Do not edit this line. ASTAspectInterfaceInhAttributeDeclaration.java */

package org.jastadd.jrag.AST;

public class ASTAspectInterfaceInhAttributeDeclaration extends SimpleNode {
  public ASTAspectInterfaceInhAttributeDeclaration(int id) {
    super(id);
  }

  public ASTAspectInterfaceInhAttributeDeclaration(JragParser p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(JragParserVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
}
