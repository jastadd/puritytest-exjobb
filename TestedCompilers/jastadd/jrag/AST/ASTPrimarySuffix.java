/* Generated By:JJTree: Do not edit this line. ASTPrimarySuffix.java */

package org.jastadd.jrag.AST;

public class ASTPrimarySuffix extends SimpleNode {
  public ASTPrimarySuffix(int id) {
    super(id);
  }

  public ASTPrimarySuffix(JragParser p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(JragParserVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
}
