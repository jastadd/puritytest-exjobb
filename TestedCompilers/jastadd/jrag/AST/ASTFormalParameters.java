/* Generated By:JJTree: Do not edit this line. ASTFormalParameters.java */

package org.jastadd.jrag.AST;

public class ASTFormalParameters extends SimpleNode {
  public ASTFormalParameters(int id) {
    super(id);
  }

  public ASTFormalParameters(JragParser p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(JragParserVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
}
