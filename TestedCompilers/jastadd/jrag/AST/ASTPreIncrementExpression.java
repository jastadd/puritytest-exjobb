/* Generated By:JJTree: Do not edit this line. ASTPreIncrementExpression.java */

package org.jastadd.jrag.AST;

public class ASTPreIncrementExpression extends SimpleNode {
  public ASTPreIncrementExpression(int id) {
    super(id);
  }

  public ASTPreIncrementExpression(JragParser p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(JragParserVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
}
