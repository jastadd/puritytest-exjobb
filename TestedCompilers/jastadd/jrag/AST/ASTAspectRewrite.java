/* Generated By:JJTree: Do not edit this line. ASTAspectRewrite.java */

package org.jastadd.jrag.AST;

public class ASTAspectRewrite extends SimpleNode {
  public ASTAspectRewrite(int id) {
    super(id);
  }

  public ASTAspectRewrite(JragParser p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(JragParserVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
}
