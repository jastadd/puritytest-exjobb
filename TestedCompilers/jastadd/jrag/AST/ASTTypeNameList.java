/* Generated By:JJTree: Do not edit this line. ASTTypeNameList.java */

package org.jastadd.jrag.AST;

public class ASTTypeNameList extends SimpleNode {
  public ASTTypeNameList(int id) {
    super(id);
  }

  public ASTTypeNameList(JragParser p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(JragParserVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
}
