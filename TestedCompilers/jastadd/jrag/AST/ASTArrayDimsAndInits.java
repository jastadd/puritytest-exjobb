/* Generated By:JJTree: Do not edit this line. ASTArrayDimsAndInits.java */

package org.jastadd.jrag.AST;

public class ASTArrayDimsAndInits extends SimpleNode {
  public ASTArrayDimsAndInits(int id) {
    super(id);
  }

  public ASTArrayDimsAndInits(JragParser p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(JragParserVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
}
