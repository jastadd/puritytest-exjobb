/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package org.jastadd.ast.AST;
import org.jastadd.ast.AST.*;
import java.util.*;
import org.jastadd.Problem;
import org.jastadd.jrag.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jastadd.jrag.AST.ASTExpression;
import org.jastadd.jrag.Unparser;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.io.PrintStream;
import java.io.*;
import java.util.regex.*;
import org.jastadd.JastAdd;
import org.jastadd.jrag.AST.ASTAspectMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectRefineMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectFieldDeclaration;
import org.jastadd.jrag.AST.ASTBlock;
import org.jastadd.jrag.ClassBodyObject;
import org.jastadd.jrag.AST.ASTCompilationUnit;
import org.jastadd.ast.AST.Token;
import org.jastadd.ast.AST.SimpleNode;
import org.jastadd.Configuration;
import org.jastadd.tinytemplate.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Ast.ast:50
 * @production InhEq : {@link AttrEq} ::= <span class="component">{@link Parameter}*</span> <span class="component">&lt;Name:String&gt;</span> <span class="component">&lt;FileName:String&gt;</span> <span class="component">&lt;StartLine:int&gt;</span> <span class="component">&lt;EndLine:int&gt;</span> <span class="component">&lt;Comment:String&gt;</span> <span class="component">&lt;AspectName:String&gt;</span> <span class="component">&lt;ChildName:String&gt;</span> <span class="component">[Index:{@link Parameter}]</span>;

 */
public class InhEq extends AttrEq implements Cloneable {
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:250
   */
  public String hostName = "";
  /**
   * @declaredat ASTNode:1
   */
  public InhEq(int i) {
    super(i);
  }
  /**
   * @declaredat ASTNode:5
   */
  public InhEq(Ast p, int i) {
    this(i);
    parser = p;
  }
  /**
   * @declaredat ASTNode:10
   */
  public InhEq() {
    this(0);
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:19
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[3];
    setChild(new List(), 0);
    setChild(new List(), 1);
    setChild(new Opt(), 2);
  }
  /**
   * @declaredat ASTNode:25
   */
  public InhEq(List<Annotation> p0, List<Parameter> p1, String p2, String p3, int p4, int p5, String p6, String p7, String p8, Opt<Parameter> p9) {
    setChild(p0, 0);
    setChild(p1, 1);
    setName(p2);
    setFileName(p3);
    setStartLine(p4);
    setEndLine(p5);
    setComment(p6);
    setAspectName(p7);
    setChildName(p8);
    setChild(p9, 2);
  }
  /**
   * @declaredat ASTNode:37
   */
  public void dumpTree(String indent, java.io.PrintStream out) {
    out.print(indent + "InhEq");
    out.print("\"" + getName() + "\"");
    out.print("\"" + getFileName() + "\"");
    out.print("\"" + getStartLine() + "\"");
    out.print("\"" + getEndLine() + "\"");
    out.print("\"" + getComment() + "\"");
    out.print("\"" + getAspectName() + "\"");
    out.print("\"" + getChildName() + "\"");
    String childIndent = indent + "  ";
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).dumpTree(childIndent, out);
    }
  }
  /**
   * @declaredat ASTNode:51
   */
  public Object jjtAccept(AstVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
  /**
   * @declaredat ASTNode:54
   */
  public void jjtAddChild(Node n, int i) {
    checkChild(n, i);
    super.jjtAddChild(n, i);
  }
  /**
   * @declaredat ASTNode:58
   */
  public void checkChild(Node n, int i) {
      if (i == 0) {
        if (!(n instanceof List)) {
          throw new Error("Child number 0 of AttrEq has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof Annotation)) {
            throw new Error("Child number " + k + " in AnnotationList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of Annotation");
          }
        }
      }
      if (i == 1) {
        if (!(n instanceof List)) {
          throw new Error("Child number 1 of InhEq has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof Parameter)) {
            throw new Error("Child number " + k + " in ParameterList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of Parameter");
          }
        }
      }
    if (i == 2) {
      if (!(n instanceof Opt)) {
        throw new Error("Child number 2 of InhEq has the type " +
            n.getClass().getName() + " which is not an instance of Opt");
      }
      if (((Opt) n).getNumChildNoTransform() != 0 && !(((Opt) n).getChildNoTransform(0) instanceof Parameter)) {
        throw new Error("Optional name() has the type " +
            ((Opt) n).getChildNoTransform(0).getClass().getName() +
            " which is not an instance of Parameter");
      }
    }
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:96
   */
  @SideEffect.Pure public int getNumChild() {
    return 3;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:102
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:106
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:110
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:114
   */
  @SideEffect.Fresh public InhEq clone() throws CloneNotSupportedException {
    InhEq node = (InhEq) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:119
   */
  @SideEffect.Fresh(group="_ASTNode") public InhEq copy() {
    try {
      InhEq node = (InhEq) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:138
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public InhEq fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:148
   */
  @SideEffect.Fresh(group="_ASTNode") public InhEq treeCopyNoTransform() {
    InhEq tree = (InhEq) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:168
   */
  @SideEffect.Fresh(group="_ASTNode") public InhEq treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:173
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_Name == ((InhEq) node).tokenString_Name) && (tokenString_FileName == ((InhEq) node).tokenString_FileName) && (tokenint_StartLine == ((InhEq) node).tokenint_StartLine) && (tokenint_EndLine == ((InhEq) node).tokenint_EndLine) && (tokenString_Comment == ((InhEq) node).tokenString_Comment) && (tokenString_AspectName == ((InhEq) node).tokenString_AspectName) && (tokenString_ChildName == ((InhEq) node).tokenString_ChildName);    
  }
  /**
   * Replaces the Annotation list.
   * @param list The new list node to be used as the Annotation list.
   * @apilevel high-level
   */
  public void setAnnotationList(List<Annotation> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the Annotation list.
   * @return Number of children in the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumAnnotation() {
    return getAnnotationList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Annotation list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Annotation list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumAnnotationNoTransform() {
    return getAnnotationListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Annotation list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Annotation getAnnotation(int i) {
    return (Annotation) getAnnotationList().getChild(i);
  }
  /**
   * Check whether the Annotation list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasAnnotation() {
    return getAnnotationList().getNumChild() != 0;
  }
  /**
   * Append an element to the Annotation list.
   * @param node The element to append to the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addAnnotation(Annotation node) {
    List<Annotation> list = (parent == null) ? getAnnotationListNoTransform() : getAnnotationList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addAnnotationNoTransform(Annotation node) {
    List<Annotation> list = getAnnotationListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Annotation list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setAnnotation(Annotation node, int i) {
    List<Annotation> list = getAnnotationList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Annotation list.
   * @return The node representing the Annotation list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Annotation")
  @SideEffect.Pure(group="_ASTNode") public List<Annotation> getAnnotationList() {
    List<Annotation> list = (List<Annotation>) getChild(0);
    return list;
  }
  /**
   * Retrieves the Annotation list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Annotation list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Annotation> getAnnotationListNoTransform() {
    return (List<Annotation>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the Annotation list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Annotation getAnnotationNoTransform(int i) {
    return (Annotation) getAnnotationListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Annotation list.
   * @return The node representing the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Annotation> getAnnotations() {
    return getAnnotationList();
  }
  /**
   * Retrieves the Annotation list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Annotation list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Annotation> getAnnotationsNoTransform() {
    return getAnnotationListNoTransform();
  }
  /**
   * Replaces the Parameter list.
   * @param list The new list node to be used as the Parameter list.
   * @apilevel high-level
   */
  public void setParameterList(List<Parameter> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the Parameter list.
   * @return Number of children in the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumParameter() {
    return getParameterList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Parameter list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Parameter list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumParameterNoTransform() {
    return getParameterListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Parameter list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Parameter getParameter(int i) {
    return (Parameter) getParameterList().getChild(i);
  }
  /**
   * Check whether the Parameter list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasParameter() {
    return getParameterList().getNumChild() != 0;
  }
  /**
   * Append an element to the Parameter list.
   * @param node The element to append to the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addParameter(Parameter node) {
    List<Parameter> list = (parent == null) ? getParameterListNoTransform() : getParameterList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addParameterNoTransform(Parameter node) {
    List<Parameter> list = getParameterListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Parameter list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setParameter(Parameter node, int i) {
    List<Parameter> list = getParameterList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Parameter list.
   * @return The node representing the Parameter list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Parameter")
  @SideEffect.Pure(group="_ASTNode") public List<Parameter> getParameterList() {
    List<Parameter> list = (List<Parameter>) getChild(1);
    return list;
  }
  /**
   * Retrieves the Parameter list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Parameter list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Parameter> getParameterListNoTransform() {
    return (List<Parameter>) getChildNoTransform(1);
  }
  /**
   * @return the element at index {@code i} in the Parameter list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Parameter getParameterNoTransform(int i) {
    return (Parameter) getParameterListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Parameter list.
   * @return The node representing the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Parameter> getParameters() {
    return getParameterList();
  }
  /**
   * Retrieves the Parameter list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Parameter list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Parameter> getParametersNoTransform() {
    return getParameterListNoTransform();
  }
  /**
   * Replaces the lexeme Name.
   * @param value The new value for the lexeme Name.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setName(String value) {
    tokenString_Name = value;
  }
  /**
   * Retrieves the value for the lexeme Name.
   * @return The value for the lexeme Name.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Name")
  @SideEffect.Pure(group="_ASTNode") public String getName() {
    return tokenString_Name != null ? tokenString_Name : "";
  }
  /**
   * Replaces the lexeme FileName.
   * @param value The new value for the lexeme FileName.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setFileName(String value) {
    tokenString_FileName = value;
  }
  /**
   * Retrieves the value for the lexeme FileName.
   * @return The value for the lexeme FileName.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="FileName")
  @SideEffect.Pure(group="_ASTNode") public String getFileName() {
    return tokenString_FileName != null ? tokenString_FileName : "";
  }
  /**
   * Replaces the lexeme StartLine.
   * @param value The new value for the lexeme StartLine.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setStartLine(int value) {
    tokenint_StartLine = value;
  }
  /**
   * Retrieves the value for the lexeme StartLine.
   * @return The value for the lexeme StartLine.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="StartLine")
  @SideEffect.Pure(group="_ASTNode") public int getStartLine() {
    return tokenint_StartLine;
  }
  /**
   * Replaces the lexeme EndLine.
   * @param value The new value for the lexeme EndLine.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setEndLine(int value) {
    tokenint_EndLine = value;
  }
  /**
   * Retrieves the value for the lexeme EndLine.
   * @return The value for the lexeme EndLine.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="EndLine")
  @SideEffect.Pure(group="_ASTNode") public int getEndLine() {
    return tokenint_EndLine;
  }
  /**
   * Replaces the lexeme Comment.
   * @param value The new value for the lexeme Comment.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setComment(String value) {
    tokenString_Comment = value;
  }
  /**
   * Retrieves the value for the lexeme Comment.
   * @return The value for the lexeme Comment.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Comment")
  @SideEffect.Pure(group="_ASTNode") public String getComment() {
    return tokenString_Comment != null ? tokenString_Comment : "";
  }
  /**
   * Replaces the lexeme AspectName.
   * @param value The new value for the lexeme AspectName.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setAspectName(String value) {
    tokenString_AspectName = value;
  }
  /**
   * Retrieves the value for the lexeme AspectName.
   * @return The value for the lexeme AspectName.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="AspectName")
  @SideEffect.Pure(group="_ASTNode") public String getAspectName() {
    return tokenString_AspectName != null ? tokenString_AspectName : "";
  }
  /**
   * Replaces the lexeme ChildName.
   * @param value The new value for the lexeme ChildName.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setChildName(String value) {
    tokenString_ChildName = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_ChildName;
  /**
   * Retrieves the value for the lexeme ChildName.
   * @return The value for the lexeme ChildName.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="ChildName")
  @SideEffect.Pure(group="_ASTNode") public String getChildName() {
    return tokenString_ChildName != null ? tokenString_ChildName : "";
  }
  /**
   * Replaces the optional node for the Index child. This is the <code>Opt</code>
   * node containing the child Index, not the actual child!
   * @param opt The new node to be used as the optional node for the Index child.
   * @apilevel low-level
   */
  public void setIndexOpt(Opt<Parameter> opt) {
    setChild(opt, 2);
  }
  /**
   * Replaces the (optional) Index child.
   * @param node The new node to be used as the Index child.
   * @apilevel high-level
   */
  public void setIndex(Parameter node) {
    getIndexOpt().setChild(node, 0);
  }
  /**
   * Check whether the optional Index child exists.
   * @return {@code true} if the optional Index child exists, {@code false} if it does not.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasIndex() {
    return getIndexOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) Index child.
   * @return The Index child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   */
  @SideEffect.Pure public Parameter getIndex() {
    return (Parameter) getIndexOpt().getChild(0);
  }
  /**
   * Retrieves the optional node for the Index child. This is the <code>Opt</code> node containing the child Index, not the actual child!
   * @return The optional node for child the Index child.
   * @apilevel low-level
   */
  @ASTNodeAnnotation.OptChild(name="Index")
  @SideEffect.Pure public Opt<Parameter> getIndexOpt() {
    return (Opt<Parameter>) getChild(2);
  }
  /**
   * Retrieves the optional node for child Index. This is the <code>Opt</code> node containing the child Index, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child Index.
   * @apilevel low-level
   */
  @SideEffect.Pure public Opt<Parameter> getIndexOptNoTransform() {
    return (Opt<Parameter>) getChildNoTransform(2);
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int hasUnknownChildProblem_visited = -1;
  /**
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:282
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:282")
  @SideEffect.Pure(group="_ASTNode") public boolean hasUnknownChildProblem() {
    if (hasUnknownChildProblem_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute InhEq.hasUnknownChildProblem().");
    }
    hasUnknownChildProblem_visited = state().boundariesCrossed;
    boolean hasUnknownChildProblem_value = getComponent() == null
          && getChildName().startsWith("get")
          && !getChildName().equals("getChild");
    hasUnknownChildProblem_visited = -1;
    return hasUnknownChildProblem_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int unknownChildProblem_visited = -1;
  /**
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:287
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:287")
  @SideEffect.Pure(group="_ASTNode") public Problem unknownChildProblem() {
    if (unknownChildProblem_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute InhEq.unknownChildProblem().");
    }
    unknownChildProblem_visited = state().boundariesCrossed;
    try {
        StringBuilder buf = new StringBuilder();
        buf.append(String.format("inherited equation for unknown child %s in class %s",
            childName(), hostClass().name()));
        for (Component c : hostClass().components()) {
          buf.append("\n    " + c.type() + " " + c.name());
        }
        return error(buf.toString());
      }
    finally {
      unknownChildProblem_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int hasUnknownNTAProblem_visited = -1;
  /**
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:302
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:302")
  @SideEffect.Pure(group="_ASTNode") public boolean hasUnknownNTAProblem() {
    if (hasUnknownNTAProblem_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute InhEq.hasUnknownNTAProblem().");
    }
    hasUnknownNTAProblem_visited = state().boundariesCrossed;
    boolean hasUnknownNTAProblem_value = !hasUnknownChildProblem()
          && (getChildAttrDecl() == null || !getChildAttrDecl().declaredNTA())
          && (getComponent() == null && !getChildName().equals("getChild"));
    hasUnknownNTAProblem_visited = -1;
    return hasUnknownNTAProblem_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int unknownNTAProblem_visited = -1;
  /**
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:307
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:307")
  @SideEffect.Pure(group="_ASTNode") public Problem unknownNTAProblem() {
    if (unknownNTAProblem_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute InhEq.unknownNTAProblem().");
    }
    unknownNTAProblem_visited = state().boundariesCrossed;
    try {
        StringBuilder buf = new StringBuilder();
        AttrDecl decl = getChildAttrDecl();
        if (decl == null) {
          buf.append(String.format("inherited equation for unknown NTA %s in class %s",
              childName(), hostClass().name()));
        } else if (!decl.declaredNTA()) {
          buf.append("inherited equation for attribute " + childName() + " which is not a NTA");
        }
        return error(buf.toString());
      }
    finally {
      unknownNTAProblem_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int prevEq_visited = -1;
  /**
   * @return Previous equation for this inherited attribute
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:327
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:327")
  @SideEffect.Pure(group="_ASTNode") public InhEq prevEq() {
    if (prevEq_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute InhEq.prevEq().");
    }
    prevEq_visited = state().boundariesCrossed;
    InhEq prevEq_value = hostClass().lookupInhEq(signature(), childName());
    prevEq_visited = -1;
    return prevEq_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int hasMultiEqProblem_visited = -1;
  /**
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:329
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:329")
  @SideEffect.Pure(group="_ASTNode") public boolean hasMultiEqProblem() {
    if (hasMultiEqProblem_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute InhEq.hasMultiEqProblem().");
    }
    hasMultiEqProblem_visited = state().boundariesCrossed;
    boolean hasMultiEqProblem_value = prevEq() != null && prevEq() != this;
    hasMultiEqProblem_visited = -1;
    return hasMultiEqProblem_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int multiEqProblem_visited = -1;
  /**
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:331
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:331")
  @SideEffect.Pure(group="_ASTNode") public Problem multiEqProblem() {
    if (multiEqProblem_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute InhEq.multiEqProblem().");
    }
    multiEqProblem_visited = state().boundariesCrossed;
    Problem multiEqProblem_value = errorf(
          "multiple equations for inherited attribute %s.%s in class %s and %s in class %s in %s:%d",
          childName(), name(), hostClass().name(), prevEq().name(), prevEq().hostClass().name(),
          prevEq().getFileName(), prevEq().getStartLine());
    multiEqProblem_visited = -1;
    return multiEqProblem_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int hasBadIndexProblem_visited = -1;
  /** @return {@code true} if this inherited equation has an index but is
   * declared on a synthesized NTA or a non-list child component.
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:344
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:344")
  @SideEffect.Pure(group="_ASTNode") public boolean hasBadIndexProblem() {
    if (hasBadIndexProblem_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute InhEq.hasBadIndexProblem().");
    }
    hasBadIndexProblem_visited = state().boundariesCrossed;
    boolean hasBadIndexProblem_value = hasIndex()
          && !getChildName().equals("getChild")
          && (getComponent() == null
              || !(getComponent() instanceof ListComponent));
    hasBadIndexProblem_visited = -1;
    return hasBadIndexProblem_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int badIndexProblem_visited = -1;
  /**
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:350
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:350")
  @SideEffect.Pure(group="_ASTNode") public Problem badIndexProblem() {
    if (badIndexProblem_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute InhEq.badIndexProblem().");
    }
    badIndexProblem_visited = state().boundariesCrossed;
    Problem badIndexProblem_value = errorf("may not supply index for non list child or nta %s in class %s",
              childName(), hostClass().name());
    badIndexProblem_visited = -1;
    return badIndexProblem_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int inhParametersTail_visited = -1;
  /**
   * The tail of the list of inherited attribute parameters after removing caller and child.
   * @attribute syn
   * @aspect InheritedAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:40
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="InheritedAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:40")
  @SideEffect.Pure(group="_ASTNode") public String inhParametersTail() {
    if (inhParametersTail_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute InhEq.inhParametersTail().");
    }
    inhParametersTail_visited = state().boundariesCrossed;
    String inhParametersTail_value = parameters().equals("") ? "" : (", " + parameters());
    inhParametersTail_visited = -1;
    return inhParametersTail_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int inhParametersDeclTail_visited = -1;
  /**
   * The tail of the list of inherited attribute parameter declarations after
   * removing caller and child.
   * @attribute syn
   * @aspect InheritedAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:54
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="InheritedAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:54")
  @SideEffect.Pure(group="_ASTNode") public String inhParametersDeclTail() {
    if (inhParametersDeclTail_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute InhEq.inhParametersDeclTail().");
    }
    inhParametersDeclTail_visited = state().boundariesCrossed;
    String inhParametersDeclTail_value = parametersDecl().equals("") ? "" : (", " + parametersDecl());
    inhParametersDeclTail_visited = -1;
    return inhParametersDeclTail_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int getComponent_visited = -1;
  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:681
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:681")
  @SideEffect.Pure(group="_ASTNode") public Component getComponent() {
    if (getComponent_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute InhEq.getComponent().");
    }
    getComponent_visited = state().boundariesCrossed;
    try {
        TypeDecl c = hostClass();
        if (c != null) {
          return c.component(childName());
        }
        return  null;
      }
    finally {
      getComponent_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int childName_visited = -1;
  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:689
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:689")
  @SideEffect.Pure(group="_ASTNode") public String childName() {
    if (childName_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute InhEq.childName().");
    }
    childName_visited = state().boundariesCrossed;
    String childName_value = getChildName().startsWith("get") ? getChildName().substring(3) : getChildName();
    childName_visited = -1;
    return childName_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int getChildAttrDecl_visited = -1;
  /**
   * Lookup NTA child corresponding to this inherited equation.
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:695
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:695")
  @SideEffect.Pure(group="_ASTNode") public AttrDecl getChildAttrDecl() {
    if (getChildAttrDecl_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute InhEq.getChildAttrDecl().");
    }
    getChildAttrDecl_visited = state().boundariesCrossed;
    try {
        AttrDecl decl = hostClass().lookupSynDeclPrefix(childName());
        if (decl == null) {
          decl = hostClass().lookupInhDeclPrefix(childName());
        }
        return decl;
      }
    finally {
      getChildAttrDecl_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int decl_visited = -1;
  /**
   * @attribute syn
   * @aspect LookupDecls
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:227
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="LookupDecls", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:227")
  @SideEffect.Pure(group="_ASTNode") public AttrDecl decl() {
    if (decl_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrEq.decl().");
    }
    decl_visited = state().boundariesCrossed;
    AttrDecl decl_value = grammar().inhDecls().get(signature());
    decl_visited = -1;
    return decl_value;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  @SideEffect.Local(group="_ASTNode") protected void collect_contributors_TypeDecl_attributeProblems(TypeDecl _root, @SideEffect.Local java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:199
    if (decl() == null) {
      {
        TypeDecl target = (TypeDecl) (hostClass());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:204
    if (decl() != null && !decl().parametersDecl().equals(parametersDecl())) {
      {
        TypeDecl target = (TypeDecl) (hostClass());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:277
    if (hasUnknownChildProblem()) {
      {
        TypeDecl target = (TypeDecl) (hostClass());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:297
    if (hasUnknownNTAProblem()) {
      {
        TypeDecl target = (TypeDecl) (hostClass());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:319
    if (hasMultiEqProblem()) {
      {
        TypeDecl target = (TypeDecl) (hostClass());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:336
    if (hasBadIndexProblem()) {
      {
        TypeDecl target = (TypeDecl) (hostClass());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_TypeDecl_attributeProblems(_root, _map);
  }
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_TypeDecl_attributeProblems(Collection<Problem> collection) {
    super.contributeTo_TypeDecl_attributeProblems(collection);
    if (decl() == null) {
      collection.add(errorf("missing declaration for inherited attribute %s", attributeName()));
    }
    if (decl() != null && !decl().parametersDecl().equals(parametersDecl())) {
      collection.add(errorf(
                "equation must have the same parameter names as attribute declaration in %s:%d",
                decl().getFileName(), decl().getStartLine()));
    }
    if (hasUnknownChildProblem()) {
      collection.add(unknownChildProblem());
    }
    if (hasUnknownNTAProblem()) {
      collection.add(unknownNTAProblem());
    }
    if (hasMultiEqProblem()) {
      collection.add(multiEqProblem());
    }
    if (hasBadIndexProblem()) {
      collection.add(badIndexProblem());
    }
  }
}
