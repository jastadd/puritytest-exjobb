/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package org.jastadd.ast.AST;
import org.jastadd.ast.AST.*;
import java.util.*;
import org.jastadd.Problem;
import org.jastadd.jrag.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jastadd.jrag.AST.ASTExpression;
import org.jastadd.jrag.Unparser;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.io.PrintStream;
import java.io.*;
import java.util.regex.*;
import org.jastadd.JastAdd;
import org.jastadd.jrag.AST.ASTAspectMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectRefineMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectFieldDeclaration;
import org.jastadd.jrag.AST.ASTBlock;
import org.jastadd.jrag.ClassBodyObject;
import org.jastadd.jrag.AST.ASTCompilationUnit;
import org.jastadd.ast.AST.Token;
import org.jastadd.ast.AST.SimpleNode;
import org.jastadd.Configuration;
import org.jastadd.tinytemplate.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Ast.ast:40
 * @production CollDecl : {@link AttrDecl} ::= <span class="component">&lt;Target:String&gt;</span>;

 */
public class CollDecl extends AttrDecl implements Cloneable {
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:140
   */
  public void emitComputeMethod(PrintStream out) {
    String rootType = root().name();
    TemplateContext tt = templateContext();
    tt.bind("BottomValue", getBottomValue());
    if (onePhase()) {
      tt.expand("CollDecl.computeMethod:onePhase", out);
    } else {
      tt.expand("CollDecl.computeMethod:twoPhase", out);
    }
  }
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:473
   */
  public void weaveCollectionAttribute() {
    TemplateContext tt = templateContext();

    TypeDecl astNode = grammar().lookup(config().astNodeType());
    TypeDecl rootDecl = root();
    if (astNode != null && rootDecl != null) {
      if (!astNode.processedCollectingSignature(collectionId())) {
        if (!astNode.hasCollEq(this)) {
          String s = "";
          s += tt.expand("CollDecl.collectContributors:header");
          s += tt.expand("CollDecl.collectContributors:default");
          if (isCircular() || !onePhase()) {
            s += tt.expand("CollDecl.contributeTo:default");
          }
          astNode.addClassDeclaration(s, getFileName(), getStartLine());
        }
        String surveyMethod = tt.expand("CollDecl.surveyMethod");
        rootDecl.addClassDeclaration(surveyMethod, getFileName(), getStartLine());
      }
    }
  }
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:551
   */
  public String root = null;
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:681
   */
  private String startValue;
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:683
   */
  public void setStartValue(String expression) {
    startValue = expression;
  }
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:687
   */
  public String getBottomValue() {
    return startValue;
  }
  /** The combining method used to update the collection. 
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:692
   */
  private String combOp;
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:694
   */
  public void setCombOp(String s) {
    combOp = s;
  }
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:698
   */
  public String getCombOp() {
    return combOp;
  }
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:702
   */
  private boolean circularCollection;
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:704
   */
  public void setCircularCollection(boolean b) {
    circularCollection = b;
  }
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:708
   */
  public boolean circularCollection() {
    return circularCollection;
  }
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:712
   */
  public boolean isCircular() {
    return circularCollection();
  }
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:716
   */
  private static boolean isValidIdentifierPart(String s) {
    for (int i = 0; i < s.length(); i++) {
      if (!Character.isJavaIdentifierPart(s.charAt(i))) {
        return false;
      }
    }
    return true;
  }
  /**
   * @declaredat ASTNode:1
   */
  public CollDecl(int i) {
    super(i);
  }
  /**
   * @declaredat ASTNode:5
   */
  public CollDecl(Ast p, int i) {
    this(i);
    parser = p;
  }
  /**
   * @declaredat ASTNode:10
   */
  public CollDecl() {
    this(0);
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:19
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
    setChild(new List(), 0);
    setChild(new List(), 1);
  }
  /**
   * @declaredat ASTNode:24
   */
  public CollDecl(List<Parameter> p0, String p1, String p2, CacheMode p3, String p4, int p5, int p6, boolean p7, boolean p8, String p9, String p10, List<Annotation> p11, String p12) {
    setChild(p0, 0);
    setName(p1);
    setType(p2);
    setCacheMode(p3);
    setFileName(p4);
    setStartLine(p5);
    setEndLine(p6);
    setFinal(p7);
    setNTA(p8);
    setComment(p9);
    setAspectName(p10);
    setChild(p11, 1);
    setTarget(p12);
  }
  /**
   * @declaredat ASTNode:39
   */
  public void dumpTree(String indent, java.io.PrintStream out) {
    out.print(indent + "CollDecl");
    out.print("\"" + getName() + "\"");
    out.print("\"" + getType() + "\"");
    out.print("\"" + getCacheMode() + "\"");
    out.print("\"" + getFileName() + "\"");
    out.print("\"" + getStartLine() + "\"");
    out.print("\"" + getEndLine() + "\"");
    out.print("\"" + getFinal() + "\"");
    out.print("\"" + getNTA() + "\"");
    out.print("\"" + getComment() + "\"");
    out.print("\"" + getAspectName() + "\"");
    out.print("\"" + getTarget() + "\"");
    String childIndent = indent + "  ";
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).dumpTree(childIndent, out);
    }
  }
  /**
   * @declaredat ASTNode:57
   */
  public Object jjtAccept(AstVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
  /**
   * @declaredat ASTNode:60
   */
  public void jjtAddChild(Node n, int i) {
    checkChild(n, i);
    super.jjtAddChild(n, i);
  }
  /**
   * @declaredat ASTNode:64
   */
  public void checkChild(Node n, int i) {
      if (i == 0) {
        if (!(n instanceof List)) {
          throw new Error("Child number 0 of AttrDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof Parameter)) {
            throw new Error("Child number " + k + " in ParameterList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of Parameter");
          }
        }
      }
      if (i == 1) {
        if (!(n instanceof List)) {
          throw new Error("Child number 1 of AttrDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof Annotation)) {
            throw new Error("Child number " + k + " in AnnotationList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of Annotation");
          }
        }
      }
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:91
   */
  @SideEffect.Pure public int getNumChild() {
    return 2;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:97
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:101
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    collectionId_reset();
    signature_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:107
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
    CollDecl_uses_visited = -1;
    CollDecl_uses_computed = false;
    
    CollDecl_uses_value = null;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:115
   */
  @SideEffect.Fresh public CollDecl clone() throws CloneNotSupportedException {
    CollDecl node = (CollDecl) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:120
   */
  @SideEffect.Fresh(group="_ASTNode") public CollDecl copy() {
    try {
      CollDecl node = (CollDecl) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:139
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public CollDecl fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:149
   */
  @SideEffect.Fresh(group="_ASTNode") public CollDecl treeCopyNoTransform() {
    CollDecl tree = (CollDecl) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:169
   */
  @SideEffect.Fresh(group="_ASTNode") public CollDecl treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:174
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_Name == ((CollDecl) node).tokenString_Name) && (tokenString_Type == ((CollDecl) node).tokenString_Type) && (tokenCacheMode_CacheMode == ((CollDecl) node).tokenCacheMode_CacheMode) && (tokenString_FileName == ((CollDecl) node).tokenString_FileName) && (tokenint_StartLine == ((CollDecl) node).tokenint_StartLine) && (tokenint_EndLine == ((CollDecl) node).tokenint_EndLine) && (tokenboolean_Final == ((CollDecl) node).tokenboolean_Final) && (tokenboolean_NTA == ((CollDecl) node).tokenboolean_NTA) && (tokenString_Comment == ((CollDecl) node).tokenString_Comment) && (tokenString_AspectName == ((CollDecl) node).tokenString_AspectName) && (tokenString_Target == ((CollDecl) node).tokenString_Target);    
  }
  /**
   * Replaces the Parameter list.
   * @param list The new list node to be used as the Parameter list.
   * @apilevel high-level
   */
  public void setParameterList(List<Parameter> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the Parameter list.
   * @return Number of children in the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumParameter() {
    return getParameterList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Parameter list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Parameter list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumParameterNoTransform() {
    return getParameterListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Parameter list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Parameter getParameter(int i) {
    return (Parameter) getParameterList().getChild(i);
  }
  /**
   * Check whether the Parameter list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasParameter() {
    return getParameterList().getNumChild() != 0;
  }
  /**
   * Append an element to the Parameter list.
   * @param node The element to append to the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addParameter(Parameter node) {
    List<Parameter> list = (parent == null) ? getParameterListNoTransform() : getParameterList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addParameterNoTransform(Parameter node) {
    List<Parameter> list = getParameterListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Parameter list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setParameter(Parameter node, int i) {
    List<Parameter> list = getParameterList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Parameter list.
   * @return The node representing the Parameter list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Parameter")
  @SideEffect.Pure(group="_ASTNode") public List<Parameter> getParameterList() {
    List<Parameter> list = (List<Parameter>) getChild(0);
    return list;
  }
  /**
   * Retrieves the Parameter list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Parameter list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Parameter> getParameterListNoTransform() {
    return (List<Parameter>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the Parameter list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Parameter getParameterNoTransform(int i) {
    return (Parameter) getParameterListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Parameter list.
   * @return The node representing the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Parameter> getParameters() {
    return getParameterList();
  }
  /**
   * Retrieves the Parameter list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Parameter list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Parameter> getParametersNoTransform() {
    return getParameterListNoTransform();
  }
  /**
   * Replaces the lexeme Name.
   * @param value The new value for the lexeme Name.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setName(String value) {
    tokenString_Name = value;
  }
  /**
   * Retrieves the value for the lexeme Name.
   * @return The value for the lexeme Name.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Name")
  @SideEffect.Pure(group="_ASTNode") public String getName() {
    return tokenString_Name != null ? tokenString_Name : "";
  }
  /**
   * Replaces the lexeme Type.
   * @param value The new value for the lexeme Type.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setType(String value) {
    tokenString_Type = value;
  }
  /**
   * Retrieves the value for the lexeme Type.
   * @return The value for the lexeme Type.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Type")
  @SideEffect.Pure(group="_ASTNode") public String getType() {
    return tokenString_Type != null ? tokenString_Type : "";
  }
  /**
   * Replaces the lexeme CacheMode.
   * @param value The new value for the lexeme CacheMode.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setCacheMode(CacheMode value) {
    tokenCacheMode_CacheMode = value;
  }
  /**
   * Retrieves the value for the lexeme CacheMode.
   * @return The value for the lexeme CacheMode.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="CacheMode")
  @SideEffect.Pure(group="_ASTNode") public CacheMode getCacheMode() {
    return tokenCacheMode_CacheMode;
  }
  /**
   * Replaces the lexeme FileName.
   * @param value The new value for the lexeme FileName.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setFileName(String value) {
    tokenString_FileName = value;
  }
  /**
   * Retrieves the value for the lexeme FileName.
   * @return The value for the lexeme FileName.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="FileName")
  @SideEffect.Pure(group="_ASTNode") public String getFileName() {
    return tokenString_FileName != null ? tokenString_FileName : "";
  }
  /**
   * Replaces the lexeme StartLine.
   * @param value The new value for the lexeme StartLine.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setStartLine(int value) {
    tokenint_StartLine = value;
  }
  /**
   * Retrieves the value for the lexeme StartLine.
   * @return The value for the lexeme StartLine.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="StartLine")
  @SideEffect.Pure(group="_ASTNode") public int getStartLine() {
    return tokenint_StartLine;
  }
  /**
   * Replaces the lexeme EndLine.
   * @param value The new value for the lexeme EndLine.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setEndLine(int value) {
    tokenint_EndLine = value;
  }
  /**
   * Retrieves the value for the lexeme EndLine.
   * @return The value for the lexeme EndLine.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="EndLine")
  @SideEffect.Pure(group="_ASTNode") public int getEndLine() {
    return tokenint_EndLine;
  }
  /**
   * Replaces the lexeme Final.
   * @param value The new value for the lexeme Final.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setFinal(boolean value) {
    tokenboolean_Final = value;
  }
  /**
   * Retrieves the value for the lexeme Final.
   * @return The value for the lexeme Final.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Final")
  @SideEffect.Pure(group="_ASTNode") public boolean getFinal() {
    return tokenboolean_Final;
  }
  /**
   * Replaces the lexeme NTA.
   * @param value The new value for the lexeme NTA.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setNTA(boolean value) {
    tokenboolean_NTA = value;
  }
  /**
   * Retrieves the value for the lexeme NTA.
   * @return The value for the lexeme NTA.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="NTA")
  @SideEffect.Pure(group="_ASTNode") public boolean getNTA() {
    return tokenboolean_NTA;
  }
  /**
   * Replaces the lexeme Comment.
   * @param value The new value for the lexeme Comment.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setComment(String value) {
    tokenString_Comment = value;
  }
  /**
   * Retrieves the value for the lexeme Comment.
   * @return The value for the lexeme Comment.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Comment")
  @SideEffect.Pure(group="_ASTNode") public String getComment() {
    return tokenString_Comment != null ? tokenString_Comment : "";
  }
  /**
   * Replaces the lexeme AspectName.
   * @param value The new value for the lexeme AspectName.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setAspectName(String value) {
    tokenString_AspectName = value;
  }
  /**
   * Retrieves the value for the lexeme AspectName.
   * @return The value for the lexeme AspectName.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="AspectName")
  @SideEffect.Pure(group="_ASTNode") public String getAspectName() {
    return tokenString_AspectName != null ? tokenString_AspectName : "";
  }
  /**
   * Replaces the Annotation list.
   * @param list The new list node to be used as the Annotation list.
   * @apilevel high-level
   */
  public void setAnnotationList(List<Annotation> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the Annotation list.
   * @return Number of children in the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumAnnotation() {
    return getAnnotationList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Annotation list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Annotation list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumAnnotationNoTransform() {
    return getAnnotationListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Annotation list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Annotation getAnnotation(int i) {
    return (Annotation) getAnnotationList().getChild(i);
  }
  /**
   * Check whether the Annotation list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasAnnotation() {
    return getAnnotationList().getNumChild() != 0;
  }
  /**
   * Append an element to the Annotation list.
   * @param node The element to append to the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addAnnotation(Annotation node) {
    List<Annotation> list = (parent == null) ? getAnnotationListNoTransform() : getAnnotationList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addAnnotationNoTransform(Annotation node) {
    List<Annotation> list = getAnnotationListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Annotation list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setAnnotation(Annotation node, int i) {
    List<Annotation> list = getAnnotationList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Annotation list.
   * @return The node representing the Annotation list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Annotation")
  @SideEffect.Pure(group="_ASTNode") public List<Annotation> getAnnotationList() {
    List<Annotation> list = (List<Annotation>) getChild(1);
    return list;
  }
  /**
   * Retrieves the Annotation list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Annotation list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Annotation> getAnnotationListNoTransform() {
    return (List<Annotation>) getChildNoTransform(1);
  }
  /**
   * @return the element at index {@code i} in the Annotation list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Annotation getAnnotationNoTransform(int i) {
    return (Annotation) getAnnotationListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Annotation list.
   * @return The node representing the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Annotation> getAnnotations() {
    return getAnnotationList();
  }
  /**
   * Retrieves the Annotation list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Annotation list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Annotation> getAnnotationsNoTransform() {
    return getAnnotationListNoTransform();
  }
  /**
   * Replaces the lexeme Target.
   * @param value The new value for the lexeme Target.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setTarget(String value) {
    tokenString_Target = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_Target;
  /**
   * Retrieves the value for the lexeme Target.
   * @return The value for the lexeme Target.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Target")
  @SideEffect.Pure(group="_ASTNode") public String getTarget() {
    return tokenString_Target != null ? tokenString_Target : "";
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int annotationKind_visited = -1;
  /**
   * @attribute syn
   * @aspect ASTNodeAnnotations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNodeAnnotations.jrag:30
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ASTNodeAnnotations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNodeAnnotations.jrag:30")
  @SideEffect.Pure(group="_ASTNode") public String annotationKind() {
    if (annotationKind_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.annotationKind().");
    }
    annotationKind_visited = state().boundariesCrossed;
    String annotationKind_value = "kind=ASTNodeAnnotation.Kind.COLL";
    annotationKind_visited = -1;
    return annotationKind_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map supportedAnnotation_Annotation_visited = new java.util.HashMap(4);
  /**
   * @attribute syn
   * @aspect Annotations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Annotations.jrag:55
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Annotations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Annotations.jrag:55")
  @SideEffect.Pure(group="_ASTNode") public boolean supportedAnnotation(Annotation a) {
    Object _parameters = a;
    if (Integer.valueOf(state().boundariesCrossed).equals(supportedAnnotation_Annotation_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute CollDecl.supportedAnnotation(Annotation).");
    }
    supportedAnnotation_Annotation_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    try {
        // TODO(joqvist): document these annotations!
        return a.isAnnotation("@OnePhase")
            || a.isAnnotation("@LazyCondition")
            || a.isAnnotation("@Circular")
            || a.isAnnotation("@CollectionGroup")
            || a.isAnnotation("@Naive");
      }
    finally {
      supportedAnnotation_Annotation_visited.remove(_parameters);
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int annotations_visited = -1;
  /**
   * @return the annotations of this attribute declaration.
   * @attribute syn
   * @aspect Annotations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Annotations.jrag:92
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Annotations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Annotations.jrag:92")
  @SideEffect.Pure(group="_ASTNode") public String annotations() {
    if (annotations_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.annotations().");
    }
    annotations_visited = state().boundariesCrossed;
    String annotations_value = "";
    annotations_visited = -1;
    return annotations_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int hasCollectionGroupProblem_visited = -1;
  /**
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:434
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:434")
  @SideEffect.Pure(group="_ASTNode") public boolean hasCollectionGroupProblem() {
    if (hasCollectionGroupProblem_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute CollDecl.hasCollectionGroupProblem().");
    }
    hasCollectionGroupProblem_visited = state().boundariesCrossed;
    try {
        if (hasAnnotation("@CollectionGroup")) {
          String value = getAnnotationValue("@CollectionGroup");
          return (value == null)
                || (value.equals(""))
                || (!isValidIdentifierPart(value));
        }
        return false;
      }
    finally {
      hasCollectionGroupProblem_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int collectionGroupProblem_visited = -1;
  /**
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:444
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:444")
  @SideEffect.Pure(group="_ASTNode") public Problem collectionGroupProblem() {
    if (collectionGroupProblem_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute CollDecl.collectionGroupProblem().");
    }
    collectionGroupProblem_visited = state().boundariesCrossed;
    try {
        String msg = "";
        if (hasAnnotation("@CollectionGroup")) {
          String value = getAnnotationValue("@CollectionGroup");
          if (value == null) {
            msg = "missing CollectionGroup argument";
          } else if (value.equals("")) {
            msg = "CollectionGroup argument can not be empty";
          } else if (!isValidIdentifierPart(value)) {
            msg = "CollectionGroup argument must be a valid identifier part";
          }
        }
        return error(msg);
      }
    finally {
      collectionGroupProblem_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int hasMultipleRootsProblem_visited = -1;
  /**
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:464
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:464")
  @SideEffect.Pure(group="_ASTNode") public boolean hasMultipleRootsProblem() {
    if (hasMultipleRootsProblem_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute CollDecl.hasMultipleRootsProblem().");
    }
    hasMultipleRootsProblem_visited = state().boundariesCrossed;
    boolean hasMultipleRootsProblem_value = root == null && grammar().roots().size() > 1;
    hasMultipleRootsProblem_visited = -1;
    return hasMultipleRootsProblem_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int multipleRootsProblem_visited = -1;
  /**
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:466
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:466")
  @SideEffect.Pure(group="_ASTNode") public Problem multipleRootsProblem() {
    if (multipleRootsProblem_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute CollDecl.multipleRootsProblem().");
    }
    multipleRootsProblem_visited = state().boundariesCrossed;
    try {
        StringBuilder buf = new StringBuilder();
        buf.append("multiple tree roots to search for contributions. Please explicitly select one of");
        for (ASTDecl decl : grammar().roots()) {
          buf.append(" " + decl.name());
        }
        return error(buf.toString());
      }
    finally {
      multipleRootsProblem_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int hasNoRootProblem_visited = -1;
  /**
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:481
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:481")
  @SideEffect.Pure(group="_ASTNode") public boolean hasNoRootProblem() {
    if (hasNoRootProblem_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute CollDecl.hasNoRootProblem().");
    }
    hasNoRootProblem_visited = state().boundariesCrossed;
    boolean hasNoRootProblem_value = root == null && grammar().roots().isEmpty();
    hasNoRootProblem_visited = -1;
    return hasNoRootProblem_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isCollection_visited = -1;
  /**
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:35
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:35")
  @SideEffect.Pure(group="_ASTNode") public boolean isCollection() {
    if (isCollection_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.isCollection().");
    }
    isCollection_visited = state().boundariesCrossed;
    boolean isCollection_value = true;
    isCollection_visited = -1;
    return isCollection_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int rootType_visited = -1;
  /** @return the type name of the collection root node 
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:40
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:40")
  @SideEffect.Pure(group="_ASTNode") public String rootType() {
    if (rootType_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute CollDecl.rootType().");
    }
    rootType_visited = state().boundariesCrossed;
    String rootType_value = root().name();
    rootType_visited = -1;
    return rootType_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int root_visited = -1;
  /**
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:123
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:123")
  @SideEffect.Pure(group="_ASTNode") public TypeDecl root() {
    if (root_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.root().");
    }
    root_visited = state().boundariesCrossed;
    TypeDecl root_value = root != null ? grammar().lookup(root) : super.root();
    root_visited = -1;
    return root_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int collectionId_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void collectionId_reset() {
    collectionId_computed = false;
    
    collectionId_value = null;
    collectionId_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean collectionId_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected String collectionId_value;

  /**
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:385
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:385")
  @SideEffect.Pure(group="_ASTNode") public String collectionId() {
    ASTState state = state();
    if (collectionId_computed) {
      return collectionId_value;
    }
    if (collectionId_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.collectionId().");
    }
    collectionId_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    collectionId_value = collectionId_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    collectionId_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    collectionId_visited = -1;
    return collectionId_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private String collectionId_compute() {
      String value = getAnnotationValue("@CollectionGroup");
      if (value != null) {
        return "CollectionGroup_" + value;
      }
      return signature();
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int signature_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void signature_reset() {
    signature_computed = false;
    
    signature_value = null;
    signature_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean signature_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected String signature_value;

  /**
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:397
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:397")
  @SideEffect.Pure(group="_ASTNode") public String signature() {
    ASTState state = state();
    if (signature_computed) {
      return signature_value;
    }
    if (signature_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute CollDecl.signature().");
    }
    signature_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    signature_value = signature_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    signature_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    signature_visited = -1;
    return signature_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private String signature_compute() {
      StringBuffer s = new StringBuffer();
      s.append(getTarget());
      s.append("_");
      s.append(name());
      for (int i = 0; i < getNumParameter(); i++) {
        s.append("_" + getParameter(i).getType());
      }
      return s.toString();
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int attributeKind_visited = -1;
  /**
   * @attribute syn
   * @aspect AttributeKind
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:40
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeKind", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:40")
  @SideEffect.Pure(group="_ASTNode") public String attributeKind() {
    if (attributeKind_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.attributeKind().");
    }
    attributeKind_visited = state().boundariesCrossed;
    String attributeKind_value = "coll";
    attributeKind_visited = -1;
    return attributeKind_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isLazy_visited = -1;
  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:106
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:106")
  @SideEffect.Pure(group="_ASTNode") public boolean isLazy() {
    if (isLazy_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.isLazy().");
    }
    isLazy_visited = state().boundariesCrossed;
    boolean isLazy_value = declaredNTA() || isCircular() || shouldCache(getCacheMode());
    isLazy_visited = -1;
    return isLazy_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int circularComputeRhs_visited = -1;
  /**
   * @attribute syn
   * @aspect Compute
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:797
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Compute", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:797")
  @SideEffect.Pure(group="_ASTNode") public String circularComputeRhs() {
    if (circularComputeRhs_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.circularComputeRhs().");
    }
    circularComputeRhs_visited = state().boundariesCrossed;
    String circularComputeRhs_value = String.format("combine_%s_contributions(%s)",
          signature(), getBottomValue());
    circularComputeRhs_visited = -1;
    return circularComputeRhs_value;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int CollDecl_uses_visited = -1;
  /**
   * @attribute coll
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:515
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.COLL)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:515")
  @SideEffect.Pure(group="_ASTNode") public HashSet uses() {
    ASTState state = state();
    if (CollDecl_uses_computed) {
      return CollDecl_uses_value;
    }
    if (CollDecl_uses_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute CollDecl.uses().");
    }
    CollDecl_uses_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    CollDecl_uses_value = uses_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    CollDecl_uses_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    CollDecl_uses_visited = -1;
    return CollDecl_uses_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure(group="_ASTNode") private HashSet uses_compute() {
    ASTNode node = this;
    while (node != null && !(node instanceof Grammar)) {
      node = node.getParent();
    }
    Grammar root = (Grammar) node;
    root.survey_CollDecl_uses();
    HashSet _computedValue = new LinkedHashSet();
    if (root.contributorMap_CollDecl_uses.containsKey(this)) {
      for (ASTNode contributor : root.contributorMap_CollDecl_uses.get(this)) {
        contributor.contributeTo_CollDecl_uses(_computedValue);
      }
    }
    return _computedValue;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean CollDecl_uses_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected HashSet CollDecl_uses_value;

  @SideEffect.Local(group="_ASTNode") protected void collect_contributors_TypeDecl_attributeProblems(TypeDecl _root, @SideEffect.Local java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:429
    if (hasCollectionGroupProblem()) {
      {
        TypeDecl target = (TypeDecl) (hostClass());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:459
    if (hasMultipleRootsProblem()) {
      {
        TypeDecl target = (TypeDecl) (hostClass());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:475
    if (hasNoRootProblem()) {
      {
        TypeDecl target = (TypeDecl) (hostClass());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:483
    if (uses().isEmpty()) {
      {
        TypeDecl target = (TypeDecl) (hostClass());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:502
    if (root != null && root() == null) {
      {
        TypeDecl target = (TypeDecl) (hostClass());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_TypeDecl_attributeProblems(_root, _map);
  }
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_TypeDecl_attributeProblems(Collection<Problem> collection) {
    super.contributeTo_TypeDecl_attributeProblems(collection);
    if (hasCollectionGroupProblem()) {
      collection.add(collectionGroupProblem());
    }
    if (hasMultipleRootsProblem()) {
      collection.add(multipleRootsProblem());
    }
    if (hasNoRootProblem()) {
      collection.add(error("No tree roots to search for contributions. "
                + "Please declare an explicit root node."));
    }
    if (uses().isEmpty()) {
      collection.add(warning("no contributions for this collection attribute"));
    }
    if (root != null && root() == null) {
      collection.add(errorf("Unknown collection root: \"%s\"", root));
    }
  }
}
