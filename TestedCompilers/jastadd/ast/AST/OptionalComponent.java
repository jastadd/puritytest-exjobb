/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package org.jastadd.ast.AST;
import org.jastadd.ast.AST.*;
import java.util.*;
import org.jastadd.Problem;
import org.jastadd.jrag.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jastadd.jrag.AST.ASTExpression;
import org.jastadd.jrag.Unparser;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.io.PrintStream;
import java.io.*;
import java.util.regex.*;
import org.jastadd.JastAdd;
import org.jastadd.jrag.AST.ASTAspectMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectRefineMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectFieldDeclaration;
import org.jastadd.jrag.AST.ASTBlock;
import org.jastadd.jrag.ClassBodyObject;
import org.jastadd.jrag.AST.ASTCompilationUnit;
import org.jastadd.ast.AST.Token;
import org.jastadd.ast.AST.SimpleNode;
import org.jastadd.Configuration;
import org.jastadd.tinytemplate.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Ast.ast:70
 * @production OptionalComponent : {@link Component} ::= <span class="component">{@link Id}</span>;

 */
public class OptionalComponent extends Component implements Cloneable {
  /**
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:521
   */
  public void jaddGen(int index, boolean publicModifier, ASTDecl decl) {
    TemplateContext tt = templateContext();
    tt.bind("OptType", config().optType() + "<" + getId().type() + ">");
    tt.bind("Type", getId().type());
    tt.bind("Name", getId().name());
    tt.bind("Index", String.valueOf(index));
    tt.bind("Host", decl.name());
    tt.bind("Modifier", publicModifier ? "public" : "private");

    parse(tt.expand("OptionalComponent"));
  }
  /**
   * @aspect JastAddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JastAddCodeGen.jadd:202
   */
  public String componentString() {
    return "[" + getId().componentString() + "]";
  }
  /**
   * @declaredat ASTNode:1
   */
  public OptionalComponent(int i) {
    super(i);
  }
  /**
   * @declaredat ASTNode:5
   */
  public OptionalComponent(Ast p, int i) {
    this(i);
    parser = p;
  }
  /**
   * @declaredat ASTNode:10
   */
  public OptionalComponent() {
    this(0);
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:19
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
  }
  /**
   * @declaredat ASTNode:22
   */
  public OptionalComponent(Id p0) {
    setChild(p0, 0);
  }
  /**
   * @declaredat ASTNode:25
   */
  public void dumpTree(String indent, java.io.PrintStream out) {
    out.print(indent + "OptionalComponent");
    String childIndent = indent + "  ";
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).dumpTree(childIndent, out);
    }
  }
  /**
   * @declaredat ASTNode:32
   */
  public Object jjtAccept(AstVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
  /**
   * @declaredat ASTNode:35
   */
  public void jjtAddChild(Node n, int i) {
    checkChild(n, i);
    super.jjtAddChild(n, i);
  }
  /**
   * @declaredat ASTNode:39
   */
  public void checkChild(Node n, int i) {
    if (i == 0 && !(n instanceof Id)) {
     throw new Error("Child number 0 of OptionalComponent has the type " +
       n.getClass().getName() + " which is not an instance of Id");
    }
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:46
   */
  @SideEffect.Pure public int getNumChild() {
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:52
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:56
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:60
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:64
   */
  @SideEffect.Fresh public OptionalComponent clone() throws CloneNotSupportedException {
    OptionalComponent node = (OptionalComponent) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:69
   */
  @SideEffect.Fresh(group="_ASTNode") public OptionalComponent copy() {
    try {
      OptionalComponent node = (OptionalComponent) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:88
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public OptionalComponent fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:98
   */
  @SideEffect.Fresh(group="_ASTNode") public OptionalComponent treeCopyNoTransform() {
    OptionalComponent tree = (OptionalComponent) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:118
   */
  @SideEffect.Fresh(group="_ASTNode") public OptionalComponent treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:123
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the Id child.
   * @param node The new node to replace the Id child.
   * @apilevel high-level
   */
  public void setId(Id node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the Id child.
   * @return The current node used as the Id child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Id")
  @SideEffect.Pure public Id getId() {
    return (Id) getChild(0);
  }
  /**
   * Retrieves the Id child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Id child.
   * @apilevel low-level
   */
  @SideEffect.Pure public Id getIdNoTransform() {
    return (Id) getChildNoTransform(0);
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int name_visited = -1;
  /**
   * @attribute syn
   * @aspect Names
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:106
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Names", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:106")
  @SideEffect.Pure(group="_ASTNode") public String name() {
    if (name_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Component.name().");
    }
    name_visited = state().boundariesCrossed;
    String name_value = getId().name();
    name_visited = -1;
    return name_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int type_visited = -1;
  /**
   * @attribute syn
   * @aspect Types
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:123
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Types", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:123")
  @SideEffect.Pure(group="_ASTNode") public String type() {
    if (type_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Component.type().");
    }
    type_visited = state().boundariesCrossed;
    String type_value = getId().type();
    type_visited = -1;
    return type_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int annotations_visited = -1;
  /**
   * @attribute syn
   * @aspect Annotations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Annotations.jrag:107
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Annotations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Annotations.jrag:107")
  @SideEffect.Pure(group="_ASTNode") public String annotations() {
    if (annotations_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute OptionalComponent.annotations().");
    }
    annotations_visited = state().boundariesCrossed;
    String annotations_value = "";
    annotations_visited = -1;
    return annotations_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int constrParmType_visited = -1;
  /**
   * @attribute syn
   * @aspect ConstructorParameterTypes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:120
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstructorParameterTypes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:120")
  @SideEffect.Pure(group="_ASTNode") public String constrParmType() {
    if (constrParmType_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Component.constrParmType().");
    }
    constrParmType_visited = state().boundariesCrossed;
    String constrParmType_value = config().optType() + "<" + getId().type() + ">";
    constrParmType_visited = -1;
    return constrParmType_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map checkComponentStructure_int_visited = new java.util.HashMap(4);
  /**
   * Check the structure of a single JJTree component.
   * @attribute syn
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:385
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JaddCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:385")
  @SideEffect.Pure(group="_ASTNode") public String checkComponentStructure(int childIndex) {
    Object _parameters = childIndex;
    if (Integer.valueOf(state().boundariesCrossed).equals(checkComponentStructure_int_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute Component.checkComponentStructure(int).");
    }
    checkComponentStructure_int_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    try {
        TemplateContext tt = templateContext();
        tt.bind("ChildIndex", "" + childIndex);
        tt.bind("HostClassName", hostClass().name());
        return tt.expand("OptionalComponent.checkComponentStructure");
      }
    finally {
      checkComponentStructure_int_visited.remove(_parameters);
    }
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
