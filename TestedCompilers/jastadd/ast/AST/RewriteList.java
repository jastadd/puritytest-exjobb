/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package org.jastadd.ast.AST;
import org.jastadd.ast.AST.*;
import java.util.*;
import org.jastadd.Problem;
import org.jastadd.jrag.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jastadd.jrag.AST.ASTExpression;
import org.jastadd.jrag.Unparser;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.io.PrintStream;
import java.io.*;
import java.util.regex.*;
import org.jastadd.JastAdd;
import org.jastadd.jrag.AST.ASTAspectMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectRefineMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectFieldDeclaration;
import org.jastadd.jrag.AST.ASTBlock;
import org.jastadd.jrag.ClassBodyObject;
import org.jastadd.jrag.AST.ASTCompilationUnit;
import org.jastadd.ast.AST.Token;
import org.jastadd.ast.AST.SimpleNode;
import org.jastadd.Configuration;
import org.jastadd.tinytemplate.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Ast.ast:61
 * @production RewriteList : {@link Rewrite};

 */
public class RewriteList extends Rewrite implements Cloneable {
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:1218
   */
  public String parentName;
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:1220
   */
  public String getParentName() {
    return parentName;
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:1224
   */
  public void setParentName(String name) {
    parentName = name;
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:1228
   */
  public String childName;
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:1230
   */
  public String getChildName() {
    return childName;
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:1234
   */
  public void setChildName(String name) {
    childName = name;
  }
  /**
   * @aspect Rewrites
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Rewrites.jrag:93
   */
  public boolean genRewrite(PrintStream out, int index) {
    TemplateContext tt = templateContext();
    tt.expand("Rewrite.declaredat", out);
    if (getCondition() != null) {
      tt.bind("Condition", " && " + Unparser.unparse(getCondition()));
    } else {
      tt.bind("Condition", "");
    }
    tt.expand("RewriteList.genRewrite", out);
    return false;
  }
  /**
   * @aspect Rewrites
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Rewrites.jrag:132
   */
  public void genRewritesExtra(PrintStream out, int index) {
    String ind = config().indent;
    String ind2 = config().ind(2);
    if (getResult() instanceof org.jastadd.jrag.AST.ASTBlock) {
      templateContext().expand("Rewrite.javaDoc:internal", out);
      out.println(ind + "private " + getReturnType() + " rewrite"
          + getParentName() + "_" + getChildName() + "() {");
      out.print(Unparser.unparse(getResult()));
      out.println(ind + "}");
    } else {
      templateContext().expand("Rewrite.javaDoc:internal", out);
      out.println(ind + "private " + getReturnType() + " rewrite" + getParentName() + "_" + getChildName() + "() {");
      out.println(ind2 + "return " + Unparser.unparse(getResult()) + ";");
      out.println(ind + "}");
    }
  }
  /**
   * @declaredat ASTNode:1
   */
  public RewriteList(int i) {
    super(i);
  }
  /**
   * @declaredat ASTNode:5
   */
  public RewriteList(Ast p, int i) {
    this(i);
    parser = p;
  }
  /**
   * @declaredat ASTNode:10
   */
  public RewriteList() {
    this(0);
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:19
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /**
   * @declaredat ASTNode:21
   */
  public RewriteList(String p0, int p1, int p2, String p3) {
    setFileName(p0);
    setStartLine(p1);
    setEndLine(p2);
    setAspectName(p3);
  }
  /**
   * @declaredat ASTNode:27
   */
  public void dumpTree(String indent, java.io.PrintStream out) {
    out.print(indent + "RewriteList");
    out.print("\"" + getFileName() + "\"");
    out.print("\"" + getStartLine() + "\"");
    out.print("\"" + getEndLine() + "\"");
    out.print("\"" + getAspectName() + "\"");
    String childIndent = indent + "  ";
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).dumpTree(childIndent, out);
    }
  }
  /**
   * @declaredat ASTNode:38
   */
  public Object jjtAccept(AstVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
  /**
   * @declaredat ASTNode:41
   */
  public void jjtAddChild(Node n, int i) {
    checkChild(n, i);
    super.jjtAddChild(n, i);
  }
  /**
   * @declaredat ASTNode:45
   */
  public void checkChild(Node n, int i) {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:48
   */
  @SideEffect.Pure public int getNumChild() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:54
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:58
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:62
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:66
   */
  @SideEffect.Fresh public RewriteList clone() throws CloneNotSupportedException {
    RewriteList node = (RewriteList) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:71
   */
  @SideEffect.Fresh(group="_ASTNode") public RewriteList copy() {
    try {
      RewriteList node = (RewriteList) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:90
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public RewriteList fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:100
   */
  @SideEffect.Fresh(group="_ASTNode") public RewriteList treeCopyNoTransform() {
    RewriteList tree = (RewriteList) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:120
   */
  @SideEffect.Fresh(group="_ASTNode") public RewriteList treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:125
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_FileName == ((RewriteList) node).tokenString_FileName) && (tokenint_StartLine == ((RewriteList) node).tokenint_StartLine) && (tokenint_EndLine == ((RewriteList) node).tokenint_EndLine) && (tokenString_AspectName == ((RewriteList) node).tokenString_AspectName);    
  }
  /**
   * Replaces the lexeme FileName.
   * @param value The new value for the lexeme FileName.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setFileName(String value) {
    tokenString_FileName = value;
  }
  /**
   * Retrieves the value for the lexeme FileName.
   * @return The value for the lexeme FileName.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="FileName")
  @SideEffect.Pure(group="_ASTNode") public String getFileName() {
    return tokenString_FileName != null ? tokenString_FileName : "";
  }
  /**
   * Replaces the lexeme StartLine.
   * @param value The new value for the lexeme StartLine.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setStartLine(int value) {
    tokenint_StartLine = value;
  }
  /**
   * Retrieves the value for the lexeme StartLine.
   * @return The value for the lexeme StartLine.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="StartLine")
  @SideEffect.Pure(group="_ASTNode") public int getStartLine() {
    return tokenint_StartLine;
  }
  /**
   * Replaces the lexeme EndLine.
   * @param value The new value for the lexeme EndLine.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setEndLine(int value) {
    tokenint_EndLine = value;
  }
  /**
   * Retrieves the value for the lexeme EndLine.
   * @return The value for the lexeme EndLine.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="EndLine")
  @SideEffect.Pure(group="_ASTNode") public int getEndLine() {
    return tokenint_EndLine;
  }
  /**
   * Replaces the lexeme AspectName.
   * @param value The new value for the lexeme AspectName.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setAspectName(String value) {
    tokenString_AspectName = value;
  }
  /**
   * Retrieves the value for the lexeme AspectName.
   * @return The value for the lexeme AspectName.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="AspectName")
  @SideEffect.Pure(group="_ASTNode") public String getAspectName() {
    return tokenString_AspectName != null ? tokenString_AspectName : "";
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  @SideEffect.Local(group="_ASTNode") protected void collect_contributors_TypeDecl_attributeProblems(TypeDecl _root, @SideEffect.Local java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:507
    if (config().rewriteCircularNTA()) {
      {
        TypeDecl target = (TypeDecl) (hostClass());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:513
    if (config().legacyRewrite()) {
      {
        TypeDecl target = (TypeDecl) (hostClass());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_TypeDecl_attributeProblems(_root, _map);
  }
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_TypeDecl_attributeProblems(Collection<Problem> collection) {
    super.contributeTo_TypeDecl_attributeProblems(collection);
    if (config().rewriteCircularNTA()) {
      collection.add(error("list rewrites are not supported when using cnta rewrites"));
    }
    if (config().legacyRewrite()) {
      collection.add(warning("list rewrites are deprecated and will be removed in a future version of JastAdd"));
    }
  }
}
