/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package org.jastadd.ast.AST;
import org.jastadd.ast.AST.*;
import java.util.*;
import org.jastadd.Problem;
import org.jastadd.jrag.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jastadd.jrag.AST.ASTExpression;
import org.jastadd.jrag.Unparser;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.io.PrintStream;
import java.io.*;
import java.util.regex.*;
import org.jastadd.JastAdd;
import org.jastadd.jrag.AST.ASTAspectMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectRefineMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectFieldDeclaration;
import org.jastadd.jrag.AST.ASTBlock;
import org.jastadd.jrag.ClassBodyObject;
import org.jastadd.jrag.AST.ASTCompilationUnit;
import org.jastadd.ast.AST.Token;
import org.jastadd.ast.AST.SimpleNode;
import org.jastadd.Configuration;
import org.jastadd.tinytemplate.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Ast.ast:74
 * @production TokenComponent : {@link Component} ::= <span class="component">{@link TokenId}</span>;

 */
public class TokenComponent extends Component implements Cloneable {
  /**
   * @aspect ASTErrors
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTErrors.jrag:121
   */
  public boolean declared() {
    return true;
  }
  /**
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:533
   */
  private boolean called = false;
  /**
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:535
   */
  public void jaddGen(int index, boolean publicModifier, ASTDecl decl) {
    TemplateContext tt = templateContext();
    tt.bind("Id", getTokenId().getID());
    tt.bind("Type", getTokenId().getTYPE());
    tt.bind("TypeInSignature", ASTNode.convTypeNameToSignature(getTokenId().getTYPE()));
    tt.bind("Host", decl.name());
    tt.bind("Modifier", publicModifier ? "public" : "private");

    boolean isStringToken = getTokenId().getTYPE().equals("String")
        || getTokenId().getTYPE().equals("java.lang.String");
    boolean isRedefined = decl.redefinesTokenComponent(this);
    tt.bind("IsStringToken", isStringToken);
    tt.bind("IsRedefined", isRedefined);

    parse(tt.expand("TokenComponent"));
  }
  /**
   * @aspect JastAddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JastAddCodeGen.jadd:206
   */
  public String componentString() {
    return "&lt;" + getTokenId().componentString() + "&gt;";
  }
  /**
   * @declaredat ASTNode:1
   */
  public TokenComponent(int i) {
    super(i);
  }
  /**
   * @declaredat ASTNode:5
   */
  public TokenComponent(Ast p, int i) {
    this(i);
    parser = p;
  }
  /**
   * @declaredat ASTNode:10
   */
  public TokenComponent() {
    this(0);
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:19
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
  }
  /**
   * @declaredat ASTNode:22
   */
  public TokenComponent(TokenId p0) {
    setChild(p0, 0);
  }
  /**
   * @declaredat ASTNode:25
   */
  public void dumpTree(String indent, java.io.PrintStream out) {
    out.print(indent + "TokenComponent");
    String childIndent = indent + "  ";
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).dumpTree(childIndent, out);
    }
  }
  /**
   * @declaredat ASTNode:32
   */
  public Object jjtAccept(AstVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
  /**
   * @declaredat ASTNode:35
   */
  public void jjtAddChild(Node n, int i) {
    checkChild(n, i);
    super.jjtAddChild(n, i);
  }
  /**
   * @declaredat ASTNode:39
   */
  public void checkChild(Node n, int i) {
    if (i == 0 && !(n instanceof TokenId)) {
     throw new Error("Child number 0 of TokenComponent has the type " +
       n.getClass().getName() + " which is not an instance of TokenId");
    }
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:46
   */
  @SideEffect.Pure public int getNumChild() {
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:52
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:56
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    isTokenComponent_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:61
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:65
   */
  @SideEffect.Fresh public TokenComponent clone() throws CloneNotSupportedException {
    TokenComponent node = (TokenComponent) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:70
   */
  @SideEffect.Fresh(group="_ASTNode") public TokenComponent copy() {
    try {
      TokenComponent node = (TokenComponent) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:89
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public TokenComponent fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:99
   */
  @SideEffect.Fresh(group="_ASTNode") public TokenComponent treeCopyNoTransform() {
    TokenComponent tree = (TokenComponent) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:119
   */
  @SideEffect.Fresh(group="_ASTNode") public TokenComponent treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:124
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the TokenId child.
   * @param node The new node to replace the TokenId child.
   * @apilevel high-level
   */
  public void setTokenId(TokenId node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the TokenId child.
   * @return The current node used as the TokenId child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="TokenId")
  @SideEffect.Pure public TokenId getTokenId() {
    return (TokenId) getChild(0);
  }
  /**
   * Retrieves the TokenId child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the TokenId child.
   * @apilevel low-level
   */
  @SideEffect.Pure public TokenId getTokenIdNoTransform() {
    return (TokenId) getChildNoTransform(0);
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isTokenComponent_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void isTokenComponent_reset() {
    isTokenComponent_computed = false;
    isTokenComponent_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean isTokenComponent_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean isTokenComponent_value;

  /**
   * @attribute syn
   * @aspect ASTEqual
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTEqual.jrag:51
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ASTEqual", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTEqual.jrag:51")
  @SideEffect.Pure(group="_ASTNode") public boolean isTokenComponent() {
    ASTState state = state();
    if (isTokenComponent_computed) {
      return isTokenComponent_value;
    }
    if (isTokenComponent_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Component.isTokenComponent().");
    }
    isTokenComponent_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    isTokenComponent_value = true;
    if (isFinal && _boundaries == state().boundariesCrossed) {
    isTokenComponent_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    isTokenComponent_visited = -1;
    return isTokenComponent_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int name_visited = -1;
  /**
   * @attribute syn
   * @aspect Names
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:106
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Names", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:106")
  @SideEffect.Pure(group="_ASTNode") public String name() {
    if (name_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Component.name().");
    }
    name_visited = state().boundariesCrossed;
    String name_value = getTokenId().name();
    name_visited = -1;
    return name_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int type_visited = -1;
  /**
   * @attribute syn
   * @aspect Types
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:123
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Types", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:123")
  @SideEffect.Pure(group="_ASTNode") public String type() {
    if (type_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Component.type().");
    }
    type_visited = state().boundariesCrossed;
    String type_value = getTokenId().type();
    type_visited = -1;
    return type_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int annotations_visited = -1;
  /**
   * @attribute syn
   * @aspect Annotations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Annotations.jrag:109
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Annotations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Annotations.jrag:109")
  @SideEffect.Pure(group="_ASTNode") public String annotations() {
    if (annotations_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute TokenComponent.annotations().");
    }
    annotations_visited = state().boundariesCrossed;
    String annotations_value = "";
    annotations_visited = -1;
    return annotations_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int constrParmType_visited = -1;
  /**
   * @attribute syn
   * @aspect ConstructorParameterTypes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:120
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstructorParameterTypes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:120")
  @SideEffect.Pure(group="_ASTNode") public String constrParmType() {
    if (constrParmType_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Component.constrParmType().");
    }
    constrParmType_visited = state().boundariesCrossed;
    String constrParmType_value = getTokenId().getTYPE();
    constrParmType_visited = -1;
    return constrParmType_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isPrimitive_visited = -1;
  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:304
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:304")
  @SideEffect.Pure(group="_ASTNode") public boolean isPrimitive() {
    if (isPrimitive_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute TokenComponent.isPrimitive().");
    }
    isPrimitive_visited = state().boundariesCrossed;
    boolean isPrimitive_value = AttrDecl.isPrimitiveType(getTokenId().getTYPE());
    isPrimitive_visited = -1;
    return isPrimitive_value;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
