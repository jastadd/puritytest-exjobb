/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package org.jastadd.ast.AST;
import org.jastadd.ast.AST.*;
import java.util.*;
import org.jastadd.Problem;
import org.jastadd.jrag.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jastadd.jrag.AST.ASTExpression;
import org.jastadd.jrag.Unparser;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.io.PrintStream;
import java.io.*;
import java.util.regex.*;
import org.jastadd.JastAdd;
import org.jastadd.jrag.AST.ASTAspectMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectRefineMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectFieldDeclaration;
import org.jastadd.jrag.AST.ASTBlock;
import org.jastadd.jrag.ClassBodyObject;
import org.jastadd.jrag.AST.ASTCompilationUnit;
import org.jastadd.ast.AST.Token;
import org.jastadd.ast.AST.SimpleNode;
import org.jastadd.Configuration;
import org.jastadd.tinytemplate.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Ast.ast:53
 * @production CollEq : {@link AttrEq} ::= <span class="component">{@link Parameter}*</span> <span class="component">&lt;Name:String&gt;</span> <span class="component">&lt;FileName:String&gt;</span> <span class="component">&lt;StartLine:int&gt;</span> <span class="component">&lt;EndLine:int&gt;</span> <span class="component">&lt;Comment:String&gt;</span> <span class="component">&lt;AspectName:String&gt;</span> <span class="component">&lt;Value:String&gt;</span> <span class="component">&lt;Condition:String&gt;</span> <span class="component">&lt;TargetName:String&gt;</span> <span class="component">&lt;TargetAttributeName:String&gt;</span> <span class="component">&lt;Reference:String&gt;</span>;

 */
public class CollEq extends AttrEq implements Cloneable {
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:63
   */
  private boolean iterableValue = false;
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:65
   */
  private boolean iterableTarget = false;
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:67
   */
  public boolean iterableTarget() {
    return iterableTarget;
  }
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:71
   */
  public void setIterableTarget(boolean b) {
    iterableTarget = b;
  }
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:75
   */
  public boolean iterableValue() {
    return iterableValue;
  }
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:79
   */
  public void setIterableValue(boolean b) {
    iterableValue = b;
  }
  /**
   * @aspect Comments
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Comments.jrag:199
   */
  public String docComment() {
    JavaDocParser parser = new JavaDocParser();
    TemplateContext tt = templateContext();
    tt.bind("SourceComment", parser.parse(getComment()));
    return tt.expand("CollEq.docComment");
  }
  /**
   * @declaredat ASTNode:1
   */
  public CollEq(int i) {
    super(i);
  }
  /**
   * @declaredat ASTNode:5
   */
  public CollEq(Ast p, int i) {
    this(i);
    parser = p;
  }
  /**
   * @declaredat ASTNode:10
   */
  public CollEq() {
    this(0);
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:19
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
    setChild(new List(), 0);
    setChild(new List(), 1);
  }
  /**
   * @declaredat ASTNode:24
   */
  public CollEq(List<Annotation> p0, List<Parameter> p1, String p2, String p3, int p4, int p5, String p6, String p7, String p8, String p9, String p10, String p11, String p12) {
    setChild(p0, 0);
    setChild(p1, 1);
    setName(p2);
    setFileName(p3);
    setStartLine(p4);
    setEndLine(p5);
    setComment(p6);
    setAspectName(p7);
    setValue(p8);
    setCondition(p9);
    setTargetName(p10);
    setTargetAttributeName(p11);
    setReference(p12);
  }
  /**
   * @declaredat ASTNode:39
   */
  public void dumpTree(String indent, java.io.PrintStream out) {
    out.print(indent + "CollEq");
    out.print("\"" + getName() + "\"");
    out.print("\"" + getFileName() + "\"");
    out.print("\"" + getStartLine() + "\"");
    out.print("\"" + getEndLine() + "\"");
    out.print("\"" + getComment() + "\"");
    out.print("\"" + getAspectName() + "\"");
    out.print("\"" + getValue() + "\"");
    out.print("\"" + getCondition() + "\"");
    out.print("\"" + getTargetName() + "\"");
    out.print("\"" + getTargetAttributeName() + "\"");
    out.print("\"" + getReference() + "\"");
    String childIndent = indent + "  ";
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).dumpTree(childIndent, out);
    }
  }
  /**
   * @declaredat ASTNode:57
   */
  public Object jjtAccept(AstVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
  /**
   * @declaredat ASTNode:60
   */
  public void jjtAddChild(Node n, int i) {
    checkChild(n, i);
    super.jjtAddChild(n, i);
  }
  /**
   * @declaredat ASTNode:64
   */
  public void checkChild(Node n, int i) {
      if (i == 0) {
        if (!(n instanceof List)) {
          throw new Error("Child number 0 of AttrEq has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof Annotation)) {
            throw new Error("Child number " + k + " in AnnotationList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of Annotation");
          }
        }
      }
      if (i == 1) {
        if (!(n instanceof List)) {
          throw new Error("Child number 1 of CollEq has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof Parameter)) {
            throw new Error("Child number " + k + " in ParameterList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of Parameter");
          }
        }
      }
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:91
   */
  @SideEffect.Pure public int getNumChild() {
    return 2;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:97
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:101
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    contributionSignature_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:106
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:110
   */
  @SideEffect.Fresh public CollEq clone() throws CloneNotSupportedException {
    CollEq node = (CollEq) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:115
   */
  @SideEffect.Fresh(group="_ASTNode") public CollEq copy() {
    try {
      CollEq node = (CollEq) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:134
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public CollEq fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:144
   */
  @SideEffect.Fresh(group="_ASTNode") public CollEq treeCopyNoTransform() {
    CollEq tree = (CollEq) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:164
   */
  @SideEffect.Fresh(group="_ASTNode") public CollEq treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:169
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_Name == ((CollEq) node).tokenString_Name) && (tokenString_FileName == ((CollEq) node).tokenString_FileName) && (tokenint_StartLine == ((CollEq) node).tokenint_StartLine) && (tokenint_EndLine == ((CollEq) node).tokenint_EndLine) && (tokenString_Comment == ((CollEq) node).tokenString_Comment) && (tokenString_AspectName == ((CollEq) node).tokenString_AspectName) && (tokenString_Value == ((CollEq) node).tokenString_Value) && (tokenString_Condition == ((CollEq) node).tokenString_Condition) && (tokenString_TargetName == ((CollEq) node).tokenString_TargetName) && (tokenString_TargetAttributeName == ((CollEq) node).tokenString_TargetAttributeName) && (tokenString_Reference == ((CollEq) node).tokenString_Reference);    
  }
  /**
   * Replaces the Annotation list.
   * @param list The new list node to be used as the Annotation list.
   * @apilevel high-level
   */
  public void setAnnotationList(List<Annotation> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the Annotation list.
   * @return Number of children in the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumAnnotation() {
    return getAnnotationList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Annotation list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Annotation list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumAnnotationNoTransform() {
    return getAnnotationListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Annotation list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Annotation getAnnotation(int i) {
    return (Annotation) getAnnotationList().getChild(i);
  }
  /**
   * Check whether the Annotation list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasAnnotation() {
    return getAnnotationList().getNumChild() != 0;
  }
  /**
   * Append an element to the Annotation list.
   * @param node The element to append to the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addAnnotation(Annotation node) {
    List<Annotation> list = (parent == null) ? getAnnotationListNoTransform() : getAnnotationList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addAnnotationNoTransform(Annotation node) {
    List<Annotation> list = getAnnotationListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Annotation list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setAnnotation(Annotation node, int i) {
    List<Annotation> list = getAnnotationList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Annotation list.
   * @return The node representing the Annotation list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Annotation")
  @SideEffect.Pure(group="_ASTNode") public List<Annotation> getAnnotationList() {
    List<Annotation> list = (List<Annotation>) getChild(0);
    return list;
  }
  /**
   * Retrieves the Annotation list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Annotation list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Annotation> getAnnotationListNoTransform() {
    return (List<Annotation>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the Annotation list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Annotation getAnnotationNoTransform(int i) {
    return (Annotation) getAnnotationListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Annotation list.
   * @return The node representing the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Annotation> getAnnotations() {
    return getAnnotationList();
  }
  /**
   * Retrieves the Annotation list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Annotation list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Annotation> getAnnotationsNoTransform() {
    return getAnnotationListNoTransform();
  }
  /**
   * Replaces the Parameter list.
   * @param list The new list node to be used as the Parameter list.
   * @apilevel high-level
   */
  public void setParameterList(List<Parameter> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the Parameter list.
   * @return Number of children in the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumParameter() {
    return getParameterList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Parameter list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Parameter list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumParameterNoTransform() {
    return getParameterListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Parameter list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Parameter getParameter(int i) {
    return (Parameter) getParameterList().getChild(i);
  }
  /**
   * Check whether the Parameter list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasParameter() {
    return getParameterList().getNumChild() != 0;
  }
  /**
   * Append an element to the Parameter list.
   * @param node The element to append to the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addParameter(Parameter node) {
    List<Parameter> list = (parent == null) ? getParameterListNoTransform() : getParameterList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addParameterNoTransform(Parameter node) {
    List<Parameter> list = getParameterListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Parameter list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setParameter(Parameter node, int i) {
    List<Parameter> list = getParameterList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Parameter list.
   * @return The node representing the Parameter list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Parameter")
  @SideEffect.Pure(group="_ASTNode") public List<Parameter> getParameterList() {
    List<Parameter> list = (List<Parameter>) getChild(1);
    return list;
  }
  /**
   * Retrieves the Parameter list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Parameter list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Parameter> getParameterListNoTransform() {
    return (List<Parameter>) getChildNoTransform(1);
  }
  /**
   * @return the element at index {@code i} in the Parameter list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Parameter getParameterNoTransform(int i) {
    return (Parameter) getParameterListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Parameter list.
   * @return The node representing the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Parameter> getParameters() {
    return getParameterList();
  }
  /**
   * Retrieves the Parameter list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Parameter list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Parameter> getParametersNoTransform() {
    return getParameterListNoTransform();
  }
  /**
   * Replaces the lexeme Name.
   * @param value The new value for the lexeme Name.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setName(String value) {
    tokenString_Name = value;
  }
  /**
   * Retrieves the value for the lexeme Name.
   * @return The value for the lexeme Name.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Name")
  @SideEffect.Pure(group="_ASTNode") public String getName() {
    return tokenString_Name != null ? tokenString_Name : "";
  }
  /**
   * Replaces the lexeme FileName.
   * @param value The new value for the lexeme FileName.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setFileName(String value) {
    tokenString_FileName = value;
  }
  /**
   * Retrieves the value for the lexeme FileName.
   * @return The value for the lexeme FileName.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="FileName")
  @SideEffect.Pure(group="_ASTNode") public String getFileName() {
    return tokenString_FileName != null ? tokenString_FileName : "";
  }
  /**
   * Replaces the lexeme StartLine.
   * @param value The new value for the lexeme StartLine.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setStartLine(int value) {
    tokenint_StartLine = value;
  }
  /**
   * Retrieves the value for the lexeme StartLine.
   * @return The value for the lexeme StartLine.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="StartLine")
  @SideEffect.Pure(group="_ASTNode") public int getStartLine() {
    return tokenint_StartLine;
  }
  /**
   * Replaces the lexeme EndLine.
   * @param value The new value for the lexeme EndLine.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setEndLine(int value) {
    tokenint_EndLine = value;
  }
  /**
   * Retrieves the value for the lexeme EndLine.
   * @return The value for the lexeme EndLine.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="EndLine")
  @SideEffect.Pure(group="_ASTNode") public int getEndLine() {
    return tokenint_EndLine;
  }
  /**
   * Replaces the lexeme Comment.
   * @param value The new value for the lexeme Comment.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setComment(String value) {
    tokenString_Comment = value;
  }
  /**
   * Retrieves the value for the lexeme Comment.
   * @return The value for the lexeme Comment.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Comment")
  @SideEffect.Pure(group="_ASTNode") public String getComment() {
    return tokenString_Comment != null ? tokenString_Comment : "";
  }
  /**
   * Replaces the lexeme AspectName.
   * @param value The new value for the lexeme AspectName.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setAspectName(String value) {
    tokenString_AspectName = value;
  }
  /**
   * Retrieves the value for the lexeme AspectName.
   * @return The value for the lexeme AspectName.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="AspectName")
  @SideEffect.Pure(group="_ASTNode") public String getAspectName() {
    return tokenString_AspectName != null ? tokenString_AspectName : "";
  }
  /**
   * Replaces the lexeme Value.
   * @param value The new value for the lexeme Value.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setValue(String value) {
    tokenString_Value = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_Value;
  /**
   * Retrieves the value for the lexeme Value.
   * @return The value for the lexeme Value.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Value")
  @SideEffect.Pure(group="_ASTNode") public String getValue() {
    return tokenString_Value != null ? tokenString_Value : "";
  }
  /**
   * Replaces the lexeme Condition.
   * @param value The new value for the lexeme Condition.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setCondition(String value) {
    tokenString_Condition = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_Condition;
  /**
   * Retrieves the value for the lexeme Condition.
   * @return The value for the lexeme Condition.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Condition")
  @SideEffect.Pure(group="_ASTNode") public String getCondition() {
    return tokenString_Condition != null ? tokenString_Condition : "";
  }
  /**
   * Replaces the lexeme TargetName.
   * @param value The new value for the lexeme TargetName.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setTargetName(String value) {
    tokenString_TargetName = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_TargetName;
  /**
   * Retrieves the value for the lexeme TargetName.
   * @return The value for the lexeme TargetName.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="TargetName")
  @SideEffect.Pure(group="_ASTNode") public String getTargetName() {
    return tokenString_TargetName != null ? tokenString_TargetName : "";
  }
  /**
   * Replaces the lexeme TargetAttributeName.
   * @param value The new value for the lexeme TargetAttributeName.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setTargetAttributeName(String value) {
    tokenString_TargetAttributeName = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_TargetAttributeName;
  /**
   * Retrieves the value for the lexeme TargetAttributeName.
   * @return The value for the lexeme TargetAttributeName.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="TargetAttributeName")
  @SideEffect.Pure(group="_ASTNode") public String getTargetAttributeName() {
    return tokenString_TargetAttributeName != null ? tokenString_TargetAttributeName : "";
  }
  /**
   * Replaces the lexeme Reference.
   * @param value The new value for the lexeme Reference.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setReference(String value) {
    tokenString_Reference = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_Reference;
  /**
   * Retrieves the value for the lexeme Reference.
   * @return The value for the lexeme Reference.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Reference")
  @SideEffect.Pure(group="_ASTNode") public String getReference() {
    return tokenString_Reference != null ? tokenString_Reference : "";
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int rootType_visited = -1;
  /** @return the type name of the collection root node 
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:43
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:43")
  @SideEffect.Pure(group="_ASTNode") public String rootType() {
    if (rootType_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute CollEq.rootType().");
    }
    rootType_visited = state().boundariesCrossed;
    String rootType_value = decl().rootType();
    rootType_visited = -1;
    return rootType_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int getType_visited = -1;
  /** @return the return type of the collection attribute 
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:46
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:46")
  @SideEffect.Pure(group="_ASTNode") public String getType() {
    if (getType_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute CollEq.getType().");
    }
    getType_visited = state().boundariesCrossed;
    String getType_value = decl().getType();
    getType_visited = -1;
    return getType_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int elementType_visited = -1;
  /**
   * @return the element type of this collection, or Object if the collection
   * is not parameterized
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:52
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:52")
  @SideEffect.Pure(group="_ASTNode") public String elementType() {
    if (elementType_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute CollEq.elementType().");
    }
    elementType_visited = state().boundariesCrossed;
    try {
        String type = decl().getType();
        int start = type.indexOf('<');
        int end = type.lastIndexOf('>');
        if (start != -1 && end != -1) {
          return type.substring(start + 1, end);
        } else {
          return "Object";
        }
      }
    finally {
      elementType_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int implicitTarget_visited = -1;
  /** @return {@code true} if this contribution implicitly contributes to the collection root. 
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:84
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:84")
  @SideEffect.Pure(group="_ASTNode") public boolean implicitTarget() {
    if (implicitTarget_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute CollEq.implicitTarget().");
    }
    implicitTarget_visited = state().boundariesCrossed;
    boolean implicitTarget_value = getReference().isEmpty();
    implicitTarget_visited = -1;
    return implicitTarget_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int contributionSignature_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void contributionSignature_reset() {
    contributionSignature_computed = false;
    
    contributionSignature_value = null;
    contributionSignature_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean contributionSignature_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected String contributionSignature_value;

  /**
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:162
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:162")
  @SideEffect.Pure(group="_ASTNode") public String contributionSignature() {
    ASTState state = state();
    if (contributionSignature_computed) {
      return contributionSignature_value;
    }
    if (contributionSignature_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute CollEq.contributionSignature().");
    }
    contributionSignature_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    contributionSignature_value = decl().getTarget() + "_" + signature();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    contributionSignature_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    contributionSignature_visited = -1;
    return contributionSignature_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int hasCondition_visited = -1;
  /**
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:201
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:201")
  @SideEffect.Pure(group="_ASTNode") public boolean hasCondition() {
    if (hasCondition_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute CollEq.hasCondition().");
    }
    hasCondition_visited = state().boundariesCrossed;
    boolean hasCondition_value = !getCondition().isEmpty();
    hasCondition_visited = -1;
    return hasCondition_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int collectionId_visited = -1;
  /**
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:383
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:383")
  @SideEffect.Pure(group="_ASTNode") public String collectionId() {
    if (collectionId_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute CollEq.collectionId().");
    }
    collectionId_visited = state().boundariesCrossed;
    String collectionId_value = decl().collectionId();
    collectionId_visited = -1;
    return collectionId_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int signature_visited = -1;
  /**
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:395
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:395")
  @SideEffect.Pure(group="_ASTNode") public String signature() {
    if (signature_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute CollEq.signature().");
    }
    signature_visited = state().boundariesCrossed;
    String signature_value = decl().signature();
    signature_visited = -1;
    return signature_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int decl_visited = -1;
  /**
   * Finds the declaration node of a collection equation.
   * 
   * @return the collection attribute declaration, or {@code null} if no
   * declaration was found.
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:501
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:501")
  @SideEffect.Pure(group="_ASTNode") public CollDecl decl() {
    if (decl_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute CollEq.decl().");
    }
    decl_visited = state().boundariesCrossed;
    CollDecl decl_value = grammar().lookupCollDecl(getTargetName(), getTargetAttributeName());
    decl_visited = -1;
    return decl_value;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  @SideEffect.Local(group="_ASTNode") protected void collect_contributors_TypeDecl_attributeProblems(TypeDecl _root, @SideEffect.Local java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:488
    if (decl() != null && implicitTarget() && !decl().rootType().equals(getTargetName())) {
      {
        TypeDecl target = (TypeDecl) (hostClass());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:496
    if (decl() == null) {
      {
        TypeDecl target = (TypeDecl) (hostClass());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_TypeDecl_attributeProblems(_root, _map);
  }
  @SideEffect.Local(group="_ASTNode") protected void collect_contributors_CollDecl_uses(Grammar _root, @SideEffect.Local java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:517
    if (decl() != null) {
      {
        CollDecl target = (CollDecl) (decl());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_CollDecl_uses(_root, _map);
  }
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_TypeDecl_attributeProblems(Collection<Problem> collection) {
    super.contributeTo_TypeDecl_attributeProblems(collection);
    if (decl() != null && implicitTarget() && !decl().rootType().equals(getTargetName())) {
      collection.add(errorf("contribution target expression missing for attribute '%s.%s'. "
                + "Contribution target expression is optional only when the collection root node is "
                + "the target.",
                getTargetName(), getTargetAttributeName()));
    }
    if (decl() == null) {
      collection.add(errorf("undeclared collection attribute '%s.%s'", getTargetName(),
                getTargetAttributeName()));
    }
  }
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_CollDecl_uses(HashSet collection) {
    super.contributeTo_CollDecl_uses(collection);
    if (decl() != null) {
      collection.add(this);
    }
  }
}
