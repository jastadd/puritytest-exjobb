package org.jastadd.ast.AST;

import org.jastadd.ast.AST.*;
import java.util.*;
import org.jastadd.Problem;
import org.jastadd.jrag.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jastadd.jrag.AST.ASTExpression;
import org.jastadd.jrag.Unparser;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.io.PrintStream;
import java.io.*;
import java.util.regex.*;
import org.jastadd.JastAdd;
import org.jastadd.jrag.AST.ASTAspectMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectRefineMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectFieldDeclaration;
import org.jastadd.jrag.AST.ASTBlock;
import org.jastadd.jrag.ClassBodyObject;
import org.jastadd.jrag.AST.ASTCompilationUnit;
import org.jastadd.ast.AST.Token;
import org.jastadd.ast.AST.SimpleNode;
import org.jastadd.Configuration;
import org.jastadd.tinytemplate.*;
/**
 * @ast class
 * @aspect CollectionAttributes
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:228
 */
abstract class CustomSurveyContribution extends java.lang.Object implements SurveyContribution {
  
    protected final String collName;

  
    protected final String collHost;

  
    protected final String fileName;

  
    protected final int startLine;

  
    protected final int endLine;

  
    protected final String comment;

  
    protected final String aspectName;

  

    public CustomSurveyContribution(String collName, String collHost,
        String fileName, int startLine, int endLine, String comment,
        String aspectName) {
      this.collName = collName;
      this.collHost = collHost;
      this.fileName = fileName;
      this.startLine = startLine;
      this.endLine = endLine;
      this.comment = comment;
      this.aspectName = aspectName;
    }

  

    public CollDecl lookupCollDecl(Grammar grammar) {
      CollDecl decl = grammar.lookupCollDecl(collHost, collName);
      if (decl == null) {
        throw new Error(String.format(
              "%s:%d: Can not add custom survey code for unknown collection attribute: %s.%s()",
              fileName, startLine, collHost, collName));
      }
      return decl;
    }


}
