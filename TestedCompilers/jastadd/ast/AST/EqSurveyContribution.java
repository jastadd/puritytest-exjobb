package org.jastadd.ast.AST;

import org.jastadd.ast.AST.*;
import java.util.*;
import org.jastadd.Problem;
import org.jastadd.jrag.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jastadd.jrag.AST.ASTExpression;
import org.jastadd.jrag.Unparser;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.io.PrintStream;
import java.io.*;
import java.util.regex.*;
import org.jastadd.JastAdd;
import org.jastadd.jrag.AST.ASTAspectMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectRefineMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectFieldDeclaration;
import org.jastadd.jrag.AST.ASTBlock;
import org.jastadd.jrag.ClassBodyObject;
import org.jastadd.jrag.AST.ASTCompilationUnit;
import org.jastadd.ast.AST.Token;
import org.jastadd.ast.AST.SimpleNode;
import org.jastadd.Configuration;
import org.jastadd.tinytemplate.*;
/**
 * @ast class
 * @aspect CollectionAttributes
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:207
 */
 class EqSurveyContribution extends java.lang.Object implements SurveyContribution {
  
    private final CollEq eq;

  
    public EqSurveyContribution(CollEq eq) {
      this.eq = eq;
    }

  

    @Override
    public void emitSurveyCode(CollDecl decl, PrintStream out) {
      TemplateContext tt = eq.templateContext();
      tt.bind("BottomValue", decl.getBottomValue());
      tt.bind("CombOp", decl.getCombOp());
      if (decl.onePhase()) {
        tt.bind("HasCondition", eq.hasCondition());
        tt.expand("CollEq.collectContributors:onePhase", out);
      } else {
        tt.bind("HasCondition", eq.hasCondition() && !decl.lazyCondition());
        tt.expand("CollEq.collectContributors:twoPhase", out);
      }
    }


}
