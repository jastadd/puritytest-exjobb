/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package org.jastadd.ast.AST;
import org.jastadd.ast.AST.*;
import java.util.*;
import org.jastadd.Problem;
import org.jastadd.jrag.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jastadd.jrag.AST.ASTExpression;
import org.jastadd.jrag.Unparser;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.io.PrintStream;
import java.io.*;
import java.util.regex.*;
import org.jastadd.JastAdd;
import org.jastadd.jrag.AST.ASTAspectMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectRefineMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectFieldDeclaration;
import org.jastadd.jrag.AST.ASTBlock;
import org.jastadd.jrag.ClassBodyObject;
import org.jastadd.jrag.AST.ASTCompilationUnit;
import org.jastadd.ast.AST.Token;
import org.jastadd.ast.AST.SimpleNode;
import org.jastadd.Configuration;
import org.jastadd.tinytemplate.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Ast.ast:1
 * @production Grammar : {@link ASTNode} ::= <span class="component">{@link TypeDecl}*</span> <span class="component">{@link RegionDecl}*</span>;

 */
public class Grammar extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect Lookup
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:41
   */
  public TypeDecl findClassDecl(String name, String comment,
      String fileName, int beginLine, String enclosingAspect) {
    comment = comment.trim();
    if (!comment.startsWith("//") || !comment.startsWith("/*")) {
      comment = "";
    }
    TypeDecl t = lookup(name);
    if (t == null) {
      t = new ClassDecl(new IdDecl(name), new List(), new List(), new List(),
          new List(), new List(), new List(), new List(),
          fileName, beginLine ,-1, comment,
          enclosingAspect);
      addTypeDecl(t);
    }
    return t;
  }
  /**
   * @aspect Lookup
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:58
   */
  public TypeDecl findInterfaceDecl(String name, String comment,
      String fileName, int beginLine, String enclosingAspect) {
    comment = comment.trim();
    if (!comment.startsWith("//") || !comment.startsWith("/*")) {
      comment = "";
    }
    TypeDecl t = lookup(name);
    if (t == null) {
      t = new InterfaceDecl(new IdDecl(name), new List(), new List(),
          new List(), new List(), new List(), new List(), new List(),
          fileName, beginLine, -1, comment, enclosingAspect);
      addTypeDecl(t);
    }
    return t;
  }
  /**
   * @aspect Lookup
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:74
   */
  public TypeDecl findEnumDecl(String name, String comment,
      String fileName, int beginLine, String enclosingAspect) {
    comment = comment.trim();
    if (!comment.startsWith("//") || !comment.startsWith("/*")) {
      comment = "";
    }
    TypeDecl t = lookup(name);
    if (t == null) {
      t = new EnumDecl(new IdDecl(name), new List(), new List(),
          new List(), new List(), new List(), new List(), new List(),
          fileName, beginLine, -1, comment, enclosingAspect);
      addTypeDecl(t);
    }
    return t;
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:43
   */
  public Collection<Problem> problems = new LinkedList<Problem>();
  /**
   * Add an error to the grammar.
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:48
   */
  public void error(String message, String file, int line) {
    problems.add(Problem.builder()
        .message(message)
        .sourceFile(file)
        .sourceLine(line)
        .buildError());
  }
  /**
   * Add an error to the grammar.
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:59
   */
  public void error(String message) {
    problems.add(Problem.builder()
        .message(message)
        .buildError());
  }
  /**
   * Add an error to the grammar.
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:68
   */
  public void errorf(String messagefmt, Object... args) {
    error(String.format(messagefmt, args));
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:72
   */
  public Collection<Problem> weavingProblems() {
    return problems;
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:82
   */
  public HashMap aspectMap = new HashMap();
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:84
   */
  public void registerAspect(String name, String comment) {
    aspectMap.put(name, comment);
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:89
   */
  public void addRewriteList(
      String className,
      org.jastadd.jrag.AST.SimpleNode condition,
      org.jastadd.jrag.AST.SimpleNode result,
      String type,
      String fileName,
      int startLine,
      int endLine,
      String parentName,
      String childName,
      String aspectName) {

    if (!config().rewriteEnabled()) {
      error("can not use rewrites while rewrites are disabled (enable with --rewrite=cnta)",
          fileName, startLine);
      return;
    }

    TypeDecl c = lookup(className);
    if (c != null && c instanceof ASTDecl) {
      RewriteList r = new RewriteList();
      r.setFileName(fileName);
      r.setStartLine(startLine);
      r.setEndLine(endLine);
      r.setCondition(condition);
      r.setResult(result);
      r.setReturnType(type);
      r.setParentName(parentName);
      r.setChildName(childName);
      r.setAspectName(aspectName);
      ((ASTDecl)c).addRewrite(r);
    } else if (c != null) {
      error("can not rewrite to non AST class '" + className + "'", fileName, startLine);
    } else {
      error("can not rewrite to unknown class '" + className + "'", fileName, startLine);
    }
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:127
   */
  public void addRewrite(
      String className,
      org.jastadd.jrag.AST.SimpleNode condition,
      org.jastadd.jrag.AST.SimpleNode result,
      String type,
      String fileName,
      int startLine,
      int endLine,
      String aspectName) {

    if (!config().rewriteEnabled()) {
      error("can not use rewrites while rewrites are disabled (enable with --rewrite=cnta)",
          fileName, startLine);
      return;
    }

    TypeDecl c = lookup(className);
    if (c != null && c instanceof ASTDecl) {
      Rewrite r = new Rewrite();
      r.setFileName(fileName);
      r.setStartLine(startLine);
      r.setEndLine(endLine);
      r.setCondition(condition);
      r.setResult(result);
      r.setReturnType(type);
      r.setAspectName(aspectName);
      ((ASTDecl)c).addRewrite(r);
    } else if (c != null) {
      error("can not rewrite to non AST class '" + className + "'", fileName, startLine);
    } else {
      error("can not rewrite to unknown class '" + className + "'", fileName, startLine);
    }
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:170
   */
  public ArrayList<CacheDecl> cacheDecls = new ArrayList<CacheDecl>();
  /**
   * Configures the caching behavior for an attribute.
   * 
   * @param mode The cache configuration, should be "cache" or "uncache"
   * @param hostName The node type hosting the attribute
   * @param attrName The attribute name
   * @param paramList The list of attribute parameters
   * @param fileName The name of the file with the attribute declaration
   * @param startLine The line of the attribute declaration
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:182
   */
  public void setAttributeCacheMode(String mode, String hostName, String attrName,
      org.jastadd.ast.AST.List paramList, String fileName, int startLine) {
    CacheDecl decl = new CacheDecl();
    decl.mode = mode;
    decl.hostName = hostName;
    decl.attrName = attrName;
    decl.fileName = fileName;
    decl.startLine = startLine;
    StringBuilder signature = new StringBuilder();
    signature.append(attrName);
    for (int i = 0; i < paramList.getNumChild(); i++) {
      signature.append("_" + ((Parameter) paramList.getChild(i)).getTypeInSignature());
    }
    decl.signature = signature.toString();
    cacheDecls.add(decl);
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:199
   */
  public void applyCacheMode(CacheDecl decl, Collection<Problem> problems) {
    // Lookup type.
    TypeDecl type = lookup(decl.hostName);
    if (type == null) {
      problems.add(Problem.builder()
          .message("can not find attribute %s in unknown class %s", decl.attrName, decl.hostName)
          .sourceFile(decl.fileName)
          .sourceLine(decl.startLine)
          .buildError());
      return;
    }
    // Lookup attribute; first 'syn' then 'inh'.
    AttrDecl attr = null;
    if ((attr = type.lookupSynDecl(decl.signature)) == null
        && (attr = type.lookupInhDecl(decl.signature)) == null) {
      problems.add(Problem.builder()
          .message("can not find attribute %s.%s", decl.hostName, decl.attrName)
          .sourceFile(decl.fileName)
          .sourceLine(decl.startLine)
          .buildError());
      return;
    }
    // Configure attribute caching.
    if (decl.mode.equals("cache")) {
      attr.setCacheMode(CacheMode.CACHED);
    } else if (decl.mode.equals("uncache")) {
      attr.setCacheMode(CacheMode.UNCACHED);
    } else {
      problems.add(Problem.builder()
          .message("unknown configuration %s for attribute %s.%s", decl.mode, decl.hostName,
              decl.attrName)
          .sourceFile(decl.fileName)
          .sourceLine(decl.startLine)
          .buildError());
    }
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:236
   */
  public LinkedList<SynDecl> synDecls = new LinkedList<SynDecl>();
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:238
   */
  public LinkedList<SynEq> synEqs = new LinkedList<SynEq>();
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:244
   */
  public LinkedList<InhDecl> inhDecls = new LinkedList<InhDecl>();
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:246
   */
  public LinkedList<InhEq> inhEqs = new LinkedList<InhEq>();
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:252
   */
  public void addSynDecl(
      String name,
      String type,
      String className,
      boolean isLazy,
      String fileName,
      int startLine,
      int endLine,
      org.jastadd.ast.AST.List parameterList,
      String bottomValue,
      boolean isFinal,
      boolean isNTA,
      org.jastadd.jrag.AST.SimpleNode node,
      String aspectName,
      ArrayList<String> annotations) {
    SynDecl decl = new SynDecl(
        parameterList,
        name,
        type,
        (isLazy | isNTA) ? CacheMode.LAZY : CacheMode.DEFAULT,
        fileName,
        startLine,
        endLine,
        isFinal | isNTA,
        isNTA,
        Unparser.unparseComment(node),
        aspectName,
        new List());
    for (String annotation : annotations) {
      decl.addAnnotation(new Annotation(annotation));
    }
    decl.setBottomValue(bottomValue);
    decl.hostName = className;
    synDecls.add(decl);
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:288
   */
  public void addSynEq(
      String name,
      String className,
      org.jastadd.jrag.AST.SimpleNode rhs,
      String fileName,
      int startLine,
      int endLine,
      org.jastadd.ast.AST.List list,
      org.jastadd.jrag.AST.SimpleNode node,
      String aspectName,
      ArrayList<String> annotations) {
    SynEq equ = new SynEq();
    equ.setName(name);
    equ.setFileName(fileName);
    equ.setStartLine(startLine);
    equ.setEndLine(endLine);
    equ.setRHS(rhs);
    equ.setParameterList(list);
    equ.setComment(Unparser.unparseComment(node));
    equ.setAspectName(aspectName);
    for (String annotation : annotations) {
      equ.addAnnotation(new Annotation(annotation));
    }
    equ.hostName = className;
    synEqs.add(equ);
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:315
   */
  public void addInhDecl(String name, String type, String className,
      boolean isLazy, String fileName, int startLine, int endLine,
      org.jastadd.ast.AST.List parameterList, String bottomValue,
      boolean isFinal, boolean isNTA, org.jastadd.jrag.AST.SimpleNode node,
      String aspectName, ArrayList<String> annotations) {
    InhDecl decl = new InhDecl(
        parameterList,
        name,
        type,
        (isLazy | isNTA) ? CacheMode.LAZY : CacheMode.DEFAULT,
        fileName,
        startLine,
        endLine,
        isFinal | isNTA,
        isNTA,
        Unparser.unparseComment(node),
        aspectName,
        new List()
    );
    for (String annotation : annotations) {
      decl.addAnnotation(new Annotation(annotation));
    }
    decl.setBottomValue(bottomValue);
    decl.hostName = className;
    inhDecls.add(decl);
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:342
   */
  public void addInhEq(String childName, String name, String className,
      org.jastadd.jrag.AST.SimpleNode rhs, String fileName, int startLine,
      int endLine, org.jastadd.ast.AST.List list,
      org.jastadd.jrag.AST.SimpleNode node, String aspectName,
      ArrayList<String> annotations) {
    addInhEq(childName, name, className, rhs, fileName, startLine, endLine,
        list, null, node, aspectName, annotations);
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:351
   */
  public void addInhEq(String childName, String name, String className,
      org.jastadd.jrag.AST.SimpleNode rhs, String fileName, int startLine,
      int endLine, org.jastadd.ast.AST.List list, Parameter p,
      org.jastadd.jrag.AST.SimpleNode node, String aspectName,
      ArrayList<String> annotations) {
    InhEq equ = new InhEq();
    equ.setName(name);
    equ.setFileName(fileName);
    equ.setStartLine(startLine);
    equ.setEndLine(endLine);
    equ.setRHS(rhs);
    equ.setChildName(childName);
    equ.setParameterList(list);
    equ.setComment(Unparser.unparseComment(node));
    equ.setAspectName(aspectName);
    for (String annotation : annotations) {
      equ.addAnnotation(new Annotation(annotation));
    }
    if (p != null) {
      equ.setIndex(p);
    }
    equ.hostName = className;
    inhEqs.add(equ);
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:376
   */
  public void addComponents(String className, org.jastadd.ast.AST.List componentsList) {
    TypeDecl d = lookup(className);
    if (d != null) {
      d.setComponentList(componentsList);
    }
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:393
   */
  public void addCompUnit(org.jastadd.jrag.AST.ASTCompilationUnit compUnit) {
    compilationUnits.add(compUnit);
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:397
   */
  public void addMethodDecl(org.jastadd.jrag.AST.SimpleNode n, String className,
      String fileName, String modifiers, String aspectName) {
    TypeDecl c = lookup(className);
    if (c != null) {
      ClassBodyObject obj = new ClassBodyObject(n, fileName, n.firstToken.beginLine, aspectName);
      obj.modifiers = modifiers;
      c.classBodyDecls.add(obj);
    } else {
      error("can not add member to unknown class " + className, fileName, n.firstToken.beginLine);
    }
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:419
   */
  public LinkedList<InterTypeObject> interTypeObjects = new LinkedList<InterTypeObject>();
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:421
   */
  public void addClassBodyDecl(org.jastadd.jrag.AST.SimpleNode n,
      String className, String fileName, String aspectName) {
    interTypeObjects.add(new InterTypeObject(className, new ClassBodyObject(n,
        fileName, n.firstToken.beginLine, aspectName)));
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:427
   */
  public void addClassBodyDecl(org.jastadd.jrag.AST.SimpleNode n,
      String className, String fileName, String modifiers, String aspectName) {
    ClassBodyObject obj = new ClassBodyObject(n, fileName, n.firstToken.beginLine, aspectName);
    obj.modifiers = modifiers;
    interTypeObjects.add(new InterTypeObject(className, obj));
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:454
   */
  public void processRefinements() {
    for (int i = 0; i < getNumTypeDecl(); i++) {
      TypeDecl typeDecl = getTypeDecl(i);
      if (!(typeDecl instanceof InterfaceDecl)) {
        typeDecl.processReplacements();
        typeDecl.processRefinedClassBodyDecls();
        typeDecl.processSynEqReplacements();
        typeDecl.processRefinedSynEqs();
        typeDecl.processRefinedInhEqs();
      }
    }
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:467
   */
  public void processInterfaceRefinements() {
    for (int i = 0; i < getNumTypeDecl(); i++) {
      TypeDecl typeDecl = getTypeDecl(i);
      if (typeDecl instanceof InterfaceDecl) {
        typeDecl.processReplacements();
        typeDecl.processRefinedClassBodyDecls();
        typeDecl.processSynEqReplacements();
        typeDecl.processRefinedSynEqs();
        typeDecl.processRefinedInhEqs();
      }
    }
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:482
   */
  public void addRefinedSynEq(String name, String className,
      org.jastadd.jrag.AST.SimpleNode rhs, String fileName, int startLine, int endLine,
      org.jastadd.ast.AST.List list, String aspectName, org.jastadd.jrag.AST.SimpleNode node,
      String declaredAspect) {
    TypeDecl c = lookup(className);
    if (c != null) {
      SynEq equ = new SynEq();
      equ.setName(name);
      equ.setFileName(fileName);
      equ.setStartLine(startLine);
      equ.setEndLine(endLine);
      equ.setRHS(rhs);
      equ.setParameterList(list);
      equ.refinesAspect = aspectName;
      equ.setComment(Unparser.unparseComment(node));
      equ.setAspectName(declaredAspect);
      c.refinedSynEqs.add(equ);
    } else {
      error("can not add equation for synthesized attribute " + name + " to unknown class "
          + className, fileName, startLine);
    }
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:505
   */
  public void addReplacedSynEq(String name, String className,
      org.jastadd.jrag.AST.SimpleNode rhs, String fileName, int startLine, int endLine,
      org.jastadd.ast.AST.List list, String aspectName, String secondAspectName,
      org.jastadd.jrag.AST.SimpleNode node, String declaredAspect) {
    TypeDecl c = lookup(className);
    if (c != null) {
      SynEq equ = new SynEq();
      equ.setName(name);
      equ.setFileName(fileName);
      equ.setStartLine(startLine);
      equ.setEndLine(endLine);
      equ.setRHS(rhs);
      equ.setParameterList(list);
      equ.refinesAspect = aspectName;
      equ.replacesAspect = secondAspectName;
      equ.setComment(Unparser.unparseComment(node));
      equ.setAspectName(declaredAspect);
      c.replacedSynEqs.add(equ);
    } else {
      error("can not add equation for synthesized attribute " + name + " to unknown class "
          + className, fileName, startLine);
    }
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:673
   */
  public void addRefinedInhEq(String childName, String name, String className,
      org.jastadd.jrag.AST.SimpleNode rhs, String fileName, int startLine, int endLine,
      org.jastadd.ast.AST.List list, String aspectName, org.jastadd.jrag.AST.SimpleNode node,
      String declaredAspect) {
    addRefinedInhEq(childName, name, className, rhs, fileName, startLine, endLine, list, null,
        aspectName, node, declaredAspect);
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:681
   */
  public void addRefinedInhEq(String childName, String name, String className,
      org.jastadd.jrag.AST.SimpleNode rhs, String fileName, int startLine, int endLine,
      org.jastadd.ast.AST.List list, Parameter p, String aspectName,
      org.jastadd.jrag.AST.SimpleNode node, String declaredAspect) {
    TypeDecl c = lookup(className);
    if (c != null) {
        InhEq equ = new InhEq();
        equ.setName(name);
        equ.setFileName(fileName);
        equ.setStartLine(startLine);
        equ.setEndLine(endLine);
        equ.setRHS(rhs);
        equ.setChildName(childName);
        equ.setParameterList(list);
        equ.refinesAspect = aspectName;
        equ.setComment(Unparser.unparseComment(node));
        equ.setAspectName(declaredAspect);
        if (p != null) {
          equ.setIndex(p);
        }
        c.refinedInhEqs.add(equ); // Sort in alphabetical order, then non-NTAs first.
    } else {
      error("can not add equation for inherited attribute " + name + " to unknown class "
          + className, fileName, startLine);
    }
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:814
   */
  public void addRefinedClassBodyDecl(org.jastadd.jrag.AST.SimpleNode n, String className,
      String fileName, String aspectName, String declaredAspect) {
    TypeDecl c = lookup(className);
    if (c != null) {
      ClassBodyObject o = new ClassBodyObject(n, fileName, n.firstToken.beginLine, declaredAspect);
      o.refinesAspect = aspectName;
      c.refinedClassBodyDecls.add(o);
    } else {
      error("can not add member to unknown class " + className + " in " + fileName);
    }
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:826
   */
  public void addReplacedClassBodyDecl(org.jastadd.jrag.AST.SimpleNode n,
      String className, String fileName, String aspectName, String replacedAspect,
      org.jastadd.jrag.AST.SimpleNode replacedSignature, String declaredAspect) {
    TypeDecl c = lookup(className);
    if (c != null) {
      ClassBodyObject o = new ClassBodyObject(n, fileName, n.firstToken.beginLine, declaredAspect);
      o.refinesAspect = aspectName;
      o.replaceAspect = replacedAspect;
      c.replacements.add(o);
    } else {
      error("can not add member to unknown class " + className + " in " + fileName);
    }
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:1127
   */
  public Collection<ASTCompilationUnit> compilationUnits =
      new LinkedList<ASTCompilationUnit>();
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:424
   */
  public void weaveCollectionAttributes() {
    for (int i = 0; i < getNumTypeDecl(); i++) {
      getTypeDecl(i).weaveCollectionAttributes();
    }
  }
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:519
   */
  public void addCollDecl(String name, String type, String className,
      String fileName, int startLine, int endLine, String startValue,
      String combOp, boolean isCircular, ArrayList<String> annotations,
      org.jastadd.jrag.AST.SimpleNode node, String root, String aspectName) {
    TypeDecl c = lookup(className);
    if (c != null) {
      CollDecl decl = new CollDecl();
      decl.setName(name);
      decl.setType(type);
      decl.setCacheMode(CacheMode.LAZY);
      decl.setFileName(fileName);
      decl.setStartLine(startLine);
      decl.setEndLine(endLine);
      decl.setParameterList(new List());
      decl.setStartValue(startValue);
      decl.setCombOp(combOp);
      for (String annotation : annotations) {
        decl.addAnnotation(new Annotation(annotation));
      }
      decl.setCircularCollection(isCircular || annotations.contains("@Circular"));
      decl.setComment(Unparser.unparseComment(node));
      decl.setTarget(className);
      decl.root = root;
      decl.setAspectName(aspectName);
      // TODO(joqvist): defer collection attribute weaving.
      ((TypeDecl) c).addCollDecl(decl);
    } else {
      errorf("Can not add collection attribute %s %s to unknown class %s in %s at line %d",
          type, name, className, fileName, startLine);
    }
  }
  /**
   * Adds code to the survey method for a particular collection attribute in the
   * given node type.
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:579
   */
  public void addCustomSurveyBlock(String collHost,
      String collName,
      String nodeType,
      String codeBlock,
      String fileName,
      int startLine,
      int endLine,
      org.jastadd.jrag.AST.SimpleNode commentNode,
      String aspectName,
      ArrayList<String> annotations) {
    TypeDecl type = lookup(nodeType);
    if (type != null && type instanceof ASTDecl) {
      ((ASTDecl) type).surveyContributions.add(new BlockSurveyContribution(
          collName,
          collHost,
          codeBlock,
          fileName,
          startLine,
          endLine,
          Unparser.unparseComment(commentNode),
          aspectName));
      for (String annotation : annotations) {
        errorf("annotation %s not allowed for custom survey blocks.", annotation);
      }
    } else {
      errorf("Can not add custom collection survey code to unknown class %s in %s at line %d",
          nodeType, fileName, startLine);
    }
  }
  /**
   * Adds an expression for an NTA child which should be searched during the
   * survey phase of a collection attribute.
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:613
   */
  public void addCustomSurveyExpression(String collHost,
      String collName,
      String nodeType,
      String ntaExpression,
      String fileName,
      int startLine,
      int endLine,
      org.jastadd.jrag.AST.SimpleNode commentNode,
      String aspectName,
      ArrayList<String> annotations) {
    TypeDecl type = lookup(nodeType);
    if (type != null && type instanceof ASTDecl) {
      ((ASTDecl) type).surveyContributions.add(new ExpressionSurveyContribution(
          collName,
          collHost,
          ntaExpression,
          fileName,
          startLine,
          endLine,
          Unparser.unparseComment(commentNode),
          aspectName));
      for (String annotation : annotations) {
        errorf("annotation %s not allowed for custom survey blocks.", annotation);
      }
    } else {
      errorf("Can not add custom collection survey code to unknown class %s in %s at line %d",
          nodeType, fileName, startLine);
    }
  }
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:643
   */
  public CollEq addCollEq(String collHost,
      String collName,
      String nodeType, String reference, String fileName,
      int startLine, int endLine, boolean iterableValue, boolean iterableTarget,
      org.jastadd.jrag.AST.SimpleNode commentNode, String aspectName,
      ArrayList<String> annotations) {
    TypeDecl type = lookup(nodeType);
    if (type != null && type instanceof ASTDecl) {
      CollEq equ = new CollEq(
          new List(),
          new List(),
          collHost,
          fileName,
          startLine,
          endLine,
          Unparser.unparseComment(commentNode),
          aspectName,
          "",
          "",
          collHost,
          collName,
          reference);
      equ.setIterableValue(iterableValue);
      equ.setIterableTarget(iterableTarget);
      for (String annotation : annotations) {
        equ.addAnnotation(new Annotation(annotation));
      }
      // TODO(joqvist): defer collection attribute weaving.
      ((ASTDecl) type).addCollEq(equ);
      return equ;
    } else {
      errorf("Can not add collection contribution to unknown class %s in %s at line %d",
          nodeType, fileName, startLine);
      // TODO(joqvist): defer weaving so we can return non-null.
      return null;
    }
  }
  /**
   * Generates a Dot graph from the grammar.
   * @aspect DotGraph
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Dot.jrag:38
   */
  public void genDotGraph(PrintStream out) {
    out.println("digraph {");
    out.println("  edge [dir=back];");
    out.println("  node [shape=box];");
    for (TypeDecl decl: getTypeDeclList()) {
      if (decl instanceof ASTDecl) {
        ASTDecl astDecl = (ASTDecl) decl;
        if (astDecl.superClass() != null) {
          out.format("  %s -> %s;%n", astDecl.getSuperClass().name(), decl.name());
        } else if (!astDecl.isASTNodeDecl()) {
          out.format("  %s -> %s;%n", config().astNodeType(), decl.name());
        } else {
          out.format("  %s;%n", decl.name());
        }
      }
    }
    out.println("}");
  }
  /**
   * @aspect Grammar
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Grammar.jrag:63
   */
  public void addInterface(org.jastadd.jrag.AST.SimpleNode nameList,
      String className, String fileName) {
    if (nameList == null) {
      // TODO(joqvist): Generate a proper error message.
      System.err.println("Panic");
    } else {
      TypeDecl c = lookup(className);
      if (c != null) {
        c.implementsList.add(nameList);
      } else {
        int line = nameList.firstToken.beginLine;
        error("can not add interface to unknown class " + className, fileName, line);
      }
    }
  }
  /**
   * @aspect Grammar
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Grammar.jrag:79
   */
  public void extendInterface(org.jastadd.jrag.AST.SimpleNode nameList, String className,
      String fileName) {
    if (nameList == null) {
      // TODO(joqvist): Generate a proper error message.
      System.err.println("Panic");
    } else {
      TypeDecl c = lookup(className);
      if (c instanceof InterfaceDecl) {
        c.implementsList.add(nameList);
      } else if (c != null) {
        int line = nameList.firstToken.beginLine;
        error(className + " is not an interface and can therefore not be extended", fileName, line);
      } else {
        int line = nameList.firstToken.beginLine;
        error("can not add interface to unknown interface " + className, fileName, line);
      }
    }
  }
  /**
   * Ensure no duplicate inherited declarations.
   * Warnings are reported for duplicate inherited declarations.
   * See AttributeProblems.jrag
   * @aspect InheritedAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:62
   */
  public void removeDuplicateInhDecls() {
    for (int i = 0; i < getNumTypeDecl(); ++i) {
      getTypeDecl(i).removeDuplicateInhDecls();
    }
  }
  /**
   * Find all inherited attribute declarations in the grammar. Used for
   * semantic analysis and error checking of inherited attributes.
   * 
   * @return map containing all inherited attribute declarations in
   * the grammar.
   * @aspect InheritedAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:110
   */
  Map<String, InhDecl> inhDecls() {
    Map<String, InhDecl> decls = new HashMap<String, InhDecl>();
    for (TypeDecl type : getTypeDecls()) {
      for (int i = 0; i < type.getNumInhDecl(); ++i) {
        InhDecl decl = type.getInhDecl(i);
        decls.put(decl.signature(), decl);
      }
    }
    return decls;
  }
  /**
   * Build the default AST node types (ASTNode, List, Opt)
   * and insert them into the grammar.
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:40
   */
  public void addImplicitNodeTypes() {
    ASTDecl cl;
    IdDecl name;

    // Add ASTNode.
    cl = new ASTDecl();
    name = new IdDecl();
    name.setID(config().astNodeType());
    cl.setIdDecl(name);
    cl.setFileName("");
    cl.modifiers = "public";
    addTypeDecl(cl);

    // Add List.
    cl = new ASTDecl();
    name = new IdDecl();
    name.setID(config().listType());
    cl.setIdDecl(name);
    cl.setFileName("");
    cl.modifiers = "public";
    addTypeDecl(cl);

    // Add Opt.
    cl = new ASTDecl();
    name = new IdDecl();
    name.setID(config().optType());
    cl.setIdDecl(name);
    cl.setFileName("");
    cl.modifiers = "public";
    addTypeDecl(cl);
  }
  /**
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:250
   */
  public void genReset(PrintWriter out) {
    templateContext().expand("ASTState.reset", out);
  }
  /**
   * Generate state class aspect declarations.
   * 
   * @param out Aspect output stream
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:259
   */
  public void emitStateClass(PrintWriter out) {
    templateContext().expand("ASTState", out);
  }
  /**
   * @aspect JastAddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JastAddCodeGen.jadd:43
   */
  public void weaveInterfaceIntroductions() {
    for (int i = 0; i < getNumTypeDecl(); i++) {
      if (getTypeDecl(i) instanceof InterfaceDecl) {
        InterfaceDecl d = (InterfaceDecl)getTypeDecl(i);
        String name = d.name();
        for (int j = 0; j < getNumTypeDecl(); j++) {
          if (getTypeDecl(j) instanceof ASTDecl) {
            ASTDecl dest = (ASTDecl)getTypeDecl(j);
            if (dest.implementsInterface(name)) {
              for (ClassBodyObject o : d.classBodyDecls) {
                if (o.node instanceof ASTAspectMethodDeclaration
                    || o.node instanceof ASTAspectFieldDeclaration) {
                  if (!dest.hasClassBodyDecl(o.signature())) {
                    dest.classBodyDecls.add(new ClassBodyObject(o.node, o.fileName, o.line,
                          o.getAspectName()));
                  }
                } else if (o.node instanceof ASTAspectRefineMethodDeclaration) {
                  ClassBodyObject object = new ClassBodyObject(o.node, o.fileName, o.line,
                      o.getAspectName());
                  object.refinesAspect = o.refinesAspect;
                  dest.classBodyDecls.add(object);
                } else if (o.node instanceof ASTBlock) {
                  dest.classBodyDecls.add(new ClassBodyObject(o.node, o.fileName, o.line,
                      o.getAspectName()));
                }
              }
              for (ClassBodyObject o : d.refinedClassBodyDecls) {
                  if (o.node instanceof ASTAspectMethodDeclaration
                      || o.node instanceof ASTAspectFieldDeclaration) {
                    if (!dest.hasClassBodyDecl(o.signature())) {
                      dest.refinedClassBodyDecls.add(new ClassBodyObject(o.node, o.fileName,
                          o.line, o.getAspectName()));
                    }
                  } else if (o.node instanceof ASTAspectRefineMethodDeclaration) {
                    ClassBodyObject object = new ClassBodyObject(o.node, o.fileName,
                        o.line, o.getAspectName());
                    object.refinesAspect = o.refinesAspect;
                    dest.refinedClassBodyDecls.add(object);
                  } else if (o.node instanceof ASTBlock) {
                    dest.classBodyDecls.add(new ClassBodyObject(o.node, o.fileName,
                        o.line, o.getAspectName()));
                  }
              }

              for (int k = 0; k < d.getNumSynDecl(); k++) {
                dest.addSynDecl((SynDecl)d.getSynDecl(k).fullCopy());
              }

              for (int k = 0; k < d.getNumSynEq(); k++) {
                dest.addSynEq((SynEq)d.getSynEq(k).fullCopy());
              }

              for (int k = 0; k < d.getNumInhDecl(); k++) {
                dest.addInhDecl((InhDecl)d.getInhDecl(k).fullCopy());
              }

              for (int k = 0; k < d.getNumInhEq(); k++) {
                dest.addInhEq((InhEq)d.getInhEq(k).fullCopy());
              }
            }
          }
        }
      }
    }
  }
  /**
   * @aspect JastAddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JastAddCodeGen.jadd:118
   */
  public void jastAddGen(boolean publicModifier) {
    createPackageOutputDirectory();

    for (int i = 0; i < getNumTypeDecl(); i++) {
      getTypeDecl(i).jastAddGen(publicModifier);
    }
  }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:114
   */
  public String genImportsList() {
    Set imports = new LinkedHashSet();
    for (org.jastadd.jrag.AST.ASTCompilationUnit u : compilationUnits) {
      imports.addAll(Unparser.getImports(u));
    }
    StringBuilder buf = new StringBuilder();
    for (Iterator iter = imports.iterator(); iter.hasNext(); ) {
      buf.append(iter.next());
      buf.append('\n');
    }
    return buf.toString();
  }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:739
   */
  public Iterator inhAttrSet() {
    return inhEqMap().keySet().iterator();
  }
  /**
   * @aspect Configuration
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\Options.jrag:46
   */
  private Configuration configuration = null;
  /**
   * @aspect Configuration
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\Options.jrag:48
   */
  public void setConfiguration(Configuration config) {
    configuration = config;
  }
  /**
   * If needed create the directory, and parent directories, where the AST
   * classes Java source code is generated.
   * @aspect OutputDestination
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\Output.jrag:34
   */
  public void createPackageOutputDirectory() {

    File packageDir = packageDirectory();
    if (!packageDir.isDirectory()) {
      packageDir.mkdirs();
    }
  }
  /**
   * @aspect TemplateUtil
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\TemplateUtil.jrag:94
   */
  private void loadTemplates(TinyTemplate tt, String templateFile) {
    try {
      InputStream in = getClass().getResourceAsStream("/template/" + templateFile + ".tt");
      if (in == null) {
        System.err.println("WARNING: Could not load template file " + templateFile);
        return;
      }
      tt.loadTemplates(in);
      in.close();
    } catch (TemplateParser.SyntaxError e) {
      System.err.println("WARNING: Could not load template file " + templateFile);
      System.err.println(e.getMessage());
    } catch (IOException e) {
      System.err.println("WARNING: Could not load template file " + templateFile);
      System.err.println(e.getMessage());
    }
  }
  /**
   * Generates the class representing dynamic dependency graph (DDG) nodes.
   * @aspect IncrementalDDG
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalDDG.jadd:33
   */
  public void genIncrementalDDGNode(PrintWriter out) {
    if (!config().incremental()) return;
    TemplateContext tt = templateContext();
    tt.expand("Grammar.emitDDGNode", out);
  }
  /**
   * @declaredat ASTNode:1
   */
  public Grammar(int i) {
    super(i);
    is$Final(true);
  }
  /**
   * @declaredat ASTNode:6
   */
  public Grammar(Ast p, int i) {
    this(i);
    parser = p;
  }
  /**
   * @declaredat ASTNode:11
   */
  public Grammar() {
    this(0);
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:20
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
    setChild(new List(), 0);
    setChild(new List(), 1);
  }
  /**
   * @declaredat ASTNode:25
   */
  public Grammar(List<TypeDecl> p0, List<RegionDecl> p1) {
    setChild(p0, 0);
    setChild(p1, 1);
    is$Final(true);
  }
  /**
   * @declaredat ASTNode:30
   */
  public void dumpTree(String indent, java.io.PrintStream out) {
    out.print(indent + "Grammar");
    String childIndent = indent + "  ";
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).dumpTree(childIndent, out);
    }
  }
  /**
   * @declaredat ASTNode:37
   */
  public Object jjtAccept(AstVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
  /**
   * @declaredat ASTNode:40
   */
  public void jjtAddChild(Node n, int i) {
    checkChild(n, i);
    super.jjtAddChild(n, i);
  }
  /**
   * @declaredat ASTNode:44
   */
  public void checkChild(Node n, int i) {
      if (i == 0) {
        if (!(n instanceof List)) {
          throw new Error("Child number 0 of Grammar has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof TypeDecl)) {
            throw new Error("Child number " + k + " in TypeDeclList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of TypeDecl");
          }
        }
      }
      if (i == 1) {
        if (!(n instanceof List)) {
          throw new Error("Child number 1 of Grammar has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof RegionDecl)) {
            throw new Error("Child number " + k + " in RegionDeclList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of RegionDecl");
          }
        }
      }
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:71
   */
  @SideEffect.Pure public int getNumChild() {
    return 2;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:77
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:81
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    subclassMap_reset();
    roots_reset();
    missingInhEqs_reset();
    inhEqMap_reset();
    packageDirectory_reset();
    templateContext_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:91
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
    Grammar_problems_visited = -1;
    Grammar_problems_computed = false;
    
    Grammar_problems_value = null;
    contributorMap_Grammar_problems = null;
    contributorMap_CollDecl_uses = null;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:101
   */
  @SideEffect.Fresh public Grammar clone() throws CloneNotSupportedException {
    Grammar node = (Grammar) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:106
   */
  @SideEffect.Fresh(group="_ASTNode") public Grammar copy() {
    try {
      Grammar node = (Grammar) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:125
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Grammar fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:135
   */
  @SideEffect.Fresh(group="_ASTNode") public Grammar treeCopyNoTransform() {
    Grammar tree = (Grammar) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:155
   */
  @SideEffect.Fresh(group="_ASTNode") public Grammar treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:160
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the TypeDecl list.
   * @param list The new list node to be used as the TypeDecl list.
   * @apilevel high-level
   */
  public void setTypeDeclList(List<TypeDecl> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the TypeDecl list.
   * @return Number of children in the TypeDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumTypeDecl() {
    return getTypeDeclList().getNumChild();
  }
  /**
   * Retrieves the number of children in the TypeDecl list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the TypeDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumTypeDeclNoTransform() {
    return getTypeDeclListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the TypeDecl list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the TypeDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public TypeDecl getTypeDecl(int i) {
    return (TypeDecl) getTypeDeclList().getChild(i);
  }
  /**
   * Check whether the TypeDecl list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasTypeDecl() {
    return getTypeDeclList().getNumChild() != 0;
  }
  /**
   * Append an element to the TypeDecl list.
   * @param node The element to append to the TypeDecl list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addTypeDecl(TypeDecl node) {
    List<TypeDecl> list = (parent == null) ? getTypeDeclListNoTransform() : getTypeDeclList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addTypeDeclNoTransform(TypeDecl node) {
    List<TypeDecl> list = getTypeDeclListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the TypeDecl list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setTypeDecl(TypeDecl node, int i) {
    List<TypeDecl> list = getTypeDeclList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the TypeDecl list.
   * @return The node representing the TypeDecl list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="TypeDecl")
  @SideEffect.Pure(group="_ASTNode") public List<TypeDecl> getTypeDeclList() {
    List<TypeDecl> list = (List<TypeDecl>) getChild(0);
    return list;
  }
  /**
   * Retrieves the TypeDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the TypeDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<TypeDecl> getTypeDeclListNoTransform() {
    return (List<TypeDecl>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the TypeDecl list without
   * triggering rewrites.
   */
  @SideEffect.Pure public TypeDecl getTypeDeclNoTransform(int i) {
    return (TypeDecl) getTypeDeclListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the TypeDecl list.
   * @return The node representing the TypeDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<TypeDecl> getTypeDecls() {
    return getTypeDeclList();
  }
  /**
   * Retrieves the TypeDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the TypeDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<TypeDecl> getTypeDeclsNoTransform() {
    return getTypeDeclListNoTransform();
  }
  /**
   * Replaces the RegionDecl list.
   * @param list The new list node to be used as the RegionDecl list.
   * @apilevel high-level
   */
  public void setRegionDeclList(List<RegionDecl> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the RegionDecl list.
   * @return Number of children in the RegionDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumRegionDecl() {
    return getRegionDeclList().getNumChild();
  }
  /**
   * Retrieves the number of children in the RegionDecl list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the RegionDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumRegionDeclNoTransform() {
    return getRegionDeclListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the RegionDecl list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the RegionDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public RegionDecl getRegionDecl(int i) {
    return (RegionDecl) getRegionDeclList().getChild(i);
  }
  /**
   * Check whether the RegionDecl list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasRegionDecl() {
    return getRegionDeclList().getNumChild() != 0;
  }
  /**
   * Append an element to the RegionDecl list.
   * @param node The element to append to the RegionDecl list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addRegionDecl(RegionDecl node) {
    List<RegionDecl> list = (parent == null) ? getRegionDeclListNoTransform() : getRegionDeclList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addRegionDeclNoTransform(RegionDecl node) {
    List<RegionDecl> list = getRegionDeclListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the RegionDecl list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setRegionDecl(RegionDecl node, int i) {
    List<RegionDecl> list = getRegionDeclList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the RegionDecl list.
   * @return The node representing the RegionDecl list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="RegionDecl")
  @SideEffect.Pure(group="_ASTNode") public List<RegionDecl> getRegionDeclList() {
    List<RegionDecl> list = (List<RegionDecl>) getChild(1);
    return list;
  }
  /**
   * Retrieves the RegionDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the RegionDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<RegionDecl> getRegionDeclListNoTransform() {
    return (List<RegionDecl>) getChildNoTransform(1);
  }
  /**
   * @return the element at index {@code i} in the RegionDecl list without
   * triggering rewrites.
   */
  @SideEffect.Pure public RegionDecl getRegionDeclNoTransform(int i) {
    return (RegionDecl) getRegionDeclListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the RegionDecl list.
   * @return The node representing the RegionDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<RegionDecl> getRegionDecls() {
    return getRegionDeclList();
  }
  /**
   * Retrieves the RegionDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the RegionDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<RegionDecl> getRegionDeclsNoTransform() {
    return getRegionDeclListNoTransform();
  }
  /**
   * @aspect <NoAspect>
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTErrors.jrag:42
   */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map<ASTNode, java.util.Set<ASTNode>> contributorMap_Grammar_problems = null;

  @SideEffect.Ignore protected void survey_Grammar_problems() {
    if (contributorMap_Grammar_problems == null) {
      contributorMap_Grammar_problems = new java.util.IdentityHashMap<ASTNode, java.util.Set<ASTNode>>();
      collect_contributors_Grammar_problems(this, contributorMap_Grammar_problems);
    }
  }

  /**
   * @aspect <NoAspect>
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:515
   */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map<ASTNode, java.util.Set<ASTNode>> contributorMap_CollDecl_uses = null;

  @SideEffect.Ignore protected void survey_CollDecl_uses() {
    if (contributorMap_CollDecl_uses == null) {
      contributorMap_CollDecl_uses = new java.util.IdentityHashMap<ASTNode, java.util.Set<ASTNode>>();
      collect_contributors_CollDecl_uses(this, contributorMap_CollDecl_uses);
    }
  }

/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map lookup_String_visited = new java.util.HashMap(4);
  /**
   * @attribute syn
   * @aspect Lookup
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:32
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Lookup", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:32")
  @SideEffect.Pure(group="_ASTNode") public TypeDecl lookup(String name) {
    Object _parameters = name;
    if (Integer.valueOf(state().boundariesCrossed).equals(lookup_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute Grammar.lookup(String).");
    }
    lookup_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    try {
        for (int i = 0; i < getNumTypeDecl(); i++) {
          if (getTypeDecl(i).name().equals(name)) {
            return getTypeDecl(i);
          }
        }
        return null;
      }
    finally {
      lookup_String_visited.remove(_parameters);
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int attributeProblems_visited = -1;
  /**
   * Collects semantic problems for attributes.
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:44
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:44")
  @SideEffect.Pure(group="_ASTNode") public Collection<Problem> attributeProblems() {
    if (attributeProblems_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Grammar.attributeProblems().");
    }
    attributeProblems_visited = state().boundariesCrossed;
    try {
        Collection<Problem> problems = new LinkedList<Problem>();
        if (config().inhEqCheck()) {
          problems.addAll(missingInhEqProblems());
        }
        for (int i = 0; i < getNumTypeDecl(); i++) {
          problems.addAll(getTypeDecl(i).attributeProblems());
        }
        return problems;
      }
    finally {
      attributeProblems_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int missingInhEqProblems_visited = -1;
  /**
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:55
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:55")
  @SideEffect.Pure(group="_ASTNode") public Collection<Problem> missingInhEqProblems() {
    if (missingInhEqProblems_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Grammar.missingInhEqProblems().");
    }
    missingInhEqProblems_visited = state().boundariesCrossed;
    try {
        Collection<Problem> problems = new LinkedList<Problem>();
    
        for (InhDecl inhDecl: missingInhEqs()) {
    
          if (inhDecl.hostClass().isRootNode()) {
            // This is reported as an error - skip the warning!
            continue;
          }
    
          StringBuilder buf = new StringBuilder();
          buf.append("missing inherited equation for attribute " + inhDecl.name() + " ");
    
          boolean first1 = true;
          for (Map.Entry<ASTDecl, Map<ASTDecl, String>> e: inhDecl.missingEqs().entrySet()) {
            if (!first1) {
              buf.append(", and ");
            }
            first1 = false;
            buf.append("in class " + e.getKey().name() + " when being child of ");
    
            boolean first2 = true;
            for (Map.Entry<ASTDecl, String> parent: e.getValue().entrySet()) {
              if (!first2) {
                buf.append(", ");
              }
              first2 = false;
              buf.append(parent.getKey().name());
              buf.append(" (path: " + parent.getValue() + ")");
            }
          }
          problems.add(inhDecl.warning(buf.toString()));
        }
        return problems;
      }
    finally {
      missingInhEqProblems_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int subclassMap_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void subclassMap_reset() {
    subclassMap_computed = false;
    
    subclassMap_value = null;
    subclassMap_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean subclassMap_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Map<ASTDecl,Collection<ASTDecl>> subclassMap_value;

  /**
   * @attribute syn
   * @aspect Subclasses
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:89
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Subclasses", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:89")
  @SideEffect.Pure(group="_ASTNode") public Map<ASTDecl,Collection<ASTDecl>> subclassMap() {
    ASTState state = state();
    if (subclassMap_computed) {
      return subclassMap_value;
    }
    if (subclassMap_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Grammar.subclassMap().");
    }
    subclassMap_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    subclassMap_value = subclassMap_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    subclassMap_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    subclassMap_visited = -1;
    return subclassMap_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Map<ASTDecl,Collection<ASTDecl>> subclassMap_compute() {
      Map<ASTDecl,Collection<ASTDecl>> map = new HashMap<ASTDecl,Collection<ASTDecl>>();
      for (int j = 0; j < getNumTypeDecl(); j++) {
        if (getTypeDecl(j) instanceof ASTDecl) {
          ASTDecl decl = (ASTDecl) getTypeDecl(j);
          map.put(decl, new ArrayList<ASTDecl>());
        }
      }
      for (int j = 0; j < getNumTypeDecl(); j++) {
        if (getTypeDecl(j) instanceof ASTDecl) {
          ASTDecl decl = (ASTDecl) getTypeDecl(j);
          if (decl.superClass() != null) {
            map.get(decl.superClass()).add(decl);
          }
        }
      }
      return map;
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int parentMap_visited = -1;
  /**
   * @attribute syn
   * @aspect Parents
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:127
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Parents", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:127")
  @SideEffect.Pure(group="_ASTNode") public HashMap<TypeDecl, Collection<ASTDecl>> parentMap() {
    if (parentMap_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Grammar.parentMap().");
    }
    parentMap_visited = state().boundariesCrossed;
    try {
        HashMap<TypeDecl, Collection<ASTDecl>> map = new LinkedHashMap<TypeDecl, Collection<ASTDecl>>();
        for (int j = 0; j < getNumTypeDecl(); j++) {
          if (getTypeDecl(j) instanceof ASTDecl) {
            map.put(getTypeDecl(j), new LinkedHashSet<ASTDecl>());
          }
        }
        for (int j = 0; j < getNumTypeDecl(); j++) {
          if (getTypeDecl(j) instanceof ASTDecl) {
            ASTDecl decl = (ASTDecl) getTypeDecl(j);
            for (Component component : decl.components()) {
              if (!(component instanceof TokenComponent)) {
                TypeDecl t = lookup(component.type());
                if (t != null) {
                  map.get(t).add(decl);
                }
              }
            }
            for (SynthesizedNta component : decl.getSynthesizedNtaList()) {
              TypeDecl t = lookup(component.getType());
              if (t != null) {
                map.get(t).add(decl);
              }
            }
          }
        }
        return map;
      }
    finally {
      parentMap_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int roots_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void roots_reset() {
    roots_computed = false;
    
    roots_value = null;
    roots_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean roots_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Collection<ASTDecl> roots_value;

  /**
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:408
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:408")
  @SideEffect.Pure(group="_ASTNode") public Collection<ASTDecl> roots() {
    ASTState state = state();
    if (roots_computed) {
      return roots_value;
    }
    if (roots_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Grammar.roots().");
    }
    roots_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    roots_value = roots_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    roots_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    roots_visited = -1;
    return roots_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Collection<ASTDecl> roots_compute() {
      Collection<ASTDecl> roots = new HashSet<ASTDecl>();
      for (TypeDecl decl : getTypeDeclList()) {
        if (decl.isRootNode()) {
          roots.add((ASTDecl) decl);
        }
      }
      return roots;
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int root_visited = -1;
  /**
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:418
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:418")
  @SideEffect.Pure(group="_ASTNode") public ASTDecl root() {
    if (root_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Grammar.root().");
    }
    root_visited = state().boundariesCrossed;
    ASTDecl root_value = roots().isEmpty() ? null : roots().iterator().next();
    root_visited = -1;
    return root_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map lookupCollDecl_String_String_visited = new java.util.HashMap(4);
  /**
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:503
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:503")
  @SideEffect.Pure(group="_ASTNode") public CollDecl lookupCollDecl(String hostName, String collName) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(hostName);
    _parameters.add(collName);
    if (Integer.valueOf(state().boundariesCrossed).equals(lookupCollDecl_String_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute Grammar.lookupCollDecl(String,String).");
    }
    lookupCollDecl_String_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    try {
        TypeDecl typeDecl = lookup(hostName);
        if (typeDecl != null) {
          for (CollDecl decl : typeDecl.getCollDeclList()) {
            if (decl.getName().equals(collName)) {
              return decl;
            }
          }
        }
        return null;
      }
    finally {
      lookupCollDecl_String_String_visited.remove(_parameters);
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int missingInhEqs_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void missingInhEqs_reset() {
    missingInhEqs_computed = false;
    
    missingInhEqs_value = null;
    missingInhEqs_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean missingInhEqs_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Collection<InhDecl> missingInhEqs_value;

  /**
   * @return declarations of inherited attributes that lack an equation in the
   * tree.
   * @attribute syn
   * @aspect FindInheritedEquations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:127
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="FindInheritedEquations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:127")
  @SideEffect.Pure(group="_ASTNode") public Collection<InhDecl> missingInhEqs() {
    ASTState state = state();
    if (missingInhEqs_computed) {
      return missingInhEqs_value;
    }
    if (missingInhEqs_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Grammar.missingInhEqs().");
    }
    missingInhEqs_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    missingInhEqs_value = missingInhEqs_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    missingInhEqs_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    missingInhEqs_visited = -1;
    return missingInhEqs_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Collection<InhDecl> missingInhEqs_compute() {
      Collection<InhDecl> missing = new HashSet<InhDecl>();
      for (ASTDecl root: roots()) {
        missing.addAll(root.missingInhEqs());
      }
      return missing;
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int inhEqMap_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void inhEqMap_reset() {
    inhEqMap_computed = false;
    
    inhEqMap_value = null;
    inhEqMap_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean inhEqMap_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected HashMap inhEqMap_value;

  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:743
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:743")
  @SideEffect.Pure(group="_ASTNode") public HashMap inhEqMap() {
    ASTState state = state();
    if (inhEqMap_computed) {
      return inhEqMap_value;
    }
    if (inhEqMap_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Grammar.inhEqMap().");
    }
    inhEqMap_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    inhEqMap_value = inhEqMap_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    inhEqMap_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    inhEqMap_visited = -1;
    return inhEqMap_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private HashMap inhEqMap_compute() {
      HashMap map = new LinkedHashMap();
      for (int i = 0; i < getNumTypeDecl(); i++) {
        if (getTypeDecl(i) instanceof ASTDecl) {
          map.putAll(((ASTDecl)getTypeDecl(i)).inhEqMap());
        }
      }
      return map;
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int grammar_visited = -1;
  /**
   * @attribute syn
   * @aspect Configuration
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\Options.jrag:44
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Configuration", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\Options.jrag:44")
  @SideEffect.Pure(group="_ASTNode") public Grammar grammar() {
    if (grammar_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Grammar.grammar().");
    }
    grammar_visited = state().boundariesCrossed;
    Grammar grammar_value = this;
    grammar_visited = -1;
    return grammar_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int config_visited = -1;
  /**
   * @attribute syn
   * @aspect Configuration
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\Options.jrag:58
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Configuration", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\Options.jrag:58")
  @SideEffect.Pure(group="_ASTNode") public Configuration config() {
    if (config_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Grammar.config().");
    }
    config_visited = state().boundariesCrossed;
    try {
        if (configuration == null) {
          throw new Error("Configuration object not initialized!");
        }
        return configuration;
      }
    finally {
      config_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map targetJavaFile_String_visited = new java.util.HashMap(4);
  /**
   * @param typeName the name of the generated type
   * @return the target java file for a generated type
   * @attribute syn
   * @aspect OutputDestination
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\Output.jrag:46
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="OutputDestination", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\Output.jrag:46")
  @SideEffect.Pure(group="_ASTNode") public File targetJavaFile(String typeName) {
    Object _parameters = typeName;
    if (Integer.valueOf(state().boundariesCrossed).equals(targetJavaFile_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute Grammar.targetJavaFile(String).");
    }
    targetJavaFile_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    File targetJavaFile_String_value = new File(packageDirectory(), typeName + ".java");
    targetJavaFile_String_visited.remove(_parameters);
    return targetJavaFile_String_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int packageDirectory_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void packageDirectory_reset() {
    packageDirectory_computed = false;
    
    packageDirectory_value = null;
    packageDirectory_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean packageDirectory_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected File packageDirectory_value;

  /**
   * @return The output directory for the AST package.
   * @attribute syn
   * @aspect OutputDestination
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\Output.jrag:52
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="OutputDestination", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\Output.jrag:52")
  @SideEffect.Pure(group="_ASTNode") public File packageDirectory() {
    ASTState state = state();
    if (packageDirectory_computed) {
      return packageDirectory_value;
    }
    if (packageDirectory_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Grammar.packageDirectory().");
    }
    packageDirectory_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    packageDirectory_value = packageDirectory_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    packageDirectory_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    packageDirectory_visited = -1;
    return packageDirectory_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private File packageDirectory_compute() {
      String packageName = config().packageName();
      if (packageName.isEmpty()) {
        return config().outputDir();
      } else {
        String packagePath = packageName.replace('.', File.separatorChar);
        return new File(config().outputDir(), packagePath);
      }
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int templateContext_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void templateContext_reset() {
    templateContext_computed = false;
    
    templateContext_value = null;
    templateContext_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean templateContext_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected TemplateContext templateContext_value;

  /**
   * @attribute syn
   * @aspect TemplateUtil
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\TemplateUtil.jrag:37
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="TemplateUtil", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\TemplateUtil.jrag:37")
  @SideEffect.Pure(group="_ASTNode") public TemplateContext templateContext() {
    ASTState state = state();
    if (templateContext_computed) {
      return templateContext_value;
    }
    if (templateContext_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTNode.templateContext().");
    }
    templateContext_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    templateContext_value = templateContext_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    templateContext_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    templateContext_visited = -1;
    return templateContext_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private TemplateContext templateContext_compute() {
      TinyTemplate tt = new TinyTemplate();
      tt.setIndentation(config().indent);
      tt.throwExceptions(true);
  
      loadTemplates(tt, "ast/ASTNodeAnnotation"); 
      loadTemplates(tt, "ast/SideEffectAnnotation");
      loadTemplates(tt, "ast/ASTNode");
      loadTemplates(tt, "ast/Attributes");
      loadTemplates(tt, "ast/Circular");
      loadTemplates(tt, "ast/CircularNTA");
      loadTemplates(tt, "ast/Collections");
      loadTemplates(tt, "ast/Comments");
      loadTemplates(tt, "ast/CopyNode");
      loadTemplates(tt, "ast/ImplicitDeclarations");
      loadTemplates(tt, "ast/InheritedAttributes");
      loadTemplates(tt, "ast/JJTree");
      loadTemplates(tt, "ast/List");
      loadTemplates(tt, "ast/NodeConstructor");
      loadTemplates(tt, "ast/Rewrites");
      loadTemplates(tt, "ast/State");
  
      loadTemplates(tt, "ast/components/AggregateComponent");
      loadTemplates(tt, "ast/components/ListComponent");
      loadTemplates(tt, "ast/components/OptionalComponent");
      loadTemplates(tt, "ast/components/TokenComponent");
  
      loadTemplates(tt, "core/Synchronization");
      
      loadTemplates(tt, "flush/Flush");
  
      if (config().incremental()) {
        loadTemplates(tt, "incremental/DDGNodeCopy");
        loadTemplates(tt, "incremental/Debug");
        loadTemplates(tt, "incremental/Notification");
        loadTemplates(tt, "incremental/Regions");
        loadTemplates(tt, "incremental/Rewrites");
      }
      loadTemplates(tt, "incremental/ASTChange");
      loadTemplates(tt, "incremental/DDGNode");
      loadTemplates(tt, "incremental/NTAs");
      loadTemplates(tt, "incremental/State");
      loadTemplates(tt, "incremental/Tracking");
  
      loadTemplates(tt, "trace/Tracer");
      loadTemplates(tt, "trace/TraceHooks");
  
      return new SimpleContext(tt, this);
    }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:108
   * @apilevel internal
   */
 @SideEffect.Pure public Collection<ASTDecl> Define_findSubclasses(ASTNode _callerNode, ASTNode _childNode, ASTDecl target) {
    if (_callerNode == getTypeDeclListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:112
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      {
          return subclassMap().get(target);
        }
    }
    else {
      return getParent().Define_findSubclasses(this, _callerNode, target);
    }
  }
  @SideEffect.Pure protected boolean canDefine_findSubclasses(ASTNode _callerNode, ASTNode _childNode, ASTDecl target) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:137
   * @apilevel internal
   */
 @SideEffect.Pure public RegionDecl Define_lookupRegionDecl(ASTNode _callerNode, ASTNode _childNode, String name) {
    if (_callerNode == getTypeDeclListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:138
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      {
          for (int i = 0; i < getNumRegionDecl(); i++) {
            RegionDecl decl = getRegionDecl(i);
            if (decl.name().equals(name)) {
              return decl;
            }
          }
          return null;
        }
    }
    else {
      return getParent().Define_lookupRegionDecl(this, _callerNode, name);
    }
  }
  @SideEffect.Pure protected boolean canDefine_lookupRegionDecl(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\Options.jrag:42
   * @apilevel internal
   */
 @SideEffect.Pure public Grammar Define_grammar(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return this;
  }
  @SideEffect.Pure protected boolean canDefine_grammar(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\Options.jrag:56
   * @apilevel internal
   */
 @SideEffect.Pure public Configuration Define_config(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return config();
  }
  @SideEffect.Pure protected boolean canDefine_config(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\TemplateUtil.jrag:33
   * @apilevel internal
   */
 @SideEffect.Pure public TemplateContext Define_parentContext(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return templateContext();
  }
  @SideEffect.Pure protected boolean canDefine_parentContext(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int Grammar_problems_visited = -1;
  /**
   * Collects problems in the AST.
   * @return A collection of problems in the AST
   * @attribute coll
   * @aspect ASTErrors
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTErrors.jrag:42
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.COLL)
  @ASTNodeAnnotation.Source(aspect="ASTErrors", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTErrors.jrag:42")
  @SideEffect.Pure(group="_ASTNode") public Collection<Problem> problems() {
    ASTState state = state();
    if (Grammar_problems_computed) {
      return Grammar_problems_value;
    }
    if (Grammar_problems_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Grammar.problems().");
    }
    Grammar_problems_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    Grammar_problems_value = problems_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    Grammar_problems_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    Grammar_problems_visited = -1;
    return Grammar_problems_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure(group="_ASTNode") private Collection<Problem> problems_compute() {
    ASTNode node = this;
    while (node != null && !(node instanceof Grammar)) {
      node = node.getParent();
    }
    Grammar root = (Grammar) node;
    root.survey_Grammar_problems();
    Collection<Problem> _computedValue = new LinkedList<Problem>();
    if (root.contributorMap_Grammar_problems.containsKey(this)) {
      for (ASTNode contributor : root.contributorMap_Grammar_problems.get(this)) {
        contributor.contributeTo_Grammar_problems(_computedValue);
      }
    }
    return _computedValue;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean Grammar_problems_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Collection<Problem> Grammar_problems_value;

  @SideEffect.Local(group="_ASTNode") protected void collect_contributors_Grammar_problems(Grammar _root, @SideEffect.Local java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTErrors.jrag:55
    if (roots().isEmpty()) {
      {
        Grammar target = (Grammar) (this);
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_Grammar_problems(_root, _map);
  }
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_Grammar_problems(Collection<Problem> collection) {
    super.contributeTo_Grammar_problems(collection);
    if (roots().isEmpty()) {
      collection.add(Problem.builder()
                .message("there is no root node in the grammar!")
                .buildWarning());
    }
  }
}
