package org.jastadd.ast.AST;

import org.jastadd.ast.AST.*;
import java.util.*;
import org.jastadd.Problem;
import org.jastadd.jrag.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jastadd.jrag.AST.ASTExpression;
import org.jastadd.jrag.Unparser;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.io.PrintStream;
import java.io.*;
import java.util.regex.*;
import org.jastadd.JastAdd;
import org.jastadd.jrag.AST.ASTAspectMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectRefineMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectFieldDeclaration;
import org.jastadd.jrag.AST.ASTBlock;
import org.jastadd.jrag.ClassBodyObject;
import org.jastadd.jrag.AST.ASTCompilationUnit;
import org.jastadd.ast.AST.Token;
import org.jastadd.ast.AST.SimpleNode;
import org.jastadd.Configuration;
import org.jastadd.tinytemplate.*;
/**
 * @ast class
 * @declaredat ASTNode:297
 */
public class ASTNodeAnnotation extends java.lang.Object {
  
  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
  @java.lang.annotation.Target(java.lang.annotation.ElementType.METHOD)
  @java.lang.annotation.Documented
  public @interface Child {
    String name();
  }

  
  
  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
  @java.lang.annotation.Target(java.lang.annotation.ElementType.METHOD)
  @java.lang.annotation.Documented
  public @interface ListChild {
    String name();
  }

  
  
  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
  @java.lang.annotation.Target(java.lang.annotation.ElementType.METHOD)
  @java.lang.annotation.Documented
  public @interface OptChild {
    String name();
  }

  
  
  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
  @java.lang.annotation.Target(java.lang.annotation.ElementType.METHOD)
  @java.lang.annotation.Documented
  public @interface Token {
    String name();
  }

  
  
  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
  @java.lang.annotation.Target(java.lang.annotation.ElementType.METHOD)
  @java.lang.annotation.Documented
  public @interface Attribute {
    Kind kind();
    boolean isCircular() default false;
    boolean isNTA() default false;
  }

  
  public enum Kind { SYN, INH, COLL }

  

  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
  @java.lang.annotation.Target(java.lang.annotation.ElementType.METHOD)
  @java.lang.annotation.Documented
  public @interface Source {
    String aspect() default "";
    String declaredAt() default "";
  }


}
