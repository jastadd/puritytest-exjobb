/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package org.jastadd.ast.AST;
import org.jastadd.ast.AST.*;
import java.util.*;
import org.jastadd.Problem;
import org.jastadd.jrag.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jastadd.jrag.AST.ASTExpression;
import org.jastadd.jrag.Unparser;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.io.PrintStream;
import java.io.*;
import java.util.regex.*;
import org.jastadd.JastAdd;
import org.jastadd.jrag.AST.ASTAspectMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectRefineMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectFieldDeclaration;
import org.jastadd.jrag.AST.ASTBlock;
import org.jastadd.jrag.ClassBodyObject;
import org.jastadd.jrag.AST.ASTCompilationUnit;
import org.jastadd.ast.AST.Token;
import org.jastadd.ast.AST.SimpleNode;
import org.jastadd.Configuration;
import org.jastadd.tinytemplate.*;
/**
 * @ast node
 * @production List : {@link ASTNode};

 */
@SideEffect.Entity public class List<T extends ASTNode> extends ASTNode<T> implements Cloneable, Iterable<T> {
  /**
   * @declaredat ASTNode:1
   */
  public List(int i) {
    super(i);
  }
  /**
   * @declaredat ASTNode:5
   */
  public List(Ast p, int i) {
    this(i);
    parser = p;
  }
  /**
   * @declaredat ASTNode:10
   */
  public List() {
    this(0);
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:19
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /**
   * @declaredat ASTNode:21
   */
  public List(T... initialChildren) {
    children = new ASTNode[initialChildren.length];
    for (int i = 0; i < children.length; ++i) {
      addChild(initialChildren[i]);
    }
  }
  /**
   * @declaredat ASTNode:27
   */
  public void dumpTree(String indent, java.io.PrintStream out) {
    out.print(indent + "List");
    String childIndent = indent + "  ";
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).dumpTree(childIndent, out);
    }
  }
  /**
   * @declaredat ASTNode:34
   */
  public Object jjtAccept(AstVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
  /**
   * @declaredat ASTNode:37
   */
  public void jjtAddChild(Node n, int i) {
    checkChild(n, i);
    super.jjtAddChild(n, i);
  }
  /**
   * @declaredat ASTNode:41
   */
  public void checkChild(Node n, int i) {
    if (!(n instanceof ASTNode)) {
      throw new Error("The node type of child " + i + " must be an instance of ASTNode");
    }
  }
  /**
   * @declaredat ASTNode:47
   */
  private boolean list$touched = true;
  /**
   * @declaredat ASTNode:49
   */
  @SideEffect.Local public List<T> add(T node) {
    addChild(node);
    return this;
  }
  /**
   * @declaredat ASTNode:54
   */
  @SideEffect.Local public List<T> addAll(Iterable<? extends T> c) {
    for (T node : c) {
      addChild(node);
    }
    return this;
  }
  /**
   * @declaredat ASTNode:61
   */
  @SideEffect.Ignore @SideEffect.Local public void insertChild(ASTNode node, int i) {
    list$touched = true;
    super.insertChild(node, i);
  }
  /**
   * @declaredat ASTNode:66
   */
  @SideEffect.Local public void addChild(T node) {
    list$touched = true;
    super.addChild(node);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:72
   */
  @SideEffect.Ignore @SideEffect.Local public void removeChild(int i) {
    list$touched = true;
    super.removeChild(i);
  }
  /**
   * @declaredat ASTNode:77
   */
  @SideEffect.Pure public int getNumChild() {
    if (list$touched) {
      for (int i = 0; i < getNumChildNoTransform(); i++) {
        getChild(i);
      }
      list$touched = false;
    }
    return getNumChildNoTransform();
  }
  /** @return an iterator to iterate over elements in this list node. 
   * @declaredat ASTNode:88
   */
  @Override
  @SideEffect.Ignore public java.util.Iterator<T> iterator() {
    return astChildIterator();
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:95
   */
  public boolean mayHaveRewrite() {
    return true;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:99
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:103
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:107
   */
  @SideEffect.Fresh public List<T> clone() throws CloneNotSupportedException {
    List node = (List) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:112
   */
  @SideEffect.Fresh(group="_ASTNode") public List<T> copy() {
    try {
      List node = (List) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:131
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public List<T> fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:141
   */
  @SideEffect.Fresh(group="_ASTNode") public List<T> treeCopyNoTransform() {
    List tree = (List) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:161
   */
  @SideEffect.Fresh(group="_ASTNode") public List<T> treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:166
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    if (list$touched) {
      for(int i = 0 ; i < getNumChildNoTransform(); i++) {
        getChild(i);
      }
      list$touched = false;
      return this;
    }
    return super.rewriteTo();
  }
}
