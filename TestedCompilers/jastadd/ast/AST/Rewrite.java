/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package org.jastadd.ast.AST;
import org.jastadd.ast.AST.*;
import java.util.*;
import org.jastadd.Problem;
import org.jastadd.jrag.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jastadd.jrag.AST.ASTExpression;
import org.jastadd.jrag.Unparser;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.io.PrintStream;
import java.io.*;
import java.util.regex.*;
import org.jastadd.JastAdd;
import org.jastadd.jrag.AST.ASTAspectMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectRefineMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectFieldDeclaration;
import org.jastadd.jrag.AST.ASTBlock;
import org.jastadd.jrag.ClassBodyObject;
import org.jastadd.jrag.AST.ASTCompilationUnit;
import org.jastadd.ast.AST.Token;
import org.jastadd.ast.AST.SimpleNode;
import org.jastadd.Configuration;
import org.jastadd.tinytemplate.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Ast.ast:59
 * @production Rewrite : {@link ASTNode} ::= <span class="component">&lt;FileName:String&gt;</span> <span class="component">&lt;StartLine:int&gt;</span> <span class="component">&lt;EndLine:int&gt;</span> <span class="component">&lt;AspectName:String&gt;</span>;

 */
public class Rewrite extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:1188
   */
  public org.jastadd.jrag.AST.SimpleNode condition;
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:1190
   */
  public org.jastadd.jrag.AST.SimpleNode getCondition() {
    return condition;
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:1194
   */
  public void setCondition(org.jastadd.jrag.AST.SimpleNode c) {
    condition = c;
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:1198
   */
  public org.jastadd.jrag.AST.SimpleNode result;
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:1200
   */
  public org.jastadd.jrag.AST.SimpleNode getResult() {
    return result;
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:1204
   */
  public void setResult(org.jastadd.jrag.AST.SimpleNode r) {
    result = r;
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:1208
   */
  public String returnType;
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:1210
   */
  public String getReturnType() {
    return returnType;
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:1214
   */
  public void setReturnType(String type) {
    returnType = type;
  }
  /**
   * @aspect Rewrites
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Rewrites.jrag:79
   */
  public boolean genRewrite(PrintStream out, int index) {
    TemplateContext tt = templateContext();
    tt.bind("RewriteIndex", "" + index);
    tt.expand("Rewrite.declaredat", out);
    if (getCondition() != null) {
      tt.bind("Condition", Unparser.unparse(getCondition()));
      tt.expand("Rewrite.genRewrite:conditional", out);
      return false;
    } else {
      tt.expand("Rewrite.genRewrite:unconditional", out);
      return true;
    }
  }
  /**
   * @aspect Rewrites
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Rewrites.jrag:105
   */
  public void genRewriteCondition(PrintStream out, int index) {
    TemplateContext tt = templateContext();
    tt.bind("RewriteIndex", "" + index);
    tt.expand("Rewrite.declaredat", out);
    if (getCondition() != null) {
      tt.bind("Condition", Unparser.unparse(getCondition()));
      tt.expand("Rewrite.condition", out);
    }
  }
  /**
   * @aspect Rewrites
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Rewrites.jrag:115
   */
  public void genRewritesExtra(PrintStream out, int index) {
    String ind = config().indent;
    String ind2 = config().ind(2);
    if (getResult() instanceof org.jastadd.jrag.AST.ASTBlock) {
      templateContext().expand("Rewrite.javaDoc:internal", out);
      out.println();
      out.println(ind + "@SideEffect.Fresh private " + getReturnType() + " rewriteRule" + index + "() {");
      out.print(Unparser.unparse(getResult()));
      out.println(ind + "}");
    } else {
      templateContext().expand("Rewrite.javaDoc:internal", out);
      out.println(ind + "@SideEffect.Fresh private " + getReturnType() + " rewriteRule" + index + "() {");
      out.println(ind2 + "return " + Unparser.unparse(getResult()) + ";");
      out.println(ind + "}");
    }
  }
  /**
   * @declaredat ASTNode:1
   */
  public Rewrite(int i) {
    super(i);
  }
  /**
   * @declaredat ASTNode:5
   */
  public Rewrite(Ast p, int i) {
    this(i);
    parser = p;
  }
  /**
   * @declaredat ASTNode:10
   */
  public Rewrite() {
    this(0);
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:19
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /**
   * @declaredat ASTNode:21
   */
  public Rewrite(String p0, int p1, int p2, String p3) {
    setFileName(p0);
    setStartLine(p1);
    setEndLine(p2);
    setAspectName(p3);
  }
  /**
   * @declaredat ASTNode:27
   */
  public void dumpTree(String indent, java.io.PrintStream out) {
    out.print(indent + "Rewrite");
    out.print("\"" + getFileName() + "\"");
    out.print("\"" + getStartLine() + "\"");
    out.print("\"" + getEndLine() + "\"");
    out.print("\"" + getAspectName() + "\"");
    String childIndent = indent + "  ";
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).dumpTree(childIndent, out);
    }
  }
  /**
   * @declaredat ASTNode:38
   */
  public Object jjtAccept(AstVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
  /**
   * @declaredat ASTNode:41
   */
  public void jjtAddChild(Node n, int i) {
    checkChild(n, i);
    super.jjtAddChild(n, i);
  }
  /**
   * @declaredat ASTNode:45
   */
  public void checkChild(Node n, int i) {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:48
   */
  @SideEffect.Pure public int getNumChild() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:54
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:58
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    aspectName_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:63
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:67
   */
  @SideEffect.Fresh public Rewrite clone() throws CloneNotSupportedException {
    Rewrite node = (Rewrite) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:72
   */
  @SideEffect.Fresh(group="_ASTNode") public Rewrite copy() {
    try {
      Rewrite node = (Rewrite) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:91
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Rewrite fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:101
   */
  @SideEffect.Fresh(group="_ASTNode") public Rewrite treeCopyNoTransform() {
    Rewrite tree = (Rewrite) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:121
   */
  @SideEffect.Fresh(group="_ASTNode") public Rewrite treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:126
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_FileName == ((Rewrite) node).tokenString_FileName) && (tokenint_StartLine == ((Rewrite) node).tokenint_StartLine) && (tokenint_EndLine == ((Rewrite) node).tokenint_EndLine) && (tokenString_AspectName == ((Rewrite) node).tokenString_AspectName);    
  }
  /**
   * Replaces the lexeme FileName.
   * @param value The new value for the lexeme FileName.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setFileName(String value) {
    tokenString_FileName = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_FileName;
  /**
   * Retrieves the value for the lexeme FileName.
   * @return The value for the lexeme FileName.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="FileName")
  @SideEffect.Pure(group="_ASTNode") public String getFileName() {
    return tokenString_FileName != null ? tokenString_FileName : "";
  }
  /**
   * Replaces the lexeme StartLine.
   * @param value The new value for the lexeme StartLine.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setStartLine(int value) {
    tokenint_StartLine = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected int tokenint_StartLine;
  /**
   * Retrieves the value for the lexeme StartLine.
   * @return The value for the lexeme StartLine.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="StartLine")
  @SideEffect.Pure(group="_ASTNode") public int getStartLine() {
    return tokenint_StartLine;
  }
  /**
   * Replaces the lexeme EndLine.
   * @param value The new value for the lexeme EndLine.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setEndLine(int value) {
    tokenint_EndLine = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected int tokenint_EndLine;
  /**
   * Retrieves the value for the lexeme EndLine.
   * @return The value for the lexeme EndLine.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="EndLine")
  @SideEffect.Pure(group="_ASTNode") public int getEndLine() {
    return tokenint_EndLine;
  }
  /**
   * Replaces the lexeme AspectName.
   * @param value The new value for the lexeme AspectName.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setAspectName(String value) {
    tokenString_AspectName = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_AspectName;
  /**
   * Retrieves the value for the lexeme AspectName.
   * @return The value for the lexeme AspectName.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="AspectName")
  @SideEffect.Pure(group="_ASTNode") public String getAspectName() {
    return tokenString_AspectName != null ? tokenString_AspectName : "";
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map error_String_visited = new java.util.HashMap(4);
  /**
   * Create a new error object with relevant file name and line number.
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:146
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:146")
  @SideEffect.Pure(group="_ASTNode") public Problem error(String message) {
    Object _parameters = message;
    if (Integer.valueOf(state().boundariesCrossed).equals(error_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute Rewrite.error(String).");
    }
    error_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    Problem error_String_value = Problem.builder()
              .message(message)
              .sourceFile(getFileName())
              .sourceLine(getStartLine())
              .buildError();
    error_String_visited.remove(_parameters);
    return error_String_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map warning_String_visited = new java.util.HashMap(4);
  /** Create a new warning with the relevant file name and line number. 
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:174
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:174")
  @SideEffect.Pure(group="_ASTNode") public Problem warning(String message) {
    Object _parameters = message;
    if (Integer.valueOf(state().boundariesCrossed).equals(warning_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute Rewrite.warning(String).");
    }
    warning_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    Problem warning_String_value = Problem.builder()
              .message(message)
              .sourceFile(getFileName())
              .sourceLine(getStartLine())
              .buildWarning();
    warning_String_visited.remove(_parameters);
    return warning_String_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int getFileNameEscaped_visited = -1;
  /**
   * @attribute syn
   * @aspect FileNameEscape
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\FileNameEscape.jrag:51
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="FileNameEscape", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\FileNameEscape.jrag:51")
  @SideEffect.Pure(group="_ASTNode") public String getFileNameEscaped() {
    if (getFileNameEscaped_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Rewrite.getFileNameEscaped().");
    }
    getFileNameEscaped_visited = state().boundariesCrossed;
    String getFileNameEscaped_value = escapedFileName(getFileName());
    getFileNameEscaped_visited = -1;
    return getFileNameEscaped_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int declaredat_visited = -1;
  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:84
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:84")
  @SideEffect.Pure(group="_ASTNode") public String declaredat() {
    if (declaredat_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Rewrite.declaredat().");
    }
    declaredat_visited = state().boundariesCrossed;
    String declaredat_value = ASTNode.declaredat(getFileName(), getStartLine());
    declaredat_visited = -1;
    return declaredat_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int sourceLocation_visited = -1;
  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:86
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:86")
  @SideEffect.Pure(group="_ASTNode") public String sourceLocation() {
    if (sourceLocation_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Rewrite.sourceLocation().");
    }
    sourceLocation_visited = state().boundariesCrossed;
    String sourceLocation_value = ASTNode.sourceLocation(getFileName(), getStartLine());
    sourceLocation_visited = -1;
    return sourceLocation_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int aspectName_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void aspectName_reset() {
    aspectName_computed = false;
    
    aspectName_value = null;
    aspectName_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean aspectName_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected String aspectName_value;

  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:753
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:753")
  @SideEffect.Pure(group="_ASTNode") public String aspectName() {
    ASTState state = state();
    if (aspectName_computed) {
      return aspectName_value;
    }
    if (aspectName_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Rewrite.aspectName().");
    }
    aspectName_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    aspectName_value = getAspectName();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    aspectName_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    aspectName_visited = -1;
    return aspectName_value;
  }
  /**
   * @attribute inh
   * @aspect Rewrites
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Rewrites.jrag:207
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="Rewrites", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Rewrites.jrag:207")
  @SideEffect.Pure(group="_ASTNode") public TypeDecl hostClass() {
    if (hostClass_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Rewrite.hostClass().");
    }
    hostClass_visited = state().boundariesCrossed;
    TypeDecl hostClass_value = getParent().Define_hostClass(this, null);
    hostClass_visited = -1;
    return hostClass_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int hostClass_visited = -1;
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
