package org.jastadd.ast.AST;

import org.jastadd.ast.AST.*;
import java.util.*;
import org.jastadd.Problem;
import org.jastadd.jrag.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jastadd.jrag.AST.ASTExpression;
import org.jastadd.jrag.Unparser;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.io.PrintStream;
import java.io.*;
import java.util.regex.*;
import org.jastadd.JastAdd;
import org.jastadd.jrag.AST.ASTAspectMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectRefineMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectFieldDeclaration;
import org.jastadd.jrag.AST.ASTBlock;
import org.jastadd.jrag.ClassBodyObject;
import org.jastadd.jrag.AST.ASTCompilationUnit;
import org.jastadd.ast.AST.Token;
import org.jastadd.ast.AST.SimpleNode;
import org.jastadd.Configuration;
import org.jastadd.tinytemplate.*;
/**
 * @ast class
 * @aspect SimpleNode
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\SimpleNode.jadd:5
 */
 class SimpleNode extends java.lang.Object implements Node {
  
    protected Node parent;

  
    protected Node[] children;

  
    protected int id;

  
    protected Ast parser;

  

    public SimpleNode(int i) {
      id = i;
    }

  

    public SimpleNode(Ast p, int i) {
      this(i);
      parser = p;
    }

  

    public void jjtOpen() {
    }

  

    public void jjtClose() {
    }

  
  
    public void jjtSetParent(Node n) { parent = n; }

  

    public Node jjtGetParent() { return parent; }

  

    public void jjtAddChild(Node n, int i) {
      if (children == null) {
        children = new Node[i + 1];
      } else if (i >= children.length) {
        Node c[] = new Node[i + 1];
        System.arraycopy(children, 0, c, 0, children.length);
        children = c;
      }
      children[i] = n;
    }

  

    public Node jjtGetChild(int i) {
      return children[i];
    }

  

    public int jjtGetNumChildren() {
      return (children == null) ? 0 : children.length;
    }

  

    /** Accept the visitor. */
    public Object jjtAccept(AstVisitor visitor, Object data) {
      return visitor.visit(this, data);
    }

  

    /** Accept the visitor. */
    public Object childrenAccept(AstVisitor visitor, Object data) {
      if (children != null) {
        for (int i = 0; i < children.length; ++i) {
          children[i].jjtAccept(visitor, data);
        }
      }
      return data;
    }

  

    /* You can override these two methods in subclasses of SimpleNode to
     * customize the way the node appears when the tree is dumped.  If
     * your output uses more than one line you should override
     * toString(String), otherwise overriding toString() is probably all
     * you need to do.
     */
    public String toString() { return AstTreeConstants.jjtNodeName[id]; }

  

    public String toString(String prefix) { return prefix + toString(); }

  

    /* Override this method if you want to customize how the node dumps
     * out its children.
     */
    public void dump(String prefix) {
      System.out.println(toString(prefix));
      if (children != null) {
        for (int i = 0; i < children.length; ++i) {
	        SimpleNode n = (SimpleNode)children[i];
	        if (n != null) {
	          n.dump(prefix + " ");
	        }
        }
      }
    }

  public Token firstToken;

  public Token lastToken;

  public void unparseComment(StringBuffer buf) {
    Token tt = firstToken.specialToken;
    if (tt != null) {
      while (tt.specialToken != null) tt = tt.specialToken;
      while (tt != null) {
        buf.append(addUnicodeEscapes(tt.image));
        tt = tt.next;
      }
    }
  }

  public String unparseComment() {
    StringBuffer buf = new StringBuffer();
    unparseComment(buf);
    return buf.toString();
  }

  private String addUnicodeEscapes(String str) {
    String retval = "";
    char ch;
    for (int i = 0; i < str.length(); i++) {
      ch = str.charAt(i);
      if ((ch < 0x20 || ch > 0x7e)
          && ch != '\t' && ch != '\n' && ch != '\r' && ch != '\f') {
        String s = "0000" + Integer.toString(ch, 16);
        retval += "\\u" + s.substring(s.length() - 4, s.length());
      } else {
        retval += ch;
      }
    }
    return retval;
  }


}
