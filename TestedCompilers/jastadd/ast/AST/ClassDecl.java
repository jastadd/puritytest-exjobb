/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package org.jastadd.ast.AST;
import org.jastadd.ast.AST.*;
import java.util.*;
import org.jastadd.Problem;
import org.jastadd.jrag.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jastadd.jrag.AST.ASTExpression;
import org.jastadd.jrag.Unparser;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.io.PrintStream;
import java.io.*;
import java.util.regex.*;
import org.jastadd.JastAdd;
import org.jastadd.jrag.AST.ASTAspectMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectRefineMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectFieldDeclaration;
import org.jastadd.jrag.AST.ASTBlock;
import org.jastadd.jrag.ClassBodyObject;
import org.jastadd.jrag.AST.ASTCompilationUnit;
import org.jastadd.ast.AST.Token;
import org.jastadd.ast.AST.SimpleNode;
import org.jastadd.Configuration;
import org.jastadd.tinytemplate.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Ast.ast:10
 * @production ClassDecl : {@link TypeDecl};

 */
public class ClassDecl extends TypeDecl implements Cloneable {
  /**
   * @aspect ComponentHostClass
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:130
   */
  public String extendsName = "java.lang.Object";
  /**
   * @aspect JastAddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JastAddCodeGen.jadd:142
   */
  public String typeDeclarationString() {
    String interfaces = interfacesString();
    if (interfaces.isEmpty()) {
      return String.format("%sclass %s%s extends %s {", modifiers(), getIdDecl().getID(),
          typeParameters, extendsName);
    } else {
      return String.format("%sclass %s%s extends %s implements %s {",
          modifiers(), getIdDecl().getID(), typeParameters, extendsName, interfaces);
    }
  }
  /**
   * @aspect JastAddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JastAddCodeGen.jadd:273
   */
  public void jastAddGen(boolean publicModifier) {
    File file = grammar().targetJavaFile(name());
    try {
      PrintStream stream = new PrintStream(new FileOutputStream(file));
      if ( !config().license.isEmpty()) {
        stream.println(config().license);
      }

      // TODO(joqvist): move to template.
      if (!config().packageName().isEmpty()) {
        stream.println("package " + config().packageName() + ";\n");
      }

      stream.print(grammar().genImportsList());
      stream.println(docComment());
      stream.println(typeDeclarationString());
      StringBuffer buf = new StringBuffer();
      for (ClassBodyObject obj : classBodyDecls) {
        org.jastadd.jrag.AST.SimpleNode n = obj.node;
        buf.append(config().indent);
        buf.append(obj.modifiers());
        n.jjtAccept(new ClassBodyDeclUnparser(), buf);
        buf.append("\n\n");
      }
      stream.println(buf.toString());

      emitAbstractSyns(stream);
      emitInhDeclarations(stream);

      stream.println("}");
      stream.close();
    } catch (FileNotFoundException f) {
      System.err.format("Could not create file %s in %s%n", file.getName(), file.getParent());
      System.exit(1);
    }
  }
  /**
   * @declaredat ASTNode:1
   */
  public ClassDecl(int i) {
    super(i);
  }
  /**
   * @declaredat ASTNode:5
   */
  public ClassDecl(Ast p, int i) {
    this(i);
    parser = p;
  }
  /**
   * @declaredat ASTNode:10
   */
  public ClassDecl() {
    this(0);
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:19
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[8];
    setChild(new List(), 1);
    setChild(new List(), 2);
    setChild(new List(), 3);
    setChild(new List(), 4);
    setChild(new List(), 5);
    setChild(new List(), 6);
    setChild(new List(), 7);
  }
  /**
   * @declaredat ASTNode:29
   */
  public ClassDecl(IdDecl p0, List<ClassBodyDecl> p1, List<SynDecl> p2, List<SynEq> p3, List<InhDecl> p4, List<InhEq> p5, List<Component> p6, List<CollDecl> p7, String p8, int p9, int p10, String p11, String p12) {
    setChild(p0, 0);
    setChild(p1, 1);
    setChild(p2, 2);
    setChild(p3, 3);
    setChild(p4, 4);
    setChild(p5, 5);
    setChild(p6, 6);
    setChild(p7, 7);
    setFileName(p8);
    setStartLine(p9);
    setEndLine(p10);
    setComment(p11);
    setAspectName(p12);
  }
  /**
   * @declaredat ASTNode:44
   */
  public void dumpTree(String indent, java.io.PrintStream out) {
    out.print(indent + "ClassDecl");
    out.print("\"" + getFileName() + "\"");
    out.print("\"" + getStartLine() + "\"");
    out.print("\"" + getEndLine() + "\"");
    out.print("\"" + getComment() + "\"");
    out.print("\"" + getAspectName() + "\"");
    String childIndent = indent + "  ";
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).dumpTree(childIndent, out);
    }
  }
  /**
   * @declaredat ASTNode:56
   */
  public Object jjtAccept(AstVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
  /**
   * @declaredat ASTNode:59
   */
  public void jjtAddChild(Node n, int i) {
    checkChild(n, i);
    super.jjtAddChild(n, i);
  }
  /**
   * @declaredat ASTNode:63
   */
  public void checkChild(Node n, int i) {
    if (i == 0 && !(n instanceof IdDecl)) {
     throw new Error("Child number 0 of TypeDecl has the type " +
       n.getClass().getName() + " which is not an instance of IdDecl");
    }
      if (i == 1) {
        if (!(n instanceof List)) {
          throw new Error("Child number 1 of TypeDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof ClassBodyDecl)) {
            throw new Error("Child number " + k + " in ClassBodyDeclList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of ClassBodyDecl");
          }
        }
      }
      if (i == 2) {
        if (!(n instanceof List)) {
          throw new Error("Child number 2 of TypeDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof SynDecl)) {
            throw new Error("Child number " + k + " in SynDeclList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of SynDecl");
          }
        }
      }
      if (i == 3) {
        if (!(n instanceof List)) {
          throw new Error("Child number 3 of TypeDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof SynEq)) {
            throw new Error("Child number " + k + " in SynEqList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of SynEq");
          }
        }
      }
      if (i == 4) {
        if (!(n instanceof List)) {
          throw new Error("Child number 4 of TypeDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof InhDecl)) {
            throw new Error("Child number " + k + " in InhDeclList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of InhDecl");
          }
        }
      }
      if (i == 5) {
        if (!(n instanceof List)) {
          throw new Error("Child number 5 of TypeDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof InhEq)) {
            throw new Error("Child number " + k + " in InhEqList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of InhEq");
          }
        }
      }
      if (i == 6) {
        if (!(n instanceof List)) {
          throw new Error("Child number 6 of TypeDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof Component)) {
            throw new Error("Child number " + k + " in ComponentList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of Component");
          }
        }
      }
      if (i == 7) {
        if (!(n instanceof List)) {
          throw new Error("Child number 7 of TypeDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof CollDecl)) {
            throw new Error("Child number " + k + " in CollDeclList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of CollDecl");
          }
        }
      }
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:154
   */
  @SideEffect.Pure public int getNumChild() {
    return 8;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:160
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:164
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:168
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:172
   */
  @SideEffect.Fresh public ClassDecl clone() throws CloneNotSupportedException {
    ClassDecl node = (ClassDecl) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:177
   */
  @SideEffect.Fresh(group="_ASTNode") public ClassDecl copy() {
    try {
      ClassDecl node = (ClassDecl) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:196
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public ClassDecl fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:206
   */
  @SideEffect.Fresh(group="_ASTNode") public ClassDecl treeCopyNoTransform() {
    ClassDecl tree = (ClassDecl) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:226
   */
  @SideEffect.Fresh(group="_ASTNode") public ClassDecl treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:231
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_FileName == ((ClassDecl) node).tokenString_FileName) && (tokenint_StartLine == ((ClassDecl) node).tokenint_StartLine) && (tokenint_EndLine == ((ClassDecl) node).tokenint_EndLine) && (tokenString_Comment == ((ClassDecl) node).tokenString_Comment) && (tokenString_AspectName == ((ClassDecl) node).tokenString_AspectName);    
  }
  /**
   * Replaces the IdDecl child.
   * @param node The new node to replace the IdDecl child.
   * @apilevel high-level
   */
  public void setIdDecl(IdDecl node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the IdDecl child.
   * @return The current node used as the IdDecl child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="IdDecl")
  @SideEffect.Pure public IdDecl getIdDecl() {
    return (IdDecl) getChild(0);
  }
  /**
   * Retrieves the IdDecl child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the IdDecl child.
   * @apilevel low-level
   */
  @SideEffect.Pure public IdDecl getIdDeclNoTransform() {
    return (IdDecl) getChildNoTransform(0);
  }
  /**
   * Replaces the ClassBodyDecl list.
   * @param list The new list node to be used as the ClassBodyDecl list.
   * @apilevel high-level
   */
  public void setClassBodyDeclList(List<ClassBodyDecl> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the ClassBodyDecl list.
   * @return Number of children in the ClassBodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumClassBodyDecl() {
    return getClassBodyDeclList().getNumChild();
  }
  /**
   * Retrieves the number of children in the ClassBodyDecl list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the ClassBodyDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumClassBodyDeclNoTransform() {
    return getClassBodyDeclListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the ClassBodyDecl list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the ClassBodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public ClassBodyDecl getClassBodyDecl(int i) {
    return (ClassBodyDecl) getClassBodyDeclList().getChild(i);
  }
  /**
   * Check whether the ClassBodyDecl list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasClassBodyDecl() {
    return getClassBodyDeclList().getNumChild() != 0;
  }
  /**
   * Append an element to the ClassBodyDecl list.
   * @param node The element to append to the ClassBodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addClassBodyDecl(ClassBodyDecl node) {
    List<ClassBodyDecl> list = (parent == null) ? getClassBodyDeclListNoTransform() : getClassBodyDeclList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addClassBodyDeclNoTransform(ClassBodyDecl node) {
    List<ClassBodyDecl> list = getClassBodyDeclListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the ClassBodyDecl list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setClassBodyDecl(ClassBodyDecl node, int i) {
    List<ClassBodyDecl> list = getClassBodyDeclList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the ClassBodyDecl list.
   * @return The node representing the ClassBodyDecl list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="ClassBodyDecl")
  @SideEffect.Pure(group="_ASTNode") public List<ClassBodyDecl> getClassBodyDeclList() {
    List<ClassBodyDecl> list = (List<ClassBodyDecl>) getChild(1);
    return list;
  }
  /**
   * Retrieves the ClassBodyDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the ClassBodyDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<ClassBodyDecl> getClassBodyDeclListNoTransform() {
    return (List<ClassBodyDecl>) getChildNoTransform(1);
  }
  /**
   * @return the element at index {@code i} in the ClassBodyDecl list without
   * triggering rewrites.
   */
  @SideEffect.Pure public ClassBodyDecl getClassBodyDeclNoTransform(int i) {
    return (ClassBodyDecl) getClassBodyDeclListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the ClassBodyDecl list.
   * @return The node representing the ClassBodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<ClassBodyDecl> getClassBodyDecls() {
    return getClassBodyDeclList();
  }
  /**
   * Retrieves the ClassBodyDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the ClassBodyDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<ClassBodyDecl> getClassBodyDeclsNoTransform() {
    return getClassBodyDeclListNoTransform();
  }
  /**
   * Replaces the SynDecl list.
   * @param list The new list node to be used as the SynDecl list.
   * @apilevel high-level
   */
  public void setSynDeclList(List<SynDecl> list) {
    setChild(list, 2);
  }
  /**
   * Retrieves the number of children in the SynDecl list.
   * @return Number of children in the SynDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumSynDecl() {
    return getSynDeclList().getNumChild();
  }
  /**
   * Retrieves the number of children in the SynDecl list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the SynDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumSynDeclNoTransform() {
    return getSynDeclListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the SynDecl list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the SynDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public SynDecl getSynDecl(int i) {
    return (SynDecl) getSynDeclList().getChild(i);
  }
  /**
   * Check whether the SynDecl list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasSynDecl() {
    return getSynDeclList().getNumChild() != 0;
  }
  /**
   * Append an element to the SynDecl list.
   * @param node The element to append to the SynDecl list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addSynDecl(SynDecl node) {
    List<SynDecl> list = (parent == null) ? getSynDeclListNoTransform() : getSynDeclList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addSynDeclNoTransform(SynDecl node) {
    List<SynDecl> list = getSynDeclListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the SynDecl list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setSynDecl(SynDecl node, int i) {
    List<SynDecl> list = getSynDeclList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the SynDecl list.
   * @return The node representing the SynDecl list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="SynDecl")
  @SideEffect.Pure(group="_ASTNode") public List<SynDecl> getSynDeclList() {
    List<SynDecl> list = (List<SynDecl>) getChild(2);
    return list;
  }
  /**
   * Retrieves the SynDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the SynDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<SynDecl> getSynDeclListNoTransform() {
    return (List<SynDecl>) getChildNoTransform(2);
  }
  /**
   * @return the element at index {@code i} in the SynDecl list without
   * triggering rewrites.
   */
  @SideEffect.Pure public SynDecl getSynDeclNoTransform(int i) {
    return (SynDecl) getSynDeclListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the SynDecl list.
   * @return The node representing the SynDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<SynDecl> getSynDecls() {
    return getSynDeclList();
  }
  /**
   * Retrieves the SynDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the SynDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<SynDecl> getSynDeclsNoTransform() {
    return getSynDeclListNoTransform();
  }
  /**
   * Replaces the SynEq list.
   * @param list The new list node to be used as the SynEq list.
   * @apilevel high-level
   */
  public void setSynEqList(List<SynEq> list) {
    setChild(list, 3);
  }
  /**
   * Retrieves the number of children in the SynEq list.
   * @return Number of children in the SynEq list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumSynEq() {
    return getSynEqList().getNumChild();
  }
  /**
   * Retrieves the number of children in the SynEq list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the SynEq list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumSynEqNoTransform() {
    return getSynEqListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the SynEq list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the SynEq list.
   * @apilevel high-level
   */
  @SideEffect.Pure public SynEq getSynEq(int i) {
    return (SynEq) getSynEqList().getChild(i);
  }
  /**
   * Check whether the SynEq list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasSynEq() {
    return getSynEqList().getNumChild() != 0;
  }
  /**
   * Append an element to the SynEq list.
   * @param node The element to append to the SynEq list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addSynEq(SynEq node) {
    List<SynEq> list = (parent == null) ? getSynEqListNoTransform() : getSynEqList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addSynEqNoTransform(SynEq node) {
    List<SynEq> list = getSynEqListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the SynEq list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setSynEq(SynEq node, int i) {
    List<SynEq> list = getSynEqList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the SynEq list.
   * @return The node representing the SynEq list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="SynEq")
  @SideEffect.Pure(group="_ASTNode") public List<SynEq> getSynEqList() {
    List<SynEq> list = (List<SynEq>) getChild(3);
    return list;
  }
  /**
   * Retrieves the SynEq list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the SynEq list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<SynEq> getSynEqListNoTransform() {
    return (List<SynEq>) getChildNoTransform(3);
  }
  /**
   * @return the element at index {@code i} in the SynEq list without
   * triggering rewrites.
   */
  @SideEffect.Pure public SynEq getSynEqNoTransform(int i) {
    return (SynEq) getSynEqListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the SynEq list.
   * @return The node representing the SynEq list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<SynEq> getSynEqs() {
    return getSynEqList();
  }
  /**
   * Retrieves the SynEq list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the SynEq list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<SynEq> getSynEqsNoTransform() {
    return getSynEqListNoTransform();
  }
  /**
   * Replaces the InhDecl list.
   * @param list The new list node to be used as the InhDecl list.
   * @apilevel high-level
   */
  public void setInhDeclList(List<InhDecl> list) {
    setChild(list, 4);
  }
  /**
   * Retrieves the number of children in the InhDecl list.
   * @return Number of children in the InhDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumInhDecl() {
    return getInhDeclList().getNumChild();
  }
  /**
   * Retrieves the number of children in the InhDecl list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the InhDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumInhDeclNoTransform() {
    return getInhDeclListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the InhDecl list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the InhDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public InhDecl getInhDecl(int i) {
    return (InhDecl) getInhDeclList().getChild(i);
  }
  /**
   * Check whether the InhDecl list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasInhDecl() {
    return getInhDeclList().getNumChild() != 0;
  }
  /**
   * Append an element to the InhDecl list.
   * @param node The element to append to the InhDecl list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addInhDecl(InhDecl node) {
    List<InhDecl> list = (parent == null) ? getInhDeclListNoTransform() : getInhDeclList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addInhDeclNoTransform(InhDecl node) {
    List<InhDecl> list = getInhDeclListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the InhDecl list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setInhDecl(InhDecl node, int i) {
    List<InhDecl> list = getInhDeclList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the InhDecl list.
   * @return The node representing the InhDecl list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="InhDecl")
  @SideEffect.Pure(group="_ASTNode") public List<InhDecl> getInhDeclList() {
    List<InhDecl> list = (List<InhDecl>) getChild(4);
    return list;
  }
  /**
   * Retrieves the InhDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the InhDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<InhDecl> getInhDeclListNoTransform() {
    return (List<InhDecl>) getChildNoTransform(4);
  }
  /**
   * @return the element at index {@code i} in the InhDecl list without
   * triggering rewrites.
   */
  @SideEffect.Pure public InhDecl getInhDeclNoTransform(int i) {
    return (InhDecl) getInhDeclListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the InhDecl list.
   * @return The node representing the InhDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<InhDecl> getInhDecls() {
    return getInhDeclList();
  }
  /**
   * Retrieves the InhDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the InhDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<InhDecl> getInhDeclsNoTransform() {
    return getInhDeclListNoTransform();
  }
  /**
   * Replaces the InhEq list.
   * @param list The new list node to be used as the InhEq list.
   * @apilevel high-level
   */
  public void setInhEqList(List<InhEq> list) {
    setChild(list, 5);
  }
  /**
   * Retrieves the number of children in the InhEq list.
   * @return Number of children in the InhEq list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumInhEq() {
    return getInhEqList().getNumChild();
  }
  /**
   * Retrieves the number of children in the InhEq list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the InhEq list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumInhEqNoTransform() {
    return getInhEqListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the InhEq list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the InhEq list.
   * @apilevel high-level
   */
  @SideEffect.Pure public InhEq getInhEq(int i) {
    return (InhEq) getInhEqList().getChild(i);
  }
  /**
   * Check whether the InhEq list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasInhEq() {
    return getInhEqList().getNumChild() != 0;
  }
  /**
   * Append an element to the InhEq list.
   * @param node The element to append to the InhEq list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addInhEq(InhEq node) {
    List<InhEq> list = (parent == null) ? getInhEqListNoTransform() : getInhEqList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addInhEqNoTransform(InhEq node) {
    List<InhEq> list = getInhEqListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the InhEq list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setInhEq(InhEq node, int i) {
    List<InhEq> list = getInhEqList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the InhEq list.
   * @return The node representing the InhEq list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="InhEq")
  @SideEffect.Pure(group="_ASTNode") public List<InhEq> getInhEqList() {
    List<InhEq> list = (List<InhEq>) getChild(5);
    return list;
  }
  /**
   * Retrieves the InhEq list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the InhEq list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<InhEq> getInhEqListNoTransform() {
    return (List<InhEq>) getChildNoTransform(5);
  }
  /**
   * @return the element at index {@code i} in the InhEq list without
   * triggering rewrites.
   */
  @SideEffect.Pure public InhEq getInhEqNoTransform(int i) {
    return (InhEq) getInhEqListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the InhEq list.
   * @return The node representing the InhEq list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<InhEq> getInhEqs() {
    return getInhEqList();
  }
  /**
   * Retrieves the InhEq list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the InhEq list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<InhEq> getInhEqsNoTransform() {
    return getInhEqListNoTransform();
  }
  /**
   * Replaces the Component list.
   * @param list The new list node to be used as the Component list.
   * @apilevel high-level
   */
  public void setComponentList(List<Component> list) {
    setChild(list, 6);
  }
  /**
   * Retrieves the number of children in the Component list.
   * @return Number of children in the Component list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumComponent() {
    return getComponentList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Component list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Component list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumComponentNoTransform() {
    return getComponentListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Component list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Component list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Component getComponent(int i) {
    return (Component) getComponentList().getChild(i);
  }
  /**
   * Check whether the Component list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasComponent() {
    return getComponentList().getNumChild() != 0;
  }
  /**
   * Append an element to the Component list.
   * @param node The element to append to the Component list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addComponent(Component node) {
    List<Component> list = (parent == null) ? getComponentListNoTransform() : getComponentList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addComponentNoTransform(Component node) {
    List<Component> list = getComponentListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Component list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setComponent(Component node, int i) {
    List<Component> list = getComponentList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Component list.
   * @return The node representing the Component list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Component")
  @SideEffect.Pure(group="_ASTNode") public List<Component> getComponentList() {
    List<Component> list = (List<Component>) getChild(6);
    return list;
  }
  /**
   * Retrieves the Component list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Component list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Component> getComponentListNoTransform() {
    return (List<Component>) getChildNoTransform(6);
  }
  /**
   * @return the element at index {@code i} in the Component list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Component getComponentNoTransform(int i) {
    return (Component) getComponentListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Component list.
   * @return The node representing the Component list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Component> getComponents() {
    return getComponentList();
  }
  /**
   * Retrieves the Component list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Component list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Component> getComponentsNoTransform() {
    return getComponentListNoTransform();
  }
  /**
   * Replaces the CollDecl list.
   * @param list The new list node to be used as the CollDecl list.
   * @apilevel high-level
   */
  public void setCollDeclList(List<CollDecl> list) {
    setChild(list, 7);
  }
  /**
   * Retrieves the number of children in the CollDecl list.
   * @return Number of children in the CollDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumCollDecl() {
    return getCollDeclList().getNumChild();
  }
  /**
   * Retrieves the number of children in the CollDecl list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the CollDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumCollDeclNoTransform() {
    return getCollDeclListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the CollDecl list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the CollDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public CollDecl getCollDecl(int i) {
    return (CollDecl) getCollDeclList().getChild(i);
  }
  /**
   * Check whether the CollDecl list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasCollDecl() {
    return getCollDeclList().getNumChild() != 0;
  }
  /**
   * Append an element to the CollDecl list.
   * @param node The element to append to the CollDecl list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addCollDecl(CollDecl node) {
    List<CollDecl> list = (parent == null) ? getCollDeclListNoTransform() : getCollDeclList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addCollDeclNoTransform(CollDecl node) {
    List<CollDecl> list = getCollDeclListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the CollDecl list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setCollDecl(CollDecl node, int i) {
    List<CollDecl> list = getCollDeclList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the CollDecl list.
   * @return The node representing the CollDecl list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="CollDecl")
  @SideEffect.Pure(group="_ASTNode") public List<CollDecl> getCollDeclList() {
    List<CollDecl> list = (List<CollDecl>) getChild(7);
    return list;
  }
  /**
   * Retrieves the CollDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the CollDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<CollDecl> getCollDeclListNoTransform() {
    return (List<CollDecl>) getChildNoTransform(7);
  }
  /**
   * @return the element at index {@code i} in the CollDecl list without
   * triggering rewrites.
   */
  @SideEffect.Pure public CollDecl getCollDeclNoTransform(int i) {
    return (CollDecl) getCollDeclListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the CollDecl list.
   * @return The node representing the CollDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<CollDecl> getCollDecls() {
    return getCollDeclList();
  }
  /**
   * Retrieves the CollDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the CollDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<CollDecl> getCollDeclsNoTransform() {
    return getCollDeclListNoTransform();
  }
  /**
   * Replaces the lexeme FileName.
   * @param value The new value for the lexeme FileName.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setFileName(String value) {
    tokenString_FileName = value;
  }
  /**
   * Retrieves the value for the lexeme FileName.
   * @return The value for the lexeme FileName.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="FileName")
  @SideEffect.Pure(group="_ASTNode") public String getFileName() {
    return tokenString_FileName != null ? tokenString_FileName : "";
  }
  /**
   * Replaces the lexeme StartLine.
   * @param value The new value for the lexeme StartLine.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setStartLine(int value) {
    tokenint_StartLine = value;
  }
  /**
   * Retrieves the value for the lexeme StartLine.
   * @return The value for the lexeme StartLine.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="StartLine")
  @SideEffect.Pure(group="_ASTNode") public int getStartLine() {
    return tokenint_StartLine;
  }
  /**
   * Replaces the lexeme EndLine.
   * @param value The new value for the lexeme EndLine.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setEndLine(int value) {
    tokenint_EndLine = value;
  }
  /**
   * Retrieves the value for the lexeme EndLine.
   * @return The value for the lexeme EndLine.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="EndLine")
  @SideEffect.Pure(group="_ASTNode") public int getEndLine() {
    return tokenint_EndLine;
  }
  /**
   * Replaces the lexeme Comment.
   * @param value The new value for the lexeme Comment.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setComment(String value) {
    tokenString_Comment = value;
  }
  /**
   * Retrieves the value for the lexeme Comment.
   * @return The value for the lexeme Comment.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Comment")
  @SideEffect.Pure(group="_ASTNode") public String getComment() {
    return tokenString_Comment != null ? tokenString_Comment : "";
  }
  /**
   * Replaces the lexeme AspectName.
   * @param value The new value for the lexeme AspectName.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setAspectName(String value) {
    tokenString_AspectName = value;
  }
  /**
   * Retrieves the value for the lexeme AspectName.
   * @return The value for the lexeme AspectName.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="AspectName")
  @SideEffect.Pure(group="_ASTNode") public String getAspectName() {
    return tokenString_AspectName != null ? tokenString_AspectName : "";
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int attributeProblems_visited = -1;
  /**
   * Hack to make collection only on ASTdecls that are not
   * ClassDecl or InterfaceDecl.
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:100
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:100")
  @SideEffect.Pure(group="_ASTNode") public Collection<Problem> attributeProblems() {
    if (attributeProblems_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ClassDecl.attributeProblems().");
    }
    attributeProblems_visited = state().boundariesCrossed;
    Collection<Problem> attributeProblems_value = Collections.emptyList();
    attributeProblems_visited = -1;
    return attributeProblems_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int typeDeclKind_visited = -1;
  /**
   * @attribute syn
   * @aspect Comments
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Comments.jrag:163
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Comments", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Comments.jrag:163")
  @SideEffect.Pure(group="_ASTNode") public String typeDeclKind() {
    if (typeDeclKind_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.typeDeclKind().");
    }
    typeDeclKind_visited = state().boundariesCrossed;
    String typeDeclKind_value = "class";
    typeDeclKind_visited = -1;
    return typeDeclKind_value;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
