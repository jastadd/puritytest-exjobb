package org.jastadd.ast.AST;

import org.jastadd.ast.AST.*;
import java.util.*;
import org.jastadd.Problem;
import org.jastadd.jrag.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jastadd.jrag.AST.ASTExpression;
import org.jastadd.jrag.Unparser;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.io.PrintStream;
import java.io.*;
import java.util.regex.*;
import org.jastadd.JastAdd;
import org.jastadd.jrag.AST.ASTAspectMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectRefineMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectFieldDeclaration;
import org.jastadd.jrag.AST.ASTBlock;
import org.jastadd.jrag.ClassBodyObject;
import org.jastadd.jrag.AST.ASTCompilationUnit;
import org.jastadd.ast.AST.Token;
import org.jastadd.ast.AST.SimpleNode;
import org.jastadd.Configuration;
import org.jastadd.tinytemplate.*;
/**
 * @ast class
 * @aspect Comments
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Comments.jrag:31
 */
 class JavaDocParser extends java.lang.Object {
  
    private static final String NL = System.getProperty("line.separator", "\n");

  
    int i;

  
    char[] javadoc;

  
    StringBuilder out;

  

    String parse(String comments) {
      if (comments == null)
        return "";

      int start = 0;
      int end = 0;
outer: while (true) {
        int lnStart = comments.indexOf("//", end);
        int newStart = comments.indexOf("/**", end);
        if (newStart == -1) {
          break;
        }
        if (lnStart != -1 && lnStart < newStart) {
          // Skip to end of line.
          int len = comments.length();
          for (int i = lnStart; i < len; ++i) {
            char c = comments.charAt(i);
            if (c == '\n') {
              start = end = i;
              continue outer;
            } else if (c == '\r') {
              start = end = i;
              if (i < len-1 && comments.charAt(i+1) == '\n') {
                start = end = end + 1;
              }
              continue outer;
            }
          }
          start = end = comments.length();
          break;
        }
        int newEnd = comments.indexOf("*/", newStart + 3) + 2;
        if (newEnd == -1) {
          break;
        }
        start = newStart;
        end = newEnd;
      }

      if (end <= start+3) {
        return "";
      }

      // Doc comment with /** and */ stripped:
      javadoc = comments.substring(start+3, end-2).toCharArray();
      out = new StringBuilder(javadoc.length);
      i = 0;

      while (i < javadoc.length) {
        if (skipNewline()) {
          skipWhitespace();
          if (skipAsterisk()) {
            out.append(NL);
            out.append(" * ");
            skipWhitespace();
          }
        } else {
          out.append(javadoc[i++]);
        }
      }
      return out.toString();
    }

  

    private boolean skipNewline() {
      if (javadoc[i] == '\r') {
        i += 1;
        if (i < javadoc.length && javadoc[i] == '\n') {
          i += 1;
        }
        return true;
      } else if (javadoc[i] == '\n') {
        i += 1;
        return true;
      }
      return false;
    }

  

    private void skipWhitespace() {
      while (i < javadoc.length && isWhitespace(javadoc[i])) {
        i += 1;
      }
    }

  

    private boolean isWhitespace(char c) {
      return Character.isWhitespace(c) && c != '\n' && c != '\n';
    }

  

    private boolean skipAsterisk() {
      if (i < javadoc.length && javadoc[i] == '*') {
        i += 1;
        return true;
      }
      return false;
    }


}
