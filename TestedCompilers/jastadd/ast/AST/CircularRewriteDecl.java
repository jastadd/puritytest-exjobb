/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package org.jastadd.ast.AST;
import org.jastadd.ast.AST.*;
import java.util.*;
import org.jastadd.Problem;
import org.jastadd.jrag.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jastadd.jrag.AST.ASTExpression;
import org.jastadd.jrag.Unparser;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.io.PrintStream;
import java.io.*;
import java.util.regex.*;
import org.jastadd.JastAdd;
import org.jastadd.jrag.AST.ASTAspectMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectRefineMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectFieldDeclaration;
import org.jastadd.jrag.AST.ASTBlock;
import org.jastadd.jrag.ClassBodyObject;
import org.jastadd.jrag.AST.ASTCompilationUnit;
import org.jastadd.ast.AST.Token;
import org.jastadd.ast.AST.SimpleNode;
import org.jastadd.Configuration;
import org.jastadd.tinytemplate.*;
/**
 * Only used to generate code for implicit circular rewrite NTAs.
 * The reason a separate node type is used for this is because we need to
 * change the code generation for the implicit circular NTA slightly.
 * The changes in code generation affect parent attachment and update condition.
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Ast.ast:36
 * @production CircularRewriteDecl : {@link AttrDecl};

 */
public class CircularRewriteDecl extends AttrDecl implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public CircularRewriteDecl(int i) {
    super(i);
  }
  /**
   * @declaredat ASTNode:5
   */
  public CircularRewriteDecl(Ast p, int i) {
    this(i);
    parser = p;
  }
  /**
   * @declaredat ASTNode:10
   */
  public CircularRewriteDecl() {
    this(0);
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:19
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
    setChild(new List(), 0);
    setChild(new List(), 1);
  }
  /**
   * @declaredat ASTNode:24
   */
  public CircularRewriteDecl(List<Parameter> p0, String p1, String p2, CacheMode p3, String p4, int p5, int p6, boolean p7, boolean p8, String p9, String p10, List<Annotation> p11) {
    setChild(p0, 0);
    setName(p1);
    setType(p2);
    setCacheMode(p3);
    setFileName(p4);
    setStartLine(p5);
    setEndLine(p6);
    setFinal(p7);
    setNTA(p8);
    setComment(p9);
    setAspectName(p10);
    setChild(p11, 1);
  }
  /**
   * @declaredat ASTNode:38
   */
  public void dumpTree(String indent, java.io.PrintStream out) {
    out.print(indent + "CircularRewriteDecl");
    out.print("\"" + getName() + "\"");
    out.print("\"" + getType() + "\"");
    out.print("\"" + getCacheMode() + "\"");
    out.print("\"" + getFileName() + "\"");
    out.print("\"" + getStartLine() + "\"");
    out.print("\"" + getEndLine() + "\"");
    out.print("\"" + getFinal() + "\"");
    out.print("\"" + getNTA() + "\"");
    out.print("\"" + getComment() + "\"");
    out.print("\"" + getAspectName() + "\"");
    String childIndent = indent + "  ";
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).dumpTree(childIndent, out);
    }
  }
  /**
   * @declaredat ASTNode:55
   */
  public Object jjtAccept(AstVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
  /**
   * @declaredat ASTNode:58
   */
  public void jjtAddChild(Node n, int i) {
    checkChild(n, i);
    super.jjtAddChild(n, i);
  }
  /**
   * @declaredat ASTNode:62
   */
  public void checkChild(Node n, int i) {
      if (i == 0) {
        if (!(n instanceof List)) {
          throw new Error("Child number 0 of AttrDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof Parameter)) {
            throw new Error("Child number " + k + " in ParameterList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of Parameter");
          }
        }
      }
      if (i == 1) {
        if (!(n instanceof List)) {
          throw new Error("Child number 1 of AttrDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof Annotation)) {
            throw new Error("Child number " + k + " in AnnotationList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of Annotation");
          }
        }
      }
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:89
   */
  @SideEffect.Pure public int getNumChild() {
    return 2;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:95
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:99
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:103
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:107
   */
  @SideEffect.Fresh public CircularRewriteDecl clone() throws CloneNotSupportedException {
    CircularRewriteDecl node = (CircularRewriteDecl) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:112
   */
  @SideEffect.Fresh(group="_ASTNode") public CircularRewriteDecl copy() {
    try {
      CircularRewriteDecl node = (CircularRewriteDecl) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:131
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public CircularRewriteDecl fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:141
   */
  @SideEffect.Fresh(group="_ASTNode") public CircularRewriteDecl treeCopyNoTransform() {
    CircularRewriteDecl tree = (CircularRewriteDecl) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:161
   */
  @SideEffect.Fresh(group="_ASTNode") public CircularRewriteDecl treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:166
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_Name == ((CircularRewriteDecl) node).tokenString_Name) && (tokenString_Type == ((CircularRewriteDecl) node).tokenString_Type) && (tokenCacheMode_CacheMode == ((CircularRewriteDecl) node).tokenCacheMode_CacheMode) && (tokenString_FileName == ((CircularRewriteDecl) node).tokenString_FileName) && (tokenint_StartLine == ((CircularRewriteDecl) node).tokenint_StartLine) && (tokenint_EndLine == ((CircularRewriteDecl) node).tokenint_EndLine) && (tokenboolean_Final == ((CircularRewriteDecl) node).tokenboolean_Final) && (tokenboolean_NTA == ((CircularRewriteDecl) node).tokenboolean_NTA) && (tokenString_Comment == ((CircularRewriteDecl) node).tokenString_Comment) && (tokenString_AspectName == ((CircularRewriteDecl) node).tokenString_AspectName);    
  }
  /**
   * Replaces the Parameter list.
   * @param list The new list node to be used as the Parameter list.
   * @apilevel high-level
   */
  public void setParameterList(List<Parameter> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the Parameter list.
   * @return Number of children in the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumParameter() {
    return getParameterList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Parameter list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Parameter list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumParameterNoTransform() {
    return getParameterListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Parameter list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Parameter getParameter(int i) {
    return (Parameter) getParameterList().getChild(i);
  }
  /**
   * Check whether the Parameter list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasParameter() {
    return getParameterList().getNumChild() != 0;
  }
  /**
   * Append an element to the Parameter list.
   * @param node The element to append to the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addParameter(Parameter node) {
    List<Parameter> list = (parent == null) ? getParameterListNoTransform() : getParameterList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addParameterNoTransform(Parameter node) {
    List<Parameter> list = getParameterListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Parameter list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setParameter(Parameter node, int i) {
    List<Parameter> list = getParameterList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Parameter list.
   * @return The node representing the Parameter list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Parameter")
  @SideEffect.Pure(group="_ASTNode") public List<Parameter> getParameterList() {
    List<Parameter> list = (List<Parameter>) getChild(0);
    return list;
  }
  /**
   * Retrieves the Parameter list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Parameter list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Parameter> getParameterListNoTransform() {
    return (List<Parameter>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the Parameter list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Parameter getParameterNoTransform(int i) {
    return (Parameter) getParameterListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Parameter list.
   * @return The node representing the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Parameter> getParameters() {
    return getParameterList();
  }
  /**
   * Retrieves the Parameter list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Parameter list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Parameter> getParametersNoTransform() {
    return getParameterListNoTransform();
  }
  /**
   * Replaces the lexeme Name.
   * @param value The new value for the lexeme Name.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setName(String value) {
    tokenString_Name = value;
  }
  /**
   * Retrieves the value for the lexeme Name.
   * @return The value for the lexeme Name.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Name")
  @SideEffect.Pure(group="_ASTNode") public String getName() {
    return tokenString_Name != null ? tokenString_Name : "";
  }
  /**
   * Replaces the lexeme Type.
   * @param value The new value for the lexeme Type.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setType(String value) {
    tokenString_Type = value;
  }
  /**
   * Retrieves the value for the lexeme Type.
   * @return The value for the lexeme Type.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Type")
  @SideEffect.Pure(group="_ASTNode") public String getType() {
    return tokenString_Type != null ? tokenString_Type : "";
  }
  /**
   * Replaces the lexeme CacheMode.
   * @param value The new value for the lexeme CacheMode.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setCacheMode(CacheMode value) {
    tokenCacheMode_CacheMode = value;
  }
  /**
   * Retrieves the value for the lexeme CacheMode.
   * @return The value for the lexeme CacheMode.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="CacheMode")
  @SideEffect.Pure(group="_ASTNode") public CacheMode getCacheMode() {
    return tokenCacheMode_CacheMode;
  }
  /**
   * Replaces the lexeme FileName.
   * @param value The new value for the lexeme FileName.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setFileName(String value) {
    tokenString_FileName = value;
  }
  /**
   * Retrieves the value for the lexeme FileName.
   * @return The value for the lexeme FileName.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="FileName")
  @SideEffect.Pure(group="_ASTNode") public String getFileName() {
    return tokenString_FileName != null ? tokenString_FileName : "";
  }
  /**
   * Replaces the lexeme StartLine.
   * @param value The new value for the lexeme StartLine.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setStartLine(int value) {
    tokenint_StartLine = value;
  }
  /**
   * Retrieves the value for the lexeme StartLine.
   * @return The value for the lexeme StartLine.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="StartLine")
  @SideEffect.Pure(group="_ASTNode") public int getStartLine() {
    return tokenint_StartLine;
  }
  /**
   * Replaces the lexeme EndLine.
   * @param value The new value for the lexeme EndLine.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setEndLine(int value) {
    tokenint_EndLine = value;
  }
  /**
   * Retrieves the value for the lexeme EndLine.
   * @return The value for the lexeme EndLine.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="EndLine")
  @SideEffect.Pure(group="_ASTNode") public int getEndLine() {
    return tokenint_EndLine;
  }
  /**
   * Replaces the lexeme Final.
   * @param value The new value for the lexeme Final.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setFinal(boolean value) {
    tokenboolean_Final = value;
  }
  /**
   * Retrieves the value for the lexeme Final.
   * @return The value for the lexeme Final.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Final")
  @SideEffect.Pure(group="_ASTNode") public boolean getFinal() {
    return tokenboolean_Final;
  }
  /**
   * Replaces the lexeme NTA.
   * @param value The new value for the lexeme NTA.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setNTA(boolean value) {
    tokenboolean_NTA = value;
  }
  /**
   * Retrieves the value for the lexeme NTA.
   * @return The value for the lexeme NTA.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="NTA")
  @SideEffect.Pure(group="_ASTNode") public boolean getNTA() {
    return tokenboolean_NTA;
  }
  /**
   * Replaces the lexeme Comment.
   * @param value The new value for the lexeme Comment.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setComment(String value) {
    tokenString_Comment = value;
  }
  /**
   * Retrieves the value for the lexeme Comment.
   * @return The value for the lexeme Comment.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Comment")
  @SideEffect.Pure(group="_ASTNode") public String getComment() {
    return tokenString_Comment != null ? tokenString_Comment : "";
  }
  /**
   * Replaces the lexeme AspectName.
   * @param value The new value for the lexeme AspectName.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setAspectName(String value) {
    tokenString_AspectName = value;
  }
  /**
   * Retrieves the value for the lexeme AspectName.
   * @return The value for the lexeme AspectName.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="AspectName")
  @SideEffect.Pure(group="_ASTNode") public String getAspectName() {
    return tokenString_AspectName != null ? tokenString_AspectName : "";
  }
  /**
   * Replaces the Annotation list.
   * @param list The new list node to be used as the Annotation list.
   * @apilevel high-level
   */
  public void setAnnotationList(List<Annotation> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the Annotation list.
   * @return Number of children in the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumAnnotation() {
    return getAnnotationList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Annotation list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Annotation list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumAnnotationNoTransform() {
    return getAnnotationListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Annotation list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Annotation getAnnotation(int i) {
    return (Annotation) getAnnotationList().getChild(i);
  }
  /**
   * Check whether the Annotation list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasAnnotation() {
    return getAnnotationList().getNumChild() != 0;
  }
  /**
   * Append an element to the Annotation list.
   * @param node The element to append to the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addAnnotation(Annotation node) {
    List<Annotation> list = (parent == null) ? getAnnotationListNoTransform() : getAnnotationList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addAnnotationNoTransform(Annotation node) {
    List<Annotation> list = getAnnotationListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Annotation list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setAnnotation(Annotation node, int i) {
    List<Annotation> list = getAnnotationList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Annotation list.
   * @return The node representing the Annotation list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Annotation")
  @SideEffect.Pure(group="_ASTNode") public List<Annotation> getAnnotationList() {
    List<Annotation> list = (List<Annotation>) getChild(1);
    return list;
  }
  /**
   * Retrieves the Annotation list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Annotation list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Annotation> getAnnotationListNoTransform() {
    return (List<Annotation>) getChildNoTransform(1);
  }
  /**
   * @return the element at index {@code i} in the Annotation list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Annotation getAnnotationNoTransform(int i) {
    return (Annotation) getAnnotationListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Annotation list.
   * @return The node representing the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Annotation> getAnnotations() {
    return getAnnotationList();
  }
  /**
   * Retrieves the Annotation list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Annotation list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Annotation> getAnnotationsNoTransform() {
    return getAnnotationListNoTransform();
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int annotationKind_visited = -1;
  /**
   * @attribute syn
   * @aspect ASTNodeAnnotations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNodeAnnotations.jrag:30
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ASTNodeAnnotations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNodeAnnotations.jrag:30")
  @SideEffect.Pure(group="_ASTNode") public String annotationKind() {
    if (annotationKind_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.annotationKind().");
    }
    annotationKind_visited = state().boundariesCrossed;
    String annotationKind_value = "kind=ASTNodeAnnotation.Kind.SYN";
    annotationKind_visited = -1;
    return annotationKind_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isCircularRewrite_visited = -1;
  /**
   * @attribute syn
   * @aspect Circular
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Circular.jrag:33
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Circular", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Circular.jrag:33")
  @SideEffect.Pure(group="_ASTNode") public boolean isCircularRewrite() {
    if (isCircularRewrite_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.isCircularRewrite().");
    }
    isCircularRewrite_visited = state().boundariesCrossed;
    boolean isCircularRewrite_value = true;
    isCircularRewrite_visited = -1;
    return isCircularRewrite_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isCircular_visited = -1;
  /**
   * @attribute syn
   * @aspect Circular
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Circular.jrag:37
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Circular", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Circular.jrag:37")
  @SideEffect.Pure(group="_ASTNode") public boolean isCircular() {
    if (isCircular_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.isCircular().");
    }
    isCircular_visited = state().boundariesCrossed;
    boolean isCircular_value = true;
    isCircular_visited = -1;
    return isCircular_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int getBottomValue_visited = -1;
  /**
   * @attribute syn
   * @aspect Circular
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Circular.jrag:43
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Circular", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Circular.jrag:43")
  @SideEffect.Pure(group="_ASTNode") public String getBottomValue() {
    if (getBottomValue_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.getBottomValue().");
    }
    getBottomValue_visited = state().boundariesCrossed;
    String getBottomValue_value = "this";
    getBottomValue_visited = -1;
    return getBottomValue_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int ntaParent_visited = -1;
  /** The parent to attach an NTA value to during circular evaluation. 
   * @attribute syn
   * @aspect Circular
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Circular.jrag:48
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Circular", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Circular.jrag:48")
  @SideEffect.Pure(group="_ASTNode") public String ntaParent() {
    if (ntaParent_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.ntaParent().");
    }
    ntaParent_visited = state().boundariesCrossed;
    String ntaParent_value = "getParent()";
    ntaParent_visited = -1;
    return ntaParent_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int attributeKind_visited = -1;
  /**
   * @attribute syn
   * @aspect AttributeKind
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:40
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeKind", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:40")
  @SideEffect.Pure(group="_ASTNode") public String attributeKind() {
    if (attributeKind_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.attributeKind().");
    }
    attributeKind_visited = state().boundariesCrossed;
    String attributeKind_value = "syn nta";
    attributeKind_visited = -1;
    return attributeKind_value;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
