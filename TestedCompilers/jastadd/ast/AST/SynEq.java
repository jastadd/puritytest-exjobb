/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package org.jastadd.ast.AST;
import org.jastadd.ast.AST.*;
import java.util.*;
import org.jastadd.Problem;
import org.jastadd.jrag.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jastadd.jrag.AST.ASTExpression;
import org.jastadd.jrag.Unparser;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.io.PrintStream;
import java.io.*;
import java.util.regex.*;
import org.jastadd.JastAdd;
import org.jastadd.jrag.AST.ASTAspectMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectRefineMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectFieldDeclaration;
import org.jastadd.jrag.AST.ASTBlock;
import org.jastadd.jrag.ClassBodyObject;
import org.jastadd.jrag.AST.ASTCompilationUnit;
import org.jastadd.ast.AST.Token;
import org.jastadd.ast.AST.SimpleNode;
import org.jastadd.Configuration;
import org.jastadd.tinytemplate.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Ast.ast:47
 * @production SynEq : {@link AttrEq} ::= <span class="component">{@link Parameter}*</span> <span class="component">&lt;Name:String&gt;</span> <span class="component">&lt;FileName:String&gt;</span> <span class="component">&lt;StartLine:int&gt;</span> <span class="component">&lt;EndLine:int&gt;</span> <span class="component">&lt;Comment:String&gt;</span> <span class="component">&lt;AspectName:String&gt;</span>;

 */
public class SynEq extends AttrEq implements Cloneable {
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:242
   */
  public String hostName = "";
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:377
   */
  public void emitComputeMethod(PrintStream out) {
    templateContext().expand("SynEq.emitComputeMethod", out);
  }
  /**
   * @declaredat ASTNode:1
   */
  public SynEq(int i) {
    super(i);
  }
  /**
   * @declaredat ASTNode:5
   */
  public SynEq(Ast p, int i) {
    this(i);
    parser = p;
  }
  /**
   * @declaredat ASTNode:10
   */
  public SynEq() {
    this(0);
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:19
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
    setChild(new List(), 0);
    setChild(new List(), 1);
  }
  /**
   * @declaredat ASTNode:24
   */
  public SynEq(List<Annotation> p0, List<Parameter> p1, String p2, String p3, int p4, int p5, String p6, String p7) {
    setChild(p0, 0);
    setChild(p1, 1);
    setName(p2);
    setFileName(p3);
    setStartLine(p4);
    setEndLine(p5);
    setComment(p6);
    setAspectName(p7);
  }
  /**
   * @declaredat ASTNode:34
   */
  public void dumpTree(String indent, java.io.PrintStream out) {
    out.print(indent + "SynEq");
    out.print("\"" + getName() + "\"");
    out.print("\"" + getFileName() + "\"");
    out.print("\"" + getStartLine() + "\"");
    out.print("\"" + getEndLine() + "\"");
    out.print("\"" + getComment() + "\"");
    out.print("\"" + getAspectName() + "\"");
    String childIndent = indent + "  ";
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).dumpTree(childIndent, out);
    }
  }
  /**
   * @declaredat ASTNode:47
   */
  public Object jjtAccept(AstVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
  /**
   * @declaredat ASTNode:50
   */
  public void jjtAddChild(Node n, int i) {
    checkChild(n, i);
    super.jjtAddChild(n, i);
  }
  /**
   * @declaredat ASTNode:54
   */
  public void checkChild(Node n, int i) {
      if (i == 0) {
        if (!(n instanceof List)) {
          throw new Error("Child number 0 of AttrEq has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof Annotation)) {
            throw new Error("Child number " + k + " in AnnotationList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of Annotation");
          }
        }
      }
      if (i == 1) {
        if (!(n instanceof List)) {
          throw new Error("Child number 1 of SynEq has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof Parameter)) {
            throw new Error("Child number " + k + " in ParameterList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of Parameter");
          }
        }
      }
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:81
   */
  @SideEffect.Pure public int getNumChild() {
    return 2;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:87
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:91
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    decl_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:96
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:100
   */
  @SideEffect.Fresh public SynEq clone() throws CloneNotSupportedException {
    SynEq node = (SynEq) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:105
   */
  @SideEffect.Fresh(group="_ASTNode") public SynEq copy() {
    try {
      SynEq node = (SynEq) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:124
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public SynEq fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:134
   */
  @SideEffect.Fresh(group="_ASTNode") public SynEq treeCopyNoTransform() {
    SynEq tree = (SynEq) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:154
   */
  @SideEffect.Fresh(group="_ASTNode") public SynEq treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:159
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_Name == ((SynEq) node).tokenString_Name) && (tokenString_FileName == ((SynEq) node).tokenString_FileName) && (tokenint_StartLine == ((SynEq) node).tokenint_StartLine) && (tokenint_EndLine == ((SynEq) node).tokenint_EndLine) && (tokenString_Comment == ((SynEq) node).tokenString_Comment) && (tokenString_AspectName == ((SynEq) node).tokenString_AspectName);    
  }
  /**
   * Replaces the Annotation list.
   * @param list The new list node to be used as the Annotation list.
   * @apilevel high-level
   */
  public void setAnnotationList(List<Annotation> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the Annotation list.
   * @return Number of children in the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumAnnotation() {
    return getAnnotationList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Annotation list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Annotation list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumAnnotationNoTransform() {
    return getAnnotationListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Annotation list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Annotation getAnnotation(int i) {
    return (Annotation) getAnnotationList().getChild(i);
  }
  /**
   * Check whether the Annotation list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasAnnotation() {
    return getAnnotationList().getNumChild() != 0;
  }
  /**
   * Append an element to the Annotation list.
   * @param node The element to append to the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addAnnotation(Annotation node) {
    List<Annotation> list = (parent == null) ? getAnnotationListNoTransform() : getAnnotationList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addAnnotationNoTransform(Annotation node) {
    List<Annotation> list = getAnnotationListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Annotation list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setAnnotation(Annotation node, int i) {
    List<Annotation> list = getAnnotationList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Annotation list.
   * @return The node representing the Annotation list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Annotation")
  @SideEffect.Pure(group="_ASTNode") public List<Annotation> getAnnotationList() {
    List<Annotation> list = (List<Annotation>) getChild(0);
    return list;
  }
  /**
   * Retrieves the Annotation list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Annotation list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Annotation> getAnnotationListNoTransform() {
    return (List<Annotation>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the Annotation list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Annotation getAnnotationNoTransform(int i) {
    return (Annotation) getAnnotationListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Annotation list.
   * @return The node representing the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Annotation> getAnnotations() {
    return getAnnotationList();
  }
  /**
   * Retrieves the Annotation list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Annotation list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Annotation> getAnnotationsNoTransform() {
    return getAnnotationListNoTransform();
  }
  /**
   * Replaces the Parameter list.
   * @param list The new list node to be used as the Parameter list.
   * @apilevel high-level
   */
  public void setParameterList(List<Parameter> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the Parameter list.
   * @return Number of children in the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumParameter() {
    return getParameterList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Parameter list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Parameter list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumParameterNoTransform() {
    return getParameterListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Parameter list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Parameter getParameter(int i) {
    return (Parameter) getParameterList().getChild(i);
  }
  /**
   * Check whether the Parameter list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasParameter() {
    return getParameterList().getNumChild() != 0;
  }
  /**
   * Append an element to the Parameter list.
   * @param node The element to append to the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addParameter(Parameter node) {
    List<Parameter> list = (parent == null) ? getParameterListNoTransform() : getParameterList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addParameterNoTransform(Parameter node) {
    List<Parameter> list = getParameterListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Parameter list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setParameter(Parameter node, int i) {
    List<Parameter> list = getParameterList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Parameter list.
   * @return The node representing the Parameter list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Parameter")
  @SideEffect.Pure(group="_ASTNode") public List<Parameter> getParameterList() {
    List<Parameter> list = (List<Parameter>) getChild(1);
    return list;
  }
  /**
   * Retrieves the Parameter list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Parameter list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Parameter> getParameterListNoTransform() {
    return (List<Parameter>) getChildNoTransform(1);
  }
  /**
   * @return the element at index {@code i} in the Parameter list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Parameter getParameterNoTransform(int i) {
    return (Parameter) getParameterListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Parameter list.
   * @return The node representing the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Parameter> getParameters() {
    return getParameterList();
  }
  /**
   * Retrieves the Parameter list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Parameter list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Parameter> getParametersNoTransform() {
    return getParameterListNoTransform();
  }
  /**
   * Replaces the lexeme Name.
   * @param value The new value for the lexeme Name.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setName(String value) {
    tokenString_Name = value;
  }
  /**
   * Retrieves the value for the lexeme Name.
   * @return The value for the lexeme Name.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Name")
  @SideEffect.Pure(group="_ASTNode") public String getName() {
    return tokenString_Name != null ? tokenString_Name : "";
  }
  /**
   * Replaces the lexeme FileName.
   * @param value The new value for the lexeme FileName.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setFileName(String value) {
    tokenString_FileName = value;
  }
  /**
   * Retrieves the value for the lexeme FileName.
   * @return The value for the lexeme FileName.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="FileName")
  @SideEffect.Pure(group="_ASTNode") public String getFileName() {
    return tokenString_FileName != null ? tokenString_FileName : "";
  }
  /**
   * Replaces the lexeme StartLine.
   * @param value The new value for the lexeme StartLine.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setStartLine(int value) {
    tokenint_StartLine = value;
  }
  /**
   * Retrieves the value for the lexeme StartLine.
   * @return The value for the lexeme StartLine.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="StartLine")
  @SideEffect.Pure(group="_ASTNode") public int getStartLine() {
    return tokenint_StartLine;
  }
  /**
   * Replaces the lexeme EndLine.
   * @param value The new value for the lexeme EndLine.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setEndLine(int value) {
    tokenint_EndLine = value;
  }
  /**
   * Retrieves the value for the lexeme EndLine.
   * @return The value for the lexeme EndLine.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="EndLine")
  @SideEffect.Pure(group="_ASTNode") public int getEndLine() {
    return tokenint_EndLine;
  }
  /**
   * Replaces the lexeme Comment.
   * @param value The new value for the lexeme Comment.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setComment(String value) {
    tokenString_Comment = value;
  }
  /**
   * Retrieves the value for the lexeme Comment.
   * @return The value for the lexeme Comment.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Comment")
  @SideEffect.Pure(group="_ASTNode") public String getComment() {
    return tokenString_Comment != null ? tokenString_Comment : "";
  }
  /**
   * Replaces the lexeme AspectName.
   * @param value The new value for the lexeme AspectName.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setAspectName(String value) {
    tokenString_AspectName = value;
  }
  /**
   * Retrieves the value for the lexeme AspectName.
   * @return The value for the lexeme AspectName.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="AspectName")
  @SideEffect.Pure(group="_ASTNode") public String getAspectName() {
    return tokenString_AspectName != null ? tokenString_AspectName : "";
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map supportedAnnotation_Annotation_visited = new java.util.HashMap(4);
  /**
   * @param a the annotation
   * @return <code>true</code> if the given annotation is supported
   * for this type of attribute equation
   * @attribute syn
   * @aspect Annotations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Annotations.jrag:69
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Annotations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Annotations.jrag:69")
  @SideEffect.Pure(group="_ASTNode") public boolean supportedAnnotation(Annotation a) {
    Object _parameters = a;
    if (Integer.valueOf(state().boundariesCrossed).equals(supportedAnnotation_Annotation_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute AttrEq.supportedAnnotation(Annotation).");
    }
    supportedAnnotation_Annotation_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean supportedAnnotation_Annotation_value = true;
    supportedAnnotation_Annotation_visited.remove(_parameters);
    return supportedAnnotation_Annotation_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int prevEq_visited = -1;
  /** @return Previous equation for same synthesized attribute. 
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:182
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:182")
  @SideEffect.Pure(group="_ASTNode") public SynEq prevEq() {
    if (prevEq_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute SynEq.prevEq().");
    }
    prevEq_visited = state().boundariesCrossed;
    SynEq prevEq_value = hostClass().lookupSynEq(signature());
    prevEq_visited = -1;
    return prevEq_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int hasComputeBlock_visited = -1;
  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:343
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:343")
  @SideEffect.Pure(group="_ASTNode") public boolean hasComputeBlock() {
    if (hasComputeBlock_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrEq.hasComputeBlock().");
    }
    hasComputeBlock_visited = state().boundariesCrossed;
    boolean hasComputeBlock_value = getRHS() instanceof ASTBlock;
    hasComputeBlock_visited = -1;
    return hasComputeBlock_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int computeCode_visited = -1;
  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:367
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:367")
  @SideEffect.Pure(group="_ASTNode") public String computeCode() {
    if (computeCode_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrEq.computeCode().");
    }
    computeCode_visited = state().boundariesCrossed;
    String computeCode_value = Unparser.unparse(getRHS());
    computeCode_visited = -1;
    return computeCode_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int canInlineExpression_visited = -1;
  /**
   * @return {@code true} if this attribute equation can be inlined in the
   * attribute method
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:424
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:424")
  @SideEffect.Pure(group="_ASTNode") public boolean canInlineExpression() {
    if (canInlineExpression_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrEq.canInlineExpression().");
    }
    canInlineExpression_visited = state().boundariesCrossed;
    boolean canInlineExpression_value = !hasComputeBlock();
    canInlineExpression_visited = -1;
    return canInlineExpression_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map canInlineBlock_AttrDecl_visited = new java.util.HashMap(4);
  /**
   * @return {@code true} if this attribute code block can be inlined in the
   * attribute method
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:432
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:432")
  @SideEffect.Pure(group="_ASTNode") public boolean canInlineBlock(AttrDecl decl) {
    Object _parameters = decl;
    if (Integer.valueOf(state().boundariesCrossed).equals(canInlineBlock_AttrDecl_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute AttrEq.canInlineBlock(AttrDecl).");
    }
    canInlineBlock_AttrDecl_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean canInlineBlock_AttrDecl_value = hasComputeBlock() && !decl.isLazy() && !decl.isNTA() && !decl.isCircular();
    canInlineBlock_AttrDecl_visited.remove(_parameters);
    return canInlineBlock_AttrDecl_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int needFresh_visited = -1;
  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:780
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:780")
  @SideEffect.Pure(group="_ASTNode") public boolean needFresh() {
    if (needFresh_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute SynEq.needFresh().");
    }
    needFresh_visited = state().boundariesCrossed;
    boolean needFresh_value = decl().isAttrNTA() || decl().getNTA();
    needFresh_visited = -1;
    return needFresh_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int decl_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void decl_reset() {
    decl_computed = false;
    
    decl_value = null;
    decl_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean decl_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected SynDecl decl_value;

  /**
   * @attribute syn
   * @aspect LookupDecls
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:232
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="LookupDecls", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:232")
  @SideEffect.Pure(group="_ASTNode") public SynDecl decl() {
    ASTState state = state();
    if (decl_computed) {
      return decl_value;
    }
    if (decl_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute SynEq.decl().");
    }
    decl_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    decl_value = hostClass().lookupSynDecl(signature());
    if (isFinal && _boundaries == state().boundariesCrossed) {
    decl_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    decl_visited = -1;
    return decl_value;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  @SideEffect.Local(group="_ASTNode") protected void collect_contributors_TypeDecl_attributeProblems(TypeDecl _root, @SideEffect.Local java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:110
    if (decl() == null) {
      {
        TypeDecl target = (TypeDecl) (hostClass());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:184
    if (decl() != null && prevEq() != null && prevEq() != this) {
      {
        TypeDecl target = (TypeDecl) (hostClass());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:191
    if (decl() != null && !decl().parametersDecl().equals(parametersDecl())) {
      {
        TypeDecl target = (TypeDecl) (hostClass());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_TypeDecl_attributeProblems(_root, _map);
  }
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_TypeDecl_attributeProblems(Collection<Problem> collection) {
    super.contributeTo_TypeDecl_attributeProblems(collection);
    if (decl() == null) {
      collection.add(errorf("synthesized attribute %s assigned in class %s is not declared",
                name(), hostClass().name()));
    }
    if (decl() != null && prevEq() != null && prevEq() != this) {
      collection.add(errorf(
                "multiple equations for synthesized attribute %s.%s, previously defined in %s:%d",
                hostClass().name(), name(), prevEq().getFileName(), prevEq().getStartLine()));
    }
    if (decl() != null && !decl().parametersDecl().equals(parametersDecl())) {
      collection.add(errorf(
                "equation must have the same parameter names as attribute declaration in %s:%d",
                decl().getFileName(), decl().getStartLine()));
    }
  }
}
