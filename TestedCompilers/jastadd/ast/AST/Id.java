/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package org.jastadd.ast.AST;
import org.jastadd.ast.AST.*;
import java.util.*;
import org.jastadd.Problem;
import org.jastadd.jrag.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jastadd.jrag.AST.ASTExpression;
import org.jastadd.jrag.Unparser;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.io.PrintStream;
import java.io.*;
import java.util.regex.*;
import org.jastadd.JastAdd;
import org.jastadd.jrag.AST.ASTAspectMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectRefineMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectFieldDeclaration;
import org.jastadd.jrag.AST.ASTBlock;
import org.jastadd.jrag.ClassBodyObject;
import org.jastadd.jrag.AST.ASTCompilationUnit;
import org.jastadd.ast.AST.Token;
import org.jastadd.ast.AST.SimpleNode;
import org.jastadd.Configuration;
import org.jastadd.tinytemplate.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Ast.ast:82
 * @production Id : {@link ASTNode} ::= <span class="component">[{@link NameNode}]</span> <span class="component">{@link IdUse}</span>;

 */
public class Id extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect JastAddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JastAddCodeGen.jadd:184
   */
  public String componentString() {
    String id = "{@link " + getIdUse().getID() + "}";
    if (hasNameNode()) {
      return getNameNode().getID() + ":" + id;
    }
    return id;
  }
  /**
   * @declaredat ASTNode:1
   */
  public Id(int i) {
    super(i);
  }
  /**
   * @declaredat ASTNode:5
   */
  public Id(Ast p, int i) {
    this(i);
    parser = p;
  }
  /**
   * @declaredat ASTNode:10
   */
  public Id() {
    this(0);
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:19
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
    setChild(new Opt(), 0);
  }
  /**
   * @declaredat ASTNode:23
   */
  public Id(Opt<NameNode> p0, IdUse p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /**
   * @declaredat ASTNode:27
   */
  public void dumpTree(String indent, java.io.PrintStream out) {
    out.print(indent + "Id");
    String childIndent = indent + "  ";
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).dumpTree(childIndent, out);
    }
  }
  /**
   * @declaredat ASTNode:34
   */
  public Object jjtAccept(AstVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
  /**
   * @declaredat ASTNode:37
   */
  public void jjtAddChild(Node n, int i) {
    checkChild(n, i);
    super.jjtAddChild(n, i);
  }
  /**
   * @declaredat ASTNode:41
   */
  public void checkChild(Node n, int i) {
    if (i == 0) {
      if (!(n instanceof Opt)) {
        throw new Error("Child number 0 of Id has the type " +
            n.getClass().getName() + " which is not an instance of Opt");
      }
      if (((Opt) n).getNumChildNoTransform() != 0 && !(((Opt) n).getChildNoTransform(0) instanceof NameNode)) {
        throw new Error("Optional name() has the type " +
            ((Opt) n).getChildNoTransform(0).getClass().getName() +
            " which is not an instance of NameNode");
      }
    }
    if (i == 1 && !(n instanceof IdUse)) {
     throw new Error("Child number 1 of Id has the type " +
       n.getClass().getName() + " which is not an instance of IdUse");
    }
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:59
   */
  @SideEffect.Pure public int getNumChild() {
    return 2;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:65
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:69
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:73
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:77
   */
  @SideEffect.Fresh public Id clone() throws CloneNotSupportedException {
    Id node = (Id) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:82
   */
  @SideEffect.Fresh(group="_ASTNode") public Id copy() {
    try {
      Id node = (Id) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:101
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Id fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:111
   */
  @SideEffect.Fresh(group="_ASTNode") public Id treeCopyNoTransform() {
    Id tree = (Id) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:131
   */
  @SideEffect.Fresh(group="_ASTNode") public Id treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:136
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the optional node for the NameNode child. This is the <code>Opt</code>
   * node containing the child NameNode, not the actual child!
   * @param opt The new node to be used as the optional node for the NameNode child.
   * @apilevel low-level
   */
  public void setNameNodeOpt(Opt<NameNode> opt) {
    setChild(opt, 0);
  }
  /**
   * Replaces the (optional) NameNode child.
   * @param node The new node to be used as the NameNode child.
   * @apilevel high-level
   */
  public void setNameNode(NameNode node) {
    getNameNodeOpt().setChild(node, 0);
  }
  /**
   * Check whether the optional NameNode child exists.
   * @return {@code true} if the optional NameNode child exists, {@code false} if it does not.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasNameNode() {
    return getNameNodeOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) NameNode child.
   * @return The NameNode child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   */
  @SideEffect.Pure public NameNode getNameNode() {
    return (NameNode) getNameNodeOpt().getChild(0);
  }
  /**
   * Retrieves the optional node for the NameNode child. This is the <code>Opt</code> node containing the child NameNode, not the actual child!
   * @return The optional node for child the NameNode child.
   * @apilevel low-level
   */
  @ASTNodeAnnotation.OptChild(name="NameNode")
  @SideEffect.Pure public Opt<NameNode> getNameNodeOpt() {
    return (Opt<NameNode>) getChild(0);
  }
  /**
   * Retrieves the optional node for child NameNode. This is the <code>Opt</code> node containing the child NameNode, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child NameNode.
   * @apilevel low-level
   */
  @SideEffect.Pure public Opt<NameNode> getNameNodeOptNoTransform() {
    return (Opt<NameNode>) getChildNoTransform(0);
  }
  /**
   * Replaces the IdUse child.
   * @param node The new node to replace the IdUse child.
   * @apilevel high-level
   */
  public void setIdUse(IdUse node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the IdUse child.
   * @return The current node used as the IdUse child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="IdUse")
  @SideEffect.Pure public IdUse getIdUse() {
    return (IdUse) getChild(1);
  }
  /**
   * Retrieves the IdUse child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the IdUse child.
   * @apilevel low-level
   */
  @SideEffect.Pure public IdUse getIdUseNoTransform() {
    return (IdUse) getChildNoTransform(1);
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int name_visited = -1;
  /**
   * @attribute syn
   * @aspect Names
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:112
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Names", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:112")
  @SideEffect.Pure(group="_ASTNode") public String name() {
    if (name_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Id.name().");
    }
    name_visited = state().boundariesCrossed;
    String name_value = hasNameNode() ?  getNameNode().name() : getIdUse().name();
    name_visited = -1;
    return name_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int type_visited = -1;
  /**
   * @attribute syn
   * @aspect Types
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:129
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Types", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:129")
  @SideEffect.Pure(group="_ASTNode") public String type() {
    if (type_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Id.type().");
    }
    type_visited = state().boundariesCrossed;
    String type_value = getIdUse().name();
    type_visited = -1;
    return type_value;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
