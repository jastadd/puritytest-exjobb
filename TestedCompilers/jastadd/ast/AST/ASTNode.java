/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package org.jastadd.ast.AST;
import org.jastadd.ast.AST.*;
import java.util.*;
import org.jastadd.Problem;
import org.jastadd.jrag.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jastadd.jrag.AST.ASTExpression;
import org.jastadd.jrag.Unparser;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.io.PrintStream;
import java.io.*;
import java.util.regex.*;
import org.jastadd.JastAdd;
import org.jastadd.jrag.AST.ASTAspectMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectRefineMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectFieldDeclaration;
import org.jastadd.jrag.AST.ASTBlock;
import org.jastadd.jrag.ClassBodyObject;
import org.jastadd.jrag.AST.ASTCompilationUnit;
import org.jastadd.ast.AST.Token;
import org.jastadd.ast.AST.SimpleNode;
import org.jastadd.Configuration;
import org.jastadd.tinytemplate.*;
/**
 * @ast node
 * @production ASTNode;

 */
@SideEffect.Entity public class ASTNode<T extends ASTNode> extends SimpleNode implements Cloneable {
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:76
   */
  public void refineError(String what, String file, int line) {
    grammar().error("can not find " + what + " to refine\n"
        + "Warning: implicitly generated methods and equations must be refined"
        + " without an explicit aspect name", file, line);
  }
  /**
   * Escape backslashes and quote characters so that the file name string can
   * be inserted into a documentation comment.
   * 
   * This avoids creating unintentional (and possibly illegal) unicode escape
   * sequences.
   * 
   * @param fileName The file name to escape
   * @return The escaped file name
   * @aspect FileNameEscape
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\FileNameEscape.jrag:47
   */
  public static String escapedFileName(String fileName) {
    return fileName.replace("\\", "\\\\").replace("\"", "\\\"");
  }
  /**
   * Generate the declaredat documentation tag. If no file name is available
   * then no tag is generated.
   * 
   * @param fileName the name of the source file containing the declaration
   * @param line the line number in the source file where the declaration occurs
   * @return the declaredat tag
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:61
   */
  public static String declaredat(String fileName, int line) {
    fileName = fileName.trim();
    if (fileName.length() == 0) {
      return "";
    } else {
      return "@declaredat " + sourceLocation(fileName, line);
    }
  }
  /**
   * @return a formatted source location with escaped filename suitable to be
   * used in documentation comments.
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:74
   */
  protected static String sourceLocation(String fileName, int line) {
    // Replace backslashes in the file path with slashes in order to avoid
    // unintentional (and possibly illegal) unicode escape sequences.
    return String.format("%s:%d", escapedFileName(fileName), line);
  }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:268
   */
  public static String boxedType(String type) {
    if (type.equals("int")) {
      return "Integer";
    } else if (type.equals("short")) {
      return "Short";
    } else if (type.equals("long")) {
      return "Long";
    } else if (type.equals("float")) {
      return "Float";
    } else if (type.equals("double")) {
      return "Double";
    } else if (type.equals("boolean")) {
      return "Boolean";
    } else if (type.equals("char")) {
      return "Character";
    } else if (type.equals("byte")) {
      return "Byte";
    } else {
      return type;
    }
  }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:290
   */
  public String fromReferenceType(String value, String type) {
    return "((" + boxedType(type) + ")" + value + ")";
  }
  /**
   * @aspect AttributeNamesAndTypes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:43
   */
  public static String convTypeNameToSignature(String s) {
    s = s.replace('.', '_');
    s = s.replace(' ', '_');
    s = s.replace(',', '_');
    s = s.replace('<', '_');
    s = s.replace('>', '_');
    s = s.replace('[', '_');
    s = s.replace('?', '_');
    s = s.replace(']', 'a');
    return s;
  }
  /**
   * @declaredat ASTNode:1
   */
  public ASTNode(int i) {
    super(i);
    init$Children();
  }
  /**
   * @declaredat ASTNode:6
   */
  public ASTNode(Ast p, int i) {
    this(i);
    parser = p;
  }
  /**
   * @declaredat ASTNode:11
   */
  public ASTNode() {
    this(0);
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:20
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /**
   * @declaredat ASTNode:22
   */
  public void dumpTree(String indent, java.io.PrintStream out) {
    out.print(indent + "ASTNode");
    String childIndent = indent + "  ";
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).dumpTree(childIndent, out);
    }
  }
  /**
   * @declaredat ASTNode:29
   */
  public Object jjtAccept(AstVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
  /**
   * @declaredat ASTNode:32
   */
  public void jjtAddChild(Node n, int i) {
    checkChild(n, i);
    if (i >= numChildren) {
      numChildren = i+1;
    }
    super.jjtAddChild(n, i);
  }
  /**
   * @declaredat ASTNode:39
   */
  public void checkChild(Node n, int i) {
  }
  /**
   * Cached child index. Child indices are assumed to never change (AST should
   * not change after construction).
   * @apilevel internal
   * @declaredat ASTNode:46
   */
  @SideEffect.Secret(group="_ASTNode")  private int childIndex = -1;
  /** @apilevel low-level 
   * @declaredat ASTNode:49
   */
  @SideEffect.Ignore public int getIndexOfChild(ASTNode node) {
    if (node == null) {
      return -1;
    }
    // Legacy rewrites with rewrite in list can change child position, so update may be needed.
    if (node.childIndex >= 0 && node.childIndex < numChildren && node == children[node.childIndex]) {
      return node.childIndex;
    }
    for (int i = 0; children != null && i < children.length; i++) {
      if (children[i] == node) {
        node.childIndex = i;
        return i;
      }
    }
    return -1;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:67
   */
  @SideEffect.Secret(group="_ASTNode") public static final boolean generatedWithCacheCycle = true;
  /** @apilevel internal 
   * @declaredat ASTNode:70
   */
  @SideEffect.Secret(group="_ASTNode") public static final boolean generatedWithComponentCheck = false;
  /** @apilevel internal 
   * @declaredat ASTNode:75
   */
  @SideEffect.Secret(group="_ASTNode") private static ASTState state = new ASTState();
  /** @apilevel internal 
   * @declaredat ASTNode:78
   */
  @SideEffect.Pure(group="_ASTNode") public final ASTState state() {
    return state;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:83
   */
  public boolean in$Circle = false;
  /** @apilevel internal 
   * @declaredat ASTNode:86
   */
  @SideEffect.Ignore public boolean in$Circle() {
    return in$Circle;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:91
   */
  @SideEffect.Ignore public void in$Circle(boolean b) {
    in$Circle = b;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:95
   */
  public boolean is$Final = false;
  /** @apilevel internal 
   * @declaredat ASTNode:98
   */
  @SideEffect.Ignore public boolean is$Final() {
    return is$Final;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:103
   */
  public void is$Final(boolean b) {
    is$Final = b;
  }
  /**
   * @return an iterator that can be used to iterate over the children of this node.
   * The iterator does not allow removing children.
   * @declaredat ASTNode:111
   */
  @SideEffect.Fresh public java.util.Iterator<T> astChildIterator() {
    return new java.util.Iterator<T>() {
      private int index = 0;

      @Override
      public boolean hasNext() {
        return index < getNumChild();
      }

      @Override
      public T next() {
        return hasNext() ? (T) getChild(index++) : null;
      }

      @Override
      public void remove() {
        throw new UnsupportedOperationException();
      }
    };
  }
  /** @return an object that can be used to iterate over the children of this node 
   * @declaredat ASTNode:133
   */
  @SideEffect.Fresh public Iterable<T> astChildren() {
    return new Iterable<T>() {
      @Override
      public java.util.Iterator<T> iterator() {
        return astChildIterator();
      }
    };
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:143
   */
  @SideEffect.Pure(Ignore=true) public T getChild(int i) {
    ASTNode node = this.getChildNoTransform(i);
    if (node == null) {
      return null;
    }
    if (node.is$Final()) {
      return (T) node;
    }
    if (!node.mayHaveRewrite()) {
      node.is$Final(this.is$Final());
      return (T) node;
    }
    if (!node.in$Circle()) {
      int rewriteState;
      int _boundaries = state().boundariesCrossed;
      do {
        state().push(ASTState.REWRITE_CHANGE);
        ASTNode oldNode = node;
        oldNode.in$Circle(true);
        node = node.rewriteTo();
        if (node != oldNode) {
          this.setChild(node, i);
        }
        oldNode.in$Circle(false);
        rewriteState = state().pop();
      } while(rewriteState == ASTState.REWRITE_CHANGE);
      if (rewriteState == ASTState.REWRITE_NOCHANGE && this.is$Final()) {
        node.is$Final(true);
        state().boundariesCrossed = _boundaries;
      } else {
      }
    } else if (this.is$Final() != node.is$Final()) {
      state().boundariesCrossed++;
    } else {
    }
    return (T) node;

  }
  /** @apilevel low-level 
   * @declaredat ASTNode:182
   */
  @SideEffect.Local public void addChild(T node) {
    setChild(node, getNumChildNoTransform());
  }
  /**
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @apilevel low-level
   * @declaredat ASTNode:189
   */
  @SideEffect.Pure(group="_ASTNode") public final T getChildNoTransform(int i) {
    if (children == null) {
      return null;
    }
    T child = (T)children[i];
    return child;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:197
   */
  protected int numChildren;
  /** @apilevel low-level 
   * @declaredat ASTNode:200
   */
  @SideEffect.Pure protected int numChildren() {
    return numChildren;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:205
   */
  @SideEffect.Pure public int getNumChild() {
    return numChildren();
  }
  /**
   * Behaves like getNumChild, but does not invoke AST transformations (rewrites).
   * @apilevel low-level
   * @declaredat ASTNode:213
   */
  @SideEffect.Pure public final int getNumChildNoTransform() {
    return numChildren();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:217
   */
  @SideEffect.Ignore public void setChild(ASTNode node, int i) {
    if (children == null) {
      children = new ASTNode[(i + 1 > 4 || !(this instanceof List)) ? i + 1 : 4];
    } else if (i >= children.length) {
      ASTNode c[] = new ASTNode[i << 1];
      System.arraycopy(children, 0, c, 0, children.length);
      children = c;
    }
    children[i] = node;
    if (i >= numChildren) {
      numChildren = i+1;
    }
    if (node != null) {
      node.setParent(this);
      node.childIndex = i;
    }
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:235
   */
  @SideEffect.Ignore @SideEffect.Local public void insertChild(ASTNode node, int i) {
    if (children == null) {
      children = new ASTNode[(i + 1 > 4 || !(this instanceof List)) ? i + 1 : 4];
      children[i] = node;
    } else {
      ASTNode c[] = new ASTNode[children.length + 1];
      System.arraycopy(children, 0, c, 0, i);
      c[i] = node;
      if (i < children.length) {
        System.arraycopy(children, i, c, i+1, children.length-i);
        for(int j = i+1; j < c.length; ++j) {
          if (c[j] != null) {
            c[j].childIndex = j;
          }
        }
      }
      children = c;
    }
    numChildren++;
    if (node != null) {
      node.setParent(this);
      node.childIndex = i;
    }
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:260
   */
  @SideEffect.Ignore @SideEffect.Local public void removeChild(int i) {
    if (children != null) {
      ASTNode child = (ASTNode) children[i];
      if (child != null) {
        child.parent = null;
        child.childIndex = -1;
      }
      // Adding a check of this instance to make sure its a List, a move of children doesn't make
      // any sense for a node unless its a list. Also, there is a problem if a child of a non-List node is removed
      // and siblings are moved one step to the right, with null at the end.
      if (this instanceof List || this instanceof Opt) {
        System.arraycopy(children, i+1, children, i, children.length-i-1);
        children[children.length-1] = null;
        numChildren--;
        // fix child indices
        for(int j = i; j < numChildren; ++j) {
          if (children[j] != null) {
            child = (ASTNode) children[j];
            child.childIndex = j;
          }
        }
      } else {
        children[i] = null;
      }
    }
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:287
   */
  @SideEffect.Pure(group="_ASTNode") public ASTNode getParent() {
    if (parent != null && ((ASTNode) parent).is$Final() != is$Final()) {
      state().boundariesCrossed++;
    }
    return (ASTNode) parent;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:294
   */
  @SideEffect.Pure(group="_ASTNode") public void setParent(ASTNode node) {
    parent = node;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:427
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:431
   */
  @SideEffect.Ignore public void flushTreeCache() {
    flushCache();
    if (children != null) {
      for (int i = 0; i < children.length; i++) {
        if (children[i] != null && ((ASTNode) children[i]).is$Final) {
          ((ASTNode) children[i]).flushTreeCache();
        }
      }
    }
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:442
   */
  @SideEffect.Ignore public void flushCache() {
    flushAttrAndCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:446
   */
  @SideEffect.Ignore public void flushAttrAndCollectionCache() {
    flushAttrCache();
    flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:451
   */
  @SideEffect.Ignore public void flushAttrCache() {
    templateContext_reset();
    grammar_reset();
    config_reset();
    parentContext_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:458
   */
  @SideEffect.Ignore public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:461
   */
  @SideEffect.Fresh public ASTNode<T> clone() throws CloneNotSupportedException {
    ASTNode node = (ASTNode) super.clone();
    if (node.is$Final()) {
      node.flushAttrAndCollectionCache();
    }
    node.in$Circle(false);
    // flush rewrites
    node.is$Final(false);
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:472
   */
  @SideEffect.Fresh(group="_ASTNode") public ASTNode<T> copy() {
    try {
      ASTNode node = (ASTNode) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:491
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public ASTNode<T> fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:501
   */
  @SideEffect.Fresh(group="_ASTNode") public ASTNode<T> treeCopyNoTransform() {
    ASTNode tree = (ASTNode) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:521
   */
  @SideEffect.Fresh(group="_ASTNode") public ASTNode<T> treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /**
   * Performs a full traversal of the tree using getChild to trigger rewrites
   * @apilevel low-level
   * @declaredat ASTNode:529
   */
  @SideEffect.Pure public void doFullTraversal() {
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).doFullTraversal();
    }
  }
  /** @apilevel internal 
   * @declaredat ASTNode:535
   */
  @SideEffect.Pure protected boolean is$Equal(ASTNode n1, ASTNode n2) {
    if (n1 == null && n2 == null) return true;
    if (n1 == null || n2 == null) return false;
    return n1.is$Equal(n2);
  }
  /** @apilevel internal 
   * @declaredat ASTNode:541
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    if (getClass() != node.getClass()) {
      return false;
    }
    if (numChildren != node.numChildren) {
      return false;
    }
    for (int i = 0; i < numChildren; i++) {
      if (children[i] == null && node.children[i] != null) {
        return false;
      }
      if (!((ASTNode)children[i]).is$Equal(((ASTNode)node.children[i]))) {
        return false;
      }
    }
    return true;
  }
  /**
   * @aspect <NoAspect>
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTErrors.jrag:42
   */
    @SideEffect.Local(group="_ASTNode") protected void collect_contributors_Grammar_problems(Grammar _root, @SideEffect.Local java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).collect_contributors_Grammar_problems(_root, _map);
    }
  }
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_Grammar_problems(Collection<Problem> collection) {
  }

  /**
   * @aspect <NoAspect>
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:91
   */
    @SideEffect.Local(group="_ASTNode") protected void collect_contributors_TypeDecl_attributeProblems(TypeDecl _root, @SideEffect.Local java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).collect_contributors_TypeDecl_attributeProblems(_root, _map);
    }
  }
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_TypeDecl_attributeProblems(Collection<Problem> collection) {
  }

  /**
   * @aspect <NoAspect>
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:515
   */
    @SideEffect.Local(group="_ASTNode") protected void collect_contributors_CollDecl_uses(Grammar _root, @SideEffect.Local java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).collect_contributors_CollDecl_uses(_root, _map);
    }
  }
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_CollDecl_uses(HashSet collection) {
  }

/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int templateContext_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void templateContext_reset() {
    templateContext_computed = false;
    
    templateContext_value = null;
    templateContext_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean templateContext_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected TemplateContext templateContext_value;

  /**
   * @attribute syn
   * @aspect TemplateUtil
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\TemplateUtil.jrag:37
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="TemplateUtil", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\TemplateUtil.jrag:37")
  @SideEffect.Pure(group="_ASTNode") public TemplateContext templateContext() {
    ASTState state = state();
    if (templateContext_computed) {
      return templateContext_value;
    }
    if (templateContext_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTNode.templateContext().");
    }
    templateContext_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    templateContext_value = templateContext_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    templateContext_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    templateContext_visited = -1;
    return templateContext_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private TemplateContext templateContext_compute() {
      return new SimpleContext(parentContext(), this);
    }
  /**
   * Provides a reference to the grammar root node for all
   * nodes in the AST.
   * @attribute inh
   * @aspect Configuration
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\Options.jrag:42
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="Configuration", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\Options.jrag:42")
  @SideEffect.Pure(group="_ASTNode") public Grammar grammar() {
    ASTState state = state();
    if (grammar_computed) {
      return grammar_value;
    }
    if (grammar_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTNode.grammar().");
    }
    grammar_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    grammar_value = getParent().Define_grammar(this, null);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    grammar_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    grammar_visited = -1;
    return grammar_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int grammar_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void grammar_reset() {
    grammar_computed = false;
    
    grammar_value = null;
    grammar_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean grammar_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Grammar grammar_value;

  /**
   * Provides a reference to the JastAdd configuration for all
   * nodes in the AST.
   * @attribute inh
   * @aspect Configuration
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\Options.jrag:56
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="Configuration", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\Options.jrag:56")
  @SideEffect.Pure(group="_ASTNode") public Configuration config() {
    ASTState state = state();
    if (config_computed) {
      return config_value;
    }
    if (config_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTNode.config().");
    }
    config_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    config_value = getParent().Define_config(this, null);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    config_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    config_visited = -1;
    return config_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int config_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void config_reset() {
    config_computed = false;
    
    config_value = null;
    config_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean config_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Configuration config_value;

  /**
   * @attribute inh
   * @aspect TemplateUtil
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\TemplateUtil.jrag:33
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="TemplateUtil", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\TemplateUtil.jrag:33")
  @SideEffect.Pure(group="_ASTNode") public TemplateContext parentContext() {
    ASTState state = state();
    if (parentContext_computed) {
      return parentContext_value;
    }
    if (parentContext_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTNode.parentContext().");
    }
    parentContext_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    parentContext_value = getParent().Define_parentContext(this, null);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    parentContext_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    parentContext_visited = -1;
    return parentContext_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int parentContext_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void parentContext_reset() {
    parentContext_computed = false;
    
    parentContext_value = null;
    parentContext_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean parentContext_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected TemplateContext parentContext_value;

  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    if (state().peek() == ASTState.REWRITE_CHANGE) {
      state().pop();
      state().push(ASTState.REWRITE_NOCHANGE);
    }
    return this;
  }
  /** @apilevel internal */
 @SideEffect.Pure public Collection<ASTDecl> Define_findSubclasses(ASTNode _callerNode, ASTNode _childNode, ASTDecl target) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_findSubclasses(self, _callerNode, target)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_findSubclasses(self, _callerNode, target);
  }

  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:112
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute findSubclasses
   */
  @SideEffect.Pure protected boolean canDefine_findSubclasses(ASTNode _callerNode, ASTNode _childNode, ASTDecl target) {
    return false;
  }
  /** @apilevel internal */
 @SideEffect.Pure public RegionDecl Define_lookupRegionDecl(ASTNode _callerNode, ASTNode _childNode, String name) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_lookupRegionDecl(self, _callerNode, name)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_lookupRegionDecl(self, _callerNode, name);
  }

  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:138
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute lookupRegionDecl
   */
  @SideEffect.Pure protected boolean canDefine_lookupRegionDecl(ASTNode _callerNode, ASTNode _childNode, String name) {
    return false;
  }
  /** @apilevel internal */
 @SideEffect.Pure public Grammar Define_grammar(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_grammar(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_grammar(self, _callerNode);
  }

  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\Options.jrag:43
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute grammar
   */
  @SideEffect.Pure protected boolean canDefine_grammar(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
 @SideEffect.Pure public Configuration Define_config(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_config(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_config(self, _callerNode);
  }

  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\Options.jrag:57
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute config
   */
  @SideEffect.Pure protected boolean canDefine_config(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
 @SideEffect.Pure public TemplateContext Define_parentContext(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_parentContext(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_parentContext(self, _callerNode);
  }

  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\core\\TemplateUtil.jrag:35
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute parentContext
   */
  @SideEffect.Pure protected boolean canDefine_parentContext(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
 @SideEffect.Pure public TypeDecl Define_hostClass(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_hostClass(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_hostClass(self, _callerNode);
  }

  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:422
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute hostClass
   */
  @SideEffect.Pure protected boolean canDefine_hostClass(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
public ASTNode rewrittenNode() { throw new Error("rewrittenNode is undefined for ASTNode"); }
}
