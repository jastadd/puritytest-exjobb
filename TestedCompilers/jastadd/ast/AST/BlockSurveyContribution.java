package org.jastadd.ast.AST;

import org.jastadd.ast.AST.*;
import java.util.*;
import org.jastadd.Problem;
import org.jastadd.jrag.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jastadd.jrag.AST.ASTExpression;
import org.jastadd.jrag.Unparser;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.io.PrintStream;
import java.io.*;
import java.util.regex.*;
import org.jastadd.JastAdd;
import org.jastadd.jrag.AST.ASTAspectMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectRefineMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectFieldDeclaration;
import org.jastadd.jrag.AST.ASTBlock;
import org.jastadd.jrag.ClassBodyObject;
import org.jastadd.jrag.AST.ASTCompilationUnit;
import org.jastadd.ast.AST.Token;
import org.jastadd.ast.AST.SimpleNode;
import org.jastadd.Configuration;
import org.jastadd.tinytemplate.*;
/**
 * @ast class
 * @aspect CollectionAttributes
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:260
 */
 class BlockSurveyContribution extends CustomSurveyContribution {
  
    private final String surveyCode;

  

    public BlockSurveyContribution(String collName, String collHost, String surveyCode,
        String fileName, int startLine, int endLine, String comment,
        String aspectName) {
      super(collName, collHost, fileName, startLine, endLine, comment, aspectName);
      this.surveyCode = surveyCode;
    }

  

    @Override
    public void emitSurveyCode(CollDecl decl, PrintStream out) {
      out.println(comment);
      String replacement;
      if (decl.onePhase()) {
        replacement = String.format(".collect_contributors_%s(_root);", decl.collectionId());
      } else {
        replacement = String.format(".collect_contributors_%s(_root, _map);", decl.collectionId());
      }
      out.println(surveyCode.replaceAll("\\.collectContributions\\(\\);", replacement));
    }


}
