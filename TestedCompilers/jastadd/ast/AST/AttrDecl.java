/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package org.jastadd.ast.AST;
import org.jastadd.ast.AST.*;
import java.util.*;
import org.jastadd.Problem;
import org.jastadd.jrag.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jastadd.jrag.AST.ASTExpression;
import org.jastadd.jrag.Unparser;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.io.PrintStream;
import java.io.*;
import java.util.regex.*;
import org.jastadd.JastAdd;
import org.jastadd.jrag.AST.ASTAspectMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectRefineMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectFieldDeclaration;
import org.jastadd.jrag.AST.ASTBlock;
import org.jastadd.jrag.ClassBodyObject;
import org.jastadd.jrag.AST.ASTCompilationUnit;
import org.jastadd.ast.AST.Token;
import org.jastadd.ast.AST.SimpleNode;
import org.jastadd.Configuration;
import org.jastadd.tinytemplate.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Ast.ast:25
 * @production AttrDecl : {@link ASTNode} ::= <span class="component">{@link Parameter}*</span> <span class="component">&lt;Name:String&gt;</span> <span class="component">&lt;Type:String&gt;</span> <span class="component">&lt;CacheMode:CacheMode&gt;</span> <span class="component">&lt;FileName:String&gt;</span> <span class="component">&lt;StartLine:int&gt;</span> <span class="component">&lt;EndLine:int&gt;</span> <span class="component">&lt;Final:boolean&gt;</span> <span class="component">&lt;NTA:boolean&gt;</span> <span class="component">&lt;Comment:String&gt;</span> <span class="component">&lt;AspectName:String&gt;</span> <span class="component">{@link Annotation}*</span>;

 */
public abstract class AttrDecl extends ASTNode<ASTNode> implements Cloneable {
  /**
   * Create a new error object with relevant file name and line number.
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:156
   */
  public Problem errorf(String messagefmt, Object... args) {
    return error(String.format(messagefmt, args));
  }
  /** Create a new warning with the relevant file name and line number. 
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:169
   */
  public Problem warningf(String messagefmt, Object... args) {
    return warning(String.format(messagefmt, args));
  }
  /**
   * @aspect Circular
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Circular.jrag:31
   */
  String bottomValue = "";
  /**
   * @aspect Circular
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Circular.jrag:52
   */
  public void setBottomValue(String expression) {
    bottomValue = expression;
  }
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:128
   */
  public void emitCircularCollectionEval(PrintStream out) {
    TemplateContext tt = templateContext();
    tt.bind("BottomValue", getBottomValue());
    tt.bind("ChangeCondition", valueComparisonExpr("new_" + signature() + "_value",
        signature() + "_value"));
    tt.bind("CircularComputeRhs", name() + "_compute()");
    tt.bind("TracePrintReturnNewValue", tracePrintReturnNewValue(signature() + "_value"));
    tt.bind("TracePrintReturnPreviousValue", tracePrintReturnPreviousValue(signature() + "_value"));
    tt.expand("AttrDecl.circularEquation:unparameterized", out);
  }
  /**
   * @aspect Comments
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Comments.jrag:185
   */
  public String docComment() {
    JavaDocParser parser = new JavaDocParser();
    TemplateContext tt = templateContext();
    tt.bind("SourceComment", parser.parse(getComment()));
    return tt.expand("AttrDecl.docComment");
  }
  /**
   * @aspect Flush
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Flush.jrag:54
   */
  public void emitResetMethod(PrintStream out) {
    if (isLazy() || isCircular()) {
      templateContext().expand("AttrDecl.resetMethod", out);
    }
  }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:189
   */
  public void emitSynDecl(PrintStream out) {
    templateContext().expand("AttrDecl.synDecl", out);
  }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:193
   */
  public void emitAbstractSynDecl(PrintStream out) {
    templateContext().expand("AttrDecl.abstractSynDecl", out);
  }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:218
   */
  public String initLazyMaps() {
    StringBuilder sb = new StringBuilder();
    if (config().lazyMaps()) {
      if (!isCircular()) {
        if (getNumParameter() != 0 && config().visitCheckEnabled() && config().legacyRewrite()) {
          sb.append(String.format("if (%s_visited == null) %s_visited = %s;\n",
              signature(), signature(), config().createDefaultMap()));
        } else if (getNumParameter() != 0 && config().visitCheckEnabled()) {
          sb.append(String.format("if (%s_visited == null) %s_visited = %s;\n",
              signature(), signature(), config().createDefaultSet()));
        }
        if (getNumParameter() != 0 && isLazy() && !simpleCacheCheck()) {
          sb.append(String.format("if (%s_computed == null) %s_computed = %s;\n",
              signature(), signature(), config().createDefaultMap()));
        }
      }
      if (getNumParameter() != 0 && (isLazy() || isCircular())) {
        sb.append(String.format("if (%s_values == null) %s_values = %s;\n",
            signature(), signature(), config().createDefaultMap()));
      }
    }
    return sb.toString();
  }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:242
   */
  public void emitVisitedDeclarations(PrintStream out) {
    if (isCircular()) {
      templateContext().expand("AttrDecl.cycleDeclaration", out);
    } else {
      if (config().visitCheckEnabled()) {
        templateContext().expand("AttrDecl.visitedDeclaration", out);
      }
      if (config().componentCheck()) {
        templateContext().expand("AttrDecl.componentCheckDeclaration", out);
      }
    }
  }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:255
   */
  public void emitCacheDeclarations(PrintStream out) {
    templateContext().expand("AttrDecl.cacheDeclarations", out);
  }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:298
   */
  static public boolean isPrimitiveType(String type) {
    return type.equals("int") || type.equals("short") || type.equals("long")
        || type.equals("float") || type.equals("double") || type.equals("boolean")
        || type.equals("char") || type.equals("byte");
  }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:306
   */
  public String parameterStructure() {
    if (!isParameterized() || (!isLazy() && !isCircular() && !config().visitCheckEnabled())) {
      return "";
    } else if (getNumParameter() == 1) {
      return "Object _parameters = " + getParameter(0).getName() + ";\n";
    } else {
      StringBuilder sb = new StringBuilder();
      sb.append("java.util.List _parameters = new java.util.ArrayList("
          + getNumParameter() + ");\n");
      for (int i = 0; i < getNumParameter(); i++) {
        sb.append("_parameters.add(" + getParameter(i).getName() + ");\n");
      }
      return sb.toString();
    }
  }
  /**
   * Generate the state variable if needed.
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:325
   */
  public String lazyState() {
    if (isLazy()) {
      return config().stateClassName() + " state = state();";
    } else {
      return "";
    }
  }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:333
   */
  public String cacheStoreCondition() {
    // This cache store condition is only used with the old (legacy) rewrite implementation.
    // It is kept for backward compatibility.
    if (!config().rewriteEnabled() || getFinal()) {
      return "true";
    } else {
      return "isFinal && _boundaries == state().boundariesCrossed";
    }
  }
  /**
   * @return Compute method without "_compute" suffix
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:350
   */
  public void emitInlineComputeWithTry(PrintStream out, AttrEq equ) {
    TemplateContext tt = templateContext();
    tt.bind("ParamDecl", equ.parametersDecl());
    tt.bind("ComputeBody", equ.computeCode());
    tt.expand("AttrDecl.emitInlineComputeWithTry", out);
  }
  /**
   * @return Compute method without "_compute" suffix
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:360
   */
  public void emitInlineComputeWithoutTry(PrintStream out, AttrEq equ) {
    TemplateContext tt = templateContext();
    tt.bind("ParamDecl", equ.parametersDecl());
    tt.bind("ComputeBody", equ.computeCode());
    tt.expand("AttrDecl.emitInlineComputeWithoutTry", out);
  }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:392
   */
  public void emitEquation(PrintStream out, String paramDecl) {
    TemplateContext tt = templateContext();
    tt.bind("ParamDecl", paramDecl);
    tt.expand("AttrDecl.emitEquation", out);
  }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:398
   */
  public void emitCircularEquationNoParams(PrintStream out) {
    TemplateContext tt = templateContext();
    tt.bind("ChangeCondition", valueComparisonExpr("new_" + signature()
        + "_value", signature() + "_value"));
    tt.bind("BottomValue", getBottomValue());
    tt.bind("TracePrintReturnNewValue", tracePrintReturnNewValue(signature() + "_value"));
    tt.bind("TracePrintReturnPreviousValue", tracePrintReturnPreviousValue(signature() + "_value"));
    tt.expand("AttrDecl.circularEquation:unparameterized", out);
  }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:408
   */
  public void emitCircularEquationWithParams(PrintStream out, String paramDecl) {
    TemplateContext tt = templateContext();
    tt.bind("BottomValue", getBottomValue());
    tt.bind("ChangeCondition", valueComparisonExpr("new_" + signature() + "_value",
        fromReferenceType("_value.value", getType())));
    tt.bind("ParamDecl", paramDecl);
    tt.bind("TracePrintReturnNewValue", tracePrintReturnNewValue("new_" + signature() + "_value"));
    tt.bind("TracePrintReturnPreviousValue", tracePrintReturnPreviousValue(
          fromReferenceType(signature() + "_values.get(_parameters)" , getType())));
    tt.expand("AttrDecl.circularEquation:parameterized", out);
  }
  /**
   * Generates the method to compute an attribute with a specific equation
   * @param out
   * @param equ the equation to generate code for
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:442
   */
  public void emitAttrEquation(PrintStream out, AttrEq equ) {
    if (equ.canInlineExpression()) {
      if (isCircular()) {
        templateContext().bind("CircularComputeRhs", equ.computeCode());
      } else {
        templateContext().bind("ComputeRhs", equ.computeCode());
      }
      emitAttrEquation(out, equ.parametersDecl());
    } else if (equ.canInlineBlock(this)) {
      if (!declaredNTA() && !config().traceCompute()
          && !config().visitCheckEnabled()
          && !config().componentCheck()) {
        emitInlineComputeWithoutTry(out, equ);
      } else {
        emitInlineComputeWithTry(out, equ);
      }
    } else {
      if (isCircular()) {
        templateContext().bind("CircularComputeRhs", circularComputeRhs());
      } else {
        templateContext().bind("ComputeRhs", computeRhs());
      }
      emitAttrEquation(out, equ.parametersDecl());
      equ.emitComputeMethod(out);
    }
  }
  /**
   * Generates the Java method to evaluate an attribute.
   * @param out
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:473
   */
  public void emitAttrEquation(PrintStream out) {
    if (isCircular()) {
      templateContext().bind("CircularComputeRhs", circularComputeRhs());
    } else {
      templateContext().bind("ComputeRhs", computeRhs());
    }
    emitAttrEquation(out, parametersDecl());
  }
  /**
   * Generates the Java method to evaluate an attribute.
   * @param out
   * @param paramDecl
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:487
   */
  public void emitAttrEquation(PrintStream out, String paramDecl) {
    if (isCircular()) {
      TemplateContext tt = templateContext();
      if (getNumParameter() == 0) {
        emitCircularEquationNoParams(out);
      } else {
        emitCircularEquationWithParams(out, paramDecl);
      }
    } else {
      emitEquation(out, paramDecl);
    }
  }
  /**
   * Generate code to test if two attribute values differ based on the type of the attribute.
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:505
   */
  public String valueComparisonExpr(String oldValue, String newValue) {
    if (isPrimitive() || isCircularRewrite()) {
      return String.format("%s != %s", oldValue, newValue);
    } else if (declaredNTA() && isCircular()) {
      return String.format("!is$Equal(%s, %s)", oldValue, newValue);
    } else {
      return String.format("(%s == null && %s != null) || (%s != null && !%s.equals(%s))",
          oldValue, newValue, oldValue, oldValue, newValue);
    }
  }
  /**
   * @return {@code true} if this attribute matches an NTA component declared
   * in the grammar
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:560
   */
  public boolean isNTA() {
    return false;
  }
  /**
   * @return the index of the NTA in the child array
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:576
   */
  public int indexNTAchild() {
    Component comp = findCorrespondingNTA();
    TypeDecl c = hostClass();
    while (c != null) {
      int index = 0;
      for (Component next : c.components()) {
        if (next == comp) {
          return index;
        }
        if (!(next instanceof TokenComponent)) {
          index++;
        }
      }
      c = c instanceof ASTDecl ? ((ASTDecl)c).superClass() : null;
    }
    return -1;
  }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:594
   */
  public Component findCorrespondingNTA() {
    if (!getName().startsWith("get"))
      return null;
    String attrName = getName().substring(3);
    TypeDecl c = hostClass();
    while (c != null) {
      for (Component comp : c.components()) {
        if (comp.name().equals(attrName) && (
            comp instanceof OptionalComponentNTA
            || comp instanceof TokenComponentNTA
            || comp instanceof AggregateComponentNTA )) {
          return comp;
        }
        if (attrName.equals(comp.name() + "Opt") && comp instanceof OptionalComponentNTA) {
          return comp;
        }
        if (attrName.equals(comp.name() + "List") && comp instanceof ListComponentNTA) {
          return comp;
        }
      }
      c = c instanceof ASTDecl ? ((ASTDecl)c).superClass() : null;
    }
    return null;
  }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:619
   */
  public String higherOrderAttributeCode() {
    return "";
  }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:656
   */
  public void emitInhDecl(PrintStream out) {
    templateContext().expand("AttrDecl.inhDecl", out);
  }
  /**
   * @aspect Trace
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:94
   */
  protected String trace(String s) {
    // TODO(joqvist): remove this method? 
    return "";
  }
  /**
   * @declaredat ASTNode:1
   */
  public AttrDecl(int i) {
    super(i);
  }
  /**
   * @declaredat ASTNode:5
   */
  public AttrDecl(Ast p, int i) {
    this(i);
    parser = p;
  }
  /**
   * @declaredat ASTNode:10
   */
  public AttrDecl() {
    this(0);
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:19
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
    setChild(new List(), 0);
    setChild(new List(), 1);
  }
  /**
   * @declaredat ASTNode:24
   */
  public AttrDecl(List<Parameter> p0, String p1, String p2, CacheMode p3, String p4, int p5, int p6, boolean p7, boolean p8, String p9, String p10, List<Annotation> p11) {
    setChild(p0, 0);
    setName(p1);
    setType(p2);
    setCacheMode(p3);
    setFileName(p4);
    setStartLine(p5);
    setEndLine(p6);
    setFinal(p7);
    setNTA(p8);
    setComment(p9);
    setAspectName(p10);
    setChild(p11, 1);
  }
  /**
   * @declaredat ASTNode:38
   */
  public void dumpTree(String indent, java.io.PrintStream out) {
    out.print(indent + "AttrDecl");
    out.print("\"" + getName() + "\"");
    out.print("\"" + getType() + "\"");
    out.print("\"" + getCacheMode() + "\"");
    out.print("\"" + getFileName() + "\"");
    out.print("\"" + getStartLine() + "\"");
    out.print("\"" + getEndLine() + "\"");
    out.print("\"" + getFinal() + "\"");
    out.print("\"" + getNTA() + "\"");
    out.print("\"" + getComment() + "\"");
    out.print("\"" + getAspectName() + "\"");
    String childIndent = indent + "  ";
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).dumpTree(childIndent, out);
    }
  }
  /**
   * @declaredat ASTNode:55
   */
  public Object jjtAccept(AstVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
  /**
   * @declaredat ASTNode:58
   */
  public void jjtAddChild(Node n, int i) {
    checkChild(n, i);
    super.jjtAddChild(n, i);
  }
  /**
   * @declaredat ASTNode:62
   */
  public void checkChild(Node n, int i) {
      if (i == 0) {
        if (!(n instanceof List)) {
          throw new Error("Child number 0 of AttrDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof Parameter)) {
            throw new Error("Child number " + k + " in ParameterList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of Parameter");
          }
        }
      }
      if (i == 1) {
        if (!(n instanceof List)) {
          throw new Error("Child number 1 of AttrDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof Annotation)) {
            throw new Error("Child number " + k + " in AnnotationList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of Annotation");
          }
        }
      }
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:89
   */
  @SideEffect.Pure public int getNumChild() {
    return 2;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:95
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:99
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    collectionId_reset();
    signature_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:105
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:109
   */
  @SideEffect.Fresh public AttrDecl clone() throws CloneNotSupportedException {
    AttrDecl node = (AttrDecl) super.clone();
    return node;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:120
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public abstract AttrDecl fullCopy();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:128
   */
  @SideEffect.Fresh(group="_ASTNode") public abstract AttrDecl treeCopyNoTransform();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:136
   */
  @SideEffect.Fresh(group="_ASTNode") public abstract AttrDecl treeCopy();
  /**
   * Replaces the Parameter list.
   * @param list The new list node to be used as the Parameter list.
   * @apilevel high-level
   */
  public void setParameterList(List<Parameter> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the Parameter list.
   * @return Number of children in the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumParameter() {
    return getParameterList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Parameter list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Parameter list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumParameterNoTransform() {
    return getParameterListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Parameter list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Parameter getParameter(int i) {
    return (Parameter) getParameterList().getChild(i);
  }
  /**
   * Check whether the Parameter list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasParameter() {
    return getParameterList().getNumChild() != 0;
  }
  /**
   * Append an element to the Parameter list.
   * @param node The element to append to the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addParameter(Parameter node) {
    List<Parameter> list = (parent == null) ? getParameterListNoTransform() : getParameterList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addParameterNoTransform(Parameter node) {
    List<Parameter> list = getParameterListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Parameter list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setParameter(Parameter node, int i) {
    List<Parameter> list = getParameterList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Parameter list.
   * @return The node representing the Parameter list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Parameter")
  @SideEffect.Pure(group="_ASTNode") public List<Parameter> getParameterList() {
    List<Parameter> list = (List<Parameter>) getChild(0);
    return list;
  }
  /**
   * Retrieves the Parameter list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Parameter list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Parameter> getParameterListNoTransform() {
    return (List<Parameter>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the Parameter list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Parameter getParameterNoTransform(int i) {
    return (Parameter) getParameterListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Parameter list.
   * @return The node representing the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Parameter> getParameters() {
    return getParameterList();
  }
  /**
   * Retrieves the Parameter list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Parameter list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Parameter> getParametersNoTransform() {
    return getParameterListNoTransform();
  }
  /**
   * Replaces the lexeme Name.
   * @param value The new value for the lexeme Name.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setName(String value) {
    tokenString_Name = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_Name;
  /**
   * Retrieves the value for the lexeme Name.
   * @return The value for the lexeme Name.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Name")
  @SideEffect.Pure(group="_ASTNode") public String getName() {
    return tokenString_Name != null ? tokenString_Name : "";
  }
  /**
   * Replaces the lexeme Type.
   * @param value The new value for the lexeme Type.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setType(String value) {
    tokenString_Type = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_Type;
  /**
   * Retrieves the value for the lexeme Type.
   * @return The value for the lexeme Type.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Type")
  @SideEffect.Pure(group="_ASTNode") public String getType() {
    return tokenString_Type != null ? tokenString_Type : "";
  }
  /**
   * Replaces the lexeme CacheMode.
   * @param value The new value for the lexeme CacheMode.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setCacheMode(CacheMode value) {
    tokenCacheMode_CacheMode = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected CacheMode tokenCacheMode_CacheMode;
  /**
   * Retrieves the value for the lexeme CacheMode.
   * @return The value for the lexeme CacheMode.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="CacheMode")
  @SideEffect.Pure(group="_ASTNode") public CacheMode getCacheMode() {
    return tokenCacheMode_CacheMode;
  }
  /**
   * Replaces the lexeme FileName.
   * @param value The new value for the lexeme FileName.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setFileName(String value) {
    tokenString_FileName = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_FileName;
  /**
   * Retrieves the value for the lexeme FileName.
   * @return The value for the lexeme FileName.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="FileName")
  @SideEffect.Pure(group="_ASTNode") public String getFileName() {
    return tokenString_FileName != null ? tokenString_FileName : "";
  }
  /**
   * Replaces the lexeme StartLine.
   * @param value The new value for the lexeme StartLine.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setStartLine(int value) {
    tokenint_StartLine = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected int tokenint_StartLine;
  /**
   * Retrieves the value for the lexeme StartLine.
   * @return The value for the lexeme StartLine.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="StartLine")
  @SideEffect.Pure(group="_ASTNode") public int getStartLine() {
    return tokenint_StartLine;
  }
  /**
   * Replaces the lexeme EndLine.
   * @param value The new value for the lexeme EndLine.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setEndLine(int value) {
    tokenint_EndLine = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected int tokenint_EndLine;
  /**
   * Retrieves the value for the lexeme EndLine.
   * @return The value for the lexeme EndLine.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="EndLine")
  @SideEffect.Pure(group="_ASTNode") public int getEndLine() {
    return tokenint_EndLine;
  }
  /**
   * Replaces the lexeme Final.
   * @param value The new value for the lexeme Final.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setFinal(boolean value) {
    tokenboolean_Final = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected boolean tokenboolean_Final;
  /**
   * Retrieves the value for the lexeme Final.
   * @return The value for the lexeme Final.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Final")
  @SideEffect.Pure(group="_ASTNode") public boolean getFinal() {
    return tokenboolean_Final;
  }
  /**
   * Replaces the lexeme NTA.
   * @param value The new value for the lexeme NTA.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setNTA(boolean value) {
    tokenboolean_NTA = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected boolean tokenboolean_NTA;
  /**
   * Retrieves the value for the lexeme NTA.
   * @return The value for the lexeme NTA.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="NTA")
  @SideEffect.Pure(group="_ASTNode") public boolean getNTA() {
    return tokenboolean_NTA;
  }
  /**
   * Replaces the lexeme Comment.
   * @param value The new value for the lexeme Comment.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setComment(String value) {
    tokenString_Comment = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_Comment;
  /**
   * Retrieves the value for the lexeme Comment.
   * @return The value for the lexeme Comment.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Comment")
  @SideEffect.Pure(group="_ASTNode") public String getComment() {
    return tokenString_Comment != null ? tokenString_Comment : "";
  }
  /**
   * Replaces the lexeme AspectName.
   * @param value The new value for the lexeme AspectName.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setAspectName(String value) {
    tokenString_AspectName = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_AspectName;
  /**
   * Retrieves the value for the lexeme AspectName.
   * @return The value for the lexeme AspectName.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="AspectName")
  @SideEffect.Pure(group="_ASTNode") public String getAspectName() {
    return tokenString_AspectName != null ? tokenString_AspectName : "";
  }
  /**
   * Replaces the Annotation list.
   * @param list The new list node to be used as the Annotation list.
   * @apilevel high-level
   */
  public void setAnnotationList(List<Annotation> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the Annotation list.
   * @return Number of children in the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumAnnotation() {
    return getAnnotationList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Annotation list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Annotation list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumAnnotationNoTransform() {
    return getAnnotationListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Annotation list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Annotation getAnnotation(int i) {
    return (Annotation) getAnnotationList().getChild(i);
  }
  /**
   * Check whether the Annotation list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasAnnotation() {
    return getAnnotationList().getNumChild() != 0;
  }
  /**
   * Append an element to the Annotation list.
   * @param node The element to append to the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addAnnotation(Annotation node) {
    List<Annotation> list = (parent == null) ? getAnnotationListNoTransform() : getAnnotationList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addAnnotationNoTransform(Annotation node) {
    List<Annotation> list = getAnnotationListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Annotation list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setAnnotation(Annotation node, int i) {
    List<Annotation> list = getAnnotationList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Annotation list.
   * @return The node representing the Annotation list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Annotation")
  @SideEffect.Pure(group="_ASTNode") public List<Annotation> getAnnotationList() {
    List<Annotation> list = (List<Annotation>) getChild(1);
    return list;
  }
  /**
   * Retrieves the Annotation list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Annotation list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Annotation> getAnnotationListNoTransform() {
    return (List<Annotation>) getChildNoTransform(1);
  }
  /**
   * @return the element at index {@code i} in the Annotation list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Annotation getAnnotationNoTransform(int i) {
    return (Annotation) getAnnotationListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Annotation list.
   * @return The node representing the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Annotation> getAnnotations() {
    return getAnnotationList();
  }
  /**
   * Retrieves the Annotation list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Annotation list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Annotation> getAnnotationsNoTransform() {
    return getAnnotationListNoTransform();
  }
  /**
   * @attribute syn
   * @aspect ASTNodeAnnotations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNodeAnnotations.jrag:30
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ASTNodeAnnotations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNodeAnnotations.jrag:30")
  @SideEffect.Pure(group="_ASTNode") public abstract String annotationKind();
  /**
   * @attribute syn
   * @aspect AttributeKind
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:40
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeKind", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:40")
  @SideEffect.Pure(group="_ASTNode") public abstract String attributeKind();
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int className_visited = -1;
  /**
   * @attribute syn
   * @aspect ComponentHostClass
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:137
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ComponentHostClass", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:137")
  @SideEffect.Pure(group="_ASTNode") public String className() {
    if (className_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.className().");
    }
    className_visited = state().boundariesCrossed;
    String className_value = hostClass().name();
    className_visited = -1;
    return className_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int attributeAnnotationValue_visited = -1;
  /**
   * @attribute syn
   * @aspect ASTNodeAnnotations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNodeAnnotations.jrag:40
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ASTNodeAnnotations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNodeAnnotations.jrag:40")
  @SideEffect.Pure(group="_ASTNode") public String attributeAnnotationValue() {
    if (attributeAnnotationValue_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.attributeAnnotationValue().");
    }
    attributeAnnotationValue_visited = state().boundariesCrossed;
    try {
        StringBuilder sb = new StringBuilder();
        sb.append(annotationKind());
        if (isCircular()) {
          sb.append(", isCircular=true");
        }
        if (getNTA() || isNTA()) {
          sb.append(", isNTA=true");
        }
        return sb.toString();
      }
    finally {
      attributeAnnotationValue_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int sourceLocation_visited = -1;
  /**
   * @attribute syn
   * @aspect ASTNodeAnnotations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNodeAnnotations.jrag:52
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ASTNodeAnnotations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNodeAnnotations.jrag:52")
  @SideEffect.Pure(group="_ASTNode") public String sourceLocation() {
    if (sourceLocation_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.sourceLocation().");
    }
    sourceLocation_visited = state().boundariesCrossed;
    String sourceLocation_value = ASTNode.sourceLocation(getFileName().trim(), getStartLine());
    sourceLocation_visited = -1;
    return sourceLocation_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map supportedAnnotation_Annotation_visited = new java.util.HashMap(4);
  /**
   * @param a the annotation
   * @return <code>true</code> if the given annotation is supported
   * for this type of attribute declaration
   * @attribute syn
   * @aspect Annotations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Annotations.jrag:51
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Annotations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Annotations.jrag:51")
  @SideEffect.Pure(group="_ASTNode") public boolean supportedAnnotation(Annotation a) {
    Object _parameters = a;
    if (Integer.valueOf(state().boundariesCrossed).equals(supportedAnnotation_Annotation_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.supportedAnnotation(Annotation).");
    }
    supportedAnnotation_Annotation_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean supportedAnnotation_Annotation_value = false;
    supportedAnnotation_Annotation_visited.remove(_parameters);
    return supportedAnnotation_Annotation_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int annotations_visited = -1;
  /**
   * @return the annotations of this attribute declaration.
   * @attribute syn
   * @aspect Annotations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Annotations.jrag:92
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Annotations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Annotations.jrag:92")
  @SideEffect.Pure(group="_ASTNode") public String annotations() {
    if (annotations_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.annotations().");
    }
    annotations_visited = state().boundariesCrossed;
    try {
        StringBuilder sb = new StringBuilder();
        for (Annotation annotation : getAnnotationList()) {
          if (sb.length() > 0) {
            sb.append("\n");
          }
          sb.append(annotation.getAnnotation());
        }
        return sb.toString();
      }
    finally {
      annotations_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map error_String_visited = new java.util.HashMap(4);
  /**
   * Create a new error object with relevant file name and line number.
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:136
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:136")
  @SideEffect.Pure(group="_ASTNode") public Problem error(String message) {
    Object _parameters = message;
    if (Integer.valueOf(state().boundariesCrossed).equals(error_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.error(String).");
    }
    error_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    Problem error_String_value = Problem.builder()
              .message(message)
              .sourceFile(getFileName())
              .sourceLine(getStartLine())
              .buildError();
    error_String_visited.remove(_parameters);
    return error_String_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map warning_String_visited = new java.util.HashMap(4);
  /** Create a new warning with the relevant file name and line number. 
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:161
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:161")
  @SideEffect.Pure(group="_ASTNode") public Problem warning(String message) {
    Object _parameters = message;
    if (Integer.valueOf(state().boundariesCrossed).equals(warning_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.warning(String).");
    }
    warning_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    Problem warning_String_value = Problem.builder()
              .message(message)
              .sourceFile(getFileName())
              .sourceLine(getStartLine())
              .buildWarning();
    warning_String_visited.remove(_parameters);
    return warning_String_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int hasUnsupportedAnnotation_visited = -1;
  /**
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:374
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:374")
  @SideEffect.Pure(group="_ASTNode") public boolean hasUnsupportedAnnotation() {
    if (hasUnsupportedAnnotation_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.hasUnsupportedAnnotation().");
    }
    hasUnsupportedAnnotation_visited = state().boundariesCrossed;
    try {
        for (Annotation annotation : getAnnotationList()) {
          if (!supportedAnnotation(annotation)) {
            return true;
          }
        }
        return false;
      }
    finally {
      hasUnsupportedAnnotation_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int unsupportedAnnotationProblem_visited = -1;
  /**
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:383
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:383")
  @SideEffect.Pure(group="_ASTNode") public Problem unsupportedAnnotationProblem() {
    if (unsupportedAnnotationProblem_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.unsupportedAnnotationProblem().");
    }
    unsupportedAnnotationProblem_visited = state().boundariesCrossed;
    try {
        StringBuilder buf = new StringBuilder();
        buf.append("unsupported annotations for this attribute declaration: ");
        boolean first = true;
        for (Annotation annotation: getAnnotationList()) {
          if (!supportedAnnotation(annotation)) {
            if (!first) {
              buf.append(", ");
            }
            first = false;
            buf.append(annotation.getAnnotation());
          }
        }
        return error(buf.toString());
      }
    finally {
      unsupportedAnnotationProblem_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int parametersDecl_visited = -1;
  /**
   * @attribute syn
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:1164
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Attributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:1164")
  @SideEffect.Pure(group="_ASTNode") public String parametersDecl() {
    if (parametersDecl_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.parametersDecl().");
    }
    parametersDecl_visited = state().boundariesCrossed;
    try {
        StringBuffer s = new StringBuffer();
        for (int i = 0; i < getNumParameter(); i++) {
          Parameter p = getParameter(i);
          s.append(p.getType() + " " + p.getName());
          if (i < getNumParameter() - 1) {
            s.append(", ");
          }
        }
        return s.toString();
      }
    finally {
      parametersDecl_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int parameters_visited = -1;
  /**
   * @attribute syn
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:1176
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Attributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:1176")
  @SideEffect.Pure(group="_ASTNode") public String parameters() {
    if (parameters_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.parameters().");
    }
    parameters_visited = state().boundariesCrossed;
    try {
        StringBuffer s = new StringBuffer();
        for (int i = 0; i < getNumParameter(); i++) {
          Parameter p = getParameter(i);
          s.append(p.getName());
          if (i < getNumParameter() - 1) {
            s.append(", ");
          }
        }
        return s.toString();
      }
    finally {
      parameters_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isCircularRewrite_visited = -1;
  /**
   * @attribute syn
   * @aspect Circular
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Circular.jrag:33
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Circular", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Circular.jrag:33")
  @SideEffect.Pure(group="_ASTNode") public boolean isCircularRewrite() {
    if (isCircularRewrite_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.isCircularRewrite().");
    }
    isCircularRewrite_visited = state().boundariesCrossed;
    boolean isCircularRewrite_value = false;
    isCircularRewrite_visited = -1;
    return isCircularRewrite_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isCircular_visited = -1;
  /**
   * @attribute syn
   * @aspect Circular
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Circular.jrag:37
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Circular", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Circular.jrag:37")
  @SideEffect.Pure(group="_ASTNode") public boolean isCircular() {
    if (isCircular_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.isCircular().");
    }
    isCircular_visited = state().boundariesCrossed;
    boolean isCircular_value = hasBottomValue();
    isCircular_visited = -1;
    return isCircular_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int hasBottomValue_visited = -1;
  /**
   * @attribute syn
   * @aspect Circular
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Circular.jrag:41
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Circular", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Circular.jrag:41")
  @SideEffect.Pure(group="_ASTNode") public boolean hasBottomValue() {
    if (hasBottomValue_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.hasBottomValue().");
    }
    hasBottomValue_visited = state().boundariesCrossed;
    boolean hasBottomValue_value = bottomValue != null && !bottomValue.isEmpty();
    hasBottomValue_visited = -1;
    return hasBottomValue_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int getBottomValue_visited = -1;
  /**
   * @attribute syn
   * @aspect Circular
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Circular.jrag:43
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Circular", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Circular.jrag:43")
  @SideEffect.Pure(group="_ASTNode") public String getBottomValue() {
    if (getBottomValue_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.getBottomValue().");
    }
    getBottomValue_visited = state().boundariesCrossed;
    String getBottomValue_value = bottomValue;
    getBottomValue_visited = -1;
    return getBottomValue_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int ntaParent_visited = -1;
  /** The parent to attach an NTA value to during circular evaluation. 
   * @attribute syn
   * @aspect Circular
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Circular.jrag:48
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Circular", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Circular.jrag:48")
  @SideEffect.Pure(group="_ASTNode") public String ntaParent() {
    if (ntaParent_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.ntaParent().");
    }
    ntaParent_visited = state().boundariesCrossed;
    String ntaParent_value = "this";
    ntaParent_visited = -1;
    return ntaParent_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isCollection_visited = -1;
  /**
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:35
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:35")
  @SideEffect.Pure(group="_ASTNode") public boolean isCollection() {
    if (isCollection_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.isCollection().");
    }
    isCollection_visited = state().boundariesCrossed;
    boolean isCollection_value = false;
    isCollection_visited = -1;
    return isCollection_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int root_visited = -1;
  /**
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:123
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:123")
  @SideEffect.Pure(group="_ASTNode") public TypeDecl root() {
    if (root_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.root().");
    }
    root_visited = state().boundariesCrossed;
    TypeDecl root_value = grammar().root();
    root_visited = -1;
    return root_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int lazyCondition_visited = -1;
  /**
   * Lazy condition means that the condition is evaluated during the
   * combination phase in two-phase collection evaluation, as opposed to
   * eagerly evaluating the condition during the survey phase.
   * 
   * <p>This corresponds to "late condition evaluation" in the collection
   * attribute paper, as opposed to "early condition evaluation".
   * 
   * <p>Defaults to {@code true}.
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:174
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:174")
  @SideEffect.Pure(group="_ASTNode") public boolean lazyCondition() {
    if (lazyCondition_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.lazyCondition().");
    }
    lazyCondition_visited = state().boundariesCrossed;
    boolean lazyCondition_value = hasAnnotation("@LazyCondition");
    lazyCondition_visited = -1;
    return lazyCondition_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int onePhase_visited = -1;
  /**
   * One phase evaluation combines the survey and combination phases.
   * 
   * <p>Defaults to {@code false}.
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:182
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:182")
  @SideEffect.Pure(group="_ASTNode") public boolean onePhase() {
    if (onePhase_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.onePhase().");
    }
    onePhase_visited = state().boundariesCrossed;
    boolean onePhase_value = hasAnnotation("@OnePhase");
    onePhase_visited = -1;
    return onePhase_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int naive_visited = -1;
  /**
   * In naive evaluation the tree is traversed once per collection attribute,
   * and there is only one phase.
   * 
   * <p>Defaults to {@code false}.
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:191
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:191")
  @SideEffect.Pure(group="_ASTNode") public boolean naive() {
    if (naive_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.naive().");
    }
    naive_visited = state().boundariesCrossed;
    boolean naive_value = hasAnnotation("@Naive");
    naive_visited = -1;
    return naive_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int collectionId_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void collectionId_reset() {
    collectionId_computed = false;
    
    collectionId_value = null;
    collectionId_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean collectionId_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected String collectionId_value;

  /**
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:385
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:385")
  @SideEffect.Pure(group="_ASTNode") public String collectionId() {
    ASTState state = state();
    if (collectionId_computed) {
      return collectionId_value;
    }
    if (collectionId_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.collectionId().");
    }
    collectionId_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    collectionId_value = signature();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    collectionId_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    collectionId_visited = -1;
    return collectionId_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map hasAnnotation_String_visited = new java.util.HashMap(4);
  /**
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:553
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:553")
  @SideEffect.Pure(group="_ASTNode") public boolean hasAnnotation(String name) {
    Object _parameters = name;
    if (Integer.valueOf(state().boundariesCrossed).equals(hasAnnotation_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.hasAnnotation(String).");
    }
    hasAnnotation_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    try {
        for (int i = 0; i < getNumAnnotation(); ++i) {
          if (getAnnotation(i).isAnnotation(name)) {
            return true;
          }
        }
        return false;
      }
    finally {
      hasAnnotation_String_visited.remove(_parameters);
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map getAnnotationValue_String_visited = new java.util.HashMap(4);
  /**
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:562
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:562")
  @SideEffect.Pure(group="_ASTNode") public String getAnnotationValue(String name) {
    Object _parameters = name;
    if (Integer.valueOf(state().boundariesCrossed).equals(getAnnotationValue_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.getAnnotationValue(String).");
    }
    getAnnotationValue_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    try {
        for (int i = 0; i < getNumAnnotation(); ++i) {
          if (getAnnotation(i).isAnnotation(name)) {
            return getAnnotation(i).value();
          }
        }
        // TODO(jesper): don't return null here, use a null object.
        return null;
      }
    finally {
      getAnnotationValue_String_visited.remove(_parameters);
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int resetCache_visited = -1;
  /**
   * @attribute syn
   * @aspect Flush
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Flush.jrag:50
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Flush", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Flush.jrag:50")
  @SideEffect.Pure(group="_ASTNode") public String resetCache() {
    if (resetCache_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.resetCache().");
    }
    resetCache_visited = state().boundariesCrossed;
    String resetCache_value = templateContext().expand("AttrDecl.resetAttrCache");
    resetCache_visited = -1;
    return resetCache_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int resetVisit_visited = -1;
  /**
   * @attribute syn
   * @aspect Flush
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Flush.jrag:52
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Flush", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Flush.jrag:52")
  @SideEffect.Pure(group="_ASTNode") public String resetVisit() {
    if (resetVisit_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.resetVisit().");
    }
    resetVisit_visited = state().boundariesCrossed;
    String resetVisit_value = templateContext().expand("AttrDecl.resetAttrVisit");
    resetVisit_visited = -1;
    return resetVisit_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isDuplicateInhDecl_visited = -1;
  /**
   * @attribute syn
   * @aspect InheritedAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:80
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="InheritedAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:80")
  @SideEffect.Pure(group="_ASTNode") public boolean isDuplicateInhDecl() {
    if (isDuplicateInhDecl_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.isDuplicateInhDecl().");
    }
    isDuplicateInhDecl_visited = state().boundariesCrossed;
    boolean isDuplicateInhDecl_value = false;
    isDuplicateInhDecl_visited = -1;
    return isDuplicateInhDecl_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int declaredat_visited = -1;
  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:80
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:80")
  @SideEffect.Pure(group="_ASTNode") public String declaredat() {
    if (declaredat_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.declaredat().");
    }
    declaredat_visited = state().boundariesCrossed;
    String declaredat_value = ASTNode.declaredat(getFileName(), getStartLine());
    declaredat_visited = -1;
    return declaredat_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map shouldCache_CacheMode_visited = new java.util.HashMap(4);
  /**
   * Decide if an attribute should be cached based on the cache mode and
   * current global configuration.
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:92
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:92")
  @SideEffect.Pure(group="_ASTNode") public boolean shouldCache(CacheMode mode) {
    Object _parameters = mode;
    if (Integer.valueOf(state().boundariesCrossed).equals(shouldCache_CacheMode_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.shouldCache(CacheMode).");
    }
    shouldCache_CacheMode_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    try {
        switch (mode) {
          case DEFAULT:
            return config().cacheAll();
          case LAZY:
            return !config().cacheNone() || isNTA();
          case CACHED:
            return true;
          case UNCACHED:
            return isNTA();
        }
        throw new Error("unhandled cache mode");
      }
    finally {
      shouldCache_CacheMode_visited.remove(_parameters);
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isLazy_visited = -1;
  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:106
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:106")
  @SideEffect.Pure(group="_ASTNode") public boolean isLazy() {
    if (isLazy_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.isLazy().");
    }
    isLazy_visited = state().boundariesCrossed;
    boolean isLazy_value = declaredNTA() || isCircular() || shouldCache(getCacheMode());
    isLazy_visited = -1;
    return isLazy_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int declaredNTA_visited = -1;
  /**
   * @return {@code true} if the attribute is declared as NTA in the aspect file
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:262
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:262")
  @SideEffect.Pure(group="_ASTNode") public boolean declaredNTA() {
    if (declaredNTA_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.declaredNTA().");
    }
    declaredNTA_visited = state().boundariesCrossed;
    boolean declaredNTA_value = false;
    declaredNTA_visited = -1;
    return declaredNTA_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int boxedType_visited = -1;
  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:294
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:294")
  @SideEffect.Pure(group="_ASTNode") public String boxedType() {
    if (boxedType_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.boxedType().");
    }
    boxedType_visited = state().boundariesCrossed;
    String boxedType_value = boxedType(getType());
    boxedType_visited = -1;
    return boxedType_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isPrimitive_visited = -1;
  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:296
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:296")
  @SideEffect.Pure(group="_ASTNode") public boolean isPrimitive() {
    if (isPrimitive_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.isPrimitive().");
    }
    isPrimitive_visited = state().boundariesCrossed;
    boolean isPrimitive_value = isPrimitiveType(getType());
    isPrimitive_visited = -1;
    return isPrimitive_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int cycleLimitCheck_visited = -1;
  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:390
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:390")
  @SideEffect.Pure(group="_ASTNode") public String cycleLimitCheck() {
    if (cycleLimitCheck_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.cycleLimitCheck().");
    }
    cycleLimitCheck_visited = state().boundariesCrossed;
    String cycleLimitCheck_value = "";
    cycleLimitCheck_visited = -1;
    return cycleLimitCheck_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isParameterized_visited = -1;
  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:500
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:500")
  @SideEffect.Pure(group="_ASTNode") public boolean isParameterized() {
    if (isParameterized_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.isParameterized().");
    }
    isParameterized_visited = state().boundariesCrossed;
    boolean isParameterized_value = getNumParameter() != 0;
    isParameterized_visited = -1;
    return isParameterized_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int hasCache_visited = -1;
  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:773
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:773")
  @SideEffect.Pure(group="_ASTNode") public boolean hasCache() {
    if (hasCache_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.hasCache().");
    }
    hasCache_visited = state().boundariesCrossed;
    boolean hasCache_value = isLazy() || isCircular();
    hasCache_visited = -1;
    return hasCache_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isAttrNTA_visited = -1;
  /**
   * @return {@code true} if the attribute declaration corresponds to a
   * non-token NTA component
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:779
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:779")
  @SideEffect.Pure(group="_ASTNode") public boolean isAttrNTA() {
    if (isAttrNTA_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.isAttrNTA().");
    }
    isAttrNTA_visited = state().boundariesCrossed;
    boolean isAttrNTA_value = isNTA() && !(findCorrespondingNTA() instanceof TokenComponent);
    isAttrNTA_visited = -1;
    return isAttrNTA_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int computeRhs_visited = -1;
  /**
   * @attribute syn
   * @aspect Compute
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:784
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Compute", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:784")
  @SideEffect.Pure(group="_ASTNode") public String computeRhs() {
    if (computeRhs_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.computeRhs().");
    }
    computeRhs_visited = state().boundariesCrossed;
    String computeRhs_value = name() + "_compute(" + parameters() + ")";
    computeRhs_visited = -1;
    return computeRhs_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int computeLhs_visited = -1;
  /**
   * @attribute syn
   * @aspect Compute
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:789
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Compute", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:789")
  @SideEffect.Pure(group="_ASTNode") public String computeLhs() {
    if (computeLhs_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.computeLhs().");
    }
    computeLhs_visited = state().boundariesCrossed;
    try {
        if (isLazy() && getNumParameter() == 0) {
          return signature() + "_value";
        } else {
          return getType() + " " + signature() + "_value";
        }
      }
    finally {
      computeLhs_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int circularComputeRhs_visited = -1;
  /**
   * @attribute syn
   * @aspect Compute
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:797
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Compute", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:797")
  @SideEffect.Pure(group="_ASTNode") public String circularComputeRhs() {
    if (circularComputeRhs_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.circularComputeRhs().");
    }
    circularComputeRhs_visited = state().boundariesCrossed;
    String circularComputeRhs_value = computeRhs();
    circularComputeRhs_visited = -1;
    return circularComputeRhs_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int simpleCacheCheck_visited = -1;
  /**
   * @attribute syn
   * @aspect Compute
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:802
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Compute", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:802")
  @SideEffect.Pure(group="_ASTNode") public boolean simpleCacheCheck() {
    if (simpleCacheCheck_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.simpleCacheCheck().");
    }
    simpleCacheCheck_visited = state().boundariesCrossed;
    boolean simpleCacheCheck_value = !config().safeLazy() || isCircular() || declaredNTA() || isAttrNTA();
    simpleCacheCheck_visited = -1;
    return simpleCacheCheck_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int name_visited = -1;
  /**
   * @attribute syn
   * @aspect AttributeNamesAndTypes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:38
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeNamesAndTypes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:38")
  @SideEffect.Pure(group="_ASTNode") public String name() {
    if (name_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.name().");
    }
    name_visited = state().boundariesCrossed;
    String name_value = getName();
    name_visited = -1;
    return name_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int type_visited = -1;
  /**
   * @attribute syn
   * @aspect AttributeNamesAndTypes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:39
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeNamesAndTypes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:39")
  @SideEffect.Pure(group="_ASTNode") public String type() {
    if (type_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.type().");
    }
    type_visited = state().boundariesCrossed;
    String type_value = getType();
    type_visited = -1;
    return type_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int signature_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void signature_reset() {
    signature_computed = false;
    
    signature_value = null;
    signature_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean signature_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected String signature_value;

  /**
   * @attribute syn
   * @aspect AttributeNamesAndTypes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:66
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeNamesAndTypes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:66")
  @SideEffect.Pure(group="_ASTNode") public String signature() {
    ASTState state = state();
    if (signature_computed) {
      return signature_value;
    }
    if (signature_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.signature().");
    }
    signature_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    signature_value = signature_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    signature_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    signature_visited = -1;
    return signature_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private String signature_compute() {
      StringBuilder sb = new StringBuilder();
      sb.append(getName());
      for (int i = 0; i < getNumParameter(); i++) {
        sb.append("_" + getParameter(i).getTypeInSignature());
      }
      return sb.toString();
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map tracePrintReturnPreviousValue_String_visited = new java.util.HashMap(4);
  /**
   * @attribute syn
   * @aspect Trace
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:32
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Trace", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:32")
  @SideEffect.Pure(group="_ASTNode") public String tracePrintReturnPreviousValue(String varName) {
    Object _parameters = varName;
    if (Integer.valueOf(state().boundariesCrossed).equals(tracePrintReturnPreviousValue_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.tracePrintReturnPreviousValue(String).");
    }
    tracePrintReturnPreviousValue_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    String tracePrintReturnPreviousValue_String_value = "";
    tracePrintReturnPreviousValue_String_visited.remove(_parameters);
    return tracePrintReturnPreviousValue_String_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map tracePrintReturnNewValue_String_visited = new java.util.HashMap(4);
  /**
   * @attribute syn
   * @aspect Trace
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:33
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Trace", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:33")
  @SideEffect.Pure(group="_ASTNode") public String tracePrintReturnNewValue(String varName) {
    Object _parameters = varName;
    if (Integer.valueOf(state().boundariesCrossed).equals(tracePrintReturnNewValue_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.tracePrintReturnNewValue(String).");
    }
    tracePrintReturnNewValue_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    String tracePrintReturnNewValue_String_value = "";
    tracePrintReturnNewValue_String_visited.remove(_parameters);
    return tracePrintReturnNewValue_String_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int tracePrintBeginComputingValue_visited = -1;
  /**
   * @attribute syn
   * @aspect Trace
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:34
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Trace", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:34")
  @SideEffect.Pure(group="_ASTNode") public String tracePrintBeginComputingValue() {
    if (tracePrintBeginComputingValue_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.tracePrintBeginComputingValue().");
    }
    tracePrintBeginComputingValue_visited = state().boundariesCrossed;
    String tracePrintBeginComputingValue_value = "";
    tracePrintBeginComputingValue_visited = -1;
    return tracePrintBeginComputingValue_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int tracePrintCycleBeginString_visited = -1;
  /**
   * @attribute syn
   * @aspect Trace
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:35
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Trace", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:35")
  @SideEffect.Pure(group="_ASTNode") public String tracePrintCycleBeginString() {
    if (tracePrintCycleBeginString_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.tracePrintCycleBeginString().");
    }
    tracePrintCycleBeginString_visited = state().boundariesCrossed;
    String tracePrintCycleBeginString_value = "";
    tracePrintCycleBeginString_visited = -1;
    return tracePrintCycleBeginString_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int tracePrintCycleEndString_visited = -1;
  /**
   * @attribute syn
   * @aspect Trace
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:36
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Trace", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:36")
  @SideEffect.Pure(group="_ASTNode") public String tracePrintCycleEndString() {
    if (tracePrintCycleEndString_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.tracePrintCycleEndString().");
    }
    tracePrintCycleEndString_visited = state().boundariesCrossed;
    String tracePrintCycleEndString_value = "";
    tracePrintCycleEndString_visited = -1;
    return tracePrintCycleEndString_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int tracePrintStartingCycle_visited = -1;
  /**
   * @attribute syn
   * @aspect Trace
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:37
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Trace", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:37")
  @SideEffect.Pure(group="_ASTNode") public String tracePrintStartingCycle() {
    if (tracePrintStartingCycle_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.tracePrintStartingCycle().");
    }
    tracePrintStartingCycle_visited = state().boundariesCrossed;
    String tracePrintStartingCycle_value = "";
    tracePrintStartingCycle_visited = -1;
    return tracePrintStartingCycle_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int traceComputeContext_visited = -1;
  /**
   * @attribute syn
   * @aspect Trace
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:38
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Trace", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:38")
  @SideEffect.Pure(group="_ASTNode") public String traceComputeContext() {
    if (traceComputeContext_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.traceComputeContext().");
    }
    traceComputeContext_visited = state().boundariesCrossed;
    String traceComputeContext_value = "";
    traceComputeContext_visited = -1;
    return traceComputeContext_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int hostClassName_visited = -1;
  /**
   * @attribute syn
   * @aspect Trace
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:40
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Trace", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:40")
  @SideEffect.Pure(group="_ASTNode") public String hostClassName() {
    if (hostClassName_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.hostClassName().");
    }
    hostClassName_visited = state().boundariesCrossed;
    String hostClassName_value = hostClass().name();
    hostClassName_visited = -1;
    return hostClassName_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int traceSignature_visited = -1;
  /**
   * @attribute syn
   * @aspect Trace
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:42
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Trace", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:42")
  @SideEffect.Pure(group="_ASTNode") public String traceSignature() {
    if (traceSignature_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.traceSignature().");
    }
    traceSignature_visited = state().boundariesCrossed;
    try {
        StringBuilder b = new StringBuilder();
        b.append(getAspectName());
        b.append(": ");
        if (this instanceof SynDecl) {
          b.append("syn ");
        } else if (this instanceof InhDecl) {
          b.append("inh ");
        } else if (this instanceof CollDecl) {
          b.append("coll ");
        }
        if (isLazy()) {
          b.append("lazy ");
        }
        if (isCircular()) {
          b.append(" circular");
        }
    
        b.append(getType());
        b.append(" ");
        b.append(hostClassName());
        b.append(".");
        b.append(name());
        b.append("(");
        for (int i = 0; i < getNumParameter(); i++) {
          if (i != 0) {
            b.append(", ");
          }
          b.append(getParameter(i).getType());
          b.append(" ");
          b.append(getParameter(i).getName());
        }
        b.append(")");
        b.append(", ");
        b.append("this = \" + this.getClass().getName() + \"@\"+ "
            + "Integer.toHexString(this.hashCode()) + \"");
        for (int i = 0; i < getNumParameter(); i++) {
          String name = getParameter(i).getName();
          String type = getParameter(i).getType();
          b.append(", ");
          b.append(name);
          b.append(" = ");
          if (isPrimitiveType(type) || type.equals("String") || type.equals("java.lang.String")) {
            b.append("\" + " + name + " + \"");
          } else {
            b.append("\" + " + name + ".getClass().getName() + \"@\" + Integer.toHexString("
                + name + ".hashCode()) + \"");
          }
        }
        return b.toString();
      }
    finally {
      traceSignature_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int traceBeginAttr_visited = -1;
  /**
   * @attribute syn
   * @aspect Trace
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:99
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Trace", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:99")
  @SideEffect.Pure(group="_ASTNode") public String traceBeginAttr() {
    if (traceBeginAttr_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.traceBeginAttr().");
    }
    traceBeginAttr_visited = state().boundariesCrossed;
    String traceBeginAttr_value = trace("begin " + traceSignature());
    traceBeginAttr_visited = -1;
    return traceBeginAttr_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int traceEndAttr_visited = -1;
  /**
   * @attribute syn
   * @aspect Trace
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:100
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Trace", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:100")
  @SideEffect.Pure(group="_ASTNode") public String traceEndAttr() {
    if (traceEndAttr_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.traceEndAttr().");
    }
    traceEndAttr_visited = state().boundariesCrossed;
    String traceEndAttr_value = trace("end " + traceSignature());
    traceEndAttr_visited = -1;
    return traceEndAttr_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int traceEndCachedAttr_visited = -1;
  /**
   * @attribute syn
   * @aspect Trace
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:101
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Trace", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:101")
  @SideEffect.Pure(group="_ASTNode") public String traceEndCachedAttr() {
    if (traceEndCachedAttr_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.traceEndCachedAttr().");
    }
    traceEndCachedAttr_visited = state().boundariesCrossed;
    String traceEndCachedAttr_value = trace("end cached " + traceSignature());
    traceEndCachedAttr_visited = -1;
    return traceEndCachedAttr_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int hasOneParameter_visited = -1;
  /**
   * @attribute syn
   * @aspect NewTrace
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:105
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NewTrace", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:105")
  @SideEffect.Pure(group="_ASTNode") public boolean hasOneParameter() {
    if (hasOneParameter_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.hasOneParameter().");
    }
    hasOneParameter_visited = state().boundariesCrossed;
    boolean hasOneParameter_value = getNumParameter() == 1;
    hasOneParameter_visited = -1;
    return hasOneParameter_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int parameterTypesAsCSV_visited = -1;
  /**
   * @attribute syn
   * @aspect NewTrace
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:107
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NewTrace", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:107")
  @SideEffect.Pure(group="_ASTNode") public String parameterTypesAsCSV() {
    if (parameterTypesAsCSV_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.parameterTypesAsCSV().");
    }
    parameterTypesAsCSV_visited = state().boundariesCrossed;
    try {
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (Parameter p: getParameters()) {
          if (first) {
            first = false;
          } else {
            // Do not insert space since the trace output can be sorted
            // using the UNIX command sort (and spaces creates new columns).
            sb.append(",");
          }
          sb.append(p.getTypeInSignature());
        }
        return sb.toString();
      }
    finally {
      parameterTypesAsCSV_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int signatureJavaStyle_visited = -1;
  /**
   * The signature of this attribute, in Java format.
   * @attribute syn
   * @aspect NewTrace
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:126
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NewTrace", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Trace.jrag:126")
  @SideEffect.Pure(group="_ASTNode") public String signatureJavaStyle() {
    if (signatureJavaStyle_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.signatureJavaStyle().");
    }
    signatureJavaStyle_visited = state().boundariesCrossed;
    try {
        return getName() + "(" + parameterTypesAsCSV() + ")";
      }
    finally {
      signatureJavaStyle_visited = -1;
    }
  }
  /**
   * @attribute inh
   * @aspect AttributeHostClass
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:33
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="AttributeHostClass", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:33")
  @SideEffect.Pure(group="_ASTNode") public TypeDecl hostClass() {
    if (hostClass_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrDecl.hostClass().");
    }
    hostClass_visited = state().boundariesCrossed;
    TypeDecl hostClass_value = getParent().Define_hostClass(this, null);
    hostClass_visited = -1;
    return hostClass_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int hostClass_visited = -1;
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  @SideEffect.Local(group="_ASTNode") protected void collect_contributors_TypeDecl_attributeProblems(TypeDecl _root, @SideEffect.Local java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:369
    if (hasUnsupportedAnnotation()) {
      {
        TypeDecl target = (TypeDecl) (hostClass());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_TypeDecl_attributeProblems(_root, _map);
  }
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_TypeDecl_attributeProblems(Collection<Problem> collection) {
    super.contributeTo_TypeDecl_attributeProblems(collection);
    if (hasUnsupportedAnnotation()) {
      collection.add(unsupportedAnnotationProblem());
    }
  }
}
