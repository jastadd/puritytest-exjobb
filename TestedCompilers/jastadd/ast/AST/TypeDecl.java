/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package org.jastadd.ast.AST;
import org.jastadd.ast.AST.*;
import java.util.*;
import org.jastadd.Problem;
import org.jastadd.jrag.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jastadd.jrag.AST.ASTExpression;
import org.jastadd.jrag.Unparser;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.io.PrintStream;
import java.io.*;
import java.util.regex.*;
import org.jastadd.JastAdd;
import org.jastadd.jrag.AST.ASTAspectMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectRefineMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectFieldDeclaration;
import org.jastadd.jrag.AST.ASTBlock;
import org.jastadd.jrag.ClassBodyObject;
import org.jastadd.jrag.AST.ASTCompilationUnit;
import org.jastadd.ast.AST.Token;
import org.jastadd.ast.AST.SimpleNode;
import org.jastadd.Configuration;
import org.jastadd.tinytemplate.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Ast.ast:6
 * @production TypeDecl : {@link ASTNode} ::= <span class="component">{@link IdDecl}</span> <span class="component">{@link ClassBodyDecl}*</span> <span class="component">{@link SynDecl}*</span> <span class="component">{@link SynEq}*</span> <span class="component">{@link InhDecl}*</span> <span class="component">{@link InhEq}*</span> <span class="component">{@link Component}*</span> <span class="component">{@link CollDecl}*</span> <span class="component">&lt;FileName:String&gt;</span> <span class="component">&lt;StartLine:int&gt;</span> <span class="component">&lt;EndLine:int&gt;</span> <span class="component">&lt;Comment:String&gt;</span> <span class="component">&lt;AspectName:String&gt;</span>;

 */
public abstract class TypeDecl extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:434
   */
  public Collection<ClassBodyObject> classBodyDecls = new LinkedHashSet<ClassBodyObject>();
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:480
   */
  public LinkedList refinedSynEqs = new LinkedList();
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:531
   */
  public LinkedList replacedSynEqs = new LinkedList();
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:533
   */
  public void processSynEqReplacements() {
    boolean change = true;
    ArrayList list = new ArrayList();
    while (change && !replacedSynEqs.isEmpty()) {
      change = false;
      for (Iterator iter = refinedSynEqs.iterator(); iter.hasNext(); ) {
        SynEq decl = (SynEq)iter.next();
        SynEq refinedDecl = null;
        for (Iterator outerIter = replacedSynEqs.iterator(); outerIter.hasNext(); ) {
          SynEq refinedCandidate = (SynEq)outerIter.next();
          boolean legacyCondition = config().refineLegacy()
              && decl.legacyAspectName().equals(refinedCandidate.replacesAspect);
          if (decl.signature().equals(refinedCandidate.signature())
              && (decl.aspectName().equals(refinedCandidate.replacesAspect) || legacyCondition)) {
            change = true;
            if (refinedDecl == null) {
              refinedDecl = refinedCandidate;
            } else {
              grammar().error("refinement previously defined at " +
                  refinedCandidate.getFileName() + ":" + refinedCandidate.getStartLine(),
                  refinedDecl.getFileName(), refinedDecl.getStartLine());
            }
            outerIter.remove();
          }
        }
        if (refinedDecl != null) {
          iter.remove();
          refinedDecl.replacesAspect = null;
          list.add(refinedDecl);
        }
      }
      refinedSynEqs.addAll(list);
    }
    for (Iterator iter = replacedSynEqs.iterator(); iter.hasNext(); ) {
      SynEq decl = (SynEq)iter.next();
      refineError("equation", decl.getFileName(), decl.getStartLine());
    }
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:578
   */
  public void processRefinedSynEqs() {
    boolean change = true;
    while (change && !refinedSynEqs.isEmpty()) {
      change = false;
      for (int i = 0; i < getNumSynEq(); i++) {
        SynEq equ = getSynEq(i);
        SynEq refinedEqu = null;
        for (Iterator outerIter = refinedSynEqs.iterator(); outerIter.hasNext(); ) {
          SynEq refinedCandidate = (SynEq)outerIter.next();
          boolean legacyCondition = config().refineLegacy()
              && equ.legacyAspectName().equals(refinedCandidate.refinesAspect);
          if (equ.signature().equals(refinedCandidate.signature())
             && (equ.aspectName().equals(refinedCandidate.refinesAspect) || legacyCondition)) {
            change = true;
            if (refinedEqu == null) {
              refinedEqu = refinedCandidate;
            } else {
              grammar().error("refinement previously defined at " +
                  refinedCandidate.getFileName() + ":" + refinedCandidate.getStartLine(),
                  refinedEqu.getFileName(), refinedEqu.getStartLine());
            }
            outerIter.remove();
          }
        }
        if (refinedEqu != null) {
          refineWith(equ, refinedEqu);
        }
      }
    }
    for (Iterator iter = refinedSynEqs.iterator(); iter.hasNext(); ) {
      SynEq equ = (SynEq)iter.next();
      refineError("syn equation", equ.getFileName(), equ.getStartLine());
    }
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:613
   */
  public void refineWith(SynEq equ, SynEq refinedEqu) {
    StringBuffer s = new StringBuffer();
    String newMethodName = "refined_" + equ.aspectName() + "_" + name() + "_" + equ.signature();
    if (equ.decl() == null) {
      throw new Error("Error: could not find declaration of refined synthesized equation "
          + equ.name() + " (" + equ.getFileName() + ":" + equ.getStartLine() + ")");
    }
    s.append("private " + equ.decl().getType() + " " + newMethodName + "("
        + equ.parametersDecl() + ")\n");
    if (equ.getRHS() instanceof ASTBlock) {
      s.append(Unparser.unparse(equ.getRHS()));
    } else {
      s.append("{ return " + Unparser.unparse(equ.getRHS()) + "; }");
    }
    org.jastadd.jrag.AST.SimpleNode n = new org.jastadd.jrag.AST.ASTBlock(0);
    n.firstToken = n.lastToken = org.jastadd.jrag.AST.Token.newToken(0);
    n.firstToken.image = s.toString();
    ClassBodyObject object = new ClassBodyObject(n, equ.getFileName(),
        equ.getStartLine(), equ.getAspectName());

    if (refinedEqu.getRHS() instanceof org.jastadd.jrag.AST.ASTBlock) {
      n = new org.jastadd.jrag.AST.ASTBlock(0);
    } else {
      n = new org.jastadd.jrag.AST.SimpleNode(0);
    }

    n.firstToken = n.lastToken = org.jastadd.jrag.AST.Token.newToken(0);
    s = new StringBuffer();
    refinedEqu.getRHS().jjtAccept(new ClassBodyDeclUnparser(), s);

    String pattern = "\\brefined\\b";
    if (config().refineLegacy()) {
      StringBuffer buf = new StringBuffer();
      buf.append("(");
      buf.append(pattern);
      buf.append(")|(");
      buf.append("\\b");
      buf.append(equ.legacyAspectName());
      buf.append("\\.[a-zA-Z0-9_$]+\\.");
      buf.append(equ.name());
      buf.append("\\b)");
      pattern = buf.toString();
    }
    Matcher matcher = Pattern.compile(pattern).matcher(s.toString());
    if (matcher.find()) {
      n.firstToken.image = matcher.replaceAll(newMethodName);
      classBodyDecls.add(object);
    } else {
      n.firstToken.image = s.toString();
    }
    // Change body of original equation to the refined body.
    equ.setRHS(n);
    equ.setFileName(refinedEqu.getFileName());
    equ.setStartLine(refinedEqu.getStartLine());
    equ.setEndLine(refinedEqu.getEndLine());
    equ.setAspectName(refinedEqu.getAspectName());
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:671
   */
  public LinkedList refinedInhEqs = new LinkedList();
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:708
   */
  public void processRefinedInhEqs() {
    boolean change = true;
    while (change && !refinedInhEqs.isEmpty()) {
      change = false;
      for (int i = 0; i < getNumInhEq(); i++) {
        InhEq equ = getInhEq(i);
        InhEq refinedEqu = null;
        for (Iterator outerIter = refinedInhEqs.iterator(); outerIter.hasNext(); ) {
          InhEq refinedCandidate = (InhEq)outerIter.next();
          boolean legacyCondition = config().refineLegacy() &&
              equ.legacyAspectName().equals(refinedCandidate.refinesAspect);
          if (equ.signature().equals(refinedCandidate.signature()) &&
             equ.childName().equals(refinedCandidate.childName()) &&
             (equ.aspectName().equals(refinedCandidate.refinesAspect) || legacyCondition)) {
            change = true;
            if (refinedEqu == null) {
              refinedEqu = refinedCandidate;
            } else {
              grammar().error("refinement previously defined at " +
                  refinedCandidate.getFileName() + ":" + refinedCandidate.getStartLine(),
                  refinedEqu.getFileName(), refinedEqu.getStartLine());
            }
            outerIter.remove();
          }
        }
        if (refinedEqu != null) {
          refineWith(equ, refinedEqu);
        }
      }
    }
    for (Iterator iter = refinedInhEqs.iterator(); iter.hasNext(); ) {
      InhEq equ = (InhEq)iter.next();
      refineError("inh equation", equ.getFileName(), equ.getStartLine());
    }
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:744
   */
  public void refineWith(InhEq equ, InhEq refinedEqu) {
    // Build a ClassBodyObject from the original equation.
    StringBuffer s = new StringBuffer();
    String newMethodName = "refined_" + equ.aspectName() + "_" + name() + "_" + equ.childName()
        + "_" + equ.signature();

    String indexName = "";
    String indexDecl = "";
    boolean isList = equ.getComponent() instanceof ListComponent;
    if (isList) {
      indexName = equ.hasIndex() ? equ.getIndex().getName() : "childIndex";
      indexDecl = "int " + indexName;
      if (equ.getNumParameter() != 0) {
        indexName += ", ";
        indexDecl += ", ";
      }
    }

    if (equ.getChildAttrDecl().declaredNTA()){
    	s.append("@SideEffect.Fresh private " + equ.decl().getType() + " " + newMethodName + "(" +
    			  indexDecl + equ.parametersDecl() + ")\n");
    }else{
    	s.append("@SideEffect.Pure private " + equ.decl().getType() + " " + newMethodName + "(" +
    			indexDecl + equ.parametersDecl() + ")\n");
    }
    if (equ.getRHS() instanceof ASTBlock) {
      s.append(Unparser.unparse(equ.getRHS()));
    } else {
      s.append("{ return " + Unparser.unparse(equ.getRHS()) + "; }");
    }
    org.jastadd.jrag.AST.SimpleNode n = new org.jastadd.jrag.AST.ASTBlock(0);
    n.firstToken = n.lastToken = org.jastadd.jrag.AST.Token.newToken(0);
    n.firstToken.image = s.toString();
    ClassBodyObject object = new ClassBodyObject(n, equ.getFileName(),
        equ.getStartLine(), equ.getAspectName());
    // Change references to original equation in refined body.
    if (refinedEqu.getRHS() instanceof org.jastadd.jrag.AST.ASTBlock) {
      n = new org.jastadd.jrag.AST.ASTBlock(0);
    } else {
      n = new org.jastadd.jrag.AST.SimpleNode(0);
    }
    n.firstToken = n.lastToken = org.jastadd.jrag.AST.Token.newToken(0);
    s = new StringBuffer();
    refinedEqu.getRHS().jjtAccept(new ClassBodyDeclUnparser(), s);

    String pattern = "refined\\(";
    if (config().refineLegacy()) {
      pattern = String.format("(%s)|(%s\\.[a-zA-Z0-9]+\\.get%s\\([^\\)]*\\)\\.%s\\()",
          pattern, equ.legacyAspectName(), equ.childName(), equ.name());
    }

    Matcher matcher = Pattern.compile(pattern).matcher(s.toString());
    if (matcher.find()) {
      n.firstToken.image = matcher.replaceAll(newMethodName + "(" + indexName);
      classBodyDecls.add(object);
    } else {
      n.firstToken.image = s.toString();
    }

    // Change body of original equation to the refined body.
    equ.setRHS(n);
    equ.setFileName(refinedEqu.getFileName());
    equ.setStartLine(refinedEqu.getStartLine());
    equ.setEndLine(refinedEqu.getEndLine());
    equ.setAspectName(refinedEqu.getAspectName());
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:811
   */
  public Collection<ClassBodyObject> refinedClassBodyDecls =
      new LinkedList<ClassBodyObject>();
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:853
   */
  public void processRefinedClassBodyDecls() {
    boolean change = true;
    Collection<ClassBodyObject> list = new ArrayList<ClassBodyObject>();

    while (change && !refinedClassBodyDecls.isEmpty()) {
      change = false;
      Iterator<ClassBodyObject> iter = classBodyDecls.iterator();
      while (iter.hasNext()) {
        ClassBodyObject decl = iter.next();
        ClassBodyObject refinedDecl = null;
        Iterator<ClassBodyObject> outerIter = refinedClassBodyDecls.iterator();
        while (outerIter.hasNext()) {
          ClassBodyObject refinedCandidate = outerIter.next();

          boolean legacyCondition = config().refineLegacy()
              && decl.legacyAspectName().equals(refinedCandidate.refinesAspect);
          if (decl.signature().equals(refinedCandidate.signature())
              && (decl.aspectName().equals(refinedCandidate.refinesAspect) || legacyCondition)) {
            change = true;
            if (refinedDecl == null) {
              refinedDecl = refinedCandidate;
            } else {
              grammar().error("refinement previously defined at "
                  + refinedCandidate.getFileName() + ":" + refinedCandidate.getStartLine(),
                  refinedDecl.getFileName(), refinedDecl.getStartLine());
            }
            outerIter.remove();
          }
        }
        if (refinedDecl != null) {
          if (!refineWith(decl, refinedDecl)) {
            iter.remove();
          }
          list.add(refinedDecl);
        }
      }
      classBodyDecls.addAll(list);
    }
    for (Iterator iter = refinedClassBodyDecls.iterator(); iter.hasNext(); ) {
      ClassBodyObject decl = (ClassBodyObject)iter.next();
      refineError("method", decl.getFileName(), decl.getStartLine());
    }
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:897
   */
  public LinkedList replacements = new LinkedList();
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:899
   */
  public void processReplacements() {
    boolean change = true;
    ArrayList list = new ArrayList();
    while (change && !replacements.isEmpty()) {
      change = false;
      for (Iterator iter = refinedClassBodyDecls.iterator(); iter.hasNext(); ) {
        ClassBodyObject decl = (ClassBodyObject)iter.next();
        ClassBodyObject refinedDecl = null;
        for (Iterator outerIter = replacements.iterator(); outerIter.hasNext(); ) {
          ClassBodyObject refinedCandidate = (ClassBodyObject)outerIter.next();
          boolean legacyCondition = config().refineLegacy()
              && decl.legacyAspectName().equals(refinedCandidate.replaceAspect);
          if (decl.signature().equals(refinedCandidate.signature())
             && (decl.aspectName().equals(refinedCandidate.replaceAspect) || legacyCondition)) {
            change = true;
            if (refinedDecl == null) {
              refinedDecl = refinedCandidate;
            } else {
              grammar().error("refinement previously defined at " +
                  refinedCandidate.getFileName() + ":" + refinedCandidate.getStartLine(),
                  refinedDecl.getFileName(), refinedDecl.getStartLine());
            }
            outerIter.remove();
          }
        }
        if (refinedDecl != null) {
          iter.remove();
          replaceWith(refinedDecl);
          list.add(refinedDecl);
        }
      }
      refinedClassBodyDecls.addAll(list);
    }
    for (Iterator iter = replacements.iterator(); iter.hasNext(); ) {
      ClassBodyObject decl = (ClassBodyObject)iter.next();
      refineError("method", decl.getFileName(), decl.getStartLine());
    }
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:938
   */
  private void replaceWith(ClassBodyObject o) {
    o.replaceAspect = null;

    org.jastadd.jrag.AST.SimpleNode node = o.node;
    // The first two children contain the extra signature in the refine to declaration.
    node.firstToken =
        ((org.jastadd.jrag.AST.SimpleNode) node.jjtGetChild(1)).lastToken.next.next.next;
    node.jjtAddChild(node.jjtGetChild(2), 0);
    node.jjtAddChild(node.jjtGetChild(3), 1);
    node.jjtAddChild(node.jjtGetChild(4), 2);
    // Clear remaining children.
    for (int i = 3; i < node.jjtGetNumChildren(); i++) {
      node.jjtAddChild(null, i);
    }
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:954
   */
  public boolean refineWith(ClassBodyObject decl, ClassBodyObject refinedDecl) {
    org.jastadd.jrag.AST.SimpleNode node = refinedDecl.node;
    boolean keep = true;

    if (node instanceof org.jastadd.jrag.AST.ASTAspectConstructorDeclaration
        || node instanceof org.jastadd.jrag.AST.ASTAspectRefineConstructorDeclaration) {
      // The name of a constructor is the same as the type name.
      String methodName = name();

      // Add prefix "void refined_".
      org.jastadd.jrag.AST.Token t1 = ((org.jastadd.jrag.AST.SimpleNode)decl.node.jjtGetChild(0)).firstToken;
      org.jastadd.jrag.AST.Token t2 = ((org.jastadd.jrag.AST.SimpleNode)decl.node).firstToken;
      while (t2.next != t1) {
        t2 = t2.next;
      }
      t2.image = "void refined_" + decl.aspectName() + "_" + name() + "_" + t2.image;

      // Find block node.
      org.jastadd.jrag.AST.SimpleNode parent = node;
      boolean first = true;
      keep = false;
      for (int index = 1; index < parent.jjtGetNumChildren(); index++) {
        org.jastadd.jrag.AST.SimpleNode child =
            (org.jastadd.jrag.AST.SimpleNode)parent.jjtGetChild(index);
        if (child instanceof org.jastadd.jrag.AST.ASTBlockStatement
            || child instanceof org.jastadd.jrag.AST.ASTExplicitConstructorInvocation) {
          node = child;
          // Replace "aspectName.typeName.methodName" in refinedDecl with
          // "refined_aspectName_methodName".
          StringBuffer buf = new StringBuffer();
          node.jjtAccept(new ClassBodyDeclUnparser(), buf);
          String s = buf.toString();

          String pattern = "\\brefined\\b";
          if (config().refineLegacy()) {
            buf = new StringBuffer();
            buf.append("(");
            buf.append(pattern);
            buf.append(")|(");
            buf.append("\\b");
            buf.append(decl.legacyAspectName());
            buf.append("\\.[a-zA-Z0-9_$]+\\.");
            buf.append(methodName);
            buf.append("\\b)");
            pattern = buf.toString();
          }
          String newContents = "refined_" + decl.aspectName() + "_" + name() + "_" + methodName;
          // TODO: update keep to false if no strings are replaced.

          Matcher matcher = Pattern.compile(pattern).matcher(s);
          if (matcher.find()) {
            s = matcher.replaceAll(newContents);
            keep = true;
          }

          if (first) {
            s = " {" + s;
            first = false;
          }
          if (index == (parent.jjtGetNumChildren() - 1)) {
            s = s + "\n}\n";
          }

          org.jastadd.jrag.AST.Token token = org.jastadd.jrag.AST.Token.newToken(0);
          token.image = s;

          ((org.jastadd.jrag.AST.SimpleNode)parent.jjtGetChild(index-1)).lastToken.next = token;
          token.next = token;
          node = new org.jastadd.jrag.AST.ASTBlock(0);
          parent.lastToken = token;
          node.firstToken = node.lastToken = token;
          parent.jjtAddChild(node, index);
          node.jjtSetParent(parent);
        }
      }

      parent = decl.node;
      first = true;
      for (int index = 1; index < parent.jjtGetNumChildren(); index++) {
        org.jastadd.jrag.AST.SimpleNode child = (org.jastadd.jrag.AST.SimpleNode)parent.jjtGetChild(index);
        if (child instanceof org.jastadd.jrag.AST.ASTExplicitConstructorInvocation) {
          node = child;
          // Replace "aspectName.typeName.methodName" in refinedDecl with
          // "refined_aspectName_methodName".
          StringBuffer buf = new StringBuffer();
          node.jjtAccept(new ClassBodyDeclUnparser(), buf);
          String s = buf.toString();
          if (child instanceof org.jastadd.jrag.AST.ASTExplicitConstructorInvocation) {
            s = "";
          }
          if (first) {
            s = " {" + s;
            first = false;
          }

          org.jastadd.jrag.AST.Token token = org.jastadd.jrag.AST.Token.newToken(0);
          token.image = s;

          ((org.jastadd.jrag.AST.SimpleNode)parent.jjtGetChild(index-1)).lastToken.next = token;
          token.next = node.lastToken.next;
          node = new org.jastadd.jrag.AST.ASTExplicitConstructorInvocation(0);
          node.firstToken = node.lastToken = token;
          parent.jjtAddChild(node, index);
          node.jjtSetParent(parent);
        }
      }
    } else if (node instanceof org.jastadd.jrag.AST.ASTAspectMethodDeclaration
        || node instanceof org.jastadd.jrag.AST.ASTAspectRefineMethodDeclaration) {
      // Retrieve methodName.
      String idDecl = ((org.jastadd.jrag.AST.SimpleNode)decl.node.jjtGetChild(1)).firstToken.image;
      String methodName = idDecl.trim();

      // Add prefix refined_aspectName_.
      idDecl = idDecl.replaceAll(methodName,
          "refined_" + decl.aspectName() + "_" + name() + "_" + methodName);
      ((org.jastadd.jrag.AST.SimpleNode)decl.node.jjtGetChild(1)).firstToken.image = idDecl;

      org.jastadd.jrag.AST.SimpleNode parent = node;
      int index = 2;
      while (index < node.jjtGetNumChildren()
          && !(node.jjtGetChild(index) instanceof org.jastadd.jrag.AST.ASTBlock)) {
        index++;
      }
      if (index >= node.jjtGetNumChildren()) {
        throw new Error("Could not find block node");
      }
      node = (org.jastadd.jrag.AST.SimpleNode)node.jjtGetChild(index);

      // Replace "aspectName.typeName.methodName" in refinedDecl with
      // "refined_aspectName_methodName".
      StringBuffer buf = new StringBuffer();
      node.jjtAccept(new ClassBodyDeclUnparser(), buf);
      String s = buf.toString();
      String pattern = "\\brefined\\b";
      if (config().refineLegacy()) {
        buf = new StringBuffer();
        buf.append("(");
        buf.append(pattern);
        buf.append(")|(");
        buf.append("\\b");
        buf.append(decl.legacyAspectName());
        buf.append("\\.[a-zA-Z0-9_$]+\\.");
        buf.append(methodName);
        buf.append("\\b)");
        pattern = buf.toString();
      }
      String newContents = "refined_" + decl.aspectName() + "_" + name() + "_" + methodName;
      // TODO: update keep to false if no strings are replaced

      Matcher matcher = Pattern.compile(pattern).matcher(s);
      if (matcher.find()) {
        s = matcher.replaceAll(newContents);
      } else {
        keep = false;
      }

      org.jastadd.jrag.AST.Token token = org.jastadd.jrag.AST.Token.newToken(0);
      token.image = s;

      ((org.jastadd.jrag.AST.SimpleNode)parent.jjtGetChild(index-1)).lastToken.next = token;
      token.next = token;
      node = new org.jastadd.jrag.AST.ASTBlock(0);
      parent.lastToken = token;
      node.firstToken = node.lastToken = token;
      parent.jjtAddChild(node, index);
      node.jjtSetParent(parent);
    } else {
      throw new Error("Unexpected node type " + node.getClass().getName());
    }

    return keep;
  }
  /**
   * Find interface collection attribute declarations.
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:89
   */
  public Collection<CollDecl> interfaceCollDecls() {
    LinkedList<CollDecl> list = new LinkedList<CollDecl>();
    for (InterfaceDecl type : implementedInterfaces()) {
      for (CollDecl decl : type.getCollDeclList()) {
        list.add(decl);
      }
      list.addAll(type.interfaceCollDecls());
    }
    return list;
  }
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:430
   */
  public void weaveCollectionAttributes() { }
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:432
   */
  private Set<String> processedCollectingSignatures = new HashSet<String>();
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:434
   */
  protected boolean processedCollectingSignature(String signature) {
    if (processedCollectingSignatures.contains(signature)) {
      return true;
    } else {
      processedCollectingSignatures.add(signature);
      return false;
    }
  }
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:443
   */
  protected String collectionReset() {
    StringBuilder res = new StringBuilder();
    for (int k = 0; k < grammar().getNumTypeDecl(); k++) {
      TypeDecl typeDecl = grammar().getTypeDecl(k);
      for (CollDecl decl : typeDecl.interfaceCollDecls()) {
        TemplateContext tt = decl.templateContext();
        if (decl.root() == this) {
          res.append(tt.expand("Collection.flush"));
        }
      }
      for (int i = 0; i < typeDecl.getNumCollDecl(); i++) {
        CollDecl attr = typeDecl.getCollDecl(i);
        TemplateContext tt = attr.templateContext();
        if (attr.root() == this) {
          res.append(tt.expand("Collection.flush"));
        }
      }
    }
    return res.toString();
  }
  /**
   * @aspect Comments
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Comments.jrag:151
   */
  public String docComment() {
    JavaDocParser parser = new JavaDocParser();
    TemplateContext tt = templateContext();
    tt.bind("SourceComment", parser.parse(getComment()));
    tt.bind("HasAspectName", getAspectName().length() > 0);
    tt.bind("AspectName", getAspectName());
    String declaredat = ASTNode.declaredat(getFileName(), getStartLine());
    tt.bind("HasDeclaredAt", declaredat.length() > 0);
    tt.bind("DeclaredAt", declaredat);
    return tt.expand("TypeDecl.docComment");
  }
  /**
   * @aspect Comments
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Comments.jrag:169
   */
  public String extraDocCommentLines() {
    return "";
  }
  /** The list of interfaces this type implements. 
   * @aspect Grammar
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Grammar.jrag:39
   */
  public LinkedList implementsList = new LinkedList();
  /**
   * Add a member to this type declaration.
   * @aspect Grammar
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Grammar.jrag:120
   */
  protected void addClassDeclaration(String declaration, String sourceFile,
      int sourceLine) {
    org.jastadd.jrag.AST.SimpleNode n = new org.jastadd.jrag.AST.ASTBlock(0);
    n.firstToken = n.lastToken = org.jastadd.jrag.AST.Token.newToken(0);
    n.firstToken.image = declaration;
    classBodyDecls.add(new ClassBodyObject(n, sourceFile, sourceLine, "<NoAspect>"));
  }
  /**
   * @aspect InheritedAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:68
   */
  public void removeDuplicateInhDecls() {
  }
  /**
   * @aspect JastAddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JastAddCodeGen.jadd:126
   */
  public String modifiers = "";
  /**
   * @aspect JastAddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JastAddCodeGen.jadd:136
   */
  public String typeParameters = "";
  /**
   * @aspect JastAddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JastAddCodeGen.jadd:138
   */
  public String typeDeclarationString() {
    return "";
  }
  /**
   * @aspect JastAddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JastAddCodeGen.jadd:214
   */
  public void jastAddGen(boolean publicModifier) {
  }
  /**
   * @aspect JastAddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JastAddCodeGen.jadd:382
   */
  public String interfacesString() {
    StringBuffer buf = new StringBuffer();
    Iterator iter = implementsList.iterator();
    if (iter.hasNext()) {
      buf.append(Unparser.unparse((org.jastadd.jrag.AST.SimpleNode) iter.next()));
      while (iter.hasNext()) {
        buf.append(", " + Unparser.unparse((org.jastadd.jrag.AST.SimpleNode) iter.next()));
      }
    }
    return buf.toString();
  }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:167
   */
  public void emitMembers(PrintStream out) { }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:182
   */
  public void emitAbstractSyns(PrintStream out) {
    for (int i = 0; i < getNumSynDecl(); i++) {
      AttrDecl attr = getSynDecl(i);
      attr.emitSynDecl(out);
    }
  }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:381
   */
  public boolean hasLazySynEqFor(AttrDecl attr) {
    if (attr instanceof SynDecl) {
      SynEq synEq = lookupSynEq(attr.signature());
      return synEq != null && (synEq.decl().isLazy() || synEq.decl().isCircular());
    }
    return false;
  }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:649
   */
  public void emitInhDeclarations(PrintStream out) {
    for (int i = 0; i < getNumInhDecl(); i++) {
       AttrDecl attr = getInhDecl(i);
       attr.emitInhDecl(out);
    }
  }
  /**
   * @return list of inherited attributes with equations at this node.
   * @aspect ASTDecl
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:260
   */
  public Collection<String> inhAttrSet() {
    return inhEqMap().keySet();
  }
  /**
   * List of equations at this node for an inherited attribute.
   * The getChild equation, if present, is always last.
   * @param id inherited attribute name
   * @return list of equations for attribute
   * @aspect ASTDecl
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:270
   */
  public Collection<InhEq> inhAttrEqs(String id) {
    LinkedList<InhEq> list = inhEqMap().get(id);
    return list != null ? list : Collections.<InhEq>emptyList();
  }
  /**
   * @declaredat ASTNode:1
   */
  public TypeDecl(int i) {
    super(i);
  }
  /**
   * @declaredat ASTNode:5
   */
  public TypeDecl(Ast p, int i) {
    this(i);
    parser = p;
  }
  /**
   * @declaredat ASTNode:10
   */
  public TypeDecl() {
    this(0);
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:19
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[8];
    setChild(new List(), 1);
    setChild(new List(), 2);
    setChild(new List(), 3);
    setChild(new List(), 4);
    setChild(new List(), 5);
    setChild(new List(), 6);
    setChild(new List(), 7);
  }
  /**
   * @declaredat ASTNode:29
   */
  public TypeDecl(IdDecl p0, List<ClassBodyDecl> p1, List<SynDecl> p2, List<SynEq> p3, List<InhDecl> p4, List<InhEq> p5, List<Component> p6, List<CollDecl> p7, String p8, int p9, int p10, String p11, String p12) {
    setChild(p0, 0);
    setChild(p1, 1);
    setChild(p2, 2);
    setChild(p3, 3);
    setChild(p4, 4);
    setChild(p5, 5);
    setChild(p6, 6);
    setChild(p7, 7);
    setFileName(p8);
    setStartLine(p9);
    setEndLine(p10);
    setComment(p11);
    setAspectName(p12);
  }
  /**
   * @declaredat ASTNode:44
   */
  public void dumpTree(String indent, java.io.PrintStream out) {
    out.print(indent + "TypeDecl");
    out.print("\"" + getFileName() + "\"");
    out.print("\"" + getStartLine() + "\"");
    out.print("\"" + getEndLine() + "\"");
    out.print("\"" + getComment() + "\"");
    out.print("\"" + getAspectName() + "\"");
    String childIndent = indent + "  ";
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).dumpTree(childIndent, out);
    }
  }
  /**
   * @declaredat ASTNode:56
   */
  public Object jjtAccept(AstVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
  /**
   * @declaredat ASTNode:59
   */
  public void jjtAddChild(Node n, int i) {
    checkChild(n, i);
    super.jjtAddChild(n, i);
  }
  /**
   * @declaredat ASTNode:63
   */
  public void checkChild(Node n, int i) {
    if (i == 0 && !(n instanceof IdDecl)) {
     throw new Error("Child number 0 of TypeDecl has the type " +
       n.getClass().getName() + " which is not an instance of IdDecl");
    }
      if (i == 1) {
        if (!(n instanceof List)) {
          throw new Error("Child number 1 of TypeDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof ClassBodyDecl)) {
            throw new Error("Child number " + k + " in ClassBodyDeclList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of ClassBodyDecl");
          }
        }
      }
      if (i == 2) {
        if (!(n instanceof List)) {
          throw new Error("Child number 2 of TypeDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof SynDecl)) {
            throw new Error("Child number " + k + " in SynDeclList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of SynDecl");
          }
        }
      }
      if (i == 3) {
        if (!(n instanceof List)) {
          throw new Error("Child number 3 of TypeDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof SynEq)) {
            throw new Error("Child number " + k + " in SynEqList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of SynEq");
          }
        }
      }
      if (i == 4) {
        if (!(n instanceof List)) {
          throw new Error("Child number 4 of TypeDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof InhDecl)) {
            throw new Error("Child number " + k + " in InhDeclList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of InhDecl");
          }
        }
      }
      if (i == 5) {
        if (!(n instanceof List)) {
          throw new Error("Child number 5 of TypeDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof InhEq)) {
            throw new Error("Child number " + k + " in InhEqList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of InhEq");
          }
        }
      }
      if (i == 6) {
        if (!(n instanceof List)) {
          throw new Error("Child number 6 of TypeDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof Component)) {
            throw new Error("Child number " + k + " in ComponentList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of Component");
          }
        }
      }
      if (i == 7) {
        if (!(n instanceof List)) {
          throw new Error("Child number 7 of TypeDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof CollDecl)) {
            throw new Error("Child number " + k + " in CollDeclList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of CollDecl");
          }
        }
      }
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:154
   */
  @SideEffect.Pure public int getNumChild() {
    return 8;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:160
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:164
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    lookupComponent_String_reset();
    component_String_reset();
    components_reset();
    lookupInhDecl_String_reset();
    synEquations_reset();
    synDeclarations_reset();
    inhEquations_reset();
    inhDeclarations_reset();
    lookupSynDecl_String_reset();
    lookupSynEq_String_reset();
    hasInhEq_InhDecl_Component_reset();
    lookupInhEq_String_String_reset();
    inhEqMap_reset();
    findSubclasses_ASTDecl_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:182
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
    TypeDecl_attributeProblems_visited = -1;
    TypeDecl_attributeProblems_computed = false;
    
    TypeDecl_attributeProblems_value = null;
    contributorMap_TypeDecl_attributeProblems = null;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:191
   */
  @SideEffect.Fresh public TypeDecl clone() throws CloneNotSupportedException {
    TypeDecl node = (TypeDecl) super.clone();
    return node;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:202
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public abstract TypeDecl fullCopy();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:210
   */
  @SideEffect.Fresh(group="_ASTNode") public abstract TypeDecl treeCopyNoTransform();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:218
   */
  @SideEffect.Fresh(group="_ASTNode") public abstract TypeDecl treeCopy();
  /**
   * Replaces the IdDecl child.
   * @param node The new node to replace the IdDecl child.
   * @apilevel high-level
   */
  public void setIdDecl(IdDecl node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the IdDecl child.
   * @return The current node used as the IdDecl child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="IdDecl")
  @SideEffect.Pure public IdDecl getIdDecl() {
    return (IdDecl) getChild(0);
  }
  /**
   * Retrieves the IdDecl child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the IdDecl child.
   * @apilevel low-level
   */
  @SideEffect.Pure public IdDecl getIdDeclNoTransform() {
    return (IdDecl) getChildNoTransform(0);
  }
  /**
   * Replaces the ClassBodyDecl list.
   * @param list The new list node to be used as the ClassBodyDecl list.
   * @apilevel high-level
   */
  public void setClassBodyDeclList(List<ClassBodyDecl> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the ClassBodyDecl list.
   * @return Number of children in the ClassBodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumClassBodyDecl() {
    return getClassBodyDeclList().getNumChild();
  }
  /**
   * Retrieves the number of children in the ClassBodyDecl list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the ClassBodyDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumClassBodyDeclNoTransform() {
    return getClassBodyDeclListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the ClassBodyDecl list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the ClassBodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public ClassBodyDecl getClassBodyDecl(int i) {
    return (ClassBodyDecl) getClassBodyDeclList().getChild(i);
  }
  /**
   * Check whether the ClassBodyDecl list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasClassBodyDecl() {
    return getClassBodyDeclList().getNumChild() != 0;
  }
  /**
   * Append an element to the ClassBodyDecl list.
   * @param node The element to append to the ClassBodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addClassBodyDecl(ClassBodyDecl node) {
    List<ClassBodyDecl> list = (parent == null) ? getClassBodyDeclListNoTransform() : getClassBodyDeclList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addClassBodyDeclNoTransform(ClassBodyDecl node) {
    List<ClassBodyDecl> list = getClassBodyDeclListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the ClassBodyDecl list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setClassBodyDecl(ClassBodyDecl node, int i) {
    List<ClassBodyDecl> list = getClassBodyDeclList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the ClassBodyDecl list.
   * @return The node representing the ClassBodyDecl list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="ClassBodyDecl")
  @SideEffect.Pure(group="_ASTNode") public List<ClassBodyDecl> getClassBodyDeclList() {
    List<ClassBodyDecl> list = (List<ClassBodyDecl>) getChild(1);
    return list;
  }
  /**
   * Retrieves the ClassBodyDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the ClassBodyDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<ClassBodyDecl> getClassBodyDeclListNoTransform() {
    return (List<ClassBodyDecl>) getChildNoTransform(1);
  }
  /**
   * @return the element at index {@code i} in the ClassBodyDecl list without
   * triggering rewrites.
   */
  @SideEffect.Pure public ClassBodyDecl getClassBodyDeclNoTransform(int i) {
    return (ClassBodyDecl) getClassBodyDeclListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the ClassBodyDecl list.
   * @return The node representing the ClassBodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<ClassBodyDecl> getClassBodyDecls() {
    return getClassBodyDeclList();
  }
  /**
   * Retrieves the ClassBodyDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the ClassBodyDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<ClassBodyDecl> getClassBodyDeclsNoTransform() {
    return getClassBodyDeclListNoTransform();
  }
  /**
   * Replaces the SynDecl list.
   * @param list The new list node to be used as the SynDecl list.
   * @apilevel high-level
   */
  public void setSynDeclList(List<SynDecl> list) {
    setChild(list, 2);
  }
  /**
   * Retrieves the number of children in the SynDecl list.
   * @return Number of children in the SynDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumSynDecl() {
    return getSynDeclList().getNumChild();
  }
  /**
   * Retrieves the number of children in the SynDecl list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the SynDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumSynDeclNoTransform() {
    return getSynDeclListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the SynDecl list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the SynDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public SynDecl getSynDecl(int i) {
    return (SynDecl) getSynDeclList().getChild(i);
  }
  /**
   * Check whether the SynDecl list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasSynDecl() {
    return getSynDeclList().getNumChild() != 0;
  }
  /**
   * Append an element to the SynDecl list.
   * @param node The element to append to the SynDecl list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addSynDecl(SynDecl node) {
    List<SynDecl> list = (parent == null) ? getSynDeclListNoTransform() : getSynDeclList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addSynDeclNoTransform(SynDecl node) {
    List<SynDecl> list = getSynDeclListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the SynDecl list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setSynDecl(SynDecl node, int i) {
    List<SynDecl> list = getSynDeclList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the SynDecl list.
   * @return The node representing the SynDecl list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="SynDecl")
  @SideEffect.Pure(group="_ASTNode") public List<SynDecl> getSynDeclList() {
    List<SynDecl> list = (List<SynDecl>) getChild(2);
    return list;
  }
  /**
   * Retrieves the SynDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the SynDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<SynDecl> getSynDeclListNoTransform() {
    return (List<SynDecl>) getChildNoTransform(2);
  }
  /**
   * @return the element at index {@code i} in the SynDecl list without
   * triggering rewrites.
   */
  @SideEffect.Pure public SynDecl getSynDeclNoTransform(int i) {
    return (SynDecl) getSynDeclListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the SynDecl list.
   * @return The node representing the SynDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<SynDecl> getSynDecls() {
    return getSynDeclList();
  }
  /**
   * Retrieves the SynDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the SynDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<SynDecl> getSynDeclsNoTransform() {
    return getSynDeclListNoTransform();
  }
  /**
   * Replaces the SynEq list.
   * @param list The new list node to be used as the SynEq list.
   * @apilevel high-level
   */
  public void setSynEqList(List<SynEq> list) {
    setChild(list, 3);
  }
  /**
   * Retrieves the number of children in the SynEq list.
   * @return Number of children in the SynEq list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumSynEq() {
    return getSynEqList().getNumChild();
  }
  /**
   * Retrieves the number of children in the SynEq list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the SynEq list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumSynEqNoTransform() {
    return getSynEqListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the SynEq list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the SynEq list.
   * @apilevel high-level
   */
  @SideEffect.Pure public SynEq getSynEq(int i) {
    return (SynEq) getSynEqList().getChild(i);
  }
  /**
   * Check whether the SynEq list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasSynEq() {
    return getSynEqList().getNumChild() != 0;
  }
  /**
   * Append an element to the SynEq list.
   * @param node The element to append to the SynEq list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addSynEq(SynEq node) {
    List<SynEq> list = (parent == null) ? getSynEqListNoTransform() : getSynEqList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addSynEqNoTransform(SynEq node) {
    List<SynEq> list = getSynEqListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the SynEq list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setSynEq(SynEq node, int i) {
    List<SynEq> list = getSynEqList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the SynEq list.
   * @return The node representing the SynEq list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="SynEq")
  @SideEffect.Pure(group="_ASTNode") public List<SynEq> getSynEqList() {
    List<SynEq> list = (List<SynEq>) getChild(3);
    return list;
  }
  /**
   * Retrieves the SynEq list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the SynEq list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<SynEq> getSynEqListNoTransform() {
    return (List<SynEq>) getChildNoTransform(3);
  }
  /**
   * @return the element at index {@code i} in the SynEq list without
   * triggering rewrites.
   */
  @SideEffect.Pure public SynEq getSynEqNoTransform(int i) {
    return (SynEq) getSynEqListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the SynEq list.
   * @return The node representing the SynEq list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<SynEq> getSynEqs() {
    return getSynEqList();
  }
  /**
   * Retrieves the SynEq list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the SynEq list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<SynEq> getSynEqsNoTransform() {
    return getSynEqListNoTransform();
  }
  /**
   * Replaces the InhDecl list.
   * @param list The new list node to be used as the InhDecl list.
   * @apilevel high-level
   */
  public void setInhDeclList(List<InhDecl> list) {
    setChild(list, 4);
  }
  /**
   * Retrieves the number of children in the InhDecl list.
   * @return Number of children in the InhDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumInhDecl() {
    return getInhDeclList().getNumChild();
  }
  /**
   * Retrieves the number of children in the InhDecl list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the InhDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumInhDeclNoTransform() {
    return getInhDeclListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the InhDecl list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the InhDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public InhDecl getInhDecl(int i) {
    return (InhDecl) getInhDeclList().getChild(i);
  }
  /**
   * Check whether the InhDecl list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasInhDecl() {
    return getInhDeclList().getNumChild() != 0;
  }
  /**
   * Append an element to the InhDecl list.
   * @param node The element to append to the InhDecl list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addInhDecl(InhDecl node) {
    List<InhDecl> list = (parent == null) ? getInhDeclListNoTransform() : getInhDeclList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addInhDeclNoTransform(InhDecl node) {
    List<InhDecl> list = getInhDeclListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the InhDecl list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setInhDecl(InhDecl node, int i) {
    List<InhDecl> list = getInhDeclList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the InhDecl list.
   * @return The node representing the InhDecl list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="InhDecl")
  @SideEffect.Pure(group="_ASTNode") public List<InhDecl> getInhDeclList() {
    List<InhDecl> list = (List<InhDecl>) getChild(4);
    return list;
  }
  /**
   * Retrieves the InhDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the InhDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<InhDecl> getInhDeclListNoTransform() {
    return (List<InhDecl>) getChildNoTransform(4);
  }
  /**
   * @return the element at index {@code i} in the InhDecl list without
   * triggering rewrites.
   */
  @SideEffect.Pure public InhDecl getInhDeclNoTransform(int i) {
    return (InhDecl) getInhDeclListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the InhDecl list.
   * @return The node representing the InhDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<InhDecl> getInhDecls() {
    return getInhDeclList();
  }
  /**
   * Retrieves the InhDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the InhDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<InhDecl> getInhDeclsNoTransform() {
    return getInhDeclListNoTransform();
  }
  /**
   * Replaces the InhEq list.
   * @param list The new list node to be used as the InhEq list.
   * @apilevel high-level
   */
  public void setInhEqList(List<InhEq> list) {
    setChild(list, 5);
  }
  /**
   * Retrieves the number of children in the InhEq list.
   * @return Number of children in the InhEq list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumInhEq() {
    return getInhEqList().getNumChild();
  }
  /**
   * Retrieves the number of children in the InhEq list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the InhEq list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumInhEqNoTransform() {
    return getInhEqListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the InhEq list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the InhEq list.
   * @apilevel high-level
   */
  @SideEffect.Pure public InhEq getInhEq(int i) {
    return (InhEq) getInhEqList().getChild(i);
  }
  /**
   * Check whether the InhEq list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasInhEq() {
    return getInhEqList().getNumChild() != 0;
  }
  /**
   * Append an element to the InhEq list.
   * @param node The element to append to the InhEq list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addInhEq(InhEq node) {
    List<InhEq> list = (parent == null) ? getInhEqListNoTransform() : getInhEqList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addInhEqNoTransform(InhEq node) {
    List<InhEq> list = getInhEqListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the InhEq list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setInhEq(InhEq node, int i) {
    List<InhEq> list = getInhEqList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the InhEq list.
   * @return The node representing the InhEq list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="InhEq")
  @SideEffect.Pure(group="_ASTNode") public List<InhEq> getInhEqList() {
    List<InhEq> list = (List<InhEq>) getChild(5);
    return list;
  }
  /**
   * Retrieves the InhEq list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the InhEq list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<InhEq> getInhEqListNoTransform() {
    return (List<InhEq>) getChildNoTransform(5);
  }
  /**
   * @return the element at index {@code i} in the InhEq list without
   * triggering rewrites.
   */
  @SideEffect.Pure public InhEq getInhEqNoTransform(int i) {
    return (InhEq) getInhEqListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the InhEq list.
   * @return The node representing the InhEq list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<InhEq> getInhEqs() {
    return getInhEqList();
  }
  /**
   * Retrieves the InhEq list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the InhEq list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<InhEq> getInhEqsNoTransform() {
    return getInhEqListNoTransform();
  }
  /**
   * Replaces the Component list.
   * @param list The new list node to be used as the Component list.
   * @apilevel high-level
   */
  public void setComponentList(List<Component> list) {
    setChild(list, 6);
  }
  /**
   * Retrieves the number of children in the Component list.
   * @return Number of children in the Component list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumComponent() {
    return getComponentList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Component list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Component list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumComponentNoTransform() {
    return getComponentListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Component list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Component list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Component getComponent(int i) {
    return (Component) getComponentList().getChild(i);
  }
  /**
   * Check whether the Component list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasComponent() {
    return getComponentList().getNumChild() != 0;
  }
  /**
   * Append an element to the Component list.
   * @param node The element to append to the Component list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addComponent(Component node) {
    List<Component> list = (parent == null) ? getComponentListNoTransform() : getComponentList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addComponentNoTransform(Component node) {
    List<Component> list = getComponentListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Component list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setComponent(Component node, int i) {
    List<Component> list = getComponentList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Component list.
   * @return The node representing the Component list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Component")
  @SideEffect.Pure(group="_ASTNode") public List<Component> getComponentList() {
    List<Component> list = (List<Component>) getChild(6);
    return list;
  }
  /**
   * Retrieves the Component list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Component list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Component> getComponentListNoTransform() {
    return (List<Component>) getChildNoTransform(6);
  }
  /**
   * @return the element at index {@code i} in the Component list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Component getComponentNoTransform(int i) {
    return (Component) getComponentListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Component list.
   * @return The node representing the Component list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Component> getComponents() {
    return getComponentList();
  }
  /**
   * Retrieves the Component list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Component list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Component> getComponentsNoTransform() {
    return getComponentListNoTransform();
  }
  /**
   * Replaces the CollDecl list.
   * @param list The new list node to be used as the CollDecl list.
   * @apilevel high-level
   */
  public void setCollDeclList(List<CollDecl> list) {
    setChild(list, 7);
  }
  /**
   * Retrieves the number of children in the CollDecl list.
   * @return Number of children in the CollDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumCollDecl() {
    return getCollDeclList().getNumChild();
  }
  /**
   * Retrieves the number of children in the CollDecl list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the CollDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumCollDeclNoTransform() {
    return getCollDeclListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the CollDecl list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the CollDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public CollDecl getCollDecl(int i) {
    return (CollDecl) getCollDeclList().getChild(i);
  }
  /**
   * Check whether the CollDecl list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasCollDecl() {
    return getCollDeclList().getNumChild() != 0;
  }
  /**
   * Append an element to the CollDecl list.
   * @param node The element to append to the CollDecl list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addCollDecl(CollDecl node) {
    List<CollDecl> list = (parent == null) ? getCollDeclListNoTransform() : getCollDeclList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addCollDeclNoTransform(CollDecl node) {
    List<CollDecl> list = getCollDeclListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the CollDecl list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setCollDecl(CollDecl node, int i) {
    List<CollDecl> list = getCollDeclList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the CollDecl list.
   * @return The node representing the CollDecl list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="CollDecl")
  @SideEffect.Pure(group="_ASTNode") public List<CollDecl> getCollDeclList() {
    List<CollDecl> list = (List<CollDecl>) getChild(7);
    return list;
  }
  /**
   * Retrieves the CollDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the CollDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<CollDecl> getCollDeclListNoTransform() {
    return (List<CollDecl>) getChildNoTransform(7);
  }
  /**
   * @return the element at index {@code i} in the CollDecl list without
   * triggering rewrites.
   */
  @SideEffect.Pure public CollDecl getCollDeclNoTransform(int i) {
    return (CollDecl) getCollDeclListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the CollDecl list.
   * @return The node representing the CollDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<CollDecl> getCollDecls() {
    return getCollDeclList();
  }
  /**
   * Retrieves the CollDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the CollDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<CollDecl> getCollDeclsNoTransform() {
    return getCollDeclListNoTransform();
  }
  /**
   * Replaces the lexeme FileName.
   * @param value The new value for the lexeme FileName.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setFileName(String value) {
    tokenString_FileName = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_FileName;
  /**
   * Retrieves the value for the lexeme FileName.
   * @return The value for the lexeme FileName.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="FileName")
  @SideEffect.Pure(group="_ASTNode") public String getFileName() {
    return tokenString_FileName != null ? tokenString_FileName : "";
  }
  /**
   * Replaces the lexeme StartLine.
   * @param value The new value for the lexeme StartLine.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setStartLine(int value) {
    tokenint_StartLine = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected int tokenint_StartLine;
  /**
   * Retrieves the value for the lexeme StartLine.
   * @return The value for the lexeme StartLine.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="StartLine")
  @SideEffect.Pure(group="_ASTNode") public int getStartLine() {
    return tokenint_StartLine;
  }
  /**
   * Replaces the lexeme EndLine.
   * @param value The new value for the lexeme EndLine.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setEndLine(int value) {
    tokenint_EndLine = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected int tokenint_EndLine;
  /**
   * Retrieves the value for the lexeme EndLine.
   * @return The value for the lexeme EndLine.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="EndLine")
  @SideEffect.Pure(group="_ASTNode") public int getEndLine() {
    return tokenint_EndLine;
  }
  /**
   * Replaces the lexeme Comment.
   * @param value The new value for the lexeme Comment.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setComment(String value) {
    tokenString_Comment = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_Comment;
  /**
   * Retrieves the value for the lexeme Comment.
   * @return The value for the lexeme Comment.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Comment")
  @SideEffect.Pure(group="_ASTNode") public String getComment() {
    return tokenString_Comment != null ? tokenString_Comment : "";
  }
  /**
   * Replaces the lexeme AspectName.
   * @param value The new value for the lexeme AspectName.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setAspectName(String value) {
    tokenString_AspectName = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_AspectName;
  /**
   * Retrieves the value for the lexeme AspectName.
   * @return The value for the lexeme AspectName.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="AspectName")
  @SideEffect.Pure(group="_ASTNode") public String getAspectName() {
    return tokenString_AspectName != null ? tokenString_AspectName : "";
  }
  /**
   * @aspect <NoAspect>
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:91
   */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map<ASTNode, java.util.Set<ASTNode>> contributorMap_TypeDecl_attributeProblems = null;

  @SideEffect.Ignore protected void survey_TypeDecl_attributeProblems() {
    if (contributorMap_TypeDecl_attributeProblems == null) {
      contributorMap_TypeDecl_attributeProblems = new java.util.IdentityHashMap<ASTNode, java.util.Set<ASTNode>>();
      collect_contributors_TypeDecl_attributeProblems(this, contributorMap_TypeDecl_attributeProblems);
    }
  }

  /**
   * @attribute syn
   * @aspect Comments
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Comments.jrag:163
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Comments", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Comments.jrag:163")
  @SideEffect.Pure(group="_ASTNode") public abstract String typeDeclKind();
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isRootNode_visited = -1;
  /**
   * @attribute syn
   * @aspect ASTErrors
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTErrors.jrag:46
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ASTErrors", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTErrors.jrag:46")
  @SideEffect.Pure(group="_ASTNode") public boolean isRootNode() {
    if (isRootNode_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.isRootNode().");
    }
    isRootNode_visited = state().boundariesCrossed;
    boolean isRootNode_value = false;
    isRootNode_visited = -1;
    return isRootNode_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isPotentialRootNode_visited = -1;
  /**
   * @attribute syn
   * @aspect ASTErrors
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTErrors.jrag:50
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ASTErrors", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTErrors.jrag:50")
  @SideEffect.Pure(group="_ASTNode") public boolean isPotentialRootNode() {
    if (isPotentialRootNode_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.isPotentialRootNode().");
    }
    isPotentialRootNode_visited = state().boundariesCrossed;
    boolean isPotentialRootNode_value = false;
    isPotentialRootNode_visited = -1;
    return isPotentialRootNode_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map lookupComponent_String_visited = new java.util.HashMap(4);
  /** @apilevel internal */
  @SideEffect.Ignore private void lookupComponent_String_reset() {
    lookupComponent_String_values = new java.util.HashMap(4);
    lookupComponent_String_visited = new java.util.HashMap(4);
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map lookupComponent_String_values = new java.util.HashMap(4);

  /**
   * @attribute syn
   * @aspect Lookup
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:90
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Lookup", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:90")
  @SideEffect.Pure(group="_ASTNode") public Component lookupComponent(String name) {
    Object _parameters = name;
    ASTState state = state();
    if (lookupComponent_String_values.containsKey(_parameters)) {
      return (Component) lookupComponent_String_values.get(_parameters);
    }
    if (Integer.valueOf(state().boundariesCrossed).equals(lookupComponent_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.lookupComponent(String).");
    }
    lookupComponent_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    Component lookupComponent_String_value = null;
    if (isFinal && _boundaries == state().boundariesCrossed) {
    lookupComponent_String_values.put(_parameters, lookupComponent_String_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    lookupComponent_String_visited.remove(_parameters);
    return lookupComponent_String_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int name_visited = -1;
  /**
   * @attribute syn
   * @aspect Names
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:103
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Names", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:103")
  @SideEffect.Pure(group="_ASTNode") public String name() {
    if (name_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.name().");
    }
    name_visited = state().boundariesCrossed;
    String name_value = getIdDecl().name();
    name_visited = -1;
    return name_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map missingSynEqs_String_visited = new java.util.HashMap(4);
  /**
   * @param signature the signature of the attribute
   * @return the subclasses of this AST class that are missing an
   * equation for for the synthesized attribute
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:248
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:248")
  @SideEffect.Pure(group="_ASTNode") public Collection<? extends TypeDecl> missingSynEqs(String signature) {
    Object _parameters = signature;
    if (Integer.valueOf(state().boundariesCrossed).equals(missingSynEqs_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.missingSynEqs(String).");
    }
    missingSynEqs_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    Collection<? extends TypeDecl> missingSynEqs_String_value = lookupSynEq(signature) == null
          ? Collections.singleton(this)
          : Collections.<TypeDecl>emptyList();
    missingSynEqs_String_visited.remove(_parameters);
    return missingSynEqs_String_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map component_String_visited = new java.util.HashMap(4);
  /** @apilevel internal */
  @SideEffect.Ignore private void component_String_reset() {
    component_String_values = new java.util.HashMap(4);
    component_String_visited = new java.util.HashMap(4);
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map component_String_values = new java.util.HashMap(4);

  /** Find the component with the given name. 
   * @attribute syn
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:384
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Attributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:384")
  @SideEffect.Pure(group="_ASTNode") public Component component(String name) {
    Object _parameters = name;
    ASTState state = state();
    if (component_String_values.containsKey(_parameters)) {
      return (Component) component_String_values.get(_parameters);
    }
    if (Integer.valueOf(state().boundariesCrossed).equals(component_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.component(String).");
    }
    component_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    Component component_String_value = component_compute(name);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    component_String_values.put(_parameters, component_String_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    component_String_visited.remove(_parameters);
    return component_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Component component_compute(String name) {
      for (Component c : components()) {
        if (c.name().equals(name)) {
          return c;
        }
      }
      return null;
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map instanceOf_TypeDecl_visited = new java.util.HashMap(4);
  /**
   * @attribute syn
   * @aspect InstanceOf
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:77
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="InstanceOf", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:77")
  @SideEffect.Pure(group="_ASTNode") public boolean instanceOf(TypeDecl c) {
    Object _parameters = c;
    if (Integer.valueOf(state().boundariesCrossed).equals(instanceOf_TypeDecl_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.instanceOf(TypeDecl).");
    }
    instanceOf_TypeDecl_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean instanceOf_TypeDecl_value = c == this;
    instanceOf_TypeDecl_visited.remove(_parameters);
    return instanceOf_TypeDecl_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int parentsIntransitive_visited = -1;
  /**
   * @attribute syn
   * @aspect Parents
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:168
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Parents", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:168")
  @SideEffect.Pure(group="_ASTNode") public Collection<ASTDecl> parentsIntransitive() {
    if (parentsIntransitive_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.parentsIntransitive().");
    }
    parentsIntransitive_visited = state().boundariesCrossed;
    Collection<ASTDecl> parentsIntransitive_value = grammar().parentMap().get(this);
    parentsIntransitive_visited = -1;
    return parentsIntransitive_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map hasCollEq_CollDecl_visited = new java.util.HashMap(4);
  /**
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:151
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:151")
  @SideEffect.Pure(group="_ASTNode") public boolean hasCollEq(CollDecl decl) {
    Object _parameters = decl;
    if (Integer.valueOf(state().boundariesCrossed).equals(hasCollEq_CollDecl_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.hasCollEq(CollDecl).");
    }
    hasCollEq_CollDecl_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean hasCollEq_CollDecl_value = false;
    hasCollEq_CollDecl_visited.remove(_parameters);
    return hasCollEq_CollDecl_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int components_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void components_reset() {
    components_computed = false;
    
    components_value = null;
    components_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean components_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Collection<Component> components_value;

  /**
   * @attribute syn
   * @aspect Comp
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:78
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Comp", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:78")
  @SideEffect.Pure(group="_ASTNode") public Collection<Component> components() {
    ASTState state = state();
    if (components_computed) {
      return components_value;
    }
    if (components_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.components().");
    }
    components_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    components_value = components_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    components_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    components_visited = -1;
    return components_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Collection<Component> components_compute() {
      LinkedList list = new LinkedList();
      for (int i = 0; i < getNumComponent(); i++) {
        list.add(getComponent(i));
      }
      return list;
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int implementedInterfaces_visited = -1;
  /**
   * @return a collection of the interfaces this type implements.
   * @attribute syn
   * @aspect Grammar
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Grammar.jrag:101
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Grammar", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Grammar.jrag:101")
  @SideEffect.Pure(group="_ASTNode") public Collection<InterfaceDecl> implementedInterfaces() {
    if (implementedInterfaces_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.implementedInterfaces().");
    }
    implementedInterfaces_visited = state().boundariesCrossed;
    try {
        LinkedList<InterfaceDecl> list = new LinkedList<InterfaceDecl>();
        for (Iterator iter = implementsList.iterator(); iter.hasNext(); ) {
          String typename = Unparser.unparse((org.jastadd.jrag.AST.SimpleNode) iter.next());
          int index = typename.indexOf('<');
          if (index != -1) {
            typename = typename.substring(0, index);
          }
          TypeDecl type = grammar().lookup(typename);
          if (type instanceof InterfaceDecl) {
            list.add((InterfaceDecl) type);
          }
        }
        return list;
      }
    finally {
      implementedInterfaces_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map lookupInhDecl_String_visited = new java.util.HashMap(4);
  /** @apilevel internal */
  @SideEffect.Ignore private void lookupInhDecl_String_reset() {
    lookupInhDecl_String_values = new java.util.HashMap(4);
    lookupInhDecl_String_visited = new java.util.HashMap(4);
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map lookupInhDecl_String_values = new java.util.HashMap(4);

  /**
   * @attribute syn
   * @aspect InheritedAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:86
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="InheritedAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:86")
  @SideEffect.Pure(group="_ASTNode") public InhDecl lookupInhDecl(String signature) {
    Object _parameters = signature;
    ASTState state = state();
    if (lookupInhDecl_String_values.containsKey(_parameters)) {
      return (InhDecl) lookupInhDecl_String_values.get(_parameters);
    }
    if (Integer.valueOf(state().boundariesCrossed).equals(lookupInhDecl_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.lookupInhDecl(String).");
    }
    lookupInhDecl_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    InhDecl lookupInhDecl_String_value = lookupInhDecl_compute(signature);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    lookupInhDecl_String_values.put(_parameters, lookupInhDecl_String_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    lookupInhDecl_String_visited.remove(_parameters);
    return lookupInhDecl_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private InhDecl lookupInhDecl_compute(String signature) {
      for (int i = 0; i < getNumInhDecl(); i++) {
        if (getInhDecl(i).signature().equals(signature)) {
          return getInhDecl(i);
        }
      }
      return null;
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int modifiers_visited = -1;
  /**
   * @attribute syn
   * @aspect JastAddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JastAddCodeGen.jadd:128
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JastAddCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JastAddCodeGen.jadd:128")
  @SideEffect.Pure(group="_ASTNode") public String modifiers() {
    if (modifiers_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.modifiers().");
    }
    modifiers_visited = state().boundariesCrossed;
    try {
        if (modifiers == null) {
          return "";
        } else {
          return modifiers + " ";
        }
      }
    finally {
      modifiers_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map hasInhEq_String_visited = new java.util.HashMap(4);
  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:668
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:668")
  @SideEffect.Pure(group="_ASTNode") public boolean hasInhEq(String attrName) {
    Object _parameters = attrName;
    if (Integer.valueOf(state().boundariesCrossed).equals(hasInhEq_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.hasInhEq(String).");
    }
    hasInhEq_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    try {
        for (int i = 0; i < getNumInhEq(); i++) {
          InhEq equ = getInhEq(i);
          if (equ.getName().equals(attrName)) {
            return true;
          }
        }
        return false;
      }
    finally {
      hasInhEq_String_visited.remove(_parameters);
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map lookupSynDeclPrefix_String_visited = new java.util.HashMap(4);
  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:703
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:703")
  @SideEffect.Pure(group="_ASTNode") public SynDecl lookupSynDeclPrefix(String signature) {
    Object _parameters = signature;
    if (Integer.valueOf(state().boundariesCrossed).equals(lookupSynDeclPrefix_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.lookupSynDeclPrefix(String).");
    }
    lookupSynDeclPrefix_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    try {
        for (int i = 0; i < getNumSynDecl(); i++) {
          if (getSynDecl(i).signature().equals(signature)
              || getSynDecl(i).signature().startsWith(signature + "_")) {
            return getSynDecl(i);
          }
        }
        return null;
      }
    finally {
      lookupSynDeclPrefix_String_visited.remove(_parameters);
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map lookupInhDeclPrefix_String_visited = new java.util.HashMap(4);
  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:721
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:721")
  @SideEffect.Pure(group="_ASTNode") public InhDecl lookupInhDeclPrefix(String signature) {
    Object _parameters = signature;
    if (Integer.valueOf(state().boundariesCrossed).equals(lookupInhDeclPrefix_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.lookupInhDeclPrefix(String).");
    }
    lookupInhDeclPrefix_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    try {
        for (int i = 0; i < getNumInhDecl(); i++) {
          if (getInhDecl(i).signature().equals(signature)
              || getInhDecl(i).signature().startsWith(signature + "_")) {
            return getInhDecl(i);
          }
        }
        return null;
      }
    finally {
      lookupInhDeclPrefix_String_visited.remove(_parameters);
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int synEquations_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void synEquations_reset() {
    synEquations_computed = false;
    
    synEquations_value = null;
    synEquations_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean synEquations_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Set synEquations_value;

  /**
   * @attribute syn
   * @aspect AllEquations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:105
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AllEquations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:105")
  @SideEffect.Pure(group="_ASTNode") public Set synEquations() {
    ASTState state = state();
    if (synEquations_computed) {
      return synEquations_value;
    }
    if (synEquations_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.synEquations().");
    }
    synEquations_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    synEquations_value = synEquations_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    synEquations_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    synEquations_visited = -1;
    return synEquations_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Set synEquations_compute() {
      Set set = new LinkedHashSet();
      for (int i = 0; i < getNumSynEq(); i++) {
        set.add(getSynEq(i));
      }
      return set;
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int synDeclarations_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void synDeclarations_reset() {
    synDeclarations_computed = false;
    
    synDeclarations_value = null;
    synDeclarations_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean synDeclarations_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Set synDeclarations_value;

  /**
   * @attribute syn
   * @aspect AllEquations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:120
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AllEquations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:120")
  @SideEffect.Pure(group="_ASTNode") public Set synDeclarations() {
    ASTState state = state();
    if (synDeclarations_computed) {
      return synDeclarations_value;
    }
    if (synDeclarations_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.synDeclarations().");
    }
    synDeclarations_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    synDeclarations_value = synDeclarations_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    synDeclarations_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    synDeclarations_visited = -1;
    return synDeclarations_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Set synDeclarations_compute() {
      Set set = new LinkedHashSet();
      for (int i = 0; i < getNumSynDecl(); i++) {
        set.add(getSynDecl(i));
      }
      return set;
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int inhEquations_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void inhEquations_reset() {
    inhEquations_computed = false;
    
    inhEquations_value = null;
    inhEquations_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean inhEquations_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Set inhEquations_value;

  /**
   * @attribute syn
   * @aspect AllEquations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:135
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AllEquations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:135")
  @SideEffect.Pure(group="_ASTNode") public Set inhEquations() {
    ASTState state = state();
    if (inhEquations_computed) {
      return inhEquations_value;
    }
    if (inhEquations_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.inhEquations().");
    }
    inhEquations_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    inhEquations_value = inhEquations_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    inhEquations_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    inhEquations_visited = -1;
    return inhEquations_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Set inhEquations_compute() {
      Set set = new LinkedHashSet();
      for (int i = 0; i < getNumInhEq(); i++) {
        set.add(getInhEq(i));
      }
      return set;
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int inhDeclarations_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void inhDeclarations_reset() {
    inhDeclarations_computed = false;
    
    inhDeclarations_value = null;
    inhDeclarations_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean inhDeclarations_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Set inhDeclarations_value;

  /**
   * @attribute syn
   * @aspect AllEquations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:150
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AllEquations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:150")
  @SideEffect.Pure(group="_ASTNode") public Set inhDeclarations() {
    ASTState state = state();
    if (inhDeclarations_computed) {
      return inhDeclarations_value;
    }
    if (inhDeclarations_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.inhDeclarations().");
    }
    inhDeclarations_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    inhDeclarations_value = inhDeclarations_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    inhDeclarations_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    inhDeclarations_visited = -1;
    return inhDeclarations_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Set inhDeclarations_compute() {
      Set set = new LinkedHashSet();
      for (int i = 0; i < getNumInhDecl(); i++) {
        set.add(getInhDecl(i));
      }
      return set;
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map lookupSynDecl_String_visited = new java.util.HashMap(4);
  /** @apilevel internal */
  @SideEffect.Ignore private void lookupSynDecl_String_reset() {
    lookupSynDecl_String_values = new java.util.HashMap(4);
    lookupSynDecl_String_visited = new java.util.HashMap(4);
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map lookupSynDecl_String_values = new java.util.HashMap(4);

  /**
   * @attribute syn
   * @aspect BindSynEquations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:167
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="BindSynEquations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:167")
  @SideEffect.Pure(group="_ASTNode") public SynDecl lookupSynDecl(String signature) {
    Object _parameters = signature;
    ASTState state = state();
    if (lookupSynDecl_String_values.containsKey(_parameters)) {
      return (SynDecl) lookupSynDecl_String_values.get(_parameters);
    }
    if (Integer.valueOf(state().boundariesCrossed).equals(lookupSynDecl_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.lookupSynDecl(String).");
    }
    lookupSynDecl_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    SynDecl lookupSynDecl_String_value = lookupSynDecl_compute(signature);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    lookupSynDecl_String_values.put(_parameters, lookupSynDecl_String_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    lookupSynDecl_String_visited.remove(_parameters);
    return lookupSynDecl_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private SynDecl lookupSynDecl_compute(String signature) {
      for (int i = 0; i < getNumSynDecl(); i++)
        if (getSynDecl(i).signature().equals(signature))
          return getSynDecl(i);
      return null;
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map lookupSynEq_String_visited = new java.util.HashMap(4);
  /** @apilevel internal */
  @SideEffect.Ignore private void lookupSynEq_String_reset() {
    lookupSynEq_String_values = new java.util.HashMap(4);
    lookupSynEq_String_visited = new java.util.HashMap(4);
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map lookupSynEq_String_values = new java.util.HashMap(4);

  /**
   * @attribute syn
   * @aspect BindSynEquations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:181
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="BindSynEquations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:181")
  @SideEffect.Pure(group="_ASTNode") public SynEq lookupSynEq(String signature) {
    Object _parameters = signature;
    ASTState state = state();
    if (lookupSynEq_String_values.containsKey(_parameters)) {
      return (SynEq) lookupSynEq_String_values.get(_parameters);
    }
    if (Integer.valueOf(state().boundariesCrossed).equals(lookupSynEq_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.lookupSynEq(String).");
    }
    lookupSynEq_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    SynEq lookupSynEq_String_value = lookupSynEq_compute(signature);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    lookupSynEq_String_values.put(_parameters, lookupSynEq_String_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    lookupSynEq_String_visited.remove(_parameters);
    return lookupSynEq_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private SynEq lookupSynEq_compute(String signature) {
      for (int i = 0; i < getNumSynEq(); i++) {
        if (getSynEq(i).signature().equals(signature)) {
          return getSynEq(i);
        }
      }
      return null;
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map hasInhEq_InhDecl_Component_visited = new java.util.HashMap(4);
  /** @apilevel internal */
  @SideEffect.Ignore private void hasInhEq_InhDecl_Component_reset() {
    hasInhEq_InhDecl_Component_values = new java.util.HashMap(4);
    hasInhEq_InhDecl_Component_visited = new java.util.HashMap(4);
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map hasInhEq_InhDecl_Component_values = new java.util.HashMap(4);

  /**
   * @attribute syn
   * @aspect BindSynEquations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:198
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="BindSynEquations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:198")
  @SideEffect.Pure(group="_ASTNode") public boolean hasInhEq(InhDecl decl, Component c) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(decl);
    _parameters.add(c);
    ASTState state = state();
    if (hasInhEq_InhDecl_Component_values.containsKey(_parameters)) {
      return (Boolean) hasInhEq_InhDecl_Component_values.get(_parameters);
    }
    if (Integer.valueOf(state().boundariesCrossed).equals(hasInhEq_InhDecl_Component_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.hasInhEq(InhDecl,Component).");
    }
    hasInhEq_InhDecl_Component_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    boolean hasInhEq_InhDecl_Component_value = lookupInhEq(decl.signature(), c.name()) != null;
    if (isFinal && _boundaries == state().boundariesCrossed) {
    hasInhEq_InhDecl_Component_values.put(_parameters, hasInhEq_InhDecl_Component_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    hasInhEq_InhDecl_Component_visited.remove(_parameters);
    return hasInhEq_InhDecl_Component_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map lookupInhEq_String_String_visited = new java.util.HashMap(4);
  /** @apilevel internal */
  @SideEffect.Ignore private void lookupInhEq_String_String_reset() {
    lookupInhEq_String_String_values = new java.util.HashMap(4);
    lookupInhEq_String_String_visited = new java.util.HashMap(4);
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map lookupInhEq_String_String_values = new java.util.HashMap(4);

  /**
   * @attribute syn
   * @aspect BindSynEquations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:201
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="BindSynEquations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:201")
  @SideEffect.Pure(group="_ASTNode") public InhEq lookupInhEq(String signature, String childName) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(signature);
    _parameters.add(childName);
    ASTState state = state();
    if (lookupInhEq_String_String_values.containsKey(_parameters)) {
      return (InhEq) lookupInhEq_String_String_values.get(_parameters);
    }
    if (Integer.valueOf(state().boundariesCrossed).equals(lookupInhEq_String_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.lookupInhEq(String,String).");
    }
    lookupInhEq_String_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    InhEq lookupInhEq_String_String_value = lookupInhEq_compute(signature, childName);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    lookupInhEq_String_String_values.put(_parameters, lookupInhEq_String_String_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    lookupInhEq_String_String_visited.remove(_parameters);
    return lookupInhEq_String_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private InhEq lookupInhEq_compute(String signature, String childName) {
      for (int i = 0; i < getNumInhEq(); i++) {
        if (getInhEq(i).signature().equals(signature) && getInhEq(i).childName().equals(childName)) {
          return getInhEq(i);
        }
      }
      for (int i = 0; i < getNumInhEq(); i++) {
        if (getInhEq(i).signature().equals(signature) && getInhEq(i).childName().equals("Child")) {
          return getInhEq(i);
        }
      }
      return null;
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int inhEqMap_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void inhEqMap_reset() {
    inhEqMap_computed = false;
    
    inhEqMap_value = null;
    inhEqMap_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean inhEqMap_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected HashMap<String, LinkedList<InhEq>> inhEqMap_value;

  /**
   * @attribute syn
   * @aspect ASTDecl
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:236
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ASTDecl", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:236")
  @SideEffect.Pure(group="_ASTNode") public HashMap<String, LinkedList<InhEq>> inhEqMap() {
    ASTState state = state();
    if (inhEqMap_computed) {
      return inhEqMap_value;
    }
    if (inhEqMap_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.inhEqMap().");
    }
    inhEqMap_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    inhEqMap_value = inhEqMap_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    inhEqMap_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    inhEqMap_visited = -1;
    return inhEqMap_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private HashMap<String, LinkedList<InhEq>> inhEqMap_compute() {
      HashMap<String, LinkedList<InhEq>> map = new LinkedHashMap<String, LinkedList<InhEq>>();
      for (int i = 0; i < getNumInhEq(); i++) {
        InhEq equ = getInhEq(i);
        String id = equ.type() + "_" + equ.signature();
        LinkedList list = (LinkedList<InhEq>) map.get(id);
        if (list == null) {
          list = new LinkedList<InhEq>();
          map.put(id, list);
        }
        if (equ.getChildName().equals("getChild")) {
          list.add(equ); // Insert last.
        } else if (equ.getComponent() != null && equ.getComponent().isNTA()) {
          list.add(0, equ); // Insert first.
        } else {
          list.add(0, equ); // Insert first.
        }
      }
      return map;
    }
  /**
   * @attribute inh
   * @aspect Subclasses
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:108
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="Subclasses", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:108")
  @SideEffect.Pure(group="_ASTNode") public Collection<ASTDecl> findSubclasses(ASTDecl target) {
    Object _parameters = target;
    ASTState state = state();
    if (findSubclasses_ASTDecl_values.containsKey(_parameters)) {
      return (Collection<ASTDecl>) findSubclasses_ASTDecl_values.get(_parameters);
    }
    if (Integer.valueOf(state().boundariesCrossed).equals(findSubclasses_ASTDecl_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.findSubclasses(ASTDecl).");
    }
    findSubclasses_ASTDecl_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    Collection<ASTDecl> findSubclasses_ASTDecl_value = getParent().Define_findSubclasses(this, null, target);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    findSubclasses_ASTDecl_values.put(_parameters, findSubclasses_ASTDecl_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    findSubclasses_ASTDecl_visited.remove(_parameters);
    return findSubclasses_ASTDecl_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map findSubclasses_ASTDecl_visited = new java.util.HashMap(4);
  /** @apilevel internal */
  @SideEffect.Ignore private void findSubclasses_ASTDecl_reset() {
    findSubclasses_ASTDecl_values = new java.util.HashMap(4);
    findSubclasses_ASTDecl_visited = new java.util.HashMap(4);
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map findSubclasses_ASTDecl_values = new java.util.HashMap(4);

  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:128
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_hostClass(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return this;
  }
  @SideEffect.Pure protected boolean canDefine_hostClass(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int TypeDecl_attributeProblems_visited = -1;
  /**
   * @attribute coll
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:91
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.COLL)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:91")
  @SideEffect.Pure(group="_ASTNode") public Collection<Problem> attributeProblems() {
    ASTState state = state();
    if (TypeDecl_attributeProblems_computed) {
      return TypeDecl_attributeProblems_value;
    }
    if (TypeDecl_attributeProblems_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.attributeProblems().");
    }
    TypeDecl_attributeProblems_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    TypeDecl_attributeProblems_value = attributeProblems_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    TypeDecl_attributeProblems_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    TypeDecl_attributeProblems_visited = -1;
    return TypeDecl_attributeProblems_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure(group="_ASTNode") private Collection<Problem> attributeProblems_compute() {
    ASTNode node = this;
    while (node != null && !(node instanceof TypeDecl)) {
      node = node.getParent();
    }
    TypeDecl root = (TypeDecl) node;
    root.survey_TypeDecl_attributeProblems();
    Collection<Problem> _computedValue = new LinkedList<Problem>();
    if (root.contributorMap_TypeDecl_attributeProblems.containsKey(this)) {
      for (ASTNode contributor : root.contributorMap_TypeDecl_attributeProblems.get(this)) {
        contributor.contributeTo_TypeDecl_attributeProblems(_computedValue);
      }
    }
    return _computedValue;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean TypeDecl_attributeProblems_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Collection<Problem> TypeDecl_attributeProblems_value;

  @SideEffect.Local(group="_ASTNode") protected void collect_contributors_Grammar_problems(Grammar _root, @SideEffect.Local java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTErrors.jrag:63
    if (grammar().lookup(name()) != this) {
      {
        Grammar target = (Grammar) (grammar());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_Grammar_problems(_root, _map);
  }
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_Grammar_problems(Collection<Problem> collection) {
    super.contributeTo_Grammar_problems(collection);
    if (grammar().lookup(name()) != this) {
      collection.add(Problem.builder()
                .message("multiple production rule for non-terminal %s", name())
                .buildError());
    }
  }
}
