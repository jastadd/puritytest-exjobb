/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package org.jastadd.ast.AST;
import org.jastadd.ast.AST.*;
import java.util.*;
import org.jastadd.Problem;
import org.jastadd.jrag.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jastadd.jrag.AST.ASTExpression;
import org.jastadd.jrag.Unparser;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.io.PrintStream;
import java.io.*;
import java.util.regex.*;
import org.jastadd.JastAdd;
import org.jastadd.jrag.AST.ASTAspectMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectRefineMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectFieldDeclaration;
import org.jastadd.jrag.AST.ASTBlock;
import org.jastadd.jrag.ClassBodyObject;
import org.jastadd.jrag.AST.ASTCompilationUnit;
import org.jastadd.ast.AST.Token;
import org.jastadd.ast.AST.SimpleNode;
import org.jastadd.Configuration;
import org.jastadd.tinytemplate.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Ast.ast:44
 * @production AttrEq : {@link ASTNode} ::= <span class="component">{@link Parameter}*</span> <span class="component">&lt;Name:String&gt;</span> <span class="component">&lt;FileName:String&gt;</span> <span class="component">&lt;StartLine:int&gt;</span> <span class="component">&lt;EndLine:int&gt;</span> <span class="component">&lt;Comment:String&gt;</span> <span class="component">&lt;AspectName:String&gt;</span> <span class="component">{@link Annotation}*</span>;

 */
public abstract class AttrEq extends ASTNode<ASTNode> implements Cloneable {
  /**
   * Create a new error object with relevant file name and line number.
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:129
   */
  public Problem errorf(String messagefmt, Object... args) {
    return error(String.format(messagefmt, args));
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:436
   */
  public String aspectName() {
    return getAspectName();
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:440
   */
  public String legacyAspectName() {
    String name = getFileName();
    if (name.endsWith(".jrag")) {
      name = name.substring(0, name.length() - 5);
    } else if (name.endsWith(".jadd")) {
      name = name.substring(0, name.length() - 5);
    } else if (name.endsWith(".ast")) {
      name = name.substring(0, name.length() - 4);
    }
    String pattern = File.separator.equals("\\") ? pattern = "\\\\" : File.separator;
    String[] names = name.split(pattern);
    return names[names.length-1];
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:529
   */
  public String replacesAspect = null;
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:572
   */
  public String refinesAspect = null;
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:1130
   */
  public org.jastadd.jrag.AST.SimpleNode rhs;
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:1132
   */
  public void setRHS(org.jastadd.jrag.AST.SimpleNode node) {
    rhs = node;
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:1136
   */
  public org.jastadd.jrag.AST.SimpleNode getRHS() {
    return rhs;
  }
  /**
   * @aspect Comments
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Comments.jrag:192
   */
  public String docComment() {
    JavaDocParser parser = new JavaDocParser();
    TemplateContext tt = templateContext();
    tt.bind("SourceComment", parser.parse(getComment()));
    return tt.expand("AttrEq.docComment");
  }
  /**
   * Generate code for the method to compute this attribute
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:374
   */
  public void emitComputeMethod(PrintStream out) {
  }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:755
   */
  public void emitDefaultInhMethod(PrintStream out) {
    templateContext().expand("InhEq.defaultInhDefineMethod", out);
  }
  /**
   * @declaredat ASTNode:1
   */
  public AttrEq(int i) {
    super(i);
  }
  /**
   * @declaredat ASTNode:5
   */
  public AttrEq(Ast p, int i) {
    this(i);
    parser = p;
  }
  /**
   * @declaredat ASTNode:10
   */
  public AttrEq() {
    this(0);
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:19
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
    setChild(new List(), 0);
    setChild(new List(), 1);
  }
  /**
   * @declaredat ASTNode:24
   */
  public AttrEq(List<Parameter> p0, String p1, String p2, int p3, int p4, String p5, String p6, List<Annotation> p7) {
    setChild(p0, 0);
    setName(p1);
    setFileName(p2);
    setStartLine(p3);
    setEndLine(p4);
    setComment(p5);
    setAspectName(p6);
    setChild(p7, 1);
  }
  /**
   * @declaredat ASTNode:34
   */
  public void dumpTree(String indent, java.io.PrintStream out) {
    out.print(indent + "AttrEq");
    out.print("\"" + getName() + "\"");
    out.print("\"" + getFileName() + "\"");
    out.print("\"" + getStartLine() + "\"");
    out.print("\"" + getEndLine() + "\"");
    out.print("\"" + getComment() + "\"");
    out.print("\"" + getAspectName() + "\"");
    String childIndent = indent + "  ";
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).dumpTree(childIndent, out);
    }
  }
  /**
   * @declaredat ASTNode:47
   */
  public Object jjtAccept(AstVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
  /**
   * @declaredat ASTNode:50
   */
  public void jjtAddChild(Node n, int i) {
    checkChild(n, i);
    super.jjtAddChild(n, i);
  }
  /**
   * @declaredat ASTNode:54
   */
  public void checkChild(Node n, int i) {
      if (i == 0) {
        if (!(n instanceof List)) {
          throw new Error("Child number 0 of AttrEq has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof Parameter)) {
            throw new Error("Child number " + k + " in ParameterList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of Parameter");
          }
        }
      }
      if (i == 1) {
        if (!(n instanceof List)) {
          throw new Error("Child number 1 of AttrEq has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof Annotation)) {
            throw new Error("Child number " + k + " in AnnotationList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of Annotation");
          }
        }
      }
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:81
   */
  @SideEffect.Pure public int getNumChild() {
    return 2;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:87
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:91
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    signature_reset();
    attributeName_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:97
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:101
   */
  @SideEffect.Fresh public AttrEq clone() throws CloneNotSupportedException {
    AttrEq node = (AttrEq) super.clone();
    return node;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:112
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public abstract AttrEq fullCopy();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:120
   */
  @SideEffect.Fresh(group="_ASTNode") public abstract AttrEq treeCopyNoTransform();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:128
   */
  @SideEffect.Fresh(group="_ASTNode") public abstract AttrEq treeCopy();
  /**
   * Replaces the Parameter list.
   * @param list The new list node to be used as the Parameter list.
   * @apilevel high-level
   */
  public void setParameterList(List<Parameter> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the Parameter list.
   * @return Number of children in the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumParameter() {
    return getParameterList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Parameter list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Parameter list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumParameterNoTransform() {
    return getParameterListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Parameter list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Parameter getParameter(int i) {
    return (Parameter) getParameterList().getChild(i);
  }
  /**
   * Check whether the Parameter list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasParameter() {
    return getParameterList().getNumChild() != 0;
  }
  /**
   * Append an element to the Parameter list.
   * @param node The element to append to the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addParameter(Parameter node) {
    List<Parameter> list = (parent == null) ? getParameterListNoTransform() : getParameterList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addParameterNoTransform(Parameter node) {
    List<Parameter> list = getParameterListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Parameter list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setParameter(Parameter node, int i) {
    List<Parameter> list = getParameterList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Parameter list.
   * @return The node representing the Parameter list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Parameter")
  @SideEffect.Pure(group="_ASTNode") public List<Parameter> getParameterList() {
    List<Parameter> list = (List<Parameter>) getChild(0);
    return list;
  }
  /**
   * Retrieves the Parameter list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Parameter list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Parameter> getParameterListNoTransform() {
    return (List<Parameter>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the Parameter list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Parameter getParameterNoTransform(int i) {
    return (Parameter) getParameterListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Parameter list.
   * @return The node representing the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Parameter> getParameters() {
    return getParameterList();
  }
  /**
   * Retrieves the Parameter list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Parameter list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Parameter> getParametersNoTransform() {
    return getParameterListNoTransform();
  }
  /**
   * Replaces the lexeme Name.
   * @param value The new value for the lexeme Name.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setName(String value) {
    tokenString_Name = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_Name;
  /**
   * Retrieves the value for the lexeme Name.
   * @return The value for the lexeme Name.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Name")
  @SideEffect.Pure(group="_ASTNode") public String getName() {
    return tokenString_Name != null ? tokenString_Name : "";
  }
  /**
   * Replaces the lexeme FileName.
   * @param value The new value for the lexeme FileName.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setFileName(String value) {
    tokenString_FileName = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_FileName;
  /**
   * Retrieves the value for the lexeme FileName.
   * @return The value for the lexeme FileName.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="FileName")
  @SideEffect.Pure(group="_ASTNode") public String getFileName() {
    return tokenString_FileName != null ? tokenString_FileName : "";
  }
  /**
   * Replaces the lexeme StartLine.
   * @param value The new value for the lexeme StartLine.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setStartLine(int value) {
    tokenint_StartLine = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected int tokenint_StartLine;
  /**
   * Retrieves the value for the lexeme StartLine.
   * @return The value for the lexeme StartLine.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="StartLine")
  @SideEffect.Pure(group="_ASTNode") public int getStartLine() {
    return tokenint_StartLine;
  }
  /**
   * Replaces the lexeme EndLine.
   * @param value The new value for the lexeme EndLine.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setEndLine(int value) {
    tokenint_EndLine = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected int tokenint_EndLine;
  /**
   * Retrieves the value for the lexeme EndLine.
   * @return The value for the lexeme EndLine.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="EndLine")
  @SideEffect.Pure(group="_ASTNode") public int getEndLine() {
    return tokenint_EndLine;
  }
  /**
   * Replaces the lexeme Comment.
   * @param value The new value for the lexeme Comment.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setComment(String value) {
    tokenString_Comment = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_Comment;
  /**
   * Retrieves the value for the lexeme Comment.
   * @return The value for the lexeme Comment.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Comment")
  @SideEffect.Pure(group="_ASTNode") public String getComment() {
    return tokenString_Comment != null ? tokenString_Comment : "";
  }
  /**
   * Replaces the lexeme AspectName.
   * @param value The new value for the lexeme AspectName.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setAspectName(String value) {
    tokenString_AspectName = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_AspectName;
  /**
   * Retrieves the value for the lexeme AspectName.
   * @return The value for the lexeme AspectName.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="AspectName")
  @SideEffect.Pure(group="_ASTNode") public String getAspectName() {
    return tokenString_AspectName != null ? tokenString_AspectName : "";
  }
  /**
   * Replaces the Annotation list.
   * @param list The new list node to be used as the Annotation list.
   * @apilevel high-level
   */
  public void setAnnotationList(List<Annotation> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the Annotation list.
   * @return Number of children in the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumAnnotation() {
    return getAnnotationList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Annotation list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Annotation list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumAnnotationNoTransform() {
    return getAnnotationListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Annotation list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Annotation getAnnotation(int i) {
    return (Annotation) getAnnotationList().getChild(i);
  }
  /**
   * Check whether the Annotation list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasAnnotation() {
    return getAnnotationList().getNumChild() != 0;
  }
  /**
   * Append an element to the Annotation list.
   * @param node The element to append to the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addAnnotation(Annotation node) {
    List<Annotation> list = (parent == null) ? getAnnotationListNoTransform() : getAnnotationList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addAnnotationNoTransform(Annotation node) {
    List<Annotation> list = getAnnotationListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Annotation list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setAnnotation(Annotation node, int i) {
    List<Annotation> list = getAnnotationList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Annotation list.
   * @return The node representing the Annotation list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Annotation")
  @SideEffect.Pure(group="_ASTNode") public List<Annotation> getAnnotationList() {
    List<Annotation> list = (List<Annotation>) getChild(1);
    return list;
  }
  /**
   * Retrieves the Annotation list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Annotation list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Annotation> getAnnotationListNoTransform() {
    return (List<Annotation>) getChildNoTransform(1);
  }
  /**
   * @return the element at index {@code i} in the Annotation list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Annotation getAnnotationNoTransform(int i) {
    return (Annotation) getAnnotationListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Annotation list.
   * @return The node representing the Annotation list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Annotation> getAnnotations() {
    return getAnnotationList();
  }
  /**
   * Retrieves the Annotation list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Annotation list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Annotation> getAnnotationsNoTransform() {
    return getAnnotationListNoTransform();
  }
  /**
   * @attribute syn
   * @aspect LookupDecls
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:227
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="LookupDecls", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:227")
  @SideEffect.Pure(group="_ASTNode") public abstract AttrDecl decl();
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int className_visited = -1;
  /**
   * @attribute syn
   * @aspect ComponentHostClass
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:139
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ComponentHostClass", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:139")
  @SideEffect.Pure(group="_ASTNode") public String className() {
    if (className_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrEq.className().");
    }
    className_visited = state().boundariesCrossed;
    String className_value = hostClass().name();
    className_visited = -1;
    return className_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int sourceLocation_visited = -1;
  /**
   * @attribute syn
   * @aspect ASTNodeAnnotations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNodeAnnotations.jrag:55
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ASTNodeAnnotations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNodeAnnotations.jrag:55")
  @SideEffect.Pure(group="_ASTNode") public String sourceLocation() {
    if (sourceLocation_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrEq.sourceLocation().");
    }
    sourceLocation_visited = state().boundariesCrossed;
    String sourceLocation_value = ASTNode.sourceLocation(getFileName().trim(), getStartLine());
    sourceLocation_visited = -1;
    return sourceLocation_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map supportedAnnotation_Annotation_visited = new java.util.HashMap(4);
  /**
   * @param a the annotation
   * @return <code>true</code> if the given annotation is supported
   * for this type of attribute equation
   * @attribute syn
   * @aspect Annotations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Annotations.jrag:69
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Annotations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Annotations.jrag:69")
  @SideEffect.Pure(group="_ASTNode") public boolean supportedAnnotation(Annotation a) {
    Object _parameters = a;
    if (Integer.valueOf(state().boundariesCrossed).equals(supportedAnnotation_Annotation_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute AttrEq.supportedAnnotation(Annotation).");
    }
    supportedAnnotation_Annotation_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean supportedAnnotation_Annotation_value = false;
    supportedAnnotation_Annotation_visited.remove(_parameters);
    return supportedAnnotation_Annotation_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int annotations_visited = -1;
  /**
   * @return the annotations of this attribute equation.
   * @attribute syn
   * @aspect Annotations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Annotations.jrag:76
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Annotations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Annotations.jrag:76")
  @SideEffect.Pure(group="_ASTNode") public String annotations() {
    if (annotations_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrEq.annotations().");
    }
    annotations_visited = state().boundariesCrossed;
    try {
        StringBuilder sb = new StringBuilder();
        for (Annotation annotation: getAnnotationList()) {
          if (sb.length() > 0) {
            sb.append("\n");
          }
          sb.append(annotation.getAnnotation());
        }
        return sb.toString();
      }
    finally {
      annotations_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map error_String_visited = new java.util.HashMap(4);
  /**
   * Create a new error object with relevant file name and line number.
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:119
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:119")
  @SideEffect.Pure(group="_ASTNode") public Problem error(String message) {
    Object _parameters = message;
    if (Integer.valueOf(state().boundariesCrossed).equals(error_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute AttrEq.error(String).");
    }
    error_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    Problem error_String_value = Problem.builder()
              .message(message)
              .sourceFile(getFileName())
              .sourceLine(getStartLine())
              .buildError();
    error_String_visited.remove(_parameters);
    return error_String_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int hasUnsupportedAnnotation_visited = -1;
  /**
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:404
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:404")
  @SideEffect.Pure(group="_ASTNode") public boolean hasUnsupportedAnnotation() {
    if (hasUnsupportedAnnotation_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrEq.hasUnsupportedAnnotation().");
    }
    hasUnsupportedAnnotation_visited = state().boundariesCrossed;
    try {
        for (Annotation annotation: getAnnotationList()) {
          if (!supportedAnnotation(annotation)) {
            return true;
          }
        }
        return false;
      }
    finally {
      hasUnsupportedAnnotation_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int unsupportedAnnotationProblem_visited = -1;
  /**
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:413
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:413")
  @SideEffect.Pure(group="_ASTNode") public Problem unsupportedAnnotationProblem() {
    if (unsupportedAnnotationProblem_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrEq.unsupportedAnnotationProblem().");
    }
    unsupportedAnnotationProblem_visited = state().boundariesCrossed;
    try {
        StringBuilder buf = new StringBuilder();
        buf.append("unsupported annotations for this attribute equation: ");
        boolean first = true;
        for (Annotation annotation: getAnnotationList()) {
          if (!supportedAnnotation(annotation)) {
            if (!first) {
              buf.append(", ");
            }
            first = false;
            buf.append(annotation.getAnnotation());
          }
        }
        return error(buf.toString());
      }
    finally {
      unsupportedAnnotationProblem_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int parametersDecl_visited = -1;
  /**
   * @attribute syn
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:1140
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Attributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:1140")
  @SideEffect.Pure(group="_ASTNode") public String parametersDecl() {
    if (parametersDecl_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrEq.parametersDecl().");
    }
    parametersDecl_visited = state().boundariesCrossed;
    try {
        StringBuffer s = new StringBuffer();
        for (int i = 0; i < getNumParameter(); i++) {
          Parameter p = getParameter(i);
          s.append(p.getType() + " " + p.getName());
          if (i < getNumParameter() - 1) {
            s.append(", ");
          }
        }
        return s.toString();
      }
    finally {
      parametersDecl_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int parameters_visited = -1;
  /**
   * @attribute syn
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:1152
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Attributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Attributes.jrag:1152")
  @SideEffect.Pure(group="_ASTNode") public String parameters() {
    if (parameters_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrEq.parameters().");
    }
    parameters_visited = state().boundariesCrossed;
    try {
        StringBuffer s = new StringBuffer();
        for (int i = 0; i < getNumParameter(); i++) {
          Parameter p = getParameter(i);
          s.append(p.getName());
          if (i < getNumParameter() - 1) {
            s.append(", ");
          }
        }
        return s.toString();
      }
    finally {
      parameters_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int lazyCondition_visited = -1;
  /**
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:175
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:175")
  @SideEffect.Pure(group="_ASTNode") public boolean lazyCondition() {
    if (lazyCondition_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrEq.lazyCondition().");
    }
    lazyCondition_visited = state().boundariesCrossed;
    boolean lazyCondition_value = decl().lazyCondition();
    lazyCondition_visited = -1;
    return lazyCondition_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int onePhase_visited = -1;
  /**
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:183
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:183")
  @SideEffect.Pure(group="_ASTNode") public boolean onePhase() {
    if (onePhase_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrEq.onePhase().");
    }
    onePhase_visited = state().boundariesCrossed;
    boolean onePhase_value = decl().onePhase();
    onePhase_visited = -1;
    return onePhase_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int naive_visited = -1;
  /**
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:192
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:192")
  @SideEffect.Pure(group="_ASTNode") public boolean naive() {
    if (naive_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrEq.naive().");
    }
    naive_visited = state().boundariesCrossed;
    boolean naive_value = decl().naive();
    naive_visited = -1;
    return naive_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int declaredat_visited = -1;
  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:82
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:82")
  @SideEffect.Pure(group="_ASTNode") public String declaredat() {
    if (declaredat_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrEq.declaredat().");
    }
    declaredat_visited = state().boundariesCrossed;
    String declaredat_value = ASTNode.declaredat(getFileName(), getStartLine());
    declaredat_visited = -1;
    return declaredat_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int attributeKind_visited = -1;
  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:216
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:216")
  @SideEffect.Pure(group="_ASTNode") public String attributeKind() {
    if (attributeKind_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrEq.attributeKind().");
    }
    attributeKind_visited = state().boundariesCrossed;
    String attributeKind_value = decl().attributeKind();
    attributeKind_visited = -1;
    return attributeKind_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int hasComputeBlock_visited = -1;
  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:343
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:343")
  @SideEffect.Pure(group="_ASTNode") public boolean hasComputeBlock() {
    if (hasComputeBlock_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrEq.hasComputeBlock().");
    }
    hasComputeBlock_visited = state().boundariesCrossed;
    boolean hasComputeBlock_value = true;
    hasComputeBlock_visited = -1;
    return hasComputeBlock_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int computeCode_visited = -1;
  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:367
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:367")
  @SideEffect.Pure(group="_ASTNode") public String computeCode() {
    if (computeCode_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrEq.computeCode().");
    }
    computeCode_visited = state().boundariesCrossed;
    String computeCode_value = "";
    computeCode_visited = -1;
    return computeCode_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int canInlineExpression_visited = -1;
  /**
   * @return {@code true} if this attribute equation can be inlined in the
   * attribute method
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:424
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:424")
  @SideEffect.Pure(group="_ASTNode") public boolean canInlineExpression() {
    if (canInlineExpression_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrEq.canInlineExpression().");
    }
    canInlineExpression_visited = state().boundariesCrossed;
    boolean canInlineExpression_value = false;
    canInlineExpression_visited = -1;
    return canInlineExpression_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map canInlineBlock_AttrDecl_visited = new java.util.HashMap(4);
  /**
   * @return {@code true} if this attribute code block can be inlined in the
   * attribute method
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:432
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:432")
  @SideEffect.Pure(group="_ASTNode") public boolean canInlineBlock(AttrDecl decl) {
    Object _parameters = decl;
    if (Integer.valueOf(state().boundariesCrossed).equals(canInlineBlock_AttrDecl_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute AttrEq.canInlineBlock(AttrDecl).");
    }
    canInlineBlock_AttrDecl_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean canInlineBlock_AttrDecl_value = false;
    canInlineBlock_AttrDecl_visited.remove(_parameters);
    return canInlineBlock_AttrDecl_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int getType_visited = -1;
  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:545
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:545")
  @SideEffect.Pure(group="_ASTNode") public String getType() {
    if (getType_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrEq.getType().");
    }
    getType_visited = state().boundariesCrossed;
    String getType_value = decl().getType();
    getType_visited = -1;
    return getType_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int name_visited = -1;
  /**
   * @attribute syn
   * @aspect AttributeNamesAndTypes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:55
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeNamesAndTypes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:55")
  @SideEffect.Pure(group="_ASTNode") public String name() {
    if (name_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrEq.name().");
    }
    name_visited = state().boundariesCrossed;
    String name_value = getName();
    name_visited = -1;
    return name_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int type_visited = -1;
  /**
   * @attribute syn
   * @aspect AttributeNamesAndTypes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:57
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeNamesAndTypes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:57")
  @SideEffect.Pure(group="_ASTNode") public String type() {
    if (type_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrEq.type().");
    }
    type_visited = state().boundariesCrossed;
    try {
        if (decl() != null) {
          return decl().type();
        } else {
          throw new Error(getFileName() + ":" + getStartLine()
              + " could not find decl for AttrEq " + signature() + " in " + hostClass().name());
        }
      }
    finally {
      type_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int signature_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void signature_reset() {
    signature_computed = false;
    
    signature_value = null;
    signature_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean signature_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected String signature_value;

  /**
   * @attribute syn
   * @aspect AttributeNamesAndTypes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:75
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeNamesAndTypes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:75")
  @SideEffect.Pure(group="_ASTNode") public String signature() {
    ASTState state = state();
    if (signature_computed) {
      return signature_value;
    }
    if (signature_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrEq.signature().");
    }
    signature_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    signature_value = signature_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    signature_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    signature_visited = -1;
    return signature_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private String signature_compute() {
      StringBuilder sb = new StringBuilder();
      sb.append(getName());
      for (int i = 0; i < getNumParameter(); i++) {
        sb.append("_" + getParameter(i).getTypeInSignature());
      }
      return sb.toString();
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int attributeName_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void attributeName_reset() {
    attributeName_computed = false;
    
    attributeName_value = null;
    attributeName_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean attributeName_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected String attributeName_value;

  /**
   * The attribute name, including parameter list.
   * @attribute syn
   * @aspect AttributeNamesAndTypes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:87
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeNamesAndTypes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:87")
  @SideEffect.Pure(group="_ASTNode") public String attributeName() {
    ASTState state = state();
    if (attributeName_computed) {
      return attributeName_value;
    }
    if (attributeName_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrEq.attributeName().");
    }
    attributeName_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    attributeName_value = attributeName_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    attributeName_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    attributeName_visited = -1;
    return attributeName_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private String attributeName_compute() {
      StringBuilder sb = new StringBuilder();
      sb.append(getName());
      sb.append("(");
      for (int i = 0; i < getNumParameter(); i++) {
        if (i > 0) {
          sb.append(",");
        }
        sb.append(getParameter(i).getType());
        sb.append(" ");
        sb.append(getParameter(i).getName());
      }
      sb.append(")");
      return sb.toString();
    }
  /**
   * @attribute inh
   * @aspect AttributeHostClass
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:34
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="AttributeHostClass", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:34")
  @SideEffect.Pure(group="_ASTNode") public TypeDecl hostClass() {
    if (hostClass_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute AttrEq.hostClass().");
    }
    hostClass_visited = state().boundariesCrossed;
    TypeDecl hostClass_value = getParent().Define_hostClass(this, null);
    hostClass_visited = -1;
    return hostClass_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int hostClass_visited = -1;
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  @SideEffect.Local(group="_ASTNode") protected void collect_contributors_TypeDecl_attributeProblems(TypeDecl _root, @SideEffect.Local java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:399
    if (hasUnsupportedAnnotation()) {
      {
        TypeDecl target = (TypeDecl) (hostClass());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_TypeDecl_attributeProblems(_root, _map);
  }
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_TypeDecl_attributeProblems(Collection<Problem> collection) {
    super.contributeTo_TypeDecl_attributeProblems(collection);
    if (hasUnsupportedAnnotation()) {
      collection.add(unsupportedAnnotationProblem());
    }
  }
}
