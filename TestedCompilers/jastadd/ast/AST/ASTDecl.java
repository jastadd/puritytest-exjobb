/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package org.jastadd.ast.AST;
import org.jastadd.ast.AST.*;
import java.util.*;
import org.jastadd.Problem;
import org.jastadd.jrag.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jastadd.jrag.AST.ASTExpression;
import org.jastadd.jrag.Unparser;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.io.PrintStream;
import java.io.*;
import java.util.regex.*;
import org.jastadd.JastAdd;
import org.jastadd.jrag.AST.ASTAspectMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectRefineMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectFieldDeclaration;
import org.jastadd.jrag.AST.ASTBlock;
import org.jastadd.jrag.ClassBodyObject;
import org.jastadd.jrag.AST.ASTCompilationUnit;
import org.jastadd.ast.AST.Token;
import org.jastadd.ast.AST.SimpleNode;
import org.jastadd.Configuration;
import org.jastadd.tinytemplate.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Ast.ast:16
 * @production ASTDecl : {@link TypeDecl} ::= <span class="component">[{@link Abstract}]</span> <span class="component">{@link IdDecl}</span> <span class="component">[SuperClass:{@link IdUse}]</span> <span class="component">{@link Component}*</span> <span class="component">{@link SynDecl}*</span> <span class="component">{@link SynEq}*</span> <span class="component">{@link InhDecl}*</span> <span class="component">{@link InhEq}*</span> <span class="component">{@link ClassBodyDecl}*</span> <span class="component">{@link Rewrite}*</span> <span class="component">{@link CollDecl}*</span> <span class="component">{@link CollEq}*</span> <span class="component">&lt;FileName:String&gt;</span> <span class="component">&lt;StartLine:int&gt;</span> <span class="component">&lt;EndLine:int&gt;</span> <span class="component">&lt;Comment:String&gt;</span> <span class="component">{@link SynthesizedNta}*</span> <span class="component">{@link CircularRewriteDecl}</span>;

 */
public class ASTDecl extends TypeDecl implements Cloneable {
  /**
   * @aspect ASTCloneNode
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTCopyNode.jadd:39
   */
  public void emitCloneNode(PrintWriter stream) {
    templateContext().expand("ASTDecl.emitCloneNode", stream);
  }
  /**
   * @aspect ASTCloneNode
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTCopyNode.jadd:43
   */
  public void emitCopyNode(PrintWriter stream) {
    if (!hasAbstract()) {
      // We don't generate copy methods for abstract node types.
      templateContext().expand("ASTDecl.emitCopyNode", stream);
    }
  }
  /**
   * @aspect ASTCloneNode
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTCopyNode.jadd:50
   */
  public void emitFullCopy(PrintWriter stream) {
    // Build list of NTA indices - these are skipped when copying the children of this node.
    LinkedList ntaAggregateIndices = new LinkedList();
    LinkedList ntaOptIndices = new LinkedList();
    LinkedList ntaListIndices = new LinkedList();
    int i = 0;
    for (Component c : components()) {
      if (c.isNTA()) {
        if (c instanceof ListComponentNTA) {
          ntaListIndices.add(Integer.valueOf(i));
        } else if (c instanceof OptionalComponentNTA) {
          ntaOptIndices.add(Integer.valueOf(i));
        } else if (c instanceof AggregateComponentNTA) {
          ntaAggregateIndices.add(Integer.valueOf(i));
        }
      }

      if (!(c instanceof TokenComponent)) {
        // Tokens are not stored in the child array.
        i += 1;
      }
    }

    // The skipNTAs string will include a switch statement that excludes
    // the NTA children from the full copy, but for Opt and List NTAs
    // we still need to create a placeholder Opt/List node.
    String skipNTAs = "";
    String ind = config().indent;
    if (!ntaAggregateIndices.isEmpty() || !ntaOptIndices.isEmpty() ||
      !ntaListIndices.isEmpty()) {

      skipNTAs = "switch (i) {\n";
      Iterator iter;
      if (!ntaAggregateIndices.isEmpty()) {
        iter = ntaAggregateIndices.iterator();
        while (iter.hasNext()) {
          skipNTAs += "case " + (iter.next()) + ":\n";
        }
        skipNTAs += ind + "tree.children[i] = null;\n";
        skipNTAs += ind + "continue;\n";
      }
      if (!ntaOptIndices.isEmpty()) {
        iter = ntaOptIndices.iterator();
        while (iter.hasNext()) {
          skipNTAs += "case " + (iter.next()) + ":\n";
        }
        skipNTAs += ind + "tree.children[i] = new " + config().optType() + "();\n";
        skipNTAs += ind + "continue;\n";
      }
      if (!ntaListIndices.isEmpty()) {
        iter = ntaListIndices.iterator();
        while (iter.hasNext()) {
          skipNTAs += "case " + (iter.next()) + ":\n";
        }
        skipNTAs += ind + "tree.children[i] = new " + config().listType() + "();\n";
        skipNTAs += ind + "continue;\n";
      }
      skipNTAs += "}\n";
    }

    TemplateContext tt = templateContext();
    tt.bind("SkipNTAs", skipNTAs);
    
    tt.expand("ASTDecl.emitFullCopy", stream);
    tt.expand("ASTDecl.emitTreeCopyNoTransform", stream);
    tt.expand("ASTDecl.emitTreeCopy", stream);
    if (isASTNodeDecl()) {
      tt.expand("ASTNode.emitDoFullTraversal", stream);
    }
  }
  /**
   * @aspect ASTEqual
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTEqual.jrag:30
   */
  public void emitIsEqualMethods(PrintWriter stream) {

    if (hasAbstract() && !isASTNodeDecl()) {
      // Don't generate isEqual methods for abstract node types.
      return;
    }

    TemplateContext tt = templateContext();
    StringBuilder sb = new StringBuilder();
    for (Component c : components()) {
      if (c.isTokenComponent() && !c.isNTA()) {
        TokenId token = ((TokenComponent)c).getTokenId();
        String tokenVar = String.format("token%s_%s",
            ASTNode.convTypeNameToSignature(token.getTYPE()), token.getID());
        sb.append(String.format(" && (%s == ((%s) node).%s)", tokenVar, name(), tokenVar));
      }
    }
    tt.bind("IsEqualBody", sb.toString());
    tt.expand("ASTDecl.emitIsEqualMethods", stream);
  }
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:100
   */
  public void emitCollDecls(PrintStream out) {
    for (CollDecl decl : interfaceCollDecls()) {
      emitCollDecl(out, decl);
    }
    for (CollDecl decl : getCollDeclList()) {
      emitCollDecl(out, decl);
    }
  }
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:109
   */
  public void emitCollDecl(PrintStream out, CollDecl attr) {
    if (attr.circularCollection()) {
      attr.emitVisitedDeclarations(out);
      attr.emitCircularCollectionEval(out);
      attr.emitComputeMethod(out);
      attr.emitCacheDeclarations(out);
    } else {
      attr.emitVisitedDeclarations(out);
      attr.emitAttrEquation(out);
      attr.emitComputeMethod(out);
      attr.emitCacheDeclarations(out);
    }
  }
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:194
   */
  public void emitCollContributions(PrintStream out) {
    // Find all collection equations in this node and group them according to either
    // the collection declaration or group name.
    collectContributors(out);
    contributeTo(out);
  }
  /**
   * Generates the survey method for each type with a collection contribution.
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:308
   */
  private void collectContributors(PrintStream out) {
    // Mapping collection declaration to collection equations.
    HashMap<CollDecl, Collection<SurveyContribution>> map =
        new LinkedHashMap<CollDecl, Collection<SurveyContribution>>();

    for (CollEq attr : getCollEqList()) {
      CollDecl decl = attr.decl();
      Collection<SurveyContribution> equations = map.get(decl);
      if (equations == null) {
        equations = new ArrayList<SurveyContribution>();
        map.put(decl, equations);
      }
      equations.add(new EqSurveyContribution(attr));
    }

    for (CustomSurveyContribution survey : surveyContributions) {
      CollDecl decl = survey.lookupCollDecl(grammar());
      Collection<SurveyContribution> contributions = map.get(decl);
      if (contributions == null) {
        contributions = new ArrayList<SurveyContribution>();
        map.put(decl, contributions);
      }
      contributions.add(survey);
    }

    for (Map.Entry<CollDecl, Collection<SurveyContribution>> entry : map.entrySet()) {
      CollDecl decl = entry.getKey();
      Collection<SurveyContribution> equations = entry.getValue();
      decl.templateContext().expand("CollDecl.collectContributors:header", out);
      boolean skipSuperCall = false;
      for (SurveyContribution contribution : equations) {
        contribution.emitSurveyCode(decl, out);
        skipSuperCall |= contribution instanceof BlockSurveyContribution;
      }
      if (skipSuperCall) {
        out.println(config().indent + "}");
      } else {
        if (isASTNodeDecl()) {
          decl.templateContext().expand("CollDecl.collectContributors:default", out);
        } else {
          decl.templateContext().expand("CollDecl.collectContributors:end", out);
        }
      }
    }
  }
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:354
   */
  private void contributeTo(PrintStream out) {
    HashMap<CollDecl, Collection<CollEq>> map = new LinkedHashMap<CollDecl, Collection<CollEq>>();
    for (CollEq attr : getCollEqList()) {
      if (!attr.onePhase()) {
        CollDecl decl = attr.decl();
        Collection<CollEq> equations = map.get(decl);
        if (equations == null) {
          equations = new ArrayList<CollEq>();
          map.put(decl, equations);
        }
        equations.add(attr);
      }
    }

    for (Map.Entry<CollDecl, Collection<CollEq>> entry : map.entrySet()) {
      CollDecl decl = entry.getKey();
      Collection<CollEq> equations = entry.getValue();

      decl.templateContext().bind("IsAstNode", isASTNodeDecl());
      decl.templateContext().expand("CollDecl.contributeTo:header", out);
      for (CollEq equation : equations) {
        TemplateContext tt = equation.templateContext();
        tt.bind("CombOp", decl.getCombOp());
        tt.expand("CollEq.contributeStatement", out);
      }
      out.println(config().indent + "}");
    }
  }
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:464
   */
  public void weaveCollectionAttributes() {
    for (CollDecl decl : interfaceCollDecls()) {
      decl.weaveCollectionAttribute();
    }
    for (CollDecl decl : getCollDeclList()) {
      decl.weaveCollectionAttribute();
    }
  }
  /**
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:572
   */
  public final Collection<CustomSurveyContribution> surveyContributions =
      new LinkedList<CustomSurveyContribution>();
  /**
   * Returns a string with the JavaDoc comment taken from the
   * original class body object.
   * @param obj original class body object
   * @return generated javadoc comment
   * @aspect Comments
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Comments.jrag:139
   */
  public String docComment(ClassBodyObject obj) {
    JavaDocParser parser = new JavaDocParser();
    TemplateContext tt = templateContext();
    tt.bind("SourceComment", parser.parse(obj.comments));
    tt.bind("HasAspectName", obj.aspectName() != null && obj.aspectName().length() > 0);
    tt.bind("AspectName", obj.aspectName());
    String declaredat = ASTNode.declaredat(obj.getFileName(), obj.getStartLine());
    tt.bind("HasDeclaredAt", declaredat.length() > 0);
    tt.bind("DeclaredAt", declaredat);
    return tt.expand("ASTDecl.docComment");
  }
  /**
   * @aspect Comments
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Comments.jrag:173
   */
  public String extraDocCommentLines() {
    String superclass = "";
    if (hasSuperClass()) {
      superclass = " : {@link " + getSuperClass().getID() + "}";
    }
    String components = "";
    if (getNumComponent() > 0) {
      components = " ::=" + componentString();
    }
    return " * @production " + getIdDecl().getID() + superclass + components + ";\n";
  }
  /**
   * @aspect Debug
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Debug.jrag:33
   */
  public String toString() {
    return name();
  }
  /**
   * @aspect Flush
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Flush.jrag:30
   */
  public void emitFlushCache(PrintWriter out) {
    TemplateContext tt = templateContext();

    tt.expand("ASTNode.flushTreeCacheMethod", out);
    tt.expand("ASTDecl.flushCacheMethod", out);
    tt.expand("ASTDecl.flushAttrAndCollectionCacheMethod", out);

    // TODO: Figure out if flushing of NTAs are missing when this is excluded
    //tt.bind("FlushNTACache", emitFlushNTACacheString());

    StringBuilder sb = new StringBuilder();
    for (AttrDecl attr : listOfCachedAttributes()) {
      sb.append(attr.signature() + "_reset();\n");
    }

    tt.bind("FlushAttrCacheBody", sb.toString());
    tt.expand("ASTDecl.flushAttrCacheMethod", out);
    tt.expand("ASTDecl.flushCollectionCacheMethod", out);
  }
  /**
   * Creates string with code flushing NTAs in a node.
   * @aspect Flush
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Flush.jrag:63
   */
  public String emitFlushNTACacheString() {
    StringBuffer res = new StringBuffer();
    TemplateContext tt = templateContext();
    for (int c = 0; c < getNumComponent(); c++) {
      Component comp = getComponent(c);
      if (comp instanceof TokenComponent) {
        TokenComponent tokenComp = (TokenComponent)comp;
        String type = tokenComp.getTokenId().getTYPE();
        tt.bind("IsStringToken", type.equals("String") || type.equals("java.lang.String"));
        tt.bind("IsPrimitive", tokenComp.isPrimitive());
        tt.bind("Id", tokenComp.getTokenId().getID());
        tt.bind("TypeSign", ASTNode.convTypeNameToSignature(type));
        res.append(tt.expand("TokenComponent.flushNTACache"));
      }
    }
    return res.toString();
  }
  /**
   * Creates string with code flushing collection attributes in a node.
   * @aspect Flush
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Flush.jrag:84
   */
  public String flushCollectionCacheCode() {
    StringBuffer res = new StringBuffer();
    for (CollDecl decl : interfaceCollDecls()) {
      res.append(decl.resetVisit());
      res.append(decl.resetCache());
    }
    for (CollDecl decl : getCollDeclList()) {
      res.append(decl.resetVisit());
      res.append(decl.resetCache());
    }
    res.append(collectionReset());
    return res.toString();
  }
  /**
   * @aspect Grammar
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Grammar.jrag:41
   */
  public boolean implementsInterface(String name) { // DRAGONS
    for (Iterator iter = implementsList.iterator(); iter.hasNext(); ) {
      org.jastadd.jrag.AST.SimpleNode n = (org.jastadd.jrag.AST.SimpleNode) iter.next();
      StringBuffer s = new StringBuffer();
      n.jjtAccept(new Unparser(), s);
      String i = s.toString();
      int index = i.indexOf(name);
      if (index == -1) {
        continue;
      }
      if (index > 0 && Character.isJavaIdentifierPart(i.charAt(index-1))) {
        continue;
      }
      if (index + name.length() < i.length()
          && Character.isJavaIdentifierPart(i.charAt(index + name.length()))) {
        continue;
      }
      return true;
    }
    return false;
  }
  /**
   * @aspect InheritedAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:71
   */
  public void removeDuplicateInhDecls() {
    for (int i = 0; i < getNumInhDecl(); ++i) {
      AttrDecl decl = getInhDecl(i);
      if (decl.isDuplicateInhDecl()) {
        getInhDeclList().removeChild(i);
      }
    }
  }
  /**
   * @aspect FindInheritedEquations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:170
   */
  private Collection<InhDecl> missingInhEqs = null;
  /**
   * Finds inherited attribute declarations that lack equations
   * in subtrees rooted at this node.
   * 
   * @return a collection of inherited declarations "visible" from
   * AST nodes of this type
   * @aspect FindInheritedEquations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:179
   */
  public Collection<InhDecl> missingInhEqs() {
    if (missingInhEqs != null) {
      return missingInhEqs;
    }

    missingInhEqs = Collections.emptyList();

    Collection<InhDecl> missing = new HashSet<InhDecl>();
    for (Component component : components()) {
      if (component instanceof TokenComponent) {
        continue;
      }
      TypeDecl type = component.typeDecl();
      if (type instanceof ASTDecl) {
        ASTDecl decl = (ASTDecl) type;
        String childName = component.name();

        addMissingChildInhEqs(missing, childName, decl.missingInhEqs());

        // Any subtype of the component type can replace the component type.
        for (ASTDecl subtype: decl.subclassesTransitive()) {
          addMissingChildInhEqs(missing, childName, subtype.missingInhEqs());
        }
      }
    }

    for (SynthesizedNta component : getSynthesizedNtaList()) {
      TypeDecl type = grammar().lookup(component.getType());
      if (type instanceof ASTDecl) {
        ASTDecl decl = (ASTDecl) type;
        String childName = component.getName();

        addMissingChildInhEqs(missing, childName, decl.missingInhEqs());

        // Any subtype of the component type can replace the component type.
        for (ASTDecl subtype: decl.subclassesTransitive()) {
          addMissingChildInhEqs(missing, childName, subtype.missingInhEqs());
        }
      }
    }

    if (!isRootNode()) {
      // Add all inh attrs declared on this node.
      for (InhDecl decl: getInhDeclList()) {
        missing.add(decl);
      }

      // Add all missing equations from supertypes.
      for (ASTDecl supertype: supertypes()) {
        for (InhDecl decl: supertype.getInhDeclList()) {
          missing.add(decl);
        }
      }
    }

    missingInhEqs = missing;
    return missingInhEqs;
  }
  /**
   * @aspect FindInheritedEquations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:238
   */
  private void addMissingChildInhEqs(Collection<InhDecl> missing,
      String childName, Collection<InhDecl> childMissing) {
    for (InhDecl decl: childMissing) {
      if (!hasInhEqForDecl(childName, decl.signature())) {
        // We have no equation X.getY() = ...;.
        missing.add(decl);
      }
    }
  }
  /**
   * Checks if an inherited attribute is defined for this class.
   * 
   * @return a map of parents of the class for which the attribute is not
   * defined.
   * @aspect FindInheritedEquations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:299
   */
  protected Map<ASTDecl, String> missingInhEqs(String signature,
      Collection<ASTDecl> parentSet, Map<ASTDecl, Set<ASTDecl>> visited) {

    Map<ASTDecl, String> missing = new LinkedHashMap<ASTDecl, String>();
    for (ASTDecl parent: parentSet) {
      if (!parent.hasInhEq(this, signature)) {
        String path = parent.missingInhEqPath(signature, visited);
        if (path != null) {
          missing.put(parent, path + "->" + name());
        }
      }
    }
    return missing;
  }
  /**
   * The recursive search for equations of inherited attributes.
   * 
   * @return <code>null</code> if the attribute is defined, otherwise a path in
   * the abstract grammar.
   * @aspect FindInheritedEquations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:320
   */
  protected String missingInhEqPath(String signature,
      Map<ASTDecl, Set<ASTDecl>> visited) {

    if (parents().isEmpty()) {
      return name();
    }
    for (ASTDecl parent: parents()) {
      // Cache for efficiency.
      Set<ASTDecl> visitedChildren = visited.get(parent);
      if (visitedChildren == null) {
        visitedChildren = new HashSet<ASTDecl>();
        visited.put(parent, visitedChildren);
      } else if (visitedChildren.contains(this)) {
        break;
      }
      visitedChildren.add(this);
      if (!parent.hasInhEq(this, signature)) {
        String path = parent.missingInhEqPath(signature, visited);
        if (path != null) {
            return path + "->" + name();
        }
      }
    }
    return null;
  }
  /**
   * @aspect FindInheritedEquations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:348
   */
  protected boolean hasInhEq(ASTDecl child, String signature) {
    for (Component component : components()) {
      if (child.instanceOf(component.typeDecl())) {
        if (lookupInhEq(signature, component.name()) == null) {
          return false;
        }
      }
    }
    for (SynthesizedNta component : getSynthesizedNtaList()) {
      if (child.instanceOf(grammar().lookup(component.getType()))) {
        if (lookupInhEq(signature, component.getName()) == null) {
          return false;
        }
      }
    }
    return true;
  }
  /**
   * Generate method to compute inherited attribute.
   * @param out
   * @aspect InheritedAttributeCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:372
   */
  public void emitInhEquations(PrintStream out) {
    for (String attrId: inhAttrSet()) {
      emitDefineInhMethod(out, attrId);
      emitCanDefineInhMethod(out, attrId);
    }
  }
  /**
   * @aspect InheritedAttributeCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:379
   */
  public void emitDefineInhMethod(PrintStream out, String attrId) {
    Collection<InhEq> inhEqs = inhAttrEqs(attrId);
    assert(inhEqs.iterator().hasNext());

    InhDecl decl = (InhDecl) inhEqs.iterator().next().decl();

    TemplateContext tc = decl.templateContext();
    tc.bind("InhEqClauses", inhEqClauses(decl, inhEqs));
    tc.expand("InhDecl.DefineInhMethod", out);
  }
  /**
   * @aspect InheritedAttributeCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:390
   */
  private String inhEqClauses(InhDecl decl, Collection<InhEq> inhEqs) {
    StringBuffer sb = new StringBuffer();

    // Have we already generated a component equation?
    boolean hasComponentEq = false;

    for (InhEq equ : inhEqs) {
      if (hasComponentEq) {
        sb.append("else ");
      }

      String rhs = Unparser.unparse(equ.getRHS());

      TemplateContext tc = equ.templateContext();

      if (equ.getRHS() instanceof ASTBlock) {
        // Block right hand side.
        tc.bind("EvalStmt", rhs);
      } else {
        // Expression right hand side.
        tc.bind("EvalStmt", "return " + rhs + ";");
      }

      Component c = equ.getComponent();
      if (c != null) {
        if (c instanceof ListComponent) {
          tc.bind("ChildIndexVar", equ.hasIndex() ? equ.getIndex().getName() : "childIndex");
          tc.expand("InhEq.emitListClause", sb);
        } else if (c instanceof OptionalComponent) {
          tc.expand("InhEq.emitOptClause", sb);
        } else {
          tc.expand("InhEq.emitChildClause", sb);
        }
      } else if (equ.getChildName().equals("getChild")) {
        tc.bind("ChildIndexVar", equ.hasIndex() ? equ.getIndex().getName() : "childIndex");
        tc.bind("HasComponentEq", hasComponentEq);
        tc.expand("InhEq.emitDefaultClause", sb);
        // The getChild equation, if present, is always last in the equation
        // list, so we are done now.
        return sb.toString();
      } else {
        // NTA child equation.
        AttrDecl attrDecl = equ.getChildAttrDecl();
        tc.bind("IsParameterized", attrDecl.isParameterized());
        if (attrDecl.isParameterized()) {
          tc.bind("ChildIndexVar", equ.hasIndex() ? equ.getIndex().getName() : "childIndex");
          tc.bind("AttrSignature", attrDecl.signature());
        }
        tc.expand("InhEq.emitNTAClause", sb);
      }
      hasComponentEq = true;
    }

    // There was no default (getChild) equation.
    TemplateContext tc = decl.templateContext();
    tc.bind("HasComponentEq", hasComponentEq);
    tc.bind("HasEqInSupertype", superClass() != null && superClass().hasInhEq(decl.name()));
    tc.expand("InhDecl.fallthrough", sb);
    return sb.toString();
  }
  /**
   * @aspect InheritedAttributeCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:451
   */
  public void emitCanDefineInhMethod(PrintStream out, String attrId) {
    Collection<InhEq> inhEqs = inhAttrEqs(attrId);
    assert(inhEqs.iterator().hasNext());
    InhDecl decl = (InhDecl) inhEqs.iterator().next().decl();
    TemplateContext tc = decl.templateContext();
    tc.expand("InhDecl.canDefineMethod", out);
  }
  /**
   * Default constructor: creates list and opt nodes for
   * all list and opt children. Initializes NTAs.
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:112
   */
  public void emitDefaultConstructor(PrintWriter out) {
    TemplateContext tt = templateContext();
    String finalInit = "";
    if (config().legacyRewrite() && isRootNode()) {
      finalInit = "is$Final(true);";
    }
    tt.bind("FinalInit", finalInit);
    tt.expand("ASTDecl.emitDefaultConstructor", out);
  }
  /**
   * Emits the (nta) child initialization method
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:125
   */
  public void emitChildInitMethod(PrintWriter out) {
    TemplateContext tt = templateContext();

    String initChildArray = "";
    if (childCount() > 0) {
      initChildArray = "children = new " + config().astNodeType() + "[" + childCount() + "];";
      initChildArray += genIncrementalInitChildHandlers();
    }

    tt.bind("InitChildArray", initChildArray);

    StringBuffer childInit = new StringBuffer();
    childInit.append(tt.expand("State.incHookConstructionStart"));
    int i = 0;
    for (Component c : components()) {
      if (c instanceof ListComponent) {
        childInit.append("setChild(new " + config().listType() + "(), " + i + ");\n");
        i++;
      } else if (c instanceof OptionalComponent) {
        childInit.append("setChild(new " + config().optType() + "(), " + i + ");\n");
        i++;
      } else if (c instanceof AggregateComponent) {
        i++;
      }
    }
    childInit.append(tt.expand("State.incHookConstructionEnd"));

    tt.bind("ChildInit", childInit.toString());
    tt.expand("ASTDecl.emitChildInitMethod", out);
  }
  /**
   * Emits the constructor body for tree building constructors
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:159
   */
  public void emitBuildingConstructorBody(PrintWriter out) {
    TemplateContext tt = templateContext();
    out.print(config().synchronizedBlockBegin(tt));
    tt.expand("State.incHookConstructionStart", out);

    int param = 0;
    int childIndex = 0;
    for (Component c : components()) {
      if (!c.isNTA()) {
        if (c instanceof TokenComponent) {
          TokenComponent t = (TokenComponent)c;
          String tokenId = t.getTokenId().getID();
          out.format("%sset%s(p%d);%n", config().ind(2), tokenId, param);
        }
        else {
          out.format("%ssetChild(p%d, %d);%n", config().ind(2), param, childIndex);
          childIndex++;
        }
        param += 1;
      } else {
        if (c instanceof ListComponent
            || c instanceof OptionalComponent
            || c instanceof AggregateComponent) {
          childIndex++;
        }
      }
    }
    if (config().legacyRewrite() && isRootNode()) {
      out.println(config().ind(2) + "is$Final(true);");
    }

    tt.expand("State.incHookConstructionEnd", out);
    out.print(config().synchronizedBlockEnd(tt));
  }
  /**
   * Constructor to build trees bottom up
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:197
   */
  public void emitBuildingConstructor(PrintWriter out) {
    if (components().isEmpty()) {
      // We only build constructors if there are components.
      return;
    }
    out.print(config().indent + "public " + name() + "." + name() + "(");
    int paramIndex = 0;
    for (Component c : components()) {
      if (!c.isNTA()) {
        if (paramIndex != 0) out.print(", ");
        out.print(c.constrParmType() + " p" + paramIndex);
        paramIndex++;
      }
    }
    out.println(") {");
    emitBuildingConstructorBody(out);
    out.println(config().indent + "}");
  }
  /**
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:216
   */
  public void emitSymbolConstructor(PrintWriter out) {
    if (components().isEmpty()) {
      // We only build constructors if there are components.
      return;
    }
    boolean stringArg = false;
    for (Component c : components()) {
      if (!c.isNTA() && c instanceof TokenComponent && c.constrParmType().equals("String")
          || c.constrParmType().equals("java.lang.String")) {
        stringArg = true;
      }
    }
    if (!stringArg) {
      return;
    }
    out.format("%spublic %s.%s(", config().indent, name(), name());
    int paramIndex = 0;
    for (Component c : components()) {
      if (!c.isNTA()) {
        if (paramIndex != 0) out.print(", ");
        if (c instanceof TokenComponent && c.constrParmType().equals("String")
            || c.constrParmType().equals("java.lang.String")) {
          out.format("beaver.Symbol p%d", paramIndex);
        } else {
          out.format("%s p%d", c.constrParmType(), paramIndex);
        }
        paramIndex++;
      }
    }
    out.println(") {");
    emitBuildingConstructorBody(out);
    out.println(config().indent + "}");
  }
  /**
   * Generate implicit aspect declarations for the ASTNode type.
   * 
   * @param out Aspect output stream
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:304
   */
  public void emitImplicitASTNodeDecls(PrintWriter out) {
    TemplateContext tt = templateContext();
    tt.expand("ASTNode.declarations", out);
    tt.expand("ASTNode.getChild", out);
    tt.expand("ASTNode.addChild", out);
    tt.expand("ASTNode.getChildNoTransform", out);
    tt.expand("ASTNode.numChildren", out);
    tt.expand("ASTNode.setChild", out);
    tt.expand("ASTNode.insertChild", out);
    tt.expand("ASTNode.removeChild", out);
    tt.expand("ASTNode.getParent", out);
    tt.expand("ASTNode.setParent", out);
    tt.expand("ASTNodeAnnotation", out);
    tt.expand("SideEffectAnnotation", out);
    tt.expand("ASTNode.debugDecls", out);
    if (config().lineColumnNumbers()) {
      tt.expand("ASTNode.lineColumnNumbers", out);
    }
  }
  /**
   * Generate implicit aspect declarations for the List type.
   * 
   * @param out Aspect output stream
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:335
   */
  public void emitImplicitListDecls(PrintWriter out) {
    TemplateContext tt = templateContext();
    tt.expand("List.implicitAspectDecls", out);
  }
  /**
   * Generate implicit aspect declarations for regular
   * (non-ASTNode, non-List, non-Opt) node types.
   * 
   * @param out Aspect output stream
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:346
   */
  public void emitImplicitRegularNodeDecls(PrintWriter out) {
    TemplateContext tt = templateContext();
    tt.expand("RegularNodeType.getNumChild", out);
    if (config().debugMode() && isRootNode()) {
      tt.expand("RegularNodeType.debugNodeAttachmentIsRoot", out);
    }
  }
  /**
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:420
   */
  public void emitConstructor(PrintWriter out) {
    emitDefaultConstructor(out);
    emitChildInitMethod(out);

    if (numNonNTAComponent() != 0) {
      emitBuildingConstructor(out);
      if (config().useBeaverSymbol()) {
        emitSymbolConstructor(out);
      }
    }

    if (isOptDecl()) {
      TemplateContext tt = templateContext();
      tt.expand("OptDecl.constructor", out);
    }

    if (isListDecl()) {
      TemplateContext tt = templateContext();
      tt.expand("ListDecl.constructor", out);
    }
  }
  /**
   * Emit aspect declaration for the mayHaveRewrite method.
   * 
   * @param out Aspect output stream
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:447
   */
  public void emitMayHaveRewrite(PrintWriter out) {
    String ind = config().indent;
    String ind2 = config().ind(2);
    out.println(ind + "/**");
    out.println(ind + " * @apilevel internal");
    out.println(ind + " */");
    out.println(ind + "public boolean " + name() + ".mayHaveRewrite() {");
    if (config().legacyRewrite() && name().equals(config().listType())) {
      out.println(ind2 + "return true;");
    } else if (hasRewrites()) {
      out.println(ind2 + "return true;");
    } else {
      out.println(ind2 + "return false;");
    }
    out.println(ind + "}");
  }
  /**
   * Generate aspect code for implicit methods and fields in the default node
   * types ASTNode, List, Opt.
   * 
   * @param out Aspect output stream
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:470
   */
  public void emitImplicitDeclarations(PrintWriter out) {
    String s;

    emitConstructor(out);

    if (config().jjtree()) {
      TemplateContext tt = templateContext();
      tt.expand("JJTree.dumpTree", out);
      tt.expand("JJTree.jjtAccept", out);
      tt.expand("JJTree.jjtAddChild", out);
      tt.expand("JJTree.checkChild", out);
    }

    // Generate code common for all nodes by adding them to ASTNode.
    if (isASTNodeDecl()) {
      emitImplicitASTNodeDecls(out);
      grammar().genReset(out);
    } else if (isListDecl()) {
      emitImplicitListDecls(out);
    } else if (isOptDecl()) {
      // Do not override getNumChild with regular node implementation.
    } else {
      emitImplicitRegularNodeDecls(out);
    }

    if (config().rewriteEnabled()) {
      emitMayHaveRewrite(out);
    }

    emitFlushCache(out);
    emitCloneNode(out);
    emitCopyNode(out);
    emitFullCopy(out);
    emitIsEqualMethods(out);
    genIncremental(out);
  }
  /**
   * @aspect JastAddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JastAddCodeGen.jadd:109
   */
  public boolean hasClassBodyDecl(String signature) {
    for (ClassBodyObject o : classBodyDecls) {
      if (o.signature().equals(signature)) {
        return true;
      }
    }
    return false;
  }
  /**
   * @aspect JastAddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JastAddCodeGen.jadd:173
   */
  public String componentString() {
    StringBuffer buf = new StringBuffer();
    for (int i = 0; i < getNumComponent(); ++i) {
      buf.append(" ");
      buf.append("<span class=\"component\">");
      buf.append(getComponent(i).componentString());
      buf.append("</span>");
    }
    return buf.toString();
  }
  /**
   * @aspect JastAddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JastAddCodeGen.jadd:311
   */
  public void jastAddGen(boolean publicModifier) {
    File file = grammar().targetJavaFile(name());
    try {
      PrintStream stream = new PrintStream(new FileOutputStream(file));

      // Insert comment notifying that this is a generated file.
      stream.format("/* This file was generated with %s */%n",
          JastAdd.getLongVersionString());

      if (!config().license.isEmpty()) {
        stream.println(config().license);
      }

      // TODO(joqvist): move to template.
      if (!config().packageName().isEmpty()) {
        stream.format("package %s;%n", config().packageName());
      }

      stream.print(grammar().genImportsList());

      stream.println(docComment());
      if (isOptDecl() || isListDecl() || isASTNodeDecl()){ 
      	stream.print("@SideEffect.Entity public ");
      }else{
      	stream.print("public ");
      }
      if (hasAbstract()) 
        stream.print("abstract ");
      
      stream.format("class %s", name());
      if (isOptDecl() || isListDecl() || isASTNodeDecl()) {
        stream.format("<T extends %s>", config().astNodeType());
      }
      if (hasSuperClass()) {
        String name = getSuperClass().name();
        if (isListDecl() || isOptDecl()) {
          name += "<T>";
        } else if (name.equals(config().astNodeType())) {
          name += "<" + config().astNodeType() + ">";
        }
        stream.format(" extends %s", name);
      } else if (isASTNodeDecl()) {
        String superType = config().astNodeSuperType();
        if (!superType.equals("")) {
          stream.format(" extends %s", superType);
        }
      }
      stream.print(jastAddImplementsList());
      stream.println(" {");

      jastAddAttributes(stream);
      stream.println("}");
      stream.close();
    } catch (FileNotFoundException f) {
      System.err.format("Could not create file %s in %s%n", file.getName(), file.getParent());
      System.exit(1);
    }
  }
  /**
   * @aspect JastAddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JastAddCodeGen.jadd:370
   */
  public String jastAddImplementsList() {
    StringBuffer buf = new StringBuffer();
    buf.append(" implements Cloneable");
    for (Iterator iter = implementsList.iterator(); iter.hasNext(); ) {
      buf.append(", " + Unparser.unparse((org.jastadd.jrag.AST.SimpleNode) iter.next()));
    }
    if (isListDecl()) {
      buf.append(", Iterable<T>");
    }
    return buf.toString();
  }
  /**
   * @aspect JastAddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JastAddCodeGen.jadd:394
   */
  public void jastAddAttributes(PrintStream out) {
    emitMembers(out);
    emitAbstractSyns(out);
    emitSynEquations(out);
    emitInhDeclarations(out);
    emitInhEquations(out);
    if (config().rewriteEnabled()) {
      emitRewrites(out);
    }
    emitCollDecls(out);
    emitCollContributions(out);
    emitInhEqSignatures(out);
    emitRewriteAttribute(out);
  }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:169
   */
  public void emitMembers(PrintStream out) {
    for (ClassBodyObject obj : classBodyDecls) {
      org.jastadd.jrag.AST.SimpleNode node = obj.node;
      out.print(obj.modifiers());
      out.println(docComment(obj));
      out.print(config().indent);
      StringBuffer buf = new StringBuffer();
      node.jjtAccept(new ClassBodyDeclUnparser(), buf);
      out.print(buf.toString());
      out.println();
    }
  }
  /**
   * Generate abstract method declarations for synthesized attributes
   * that lack an equation.
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:201
   */
  public void emitAbstractSyns(PrintStream out) {
    for (int i = 0; i < getNumSynDecl(); i++) {
      AttrDecl attr = getSynDecl(i);
      boolean equ = false;
      for (int j = 0; j < getNumSynEq(); j++) {
        if (getSynEq(j).signature().equals(attr.signature())) {
          equ = true;
        }
      }
      if (!equ) {
        attr.emitAbstractSynDecl(out);
      }
    }
  }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:516
   */
  public void emitCacheDeclarations(PrintStream out, AttrDecl attr) {
    // All equations of an attribute need cache and visited declarations.  If
    // an equation lacks its own visited declaration and calls an overriden
    // equation it can cause a circularity exception.
    attr.emitVisitedDeclarations(out);
    if (!attr.isCircular()) {
      attr.emitResetMethod(out);
      if (attr.isLazy()) {
        attr.emitCacheDeclarations(out);
      }
    } else if (attr.getNumParameter() == 0) {
      // Non-parameterized, circular attribute.
      attr.emitResetMethod(out);
      attr.emitCacheDeclarations(out);
    } else {
      // Parameterized, circular attribute.
      attr.emitResetMethod(out);
      if (attr.declaredNTA()) {
        attr.emitCacheDeclarations(out);
      } else if (config().lazyMaps()) {
        out.format("%s@SideEffect.Secret(group=\"%s\") protected %s %s_values;%n",
            config().indent,  "_ASTNode" , config().typeDefaultMap(), attr.signature());
      } else {
        out.format("%s@SideEffect.Secret(group=\"%s\") protected %s %s_values = %s;%n", 
        config().indent, "_ASTNode", config().typeDefaultMap(), attr.signature(), config().createDefaultMap());
      }
    }
  }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:547
   */
  public void emitSynEquations(PrintStream out) {
    for (int i = 0; i < getNumSynEq(); i++) {
       AttrEq equ = getSynEq(i);
       AttrDecl attr = equ.decl();
       emitCacheDeclarations(out, attr);
       attr.emitAttrEquation(out, equ);
    }
  }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:660
   */
  public void emitInhDeclarations(PrintStream out) {
    for (int i = 0; i < getNumInhDecl(); i++) {
       InhDecl attr = getInhDecl(i);
       attr.emitAttrEquation(out);
       emitCacheDeclarations(out, attr);
    }
  }
  /**
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:760
   */
  public void emitInhEqSignatures(PrintStream out) {
    if (name().equals(config().astNodeType())) {
      for (Iterator iter = grammar().inhEqMap().entrySet().iterator(); iter.hasNext(); ) {
        java.util.Map.Entry entry = (java.util.Map.Entry)iter.next();
        String attrId = (String)entry.getKey();
        AttrEq attr = (AttrEq)((LinkedList)entry.getValue()).get(0);
        if (!hasInhEq(attr.decl().name())) {
          attr.emitDefaultInhMethod(out);
        }
      }
    }
  }
  /**
   * @aspect Rewrites
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Rewrites.jrag:34
   */
  public void emitRewrites(PrintStream out) {
    TemplateContext tt = templateContext();
    boolean unconditional = false;

    tt.expand("ASTDecl.rewriteTo:begin", out);

    if (config().legacyRewrite() && name().equals(config().listType())) {
      tt.expand("ASTDecl.emitRewrites.touch_list", out);
    }
    for (int i = 0; i < getNumRewrite(); i++) {
      Rewrite r = getRewrite(i);
      if (r.genRewrite(out, i)) {
        unconditional = true;
      }
    }

    if (name().equals(config().astNodeType())) {
      tt.expand("ASTDecl.rewriteTo:end:ASTNode", out);
    } else if (!unconditional) {
      tt.expand("ASTDecl.rewriteTo:end:conditional", out);
    } else {
      tt.expand("ASTDecl.rewriteTo:end:unconditional", out);
    }

    for (int i = 0; i < getNumRewrite(); i++) {
      Rewrite r = getRewrite(i);
      r.genRewritesExtra(out, i);
    }

    if (config().rewriteCircularNTA()) {
      // Generate the canRewrite method.
      tt.expand("ASTDecl.canRewrite:begin", out);

      if (unconditional) {
        tt.expand("ASTDecl.canRewrite:end:unconditional", out);
      } else {
        for (int i = 0; i < getNumRewrite(); i++) {
          Rewrite r = getRewrite(i);
          r.genRewriteCondition(out, i);
        }
        tt.expand("ASTDecl.canRewrite:end", out);
      }
    }
  }
  /**
   * Generates the attribute code for circular rewrites.
   * @aspect Rewrites
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Rewrites.jrag:164
   */
  public void emitRewriteAttribute(PrintStream out) {
    if (hasRewriteAttribute()) {
      // Generate the rewrittenNode attribute only if this node has rewrites and
      // a supertype does not have rewrites (if the supertype has rewrites then
      // this node inherites the rewrittenNode attribute).
      CircularRewriteDecl decl = getCircularRewriteDecl();
      TemplateContext tt = decl.templateContext();
      String newValue = "new_" + decl.signature() + "_value";
      String oldValue = decl.signature() + "_value";
      tt.bind("CircularComputeRhs", oldValue + ".rewriteTo()");
      tt.bind("ChangeCondition", String.format("%s != %s || %s.canRewrite()",
            newValue, oldValue, newValue));
      tt.bind("BottomValue", "this");
      tt.bind("TracePrintReturnNewValue",
          decl.tracePrintReturnNewValue(decl.signature() + "_value"));
      tt.bind("TracePrintReturnPreviousValue",
          decl.tracePrintReturnPreviousValue(decl.signature() + "_value"));
      tt.expand("AttrDecl.resetMethod", out);
      tt.expand("AttrDecl.cycleDeclaration", out);
      tt.expand("AttrDecl.cacheDeclarations", out);
      tt.expand("AttrDecl.circularEquation:unparameterized", out);
    } else if (isASTNodeDecl()) {
      out.format("public %s rewrittenNode() { "
          + "throw new Error(\"rewrittenNode is undefined for ASTNode\"); }%n",
          config().astNodeType());
    }
  }
  /**
   * Generate DDG nodes for storage that needs dependency tracking in an AST node.
   * The number of DDG nodes needed depend on the incremental configuration:
   * - param: children by index, parent, attributes with parameters
   * - attr:  children, parent, attributes
   * - node:  one per node
   * - region: on node if region root (non region roots use a method to find the root)
   * @aspect IncrementalDDG
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalDDG.jadd:47
   */
  public void genIncrementalHandlers(PrintWriter out) {
    if (!config().incremental()) return;
    TemplateContext tt = templateContext();
    if (config().incrementalLevelRegion() &&
        isRegionRoot() && !hasRegionRootAsSuperClass()) {
      tt.expand("ASTDecl.createNodeHandler", out);
    }
    if (config().incrementalLevelNode() &&
          name().equals(config().astNodeType())) {
      tt.expand("ASTDecl.createNodeHandler", out);
    }
    if (config().incrementalLevelParam() ||
      config().incrementalLevelAttr()) {
      if (name().equals(config().astNodeType())) {
        tt.expand("ASTDecl.createASTHandlers", out);
      }
      // Collect attributes.
      ArrayList list = new ArrayList();
      for (int k = 0; k < getNumSynDecl(); k++) {
        AttrDecl attr = getSynDecl(k);
        if (attr.isLazy() || attr.isCircular()) {
          list.add(attr);
        }
      }
      for (int k = 0; k < getNumInhDecl(); k++) {
        AttrDecl attr = getInhDecl(k);
        if (attr.isLazy() || attr.isCircular()) {
          list.add(attr);
        }
      }
      for (CollDecl decl: interfaceCollDecls()) {
        list.add(decl);
      }
      for (CollDecl decl: getCollDeclList()) {
        list.add(decl);
      }
      // Attribute code.
      for (Iterator itr = list.iterator(); itr.hasNext();) {
        AttrDecl attr = (AttrDecl)itr.next();
        tt.bind("AttributeName", attr.signature()); // TODO(joqvist): rename template variable?
        tt.bind("IsParameterized", attr.getNumParameter() > 0);
        tt.expand("ASTDecl.createAttributeHandler", out);
      }
    }
  }
  /**
   * Initialize child handlers.
   * This only applies for the incremental param configuration.
   * @aspect IncrementalDDG
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalDDG.jadd:97
   */
  public String genIncrementalInitChildHandlers() {
    if (!config().incremental()) {
      return "";
    } else {
      TemplateContext tt = templateContext();
      return tt.expand("ASTDecl.incrementalInitChildHandlers");
    }
  }
  /**
   * Generate code for copying of DDG nodes.
   * @aspect IncrementalDDG
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalDDG.jadd:109
   */
  public void genIncrementalCopyHandlers(PrintWriter out) {
    if (!config().incremental()) {
      return;
    }
    TemplateContext tt = templateContext();
    tt.bind("IsRegionRoot", isRegionRoot());
    tt.bind("CopyTokenHandlers", emitCopyTokenHandlersString());
    tt.bind("CopyAttributeHandlers", emitCopyAttributeHandlersString());
    tt.expand("ASTDecl.incrementalCopyHandlerMethod", out);
  }
  /**
   * Emit string for copying of token DDG nodes in a node.
   * @aspect IncrementalDDG
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalDDG.jadd:123
   */
  public String emitCopyTokenHandlersString() {
    StringBuilder res = new StringBuilder();
    if (config().incrementalLevelParam() || config().incrementalLevelAttr()) {
      TemplateContext tt = templateContext();
      for (int c = 0; c < getNumComponent(); c++) {
        Component comp = getComponent(c);
        if (comp instanceof TokenComponent) {
          tt.bind("Id", ((TokenComponent)comp).getTokenId().getID());
          res.append(tt.expand("TokenComponent.copyTokenHandler"));
        }
      }
    }
    return res.toString();
  }
  /**
   * Emit string for copying of attribute DDG nodes in a node.
   * @aspect IncrementalDDG
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalDDG.jadd:141
   */
  public String emitCopyAttributeHandlersString() {
    StringBuilder res = new StringBuilder();
    if (config().incrementalLevelParam() || config().incrementalLevelAttr()) {
      // Collect attributes.
      ArrayList list = new ArrayList();
      for (int k = 0; k < getNumSynDecl(); k++) {
        AttrDecl attr = getSynDecl(k);
        if (attr != null && (attr.isLazy() || attr.isCircular())) {
          list.add(attr);
        }
      }
      for (int k = 0; k < getNumInhDecl(); k++) {
        AttrDecl attr = getInhDecl(k);
        if (attr != null && (attr.isLazy() || attr.isCircular())) {
          list.add(attr);
        }
      }
      // Attribute code.
      TemplateContext tt = templateContext();
      for (Iterator itr = list.iterator(); itr.hasNext();) {
        AttrDecl attr = (AttrDecl)itr.next();
        tt.bind("AttributeName", attr.signature());// TODO rename template variable?
        tt.bind("IsParameterized", attr.getNumParameter() > 0);
        res.append(tt.expand("ASTDecl.copyAttributeHandler"));
      }
    }
    return res.toString();
  }
  /**
   * Generate code for incremental regions.
   * This is a incremental configuration that requires additional
   * methods for finding the current region, identifying region roots,
   * and tracking of region dependencies.
   * @aspect IncrementalDDG
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalDDG.jadd:176
   */
  public void genIncrementalRegions(PrintWriter out) {
    if (!config().incremental()) {
      return;
    }
    TemplateContext tt = templateContext();
    tt.bind("IsRegionRoot", isRegionRoot());
    if (config().incrementalLevelNode()) {
      tt.expand("ASTDecl.createIsRegionRootMethod", out);
      if (name().equals(config().astNodeType())) {
        tt.expand("ASTDecl.createRegionHandlerMethod", out);
      }
    }
    if (config().incrementalLevelRegion()) {
      tt.expand("ASTDecl.createIsRegionRootMethod", out);
      tt.expand("ASTDecl.createRegionHandlerMethod", out);
      tt.expand("ASTDecl.createRegionRootMethod", out);
      if (isRegionRoot() && !name().equals(config().astNodeType())) {
        tt.expand("ASTDecl.trackGetParentForRegionMethod", out);
      }
      if (isRegionLeaf()) {
        if (isListDecl() || isOptDecl() || !isASTNodeDecl()) {
          tt.bind("IsListOrOpt", isListDecl() || isOptDecl());
          tt.expand("ASTDecl.trackGetChildForRegionMethod", out);
          tt.expand("ASTDecl.trackGetChildNoTranForRegionMethod", out);
        }
      }
    }
  }
  /**
   * Generate debugging code for incremental evaluation-
   * This includes code for identifying nodes with relative node IDs,
   * dumping cached values and dummping of DDG dependencies.
   * @aspect IncrementalDebug
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalDebug.jadd:35
   */
  public void genIncrementalDebug(PrintWriter out) {
    if (!config().incrementalDebug()) return;
    TemplateContext tt = templateContext();
    tt.expand("ASTDecl.relativeNodeIDMethod", out);
    tt.expand("ASTDecl.printParamListMethod", out);
    tt.expand("ASTDecl.printValueMethod", out);
    tt.bind("DumpAttributeValues", genDumpAttributeValuesString());
    tt.expand("ASTDecl.dumpCachedValuesMethod", out);
    tt.bind("DumpTokenDeps", genDumpTokenDepsString());
    tt.bind("DumpAttributeDeps", genDumpAttributeDepsString());
    tt.expand("ASTDecl.dumpDependenciesMethod", out);
    tt.bind("DumpDepsInNTAs", genDumpDepsInNTAsString());
	tt.expand("ASTDecl.dumpDepsInTreeMethod", out);
  }
  /**
   * Generate string with code for dumping dependencies in NTAs
   * @aspect IncrementalDebug
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalDebug.jadd:53
   */
  public String genDumpDepsInNTAsString() {
    StringBuffer res = new StringBuffer();
    TemplateContext tt = templateContext();
    for (AttrDecl attr : listOfCachedAttributes()) {
      if ((attr.isNTA() || attr.getNTA()) && !attr.isPrimitive() &&
         !(attr.type().equals("String") || attr.type().equals("java.lang.String"))) {
        tt.bind("IsParameterized", attr.getNumParameter() > 0);
        tt.bind("AttrSign", attr.signature());
        res.append(tt.expand("ASTDecl.checkAndDumpNTADeps"));
      }
    }
    return res.toString();
  }
  /**
   * Generate string with code for dumping attribute dependencies.
   * @aspect IncrementalDebug
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalDebug.jadd:70
   */
  public String genDumpAttributeDepsString() {
    StringBuffer res = new StringBuffer();
    TemplateContext tt = templateContext();
    // Add dump string for each attribute
    for (AttrDecl attr : listOfCachedAttributes()) {
      tt.bind("IsParameterized", attr.getNumParameter() > 0);
      tt.bind("IsNTA", (attr.isNTA() || attr.getNTA()) && !attr.isPrimitive() &&
            !(attr.type().equals("String") || attr.type().equals("java.lang.String")));
      tt.bind("AttrSign",  attr.signature());
      res.append(tt.expand("ASTDecl.checkAndDumpAttributeDeps"));
    }
  	return res.toString();
  }
  /**
   * Generate string with code for dumping token dependencies.
   * @aspect IncrementalDebug
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalDebug.jadd:87
   */
  public String genDumpTokenDepsString() {
    StringBuffer res = new StringBuffer();
    TemplateContext tt = templateContext();
    if (config().incrementalLevelParam() || config().incrementalLevelAttr()) {
      for (Component c : components()) {
        if (c instanceof TokenComponent && !c.isNTA()) {
          tt.bind("Id",  ((TokenComponent)c).getTokenId().getID());
          res.append(tt.expand("ASTDecl.checkAndDumpTokenDeps"));
        }
      }
    }
    return res.toString();
  }
  /**
   * Generate string with code for dumping attribute values.
   * @aspect IncrementalDebug
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalDebug.jadd:104
   */
  public String genDumpAttributeValuesString() {
    StringBuffer res = new StringBuffer();
    TemplateContext tt = templateContext();
    for (AttrDecl attr : listOfCachedAttributes()) {
      tt.bind("IsParameterized", attr.getNumParameter() > 0);
      tt.bind("PrintAsObject", attr.isPrimitive() || attr.getType().equals("String"));
      tt.bind("AttrSign", attr.signature());
      res.append(tt.expand("ASTDecl.dumpCachedAttributeValue"));
    }
    return res.toString();
  }
  /**
   * @aspect IncrementalEval
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalEval.jadd:34
   */
  public void genIncremental(PrintWriter out) {
    if (!config().incremental()) return;

    TemplateContext tt = templateContext();

    tt.expand("ASTDecl.relativeNodeIDMethodWithRewrite", out);

    tt.expand("List.internalNTAList", out);

    tt.bind("NtaIndexCheck", emitNtaIndexCheckString());
    tt.expand("ASTDecl.childIsNtaMethod", out);

    genIncrementalHandlers(out);
    genIncrementalCopyHandlers(out);
    genIncrementalNotification(out);
    genIncrementalState(out);
    genIncrementalRegions(out);
    genIncrementalDebug(out);
  }
  /**
   * Returns a list of all cached attributes (syn, inh).
   * @aspect IncrementalEval
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalEval.jadd:57
   */
  public ArrayList<AttrDecl> listOfCachedAttributes() {
    ArrayList<AttrDecl> list = new ArrayList<AttrDecl>();
    if (config().rewriteCircularNTA() && hasRewriteAttribute()) {
      list.add(getCircularRewriteDecl());
    }
    for (int k = 0; k < getNumSynEq(); k++) {
      AttrDecl attr = getSynEq(k).decl();
      if (attr != null && (attr.isLazy() || attr.isCircular())) {
        list.add(attr);
      }
    }
    for (int k = 0; k < getNumInhDecl(); k++) {
      InhDecl attr = getInhDecl(k);
      if (attr.isLazy() || attr.isCircular()) {
        list.add(attr);
      }
    }
    return list;
  }
  /**
   * Returns a list of NTAs declared as attributes (syn, inh).
   * @aspect IncrementalEval
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalEval.jadd:80
   */
  public ArrayList listOfNtaAttributes() {
    ArrayList list = new ArrayList();
    for (int k = 0; k < getNumSynDecl(); k++) {
      AttrDecl attr = getSynDecl(k);
      if (attr.isNTA()) {
        list.add(attr);
      }
    }
    for (int k = 0; k < getNumInhDecl(); k++) {
      AttrDecl attr = getInhDecl(k);
      if (attr.isNTA()) {
        list.add(attr);
      }
    }
    return list;
  }
  /**
   * Creates string with code checking whether a given index
   * corresponds to the index of an NTA child defined as an
   * attribute (syn, inh).
   * @aspect IncrementalEval
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalEval.jadd:102
   */
  public String emitNtaIndexCheckString() {
    StringBuffer res = new StringBuffer();
    boolean firstAttr = true;
    for (Iterator itr = listOfNtaAttributes().iterator(); itr.hasNext();) {
      AttrDecl attr = (AttrDecl)itr.next();
      if (firstAttr) {
        res.append("if (");
        firstAttr = false;
      } else {
        res.append(" || ");
      }
      res.append("index == " + attr.indexNTAchild());
    }
    if (!firstAttr) {
      res.append(") return true;");
    }
    return res.toString();
  }
  /**
   * @aspect IncrementalNotification
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalNotification.jadd:30
   */
  public void genIncrementalNotification(PrintWriter out) {
    if (!config().incremental()) return;

    TemplateContext tt = templateContext();

    tt.expand("ASTNode.incFlushRewritesLocateEnclosingRewriteMethod", out);
    tt.expand("ASTNode.incResetRewritesMethod", out);
    tt.expand("ASTNode.incRestoreEnclosingRewriteMethod", out);
    tt.expand("ASTNode.incRestoreInitialForIndexMethod", out);
    tt.expand("ASTNode.incLocateInitialCopyMethod", out);

    tt.bind("AttrAffectedChecks", emitAttrAffectedChecksString());
    tt.expand("ASTDecl.incValueAffectedMethod", out);

    tt.bind("AttrFlushChecks", emitAttrFlushChecksString());
    tt.expand("ASTDecl.incReactToDepChangeMethod", out);

    tt.bind("FlushAttrs", emitFlushAttrsString());
    tt.expand("ASTDecl.incFlushMethod", out);

    tt.expand("ASTNode.incFlushChildMethod", out);

    tt.bind("FlushNTAs", emitFlushNTAsString());
    tt.expand("ASTDecl.incFlushNTAMethod", out);

    tt.expand("ASTDecl.incFlushRegionRootMethod", out);

    tt.expand("ASTNode.incCheckRegionForInnerRewriteMethod", out);

    tt.bind("FlushNTAsInRegion", emitFlushNTAsInRegionString());
    tt.expand("ASTDecl.incFlushRegionMethod", out);

    tt.expand("ASTNode.incFlushRegionRewritesMethod", out);

    tt.bind("FlushNtaSubTrees", emitFlushNtaSubTreesString());
    tt.bind("TransferSetsFromAttrTokenHandlers", emitTransferSetsFromAttrTokenHandlersString());
    tt.expand("ASTDecl.incFlushSubTreeMethod", out);

    tt.expand("ASTNode.incNotifyForRemoveMethod", out);
  }
  /**
   * Creates string with code flushing NTA substrings.
   * @aspect IncrementalNotification
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalNotification.jadd:74
   */
  public String emitFlushNtaSubTreesString() {
    StringBuffer res = new StringBuffer();
    TemplateContext tt = templateContext();
    for (AttrDecl attr : listOfCachedAttributes()) {
      tt.bind("IsNTA", (attr.isNTA() || attr.getNTA()) && !attr.isPrimitive()
              && !(attr.type().equals("String") || attr.type().equals("java.lang.String")));
      tt.bind("IsParameterized", attr.isParameterized());
      Component comp = attr.findCorrespondingNTA();
      tt.bind("IsNtaWithTree", comp == null || !(comp instanceof OptionalComponentNTA || comp instanceof ListComponentNTA));
      tt.bind("AttrSign", attr.signature());
      res.append(tt.expand("ASTDecl.flushNtaSubtree"));
    }
    return res.toString();
  }
  /**
   * Creates string with code transfering sets from attribute and token handlers.
   * @aspect IncrementalNotification
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalNotification.jadd:92
   */
  public String emitTransferSetsFromAttrTokenHandlersString() {
    StringBuffer res = new StringBuffer();
    TemplateContext tt = templateContext();
    for (AttrDecl attr : listOfCachedAttributes()) {
      tt.bind("IsParameterized", attr.isParameterized());
      tt.bind("AttrSign", attr.signature());
      res.append(tt.expand("ASTDecl.transferSetsFromAttributeHandler"));
    }
    for (int c = 0; c < getNumComponent(); c++) {
      Component comp = getComponent(c);
      if (comp instanceof TokenComponent) {
        tt.bind("Id", ((TokenComponent)comp).getTokenId().getID());
        res.append(tt.expand("ASTDecl.transferSetsFromTokenHandler"));
      }
    }
    return res.toString();
  }
  /**
   * Creates string with code flushing NTAs in region.
   * @aspect IncrementalNotification
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalNotification.jadd:113
   */
  public String emitFlushNTAsInRegionString() {
    StringBuffer res = new StringBuffer();
    TemplateContext tt = templateContext();
    for (AttrDecl attr : listOfCachedAttributes()) {
      tt.bind("IsNTA", (attr.isNTA() || attr.getNTA()) && !attr.isPrimitive()
            && !(attr.type().equals("String") || attr.type().equals("java.lang.String")));
      tt.bind("IsParameterized", attr.getNumParameter() > 0);
      tt.bind("AttrSign", attr.signature());
      Component comp = attr.findCorrespondingNTA();
      tt.bind("IsNtaWithTree", comp == null || comp instanceof OptionalComponentNTA || comp instanceof ListComponentNTA);
      tt.bind("AttrResetVisit", attr.resetVisit());
      tt.bind("AttrResetCache", attr.resetCache());
      res.append(tt.expand("ASTDecl.flushNTAsInRegion"));
    }
    return res.toString();
  }
  /**
   * Creates string with code flushing NTAs.
   * @aspect IncrementalNotification
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalNotification.jadd:133
   */
  public String emitFlushNTAsString() {
    StringBuffer res = new StringBuffer();
    TemplateContext tt = templateContext();
    for (AttrDecl attr : listOfCachedAttributes()) {
      if ((attr.isNTA() || attr.getNTA()) && !attr.isPrimitive() &&
      		!(attr.type().equals("String") || attr.type().equals("java.lang.String")) &&
      		attr.getNumParameter() == 0) {
        Component comp = attr.findCorrespondingNTA();
        if (comp == null || comp instanceof OptionalComponentNTA || comp instanceof ListComponentNTA) {
          tt.bind("AttrSign", attr.signature());
          tt.bind("AttrResetVisit", attr.resetVisit());
          tt.bind("AttrResetCache", attr.resetCache());
          res.append(tt.expand("ASTDecl.ntaFlush"));
        }
      }
    }
    return res.toString();
  }
  /**
   * Creates string with code checking if the value of an attribute,
   * identified with name and parameters, is a affected by a change.
   * TODO: Add support for attributes with more than one parameter
   * @aspect IncrementalNotification
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalNotification.jadd:157
   */
  public String emitAttrAffectedChecksString() {
    StringBuffer res = new StringBuffer();
    TemplateContext tt = templateContext();
    boolean firstAttr = true;
    for (AttrDecl attr : listOfCachedAttributes()) {
      res.append(firstAttr ? "" : "else ");
      tt.bind("AttrResetVisit", attr.resetVisit());
      tt.bind("AttrResetCache", attr.resetCache());
      tt.bind("IsNTA", attr.getNTA() || attr.isNTA());
      tt.bind("IsParameterized", attr.getNumParameter() > 0);
      tt.bind("IsAttrWithOneParam", attr.getNumParameter() == 1);
      tt.bind("IsPrimitiveAttr", attr.isPrimitive());
      tt.bind("AttrSign", attr.signature());
      tt.bind("AttrName", attr.getName());
      tt.bind("AttrType", attr.type());
      String attrObjectType = attr.type();
      attrObjectType = attrObjectType.substring(0,1).toUpperCase() + attrObjectType.substring(1);
      tt.bind("AttrObjectType", attrObjectType);
      tt.bind("ParamTypeSignature", attr.getNumParameter() > 0 ? attr.getParameter(0).getTypeInSignature(): "");
      res.append(tt.expand("ASTDecl.checkAttrValueAffected"));
      firstAttr = false;
    }
    return res.toString();
  }
  /**
   * Creates string with code checking if an attribute, identified with
   * name and parameters, should be flushed and dependencies notified.
   * @aspect IncrementalNotification
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalNotification.jadd:186
   */
  public String emitAttrFlushChecksString() {
    if (config().incrementalLevelNode() || config().incrementalLevelRegion()) {
      return "";
    }
    StringBuffer res = new StringBuffer();
    TemplateContext tt = templateContext();
    boolean firstAttr = true;
    for (AttrDecl attr : listOfCachedAttributes()) {
      res.append(firstAttr ? "" : "else ");
      tt.bind("IsParameterized", attr.getNumParameter() > 0);
      tt.bind("AttrSign", attr.signature());
      tt.bind("AttrType", attr.type());
      tt.bind("AttrResetVisit", attr.resetVisit());
      tt.bind("AttrResetCache", attr.resetCache());
      tt.bind("IsNTA", (attr.isNTA() || attr.getNTA()) &&
      		!attr.isPrimitive() && !(attr.type().equals("String") ||
      		attr.type().equals("java.lang.String")));
      res.append(tt.expand("ASTDecl.attrFlushCheck"));
      firstAttr = false;
    }
    return res.toString();
  }
  /**
   * Creates string with code flushing attributes.
   * @aspect IncrementalNotification
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalNotification.jadd:212
   */
  public String emitFlushAttrsString() {
    StringBuffer res = new StringBuffer();
    TemplateContext tt = templateContext();
    for (AttrDecl attr : listOfCachedAttributes()) {
      tt.bind("IsParameterized", attr.getNumParameter() > 0);
      tt.bind("AttrSign", attr.signature());
      tt.bind("AttrResetVisit", attr.resetVisit());
      tt.bind("AttrResetCache", attr.resetCache());
      tt.bind("IsNTA", (attr.isNTA() || attr.getNTA()) &&
      		!attr.isPrimitive() && !(attr.type().equals("String") ||
      		attr.type().equals("java.lang.String")));
      res.append(tt.expand("ASTDecl.attrFlush"));
    }
    return res.toString();
  }
  /**
   * @aspect IncrementalState
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalState.jadd:30
   */
  public void genIncrementalState(PrintWriter out) {

	TemplateContext tt = templateContext();

	tt.expand("ASTDecl.incStateFields", out);

	tt.bind("ChangeStateTokens", emitChangeStateTokensString());
	tt.bind("ChangeStateAttributes", emitChangeStateAttributesString());
	tt.expand("ASTDecl.incChangeStateMethod", out);

	tt.bind("ThrowAwayTokens", emitThrowAwayTokensString());
	tt.bind("ThrowAwayAttributes", emitThrowAwayAttributesString());
	tt.expand("ASTDecl.incThrowAwayMethod", out);

  }
  /**
   * @aspect IncrementalState
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalState.jadd:46
   */
  public String emitChangeStateTokensString() {
    StringBuffer res = new StringBuffer();
    TemplateContext tt = templateContext();
    for (int c = 0; c < getNumComponent(); c++) {
      Component comp = getComponent(c);
      if (comp instanceof TokenComponent) {
        tt.bind("Id", ((TokenComponent)comp).getTokenId().getID());
        res.append(tt.expand("ASTDecl.changeStateTokenHandler"));
      }
    }
 	  return res.toString();
  }
  /**
   * @aspect IncrementalState
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalState.jadd:59
   */
  public String emitChangeStateAttributesString() {
    StringBuffer res = new StringBuffer();
    TemplateContext tt = templateContext();
    for (Iterator itr = listOfCachedAttributes().iterator(); itr.hasNext();) {
      AttrDecl attr = (AttrDecl)itr.next();
      tt.bind("IsParameterized", attr.isParameterized());
      tt.bind("AttrSign", attr.signature());
      tt.bind("ChangeStateValue", (attr.isNTA() || attr.getNTA()) &&
          !attr.isPrimitive() && !(attr.type().equals("String") || attr.type().equals("java.lang.String")));
      res.append(tt.expand("ASTDecl.changeStateAttributeHandler"));
    }
    return res.toString();
  }
  /**
   * @aspect IncrementalState
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalState.jadd:73
   */
  public String emitThrowAwayTokensString() {
    StringBuffer res = new StringBuffer();
    TemplateContext tt = templateContext();
    for (int c = 0; c < getNumComponent(); c++) {
      Component comp = getComponent(c);
      if (comp instanceof TokenComponent) {
        tt.bind("Id", ((TokenComponent)comp).getTokenId().getID());
        res.append(tt.expand("ASTDecl.throwAwayTokenHandler"));
      }
    }
 	return res.toString();
  }
  /**
   * @aspect IncrementalState
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\incremental\\IncrementalState.jadd:86
   */
  public String emitThrowAwayAttributesString() {
    StringBuffer res = new StringBuffer();
    TemplateContext tt = templateContext();
    for (Iterator itr = listOfCachedAttributes().iterator(); itr.hasNext();) {
      AttrDecl attr = (AttrDecl)itr.next();
      tt.bind("IsParameterized", attr.isParameterized());
      tt.bind("AttrSign", attr.signature());
      tt.bind("ThrowAwayValue", (attr.isNTA() || attr.getNTA()) &&
          !attr.isPrimitive() && !(attr.type().equals("String") || attr.type().equals("java.lang.String")));
      res.append(tt.expand("ASTDecl.throwAwayAttributeHandler"));
    }
    return res.toString();
  }
  /**
   * @declaredat ASTNode:1
   */
  public ASTDecl(int i) {
    super(i);
  }
  /**
   * @declaredat ASTNode:5
   */
  public ASTDecl(Ast p, int i) {
    this(i);
    parser = p;
  }
  /**
   * @declaredat ASTNode:10
   */
  public ASTDecl() {
    this(0);
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:19
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[14];
    setChild(new Opt(), 0);
    setChild(new Opt(), 2);
    setChild(new List(), 3);
    setChild(new List(), 4);
    setChild(new List(), 5);
    setChild(new List(), 6);
    setChild(new List(), 7);
    setChild(new List(), 8);
    setChild(new List(), 9);
    setChild(new List(), 10);
    setChild(new List(), 11);
    setChild(new List(), 12);
  }
  /**
   * @declaredat ASTNode:34
   */
  public ASTDecl(String p0, Opt<Abstract> p1, IdDecl p2, Opt<IdUse> p3, List<Component> p4, List<SynDecl> p5, List<SynEq> p6, List<InhDecl> p7, List<InhEq> p8, List<ClassBodyDecl> p9, List<Rewrite> p10, List<CollDecl> p11, List<CollEq> p12, String p13, int p14, int p15, String p16, List<SynthesizedNta> p17) {
    setAspectName(p0);
    setChild(p1, 0);
    setChild(p2, 1);
    setChild(p3, 2);
    setChild(p4, 3);
    setChild(p5, 4);
    setChild(p6, 5);
    setChild(p7, 6);
    setChild(p8, 7);
    setChild(p9, 8);
    setChild(p10, 9);
    setChild(p11, 10);
    setChild(p12, 11);
    setFileName(p13);
    setStartLine(p14);
    setEndLine(p15);
    setComment(p16);
    setChild(p17, 12);
  }
  /**
   * @declaredat ASTNode:54
   */
  public void dumpTree(String indent, java.io.PrintStream out) {
    out.print(indent + "ASTDecl");
    out.print("\"" + getAspectName() + "\"");
    out.print("\"" + getFileName() + "\"");
    out.print("\"" + getStartLine() + "\"");
    out.print("\"" + getEndLine() + "\"");
    out.print("\"" + getComment() + "\"");
    String childIndent = indent + "  ";
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).dumpTree(childIndent, out);
    }
  }
  /**
   * @declaredat ASTNode:66
   */
  public Object jjtAccept(AstVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
  /**
   * @declaredat ASTNode:69
   */
  public void jjtAddChild(Node n, int i) {
    checkChild(n, i);
    super.jjtAddChild(n, i);
  }
  /**
   * @declaredat ASTNode:73
   */
  public void checkChild(Node n, int i) {
    if (i == 0) {
      if (!(n instanceof Opt)) {
        throw new Error("Child number 0 of ASTDecl has the type " +
            n.getClass().getName() + " which is not an instance of Opt");
      }
      if (((Opt) n).getNumChildNoTransform() != 0 && !(((Opt) n).getChildNoTransform(0) instanceof Abstract)) {
        throw new Error("Optional name() has the type " +
            ((Opt) n).getChildNoTransform(0).getClass().getName() +
            " which is not an instance of Abstract");
      }
    }
    if (i == 1 && !(n instanceof IdDecl)) {
     throw new Error("Child number 1 of ASTDecl has the type " +
       n.getClass().getName() + " which is not an instance of IdDecl");
    }
    if (i == 2) {
      if (!(n instanceof Opt)) {
        throw new Error("Child number 2 of ASTDecl has the type " +
            n.getClass().getName() + " which is not an instance of Opt");
      }
      if (((Opt) n).getNumChildNoTransform() != 0 && !(((Opt) n).getChildNoTransform(0) instanceof IdUse)) {
        throw new Error("Optional name() has the type " +
            ((Opt) n).getChildNoTransform(0).getClass().getName() +
            " which is not an instance of IdUse");
      }
    }
      if (i == 3) {
        if (!(n instanceof List)) {
          throw new Error("Child number 3 of ASTDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof Component)) {
            throw new Error("Child number " + k + " in ComponentList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of Component");
          }
        }
      }
      if (i == 4) {
        if (!(n instanceof List)) {
          throw new Error("Child number 4 of ASTDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof SynDecl)) {
            throw new Error("Child number " + k + " in SynDeclList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of SynDecl");
          }
        }
      }
      if (i == 5) {
        if (!(n instanceof List)) {
          throw new Error("Child number 5 of ASTDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof SynEq)) {
            throw new Error("Child number " + k + " in SynEqList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of SynEq");
          }
        }
      }
      if (i == 6) {
        if (!(n instanceof List)) {
          throw new Error("Child number 6 of ASTDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof InhDecl)) {
            throw new Error("Child number " + k + " in InhDeclList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of InhDecl");
          }
        }
      }
      if (i == 7) {
        if (!(n instanceof List)) {
          throw new Error("Child number 7 of ASTDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof InhEq)) {
            throw new Error("Child number " + k + " in InhEqList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of InhEq");
          }
        }
      }
      if (i == 8) {
        if (!(n instanceof List)) {
          throw new Error("Child number 8 of ASTDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof ClassBodyDecl)) {
            throw new Error("Child number " + k + " in ClassBodyDeclList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of ClassBodyDecl");
          }
        }
      }
      if (i == 9) {
        if (!(n instanceof List)) {
          throw new Error("Child number 9 of ASTDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof Rewrite)) {
            throw new Error("Child number " + k + " in RewriteList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of Rewrite");
          }
        }
      }
      if (i == 10) {
        if (!(n instanceof List)) {
          throw new Error("Child number 10 of ASTDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof CollDecl)) {
            throw new Error("Child number " + k + " in CollDeclList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of CollDecl");
          }
        }
      }
      if (i == 11) {
        if (!(n instanceof List)) {
          throw new Error("Child number 11 of ASTDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof CollEq)) {
            throw new Error("Child number " + k + " in CollEqList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of CollEq");
          }
        }
      }
      if (i == 12) {
        if (!(n instanceof List)) {
          throw new Error("Child number 12 of ASTDecl has the type " +
            n.getClass().getName() + " which is not an instance of List");
        }
        for (int k = 0; k < ((List) n).getNumChildNoTransform(); k++) {
          if (!(((List) n).getChildNoTransform(k) instanceof SynthesizedNta)) {
            throw new Error("Child number " + k + " in SynthesizedNtaList has the type " +
              ((List) n).getChildNoTransform(k).getClass().getName() + " which is not an instance of SynthesizedNta");
          }
        }
      }
    if (i == 13 && !(n instanceof CircularRewriteDecl)) {
     throw new Error("Child number 13 of ASTDecl has the type " +
       n.getClass().getName() + " which is not an instance of CircularRewriteDecl");
    }
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:226
   */
  @SideEffect.Pure public int getNumChild() {
    return 13;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:232
   */
  public boolean mayHaveRewrite() {
    return true;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:236
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    lookupComponent_String_reset();
    superClass_reset();
    getSuperClassName_reset();
    supertypes_reset();
    subclasses_reset();
    subclassesTransitive_reset();
    isRegionRoot_reset();
    hasRegionRootAsSuperClass_reset();
    isRegionLeaf_reset();
    lookupInhDecl_String_reset();
    defaultInhEqs_reset();
    componentInhEqs_reset();
    synEquations_reset();
    synDeclarations_reset();
    inhEquations_reset();
    inhDeclarations_reset();
    lookupSynDecl_String_reset();
    lookupSynEq_String_reset();
    lookupInhEq_String_String_reset();
    hasRewrites_reset();
    hasRewriteAttribute_reset();
    getCircularRewriteDecl_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:262
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:266
   */
  @SideEffect.Fresh public ASTDecl clone() throws CloneNotSupportedException {
    ASTDecl node = (ASTDecl) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:271
   */
  @SideEffect.Fresh(group="_ASTNode") public ASTDecl copy() {
    try {
      ASTDecl node = (ASTDecl) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:290
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public ASTDecl fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:300
   */
  @SideEffect.Fresh(group="_ASTNode") public ASTDecl treeCopyNoTransform() {
    ASTDecl tree = (ASTDecl) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        switch (i) {
        case 13:
          tree.children[i] = null;
          continue;
        }
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:325
   */
  @SideEffect.Fresh(group="_ASTNode") public ASTDecl treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:330
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_AspectName == ((ASTDecl) node).tokenString_AspectName) && (tokenString_FileName == ((ASTDecl) node).tokenString_FileName) && (tokenint_StartLine == ((ASTDecl) node).tokenint_StartLine) && (tokenint_EndLine == ((ASTDecl) node).tokenint_EndLine) && (tokenString_Comment == ((ASTDecl) node).tokenString_Comment);    
  }
  /**
   * Replaces the lexeme AspectName.
   * @param value The new value for the lexeme AspectName.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setAspectName(String value) {
    tokenString_AspectName = value;
  }
  /**
   * Retrieves the value for the lexeme AspectName.
   * @return The value for the lexeme AspectName.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="AspectName")
  @SideEffect.Pure(group="_ASTNode") public String getAspectName() {
    return tokenString_AspectName != null ? tokenString_AspectName : "";
  }
  /**
   * Replaces the optional node for the Abstract child. This is the <code>Opt</code>
   * node containing the child Abstract, not the actual child!
   * @param opt The new node to be used as the optional node for the Abstract child.
   * @apilevel low-level
   */
  public void setAbstractOpt(Opt<Abstract> opt) {
    setChild(opt, 0);
  }
  /**
   * Replaces the (optional) Abstract child.
   * @param node The new node to be used as the Abstract child.
   * @apilevel high-level
   */
  public void setAbstract(Abstract node) {
    getAbstractOpt().setChild(node, 0);
  }
  /**
   * Check whether the optional Abstract child exists.
   * @return {@code true} if the optional Abstract child exists, {@code false} if it does not.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasAbstract() {
    return getAbstractOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) Abstract child.
   * @return The Abstract child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   */
  @SideEffect.Pure public Abstract getAbstract() {
    return (Abstract) getAbstractOpt().getChild(0);
  }
  /**
   * Retrieves the optional node for the Abstract child. This is the <code>Opt</code> node containing the child Abstract, not the actual child!
   * @return The optional node for child the Abstract child.
   * @apilevel low-level
   */
  @ASTNodeAnnotation.OptChild(name="Abstract")
  @SideEffect.Pure public Opt<Abstract> getAbstractOpt() {
    return (Opt<Abstract>) getChild(0);
  }
  /**
   * Retrieves the optional node for child Abstract. This is the <code>Opt</code> node containing the child Abstract, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child Abstract.
   * @apilevel low-level
   */
  @SideEffect.Pure public Opt<Abstract> getAbstractOptNoTransform() {
    return (Opt<Abstract>) getChildNoTransform(0);
  }
  /**
   * Replaces the IdDecl child.
   * @param node The new node to replace the IdDecl child.
   * @apilevel high-level
   */
  public void setIdDecl(IdDecl node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the IdDecl child.
   * @return The current node used as the IdDecl child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="IdDecl")
  @SideEffect.Pure public IdDecl getIdDecl() {
    return (IdDecl) getChild(1);
  }
  /**
   * Retrieves the IdDecl child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the IdDecl child.
   * @apilevel low-level
   */
  @SideEffect.Pure public IdDecl getIdDeclNoTransform() {
    return (IdDecl) getChildNoTransform(1);
  }
  /**
   * Replaces the optional node for the SuperClass child. This is the <code>Opt</code>
   * node containing the child SuperClass, not the actual child!
   * @param opt The new node to be used as the optional node for the SuperClass child.
   * @apilevel low-level
   */
  public void setSuperClassOpt(Opt<IdUse> opt) {
    setChild(opt, 2);
  }
  /**
   * Replaces the (optional) SuperClass child.
   * @param node The new node to be used as the SuperClass child.
   * @apilevel high-level
   */
  public void setSuperClass(IdUse node) {
    getSuperClassOpt().setChild(node, 0);
  }
  /**
   * Check whether the optional SuperClass child exists.
   * @return {@code true} if the optional SuperClass child exists, {@code false} if it does not.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasSuperClass() {
    return getSuperClassOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) SuperClass child.
   * @return The SuperClass child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   */
  @SideEffect.Pure public IdUse getSuperClass() {
    return (IdUse) getSuperClassOpt().getChild(0);
  }
  /**
   * Retrieves the optional node for the SuperClass child. This is the <code>Opt</code> node containing the child SuperClass, not the actual child!
   * @return The optional node for child the SuperClass child.
   * @apilevel low-level
   */
  @ASTNodeAnnotation.OptChild(name="SuperClass")
  @SideEffect.Pure public Opt<IdUse> getSuperClassOpt() {
    return (Opt<IdUse>) getChild(2);
  }
  /**
   * Retrieves the optional node for child SuperClass. This is the <code>Opt</code> node containing the child SuperClass, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child SuperClass.
   * @apilevel low-level
   */
  @SideEffect.Pure public Opt<IdUse> getSuperClassOptNoTransform() {
    return (Opt<IdUse>) getChildNoTransform(2);
  }
  /**
   * Replaces the Component list.
   * @param list The new list node to be used as the Component list.
   * @apilevel high-level
   */
  public void setComponentList(List<Component> list) {
    setChild(list, 3);
  }
  /**
   * Retrieves the number of children in the Component list.
   * @return Number of children in the Component list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumComponent() {
    return getComponentList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Component list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Component list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumComponentNoTransform() {
    return getComponentListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Component list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Component list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Component getComponent(int i) {
    return (Component) getComponentList().getChild(i);
  }
  /**
   * Check whether the Component list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasComponent() {
    return getComponentList().getNumChild() != 0;
  }
  /**
   * Append an element to the Component list.
   * @param node The element to append to the Component list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addComponent(Component node) {
    List<Component> list = (parent == null) ? getComponentListNoTransform() : getComponentList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addComponentNoTransform(Component node) {
    List<Component> list = getComponentListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Component list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setComponent(Component node, int i) {
    List<Component> list = getComponentList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Component list.
   * @return The node representing the Component list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Component")
  @SideEffect.Pure(group="_ASTNode") public List<Component> getComponentList() {
    List<Component> list = (List<Component>) getChild(3);
    return list;
  }
  /**
   * Retrieves the Component list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Component list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Component> getComponentListNoTransform() {
    return (List<Component>) getChildNoTransform(3);
  }
  /**
   * @return the element at index {@code i} in the Component list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Component getComponentNoTransform(int i) {
    return (Component) getComponentListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Component list.
   * @return The node representing the Component list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Component> getComponents() {
    return getComponentList();
  }
  /**
   * Retrieves the Component list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Component list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Component> getComponentsNoTransform() {
    return getComponentListNoTransform();
  }
  /**
   * Replaces the SynDecl list.
   * @param list The new list node to be used as the SynDecl list.
   * @apilevel high-level
   */
  public void setSynDeclList(List<SynDecl> list) {
    setChild(list, 4);
  }
  /**
   * Retrieves the number of children in the SynDecl list.
   * @return Number of children in the SynDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumSynDecl() {
    return getSynDeclList().getNumChild();
  }
  /**
   * Retrieves the number of children in the SynDecl list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the SynDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumSynDeclNoTransform() {
    return getSynDeclListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the SynDecl list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the SynDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public SynDecl getSynDecl(int i) {
    return (SynDecl) getSynDeclList().getChild(i);
  }
  /**
   * Check whether the SynDecl list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasSynDecl() {
    return getSynDeclList().getNumChild() != 0;
  }
  /**
   * Append an element to the SynDecl list.
   * @param node The element to append to the SynDecl list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addSynDecl(SynDecl node) {
    List<SynDecl> list = (parent == null) ? getSynDeclListNoTransform() : getSynDeclList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addSynDeclNoTransform(SynDecl node) {
    List<SynDecl> list = getSynDeclListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the SynDecl list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setSynDecl(SynDecl node, int i) {
    List<SynDecl> list = getSynDeclList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the SynDecl list.
   * @return The node representing the SynDecl list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="SynDecl")
  @SideEffect.Pure(group="_ASTNode") public List<SynDecl> getSynDeclList() {
    List<SynDecl> list = (List<SynDecl>) getChild(4);
    return list;
  }
  /**
   * Retrieves the SynDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the SynDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<SynDecl> getSynDeclListNoTransform() {
    return (List<SynDecl>) getChildNoTransform(4);
  }
  /**
   * @return the element at index {@code i} in the SynDecl list without
   * triggering rewrites.
   */
  @SideEffect.Pure public SynDecl getSynDeclNoTransform(int i) {
    return (SynDecl) getSynDeclListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the SynDecl list.
   * @return The node representing the SynDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<SynDecl> getSynDecls() {
    return getSynDeclList();
  }
  /**
   * Retrieves the SynDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the SynDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<SynDecl> getSynDeclsNoTransform() {
    return getSynDeclListNoTransform();
  }
  /**
   * Replaces the SynEq list.
   * @param list The new list node to be used as the SynEq list.
   * @apilevel high-level
   */
  public void setSynEqList(List<SynEq> list) {
    setChild(list, 5);
  }
  /**
   * Retrieves the number of children in the SynEq list.
   * @return Number of children in the SynEq list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumSynEq() {
    return getSynEqList().getNumChild();
  }
  /**
   * Retrieves the number of children in the SynEq list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the SynEq list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumSynEqNoTransform() {
    return getSynEqListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the SynEq list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the SynEq list.
   * @apilevel high-level
   */
  @SideEffect.Pure public SynEq getSynEq(int i) {
    return (SynEq) getSynEqList().getChild(i);
  }
  /**
   * Check whether the SynEq list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasSynEq() {
    return getSynEqList().getNumChild() != 0;
  }
  /**
   * Append an element to the SynEq list.
   * @param node The element to append to the SynEq list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addSynEq(SynEq node) {
    List<SynEq> list = (parent == null) ? getSynEqListNoTransform() : getSynEqList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addSynEqNoTransform(SynEq node) {
    List<SynEq> list = getSynEqListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the SynEq list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setSynEq(SynEq node, int i) {
    List<SynEq> list = getSynEqList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the SynEq list.
   * @return The node representing the SynEq list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="SynEq")
  @SideEffect.Pure(group="_ASTNode") public List<SynEq> getSynEqList() {
    List<SynEq> list = (List<SynEq>) getChild(5);
    return list;
  }
  /**
   * Retrieves the SynEq list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the SynEq list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<SynEq> getSynEqListNoTransform() {
    return (List<SynEq>) getChildNoTransform(5);
  }
  /**
   * @return the element at index {@code i} in the SynEq list without
   * triggering rewrites.
   */
  @SideEffect.Pure public SynEq getSynEqNoTransform(int i) {
    return (SynEq) getSynEqListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the SynEq list.
   * @return The node representing the SynEq list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<SynEq> getSynEqs() {
    return getSynEqList();
  }
  /**
   * Retrieves the SynEq list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the SynEq list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<SynEq> getSynEqsNoTransform() {
    return getSynEqListNoTransform();
  }
  /**
   * Replaces the InhDecl list.
   * @param list The new list node to be used as the InhDecl list.
   * @apilevel high-level
   */
  public void setInhDeclList(List<InhDecl> list) {
    setChild(list, 6);
  }
  /**
   * Retrieves the number of children in the InhDecl list.
   * @return Number of children in the InhDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumInhDecl() {
    return getInhDeclList().getNumChild();
  }
  /**
   * Retrieves the number of children in the InhDecl list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the InhDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumInhDeclNoTransform() {
    return getInhDeclListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the InhDecl list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the InhDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public InhDecl getInhDecl(int i) {
    return (InhDecl) getInhDeclList().getChild(i);
  }
  /**
   * Check whether the InhDecl list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasInhDecl() {
    return getInhDeclList().getNumChild() != 0;
  }
  /**
   * Append an element to the InhDecl list.
   * @param node The element to append to the InhDecl list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addInhDecl(InhDecl node) {
    List<InhDecl> list = (parent == null) ? getInhDeclListNoTransform() : getInhDeclList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addInhDeclNoTransform(InhDecl node) {
    List<InhDecl> list = getInhDeclListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the InhDecl list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setInhDecl(InhDecl node, int i) {
    List<InhDecl> list = getInhDeclList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the InhDecl list.
   * @return The node representing the InhDecl list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="InhDecl")
  @SideEffect.Pure(group="_ASTNode") public List<InhDecl> getInhDeclList() {
    List<InhDecl> list = (List<InhDecl>) getChild(6);
    return list;
  }
  /**
   * Retrieves the InhDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the InhDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<InhDecl> getInhDeclListNoTransform() {
    return (List<InhDecl>) getChildNoTransform(6);
  }
  /**
   * @return the element at index {@code i} in the InhDecl list without
   * triggering rewrites.
   */
  @SideEffect.Pure public InhDecl getInhDeclNoTransform(int i) {
    return (InhDecl) getInhDeclListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the InhDecl list.
   * @return The node representing the InhDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<InhDecl> getInhDecls() {
    return getInhDeclList();
  }
  /**
   * Retrieves the InhDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the InhDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<InhDecl> getInhDeclsNoTransform() {
    return getInhDeclListNoTransform();
  }
  /**
   * Replaces the InhEq list.
   * @param list The new list node to be used as the InhEq list.
   * @apilevel high-level
   */
  public void setInhEqList(List<InhEq> list) {
    setChild(list, 7);
  }
  /**
   * Retrieves the number of children in the InhEq list.
   * @return Number of children in the InhEq list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumInhEq() {
    return getInhEqList().getNumChild();
  }
  /**
   * Retrieves the number of children in the InhEq list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the InhEq list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumInhEqNoTransform() {
    return getInhEqListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the InhEq list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the InhEq list.
   * @apilevel high-level
   */
  @SideEffect.Pure public InhEq getInhEq(int i) {
    return (InhEq) getInhEqList().getChild(i);
  }
  /**
   * Check whether the InhEq list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasInhEq() {
    return getInhEqList().getNumChild() != 0;
  }
  /**
   * Append an element to the InhEq list.
   * @param node The element to append to the InhEq list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addInhEq(InhEq node) {
    List<InhEq> list = (parent == null) ? getInhEqListNoTransform() : getInhEqList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addInhEqNoTransform(InhEq node) {
    List<InhEq> list = getInhEqListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the InhEq list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setInhEq(InhEq node, int i) {
    List<InhEq> list = getInhEqList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the InhEq list.
   * @return The node representing the InhEq list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="InhEq")
  @SideEffect.Pure(group="_ASTNode") public List<InhEq> getInhEqList() {
    List<InhEq> list = (List<InhEq>) getChild(7);
    return list;
  }
  /**
   * Retrieves the InhEq list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the InhEq list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<InhEq> getInhEqListNoTransform() {
    return (List<InhEq>) getChildNoTransform(7);
  }
  /**
   * @return the element at index {@code i} in the InhEq list without
   * triggering rewrites.
   */
  @SideEffect.Pure public InhEq getInhEqNoTransform(int i) {
    return (InhEq) getInhEqListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the InhEq list.
   * @return The node representing the InhEq list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<InhEq> getInhEqs() {
    return getInhEqList();
  }
  /**
   * Retrieves the InhEq list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the InhEq list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<InhEq> getInhEqsNoTransform() {
    return getInhEqListNoTransform();
  }
  /**
   * Replaces the ClassBodyDecl list.
   * @param list The new list node to be used as the ClassBodyDecl list.
   * @apilevel high-level
   */
  public void setClassBodyDeclList(List<ClassBodyDecl> list) {
    setChild(list, 8);
  }
  /**
   * Retrieves the number of children in the ClassBodyDecl list.
   * @return Number of children in the ClassBodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumClassBodyDecl() {
    return getClassBodyDeclList().getNumChild();
  }
  /**
   * Retrieves the number of children in the ClassBodyDecl list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the ClassBodyDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumClassBodyDeclNoTransform() {
    return getClassBodyDeclListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the ClassBodyDecl list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the ClassBodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public ClassBodyDecl getClassBodyDecl(int i) {
    return (ClassBodyDecl) getClassBodyDeclList().getChild(i);
  }
  /**
   * Check whether the ClassBodyDecl list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasClassBodyDecl() {
    return getClassBodyDeclList().getNumChild() != 0;
  }
  /**
   * Append an element to the ClassBodyDecl list.
   * @param node The element to append to the ClassBodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addClassBodyDecl(ClassBodyDecl node) {
    List<ClassBodyDecl> list = (parent == null) ? getClassBodyDeclListNoTransform() : getClassBodyDeclList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addClassBodyDeclNoTransform(ClassBodyDecl node) {
    List<ClassBodyDecl> list = getClassBodyDeclListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the ClassBodyDecl list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setClassBodyDecl(ClassBodyDecl node, int i) {
    List<ClassBodyDecl> list = getClassBodyDeclList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the ClassBodyDecl list.
   * @return The node representing the ClassBodyDecl list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="ClassBodyDecl")
  @SideEffect.Pure(group="_ASTNode") public List<ClassBodyDecl> getClassBodyDeclList() {
    List<ClassBodyDecl> list = (List<ClassBodyDecl>) getChild(8);
    return list;
  }
  /**
   * Retrieves the ClassBodyDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the ClassBodyDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<ClassBodyDecl> getClassBodyDeclListNoTransform() {
    return (List<ClassBodyDecl>) getChildNoTransform(8);
  }
  /**
   * @return the element at index {@code i} in the ClassBodyDecl list without
   * triggering rewrites.
   */
  @SideEffect.Pure public ClassBodyDecl getClassBodyDeclNoTransform(int i) {
    return (ClassBodyDecl) getClassBodyDeclListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the ClassBodyDecl list.
   * @return The node representing the ClassBodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<ClassBodyDecl> getClassBodyDecls() {
    return getClassBodyDeclList();
  }
  /**
   * Retrieves the ClassBodyDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the ClassBodyDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<ClassBodyDecl> getClassBodyDeclsNoTransform() {
    return getClassBodyDeclListNoTransform();
  }
  /**
   * Replaces the Rewrite list.
   * @param list The new list node to be used as the Rewrite list.
   * @apilevel high-level
   */
  public void setRewriteList(List<Rewrite> list) {
    setChild(list, 9);
  }
  /**
   * Retrieves the number of children in the Rewrite list.
   * @return Number of children in the Rewrite list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumRewrite() {
    return getRewriteList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Rewrite list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Rewrite list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumRewriteNoTransform() {
    return getRewriteListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Rewrite list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Rewrite list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Rewrite getRewrite(int i) {
    return (Rewrite) getRewriteList().getChild(i);
  }
  /**
   * Check whether the Rewrite list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasRewrite() {
    return getRewriteList().getNumChild() != 0;
  }
  /**
   * Append an element to the Rewrite list.
   * @param node The element to append to the Rewrite list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addRewrite(Rewrite node) {
    List<Rewrite> list = (parent == null) ? getRewriteListNoTransform() : getRewriteList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addRewriteNoTransform(Rewrite node) {
    List<Rewrite> list = getRewriteListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Rewrite list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setRewrite(Rewrite node, int i) {
    List<Rewrite> list = getRewriteList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Rewrite list.
   * @return The node representing the Rewrite list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Rewrite")
  @SideEffect.Pure(group="_ASTNode") public List<Rewrite> getRewriteList() {
    List<Rewrite> list = (List<Rewrite>) getChild(9);
    return list;
  }
  /**
   * Retrieves the Rewrite list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Rewrite list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Rewrite> getRewriteListNoTransform() {
    return (List<Rewrite>) getChildNoTransform(9);
  }
  /**
   * @return the element at index {@code i} in the Rewrite list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Rewrite getRewriteNoTransform(int i) {
    return (Rewrite) getRewriteListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Rewrite list.
   * @return The node representing the Rewrite list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Rewrite> getRewrites() {
    return getRewriteList();
  }
  /**
   * Retrieves the Rewrite list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Rewrite list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Rewrite> getRewritesNoTransform() {
    return getRewriteListNoTransform();
  }
  /**
   * Replaces the CollDecl list.
   * @param list The new list node to be used as the CollDecl list.
   * @apilevel high-level
   */
  public void setCollDeclList(List<CollDecl> list) {
    setChild(list, 10);
  }
  /**
   * Retrieves the number of children in the CollDecl list.
   * @return Number of children in the CollDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumCollDecl() {
    return getCollDeclList().getNumChild();
  }
  /**
   * Retrieves the number of children in the CollDecl list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the CollDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumCollDeclNoTransform() {
    return getCollDeclListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the CollDecl list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the CollDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public CollDecl getCollDecl(int i) {
    return (CollDecl) getCollDeclList().getChild(i);
  }
  /**
   * Check whether the CollDecl list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasCollDecl() {
    return getCollDeclList().getNumChild() != 0;
  }
  /**
   * Append an element to the CollDecl list.
   * @param node The element to append to the CollDecl list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addCollDecl(CollDecl node) {
    List<CollDecl> list = (parent == null) ? getCollDeclListNoTransform() : getCollDeclList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addCollDeclNoTransform(CollDecl node) {
    List<CollDecl> list = getCollDeclListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the CollDecl list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setCollDecl(CollDecl node, int i) {
    List<CollDecl> list = getCollDeclList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the CollDecl list.
   * @return The node representing the CollDecl list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="CollDecl")
  @SideEffect.Pure(group="_ASTNode") public List<CollDecl> getCollDeclList() {
    List<CollDecl> list = (List<CollDecl>) getChild(10);
    return list;
  }
  /**
   * Retrieves the CollDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the CollDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<CollDecl> getCollDeclListNoTransform() {
    return (List<CollDecl>) getChildNoTransform(10);
  }
  /**
   * @return the element at index {@code i} in the CollDecl list without
   * triggering rewrites.
   */
  @SideEffect.Pure public CollDecl getCollDeclNoTransform(int i) {
    return (CollDecl) getCollDeclListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the CollDecl list.
   * @return The node representing the CollDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<CollDecl> getCollDecls() {
    return getCollDeclList();
  }
  /**
   * Retrieves the CollDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the CollDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<CollDecl> getCollDeclsNoTransform() {
    return getCollDeclListNoTransform();
  }
  /**
   * Replaces the CollEq list.
   * @param list The new list node to be used as the CollEq list.
   * @apilevel high-level
   */
  public void setCollEqList(List<CollEq> list) {
    setChild(list, 11);
  }
  /**
   * Retrieves the number of children in the CollEq list.
   * @return Number of children in the CollEq list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumCollEq() {
    return getCollEqList().getNumChild();
  }
  /**
   * Retrieves the number of children in the CollEq list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the CollEq list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumCollEqNoTransform() {
    return getCollEqListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the CollEq list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the CollEq list.
   * @apilevel high-level
   */
  @SideEffect.Pure public CollEq getCollEq(int i) {
    return (CollEq) getCollEqList().getChild(i);
  }
  /**
   * Check whether the CollEq list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasCollEq() {
    return getCollEqList().getNumChild() != 0;
  }
  /**
   * Append an element to the CollEq list.
   * @param node The element to append to the CollEq list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addCollEq(CollEq node) {
    List<CollEq> list = (parent == null) ? getCollEqListNoTransform() : getCollEqList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addCollEqNoTransform(CollEq node) {
    List<CollEq> list = getCollEqListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the CollEq list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setCollEq(CollEq node, int i) {
    List<CollEq> list = getCollEqList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the CollEq list.
   * @return The node representing the CollEq list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="CollEq")
  @SideEffect.Pure(group="_ASTNode") public List<CollEq> getCollEqList() {
    List<CollEq> list = (List<CollEq>) getChild(11);
    return list;
  }
  /**
   * Retrieves the CollEq list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the CollEq list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<CollEq> getCollEqListNoTransform() {
    return (List<CollEq>) getChildNoTransform(11);
  }
  /**
   * @return the element at index {@code i} in the CollEq list without
   * triggering rewrites.
   */
  @SideEffect.Pure public CollEq getCollEqNoTransform(int i) {
    return (CollEq) getCollEqListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the CollEq list.
   * @return The node representing the CollEq list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<CollEq> getCollEqs() {
    return getCollEqList();
  }
  /**
   * Retrieves the CollEq list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the CollEq list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<CollEq> getCollEqsNoTransform() {
    return getCollEqListNoTransform();
  }
  /**
   * Replaces the lexeme FileName.
   * @param value The new value for the lexeme FileName.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setFileName(String value) {
    tokenString_FileName = value;
  }
  /**
   * Retrieves the value for the lexeme FileName.
   * @return The value for the lexeme FileName.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="FileName")
  @SideEffect.Pure(group="_ASTNode") public String getFileName() {
    return tokenString_FileName != null ? tokenString_FileName : "";
  }
  /**
   * Replaces the lexeme StartLine.
   * @param value The new value for the lexeme StartLine.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setStartLine(int value) {
    tokenint_StartLine = value;
  }
  /**
   * Retrieves the value for the lexeme StartLine.
   * @return The value for the lexeme StartLine.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="StartLine")
  @SideEffect.Pure(group="_ASTNode") public int getStartLine() {
    return tokenint_StartLine;
  }
  /**
   * Replaces the lexeme EndLine.
   * @param value The new value for the lexeme EndLine.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setEndLine(int value) {
    tokenint_EndLine = value;
  }
  /**
   * Retrieves the value for the lexeme EndLine.
   * @return The value for the lexeme EndLine.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="EndLine")
  @SideEffect.Pure(group="_ASTNode") public int getEndLine() {
    return tokenint_EndLine;
  }
  /**
   * Replaces the lexeme Comment.
   * @param value The new value for the lexeme Comment.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setComment(String value) {
    tokenString_Comment = value;
  }
  /**
   * Retrieves the value for the lexeme Comment.
   * @return The value for the lexeme Comment.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Comment")
  @SideEffect.Pure(group="_ASTNode") public String getComment() {
    return tokenString_Comment != null ? tokenString_Comment : "";
  }
  /**
   * Replaces the SynthesizedNta list.
   * @param list The new list node to be used as the SynthesizedNta list.
   * @apilevel high-level
   */
  public void setSynthesizedNtaList(List<SynthesizedNta> list) {
    setChild(list, 12);
  }
  /**
   * Retrieves the number of children in the SynthesizedNta list.
   * @return Number of children in the SynthesizedNta list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumSynthesizedNta() {
    return getSynthesizedNtaList().getNumChild();
  }
  /**
   * Retrieves the number of children in the SynthesizedNta list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the SynthesizedNta list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumSynthesizedNtaNoTransform() {
    return getSynthesizedNtaListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the SynthesizedNta list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the SynthesizedNta list.
   * @apilevel high-level
   */
  @SideEffect.Pure public SynthesizedNta getSynthesizedNta(int i) {
    return (SynthesizedNta) getSynthesizedNtaList().getChild(i);
  }
  /**
   * Check whether the SynthesizedNta list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasSynthesizedNta() {
    return getSynthesizedNtaList().getNumChild() != 0;
  }
  /**
   * Append an element to the SynthesizedNta list.
   * @param node The element to append to the SynthesizedNta list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addSynthesizedNta(SynthesizedNta node) {
    List<SynthesizedNta> list = (parent == null) ? getSynthesizedNtaListNoTransform() : getSynthesizedNtaList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addSynthesizedNtaNoTransform(SynthesizedNta node) {
    List<SynthesizedNta> list = getSynthesizedNtaListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the SynthesizedNta list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setSynthesizedNta(SynthesizedNta node, int i) {
    List<SynthesizedNta> list = getSynthesizedNtaList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the SynthesizedNta list.
   * @return The node representing the SynthesizedNta list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="SynthesizedNta")
  @SideEffect.Pure(group="_ASTNode") public List<SynthesizedNta> getSynthesizedNtaList() {
    List<SynthesizedNta> list = (List<SynthesizedNta>) getChild(12);
    return list;
  }
  /**
   * Retrieves the SynthesizedNta list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the SynthesizedNta list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<SynthesizedNta> getSynthesizedNtaListNoTransform() {
    return (List<SynthesizedNta>) getChildNoTransform(12);
  }
  /**
   * @return the element at index {@code i} in the SynthesizedNta list without
   * triggering rewrites.
   */
  @SideEffect.Pure public SynthesizedNta getSynthesizedNtaNoTransform(int i) {
    return (SynthesizedNta) getSynthesizedNtaListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the SynthesizedNta list.
   * @return The node representing the SynthesizedNta list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<SynthesizedNta> getSynthesizedNtas() {
    return getSynthesizedNtaList();
  }
  /**
   * Retrieves the SynthesizedNta list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the SynthesizedNta list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<SynthesizedNta> getSynthesizedNtasNoTransform() {
    return getSynthesizedNtaListNoTransform();
  }
  /**
   * Retrieves the CircularRewriteDecl child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the CircularRewriteDecl child.
   * @apilevel low-level
   */
  @SideEffect.Pure public CircularRewriteDecl getCircularRewriteDeclNoTransform() {
    return (CircularRewriteDecl) getChildNoTransform(13);
  }
  /**
   * Retrieves the child position of the optional child CircularRewriteDecl.
   * @return The the child position of the optional child CircularRewriteDecl.
   * @apilevel low-level
   */
  @SideEffect.Pure protected int getCircularRewriteDeclChildPosition() {
    return 13;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int copyReturnType_visited = -1;
  /**
   * @attribute syn
   * @aspect ASTCloneNode
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTCopyNode.jadd:31
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ASTCloneNode", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTCopyNode.jadd:31")
  @SideEffect.Pure(group="_ASTNode") public String copyReturnType() {
    if (copyReturnType_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.copyReturnType().");
    }
    copyReturnType_visited = state().boundariesCrossed;
    try {
        if (isOptDecl() || isListDecl() || isASTNodeDecl()) {
          return name() + "<T>";
        } else {
          return name();
        }
      }
    finally {
      copyReturnType_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isRootNode_visited = -1;
  /**
   * @attribute syn
   * @aspect ASTErrors
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTErrors.jrag:46
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ASTErrors", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTErrors.jrag:46")
  @SideEffect.Pure(group="_ASTNode") public boolean isRootNode() {
    if (isRootNode_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.isRootNode().");
    }
    isRootNode_visited = state().boundariesCrossed;
    boolean isRootNode_value = isPotentialRootNode() && parents().isEmpty();
    isRootNode_visited = -1;
    return isRootNode_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isPotentialRootNode_visited = -1;
  /**
   * @attribute syn
   * @aspect ASTErrors
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTErrors.jrag:50
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ASTErrors", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTErrors.jrag:50")
  @SideEffect.Pure(group="_ASTNode") public boolean isPotentialRootNode() {
    if (isPotentialRootNode_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.isPotentialRootNode().");
    }
    isPotentialRootNode_visited = state().boundariesCrossed;
    boolean isPotentialRootNode_value = !hasAbstract() && !isASTNodeDecl() && !isOptSubtype() && !isListSubtype();
    isPotentialRootNode_visited = -1;
    return isPotentialRootNode_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map lookupComponent_String_visited = new java.util.HashMap(4);
  /** @apilevel internal */
  @SideEffect.Ignore private void lookupComponent_String_reset() {
    lookupComponent_String_values = new java.util.HashMap(4);
    lookupComponent_String_visited = new java.util.HashMap(4);
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map lookupComponent_String_values = new java.util.HashMap(4);

  /**
   * @attribute syn
   * @aspect Lookup
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:90
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Lookup", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:90")
  @SideEffect.Pure(group="_ASTNode") public Component lookupComponent(String name) {
    Object _parameters = name;
    ASTState state = state();
    if (lookupComponent_String_values.containsKey(_parameters)) {
      return (Component) lookupComponent_String_values.get(_parameters);
    }
    if (Integer.valueOf(state().boundariesCrossed).equals(lookupComponent_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.lookupComponent(String).");
    }
    lookupComponent_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    Component lookupComponent_String_value = lookupComponent_compute(name);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    lookupComponent_String_values.put(_parameters, lookupComponent_String_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    lookupComponent_String_visited.remove(_parameters);
    return lookupComponent_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Component lookupComponent_compute(String name) {
      for (Component c : components()) {
        if (c.name().equals(name)) {
          return c;
        }
      }
      return null;
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int annotations_visited = -1;
  /**
   * @attribute syn
   * @aspect Annotations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Annotations.jrag:87
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Annotations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Annotations.jrag:87")
  @SideEffect.Pure(group="_ASTNode") public String annotations() {
    if (annotations_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.annotations().");
    }
    annotations_visited = state().boundariesCrossed;
    String annotations_value = "";
    annotations_visited = -1;
    return annotations_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map missingSynEqs_String_visited = new java.util.HashMap(4);
  /**
   * @param signature the signature of the attribute
   * @return the subclasses of this AST class that are missing an
   * equation for for the synthesized attribute
   * @attribute syn
   * @aspect AttributeProblems
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:248
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AttributeProblems", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\AttributeProblems.jrag:248")
  @SideEffect.Pure(group="_ASTNode") public Collection<? extends TypeDecl> missingSynEqs(String signature) {
    Object _parameters = signature;
    if (Integer.valueOf(state().boundariesCrossed).equals(missingSynEqs_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.missingSynEqs(String).");
    }
    missingSynEqs_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    try {
        if (lookupSynEq(signature) != null) {
          // If there is an equation defined for this class we are done.
          return Collections.emptyList();
        } else if (!hasAbstract()) {
          // If the class is not abstract then it is missing an equation.
          return Collections.singletonList(this);
        } else {
          // The class was abstract so we must check that all subclasses define the equation.
          Collection<TypeDecl> missing = new LinkedList<TypeDecl>();
          for (ASTDecl subclass: subclasses()) {
            missing.addAll(subclass.missingSynEqs(signature));
          }
          return missing;
        }
      }
    finally {
      missingSynEqs_String_visited.remove(_parameters);
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map testCircular_String_visited = new java.util.HashMap(4);
  /**
   * @attribute syn
   * @aspect Superclass
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:40
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Superclass", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:40")
  @SideEffect.Pure(group="_ASTNode") public boolean testCircular(String name) {
    Object _parameters = name;
    if (Integer.valueOf(state().boundariesCrossed).equals(testCircular_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.testCircular(String).");
    }
    testCircular_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    try {
        if (!hasSuperClass()) {
          return false;
        }
        if (getSuperClassName().equals(name)) {
          return true;
        }
        ASTDecl superClass = (ASTDecl) grammar().lookup(getSuperClassName());
        return superClass != null ? superClass.testCircular(name) : false;
      }
    finally {
      testCircular_String_visited.remove(_parameters);
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isCircular_visited = -1;
  /**
   * @attribute syn
   * @aspect Superclass
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:51
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Superclass", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:51")
  @SideEffect.Pure(group="_ASTNode") public boolean isCircular() {
    if (isCircular_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.isCircular().");
    }
    isCircular_visited = state().boundariesCrossed;
    boolean isCircular_value = testCircular(name());
    isCircular_visited = -1;
    return isCircular_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int superClass_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void superClass_reset() {
    superClass_computed = false;
    
    superClass_value = null;
    superClass_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean superClass_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTDecl superClass_value;

  /**
   * @attribute syn
   * @aspect Superclass
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:53
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Superclass", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:53")
  @SideEffect.Pure(group="_ASTNode") public ASTDecl superClass() {
    ASTState state = state();
    if (superClass_computed) {
      return superClass_value;
    }
    if (superClass_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.superClass().");
    }
    superClass_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    superClass_value = hasSuperClass() && !isCircular()
          ? (ASTDecl) grammar().lookup(getSuperClassName())
          : null;
    if (isFinal && _boundaries == state().boundariesCrossed) {
    superClass_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    superClass_visited = -1;
    return superClass_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int getSuperClassName_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void getSuperClassName_reset() {
    getSuperClassName_computed = false;
    
    getSuperClassName_value = null;
    getSuperClassName_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean getSuperClassName_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected String getSuperClassName_value;

  /**
   * @attribute syn
   * @aspect Superclass
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:58
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Superclass", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:58")
  @SideEffect.Pure(group="_ASTNode") public String getSuperClassName() {
    ASTState state = state();
    if (getSuperClassName_computed) {
      return getSuperClassName_value;
    }
    if (getSuperClassName_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.getSuperClassName().");
    }
    getSuperClassName_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    getSuperClassName_value = hasSuperClass() ?  getSuperClass().name() : null;
    if (isFinal && _boundaries == state().boundariesCrossed) {
    getSuperClassName_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    getSuperClassName_visited = -1;
    return getSuperClassName_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int supertypes_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void supertypes_reset() {
    supertypes_computed = false;
    
    supertypes_value = null;
    supertypes_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean supertypes_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Collection<ASTDecl> supertypes_value;

  /**
   * @return all supertypes of this AST type
   * @attribute syn
   * @aspect Superclass
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:64
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Superclass", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:64")
  @SideEffect.Pure(group="_ASTNode") public Collection<ASTDecl> supertypes() {
    ASTState state = state();
    if (supertypes_computed) {
      return supertypes_value;
    }
    if (supertypes_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.supertypes().");
    }
    supertypes_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    supertypes_value = supertypes_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    supertypes_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    supertypes_visited = -1;
    return supertypes_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Collection<ASTDecl> supertypes_compute() {
      if (superClass() != null) {
        Collection<ASTDecl> types = new LinkedList<ASTDecl>();
        types.addAll(superClass().supertypes());
        types.add(superClass());
        return types;
      } else {
        return Collections.emptyList();
      }
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map instanceOf_TypeDecl_visited = new java.util.HashMap(4);
  /**
   * @attribute syn
   * @aspect InstanceOf
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:77
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="InstanceOf", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:77")
  @SideEffect.Pure(group="_ASTNode") public boolean instanceOf(TypeDecl c) {
    Object _parameters = c;
    if (Integer.valueOf(state().boundariesCrossed).equals(instanceOf_TypeDecl_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.instanceOf(TypeDecl).");
    }
    instanceOf_TypeDecl_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    try {
        if (c == this) {
          return true;
        }
        TypeDecl superClass = superClass();
        return superClass != null ? superClass.instanceOf(c) : false;
      }
    finally {
      instanceOf_TypeDecl_visited.remove(_parameters);
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int subclasses_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void subclasses_reset() {
    subclasses_computed = false;
    
    subclasses_value = null;
    subclasses_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean subclasses_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Collection<ASTDecl> subclasses_value;

  /**
   * @attribute syn
   * @aspect Subclasses
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:110
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Subclasses", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:110")
  @SideEffect.Pure(group="_ASTNode") public Collection<ASTDecl> subclasses() {
    ASTState state = state();
    if (subclasses_computed) {
      return subclasses_value;
    }
    if (subclasses_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.subclasses().");
    }
    subclasses_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    subclasses_value = findSubclasses(this);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    subclasses_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    subclasses_visited = -1;
    return subclasses_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int subclassesTransitive_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void subclassesTransitive_reset() {
    subclassesTransitive_computed = false;
    
    subclassesTransitive_value = null;
    subclassesTransitive_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean subclassesTransitive_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Set<ASTDecl> subclassesTransitive_value;

  /**
   * @attribute syn
   * @aspect Subclasses
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:116
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Subclasses", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:116")
  @SideEffect.Pure(group="_ASTNode") public Set<ASTDecl> subclassesTransitive() {
    ASTState state = state();
    if (subclassesTransitive_computed) {
      return subclassesTransitive_value;
    }
    if (subclassesTransitive_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.subclassesTransitive().");
    }
    subclassesTransitive_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    subclassesTransitive_value = subclassesTransitive_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    subclassesTransitive_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    subclassesTransitive_visited = -1;
    return subclassesTransitive_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Set<ASTDecl> subclassesTransitive_compute() {
      Set<ASTDecl> set = new LinkedHashSet<ASTDecl>(subclasses());
      for (Iterator it = subclasses().iterator(); it.hasNext(); ) {
        ASTDecl decl = (ASTDecl) it.next();
        set.addAll(decl.subclassesTransitive());
      }
      return set;
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int parents_visited = -1;
  /**
   * @attribute syn
   * @aspect Parents
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:156
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Parents", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:156")
  @SideEffect.Pure(group="_ASTNode") public Collection<ASTDecl> parents() {
    if (parents_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.parents().");
    }
    parents_visited = state().boundariesCrossed;
    try {
        HashMap<TypeDecl, Collection<ASTDecl>> map = grammar().parentMap();
        Set<ASTDecl> parents = new LinkedHashSet<ASTDecl>();
        ASTDecl node = this;
        while (node != null) {
          parents.addAll(map.get(node));
          node = node.superClass();
        }
        return parents;
      }
    finally {
      parents_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map hasCollEq_CollDecl_visited = new java.util.HashMap(4);
  /**
   * @attribute syn
   * @aspect CollectionAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:151
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CollectionAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:151")
  @SideEffect.Pure(group="_ASTNode") public boolean hasCollEq(CollDecl decl) {
    Object _parameters = decl;
    if (Integer.valueOf(state().boundariesCrossed).equals(hasCollEq_CollDecl_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.hasCollEq(CollDecl).");
    }
    hasCollEq_CollDecl_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    try {
        for (int i = 0; i < getNumCollEq(); i++) {
          if (getCollEq(i).decl() == decl) {
            return true;
          }
        }
        return false;
      }
    finally {
      hasCollEq_CollDecl_visited.remove(_parameters);
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int typeDeclKind_visited = -1;
  /**
   * @attribute syn
   * @aspect Comments
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Comments.jrag:163
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Comments", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Comments.jrag:163")
  @SideEffect.Pure(group="_ASTNode") public String typeDeclKind() {
    if (typeDeclKind_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.typeDeclKind().");
    }
    typeDeclKind_visited = state().boundariesCrossed;
    String typeDeclKind_value = "node";
    typeDeclKind_visited = -1;
    return typeDeclKind_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int components_visited = -1;
  /**
   * @attribute syn
   * @aspect ASTDecl
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:32
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ASTDecl", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:32")
  @SideEffect.Pure(group="_ASTNode") public Collection<Component> components() {
    if (components_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.components().");
    }
    components_visited = state().boundariesCrossed;
    try {
        LinkedList<Component> list = new LinkedList<Component>();
        if (superClass() != null) {
          list.addAll(superClass().components());
        }
        for (int i = 0; i < getNumComponent(); i++) {
          boolean done = false;
          for (ListIterator<Component> iter = list.listIterator(); !done && iter.hasNext(); ) {
            Component c = iter.next();
            if (c.name().equals(getComponent(i).name()) && c.type().equals(getComponent(i).type())) {
              iter.remove();
              done = true;
            }
          }
          if (getComponent(i).isNTA()) {
            list.add(getComponent(i));
          } else {
            int j = 0;
            while (j < list.size() && !((Component) list.get(j)).isNTA()) {
              j++;
            }
            list.add(j, getComponent(i));
          }
        }
        return list;
      }
    finally {
      components_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map redefinesTokenComponent_TokenComponent_visited = new java.util.HashMap(4);
  /**
   * @attribute syn
   * @aspect ASTDecl
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:59
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ASTDecl", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:59")
  @SideEffect.Pure(group="_ASTNode") public boolean redefinesTokenComponent(TokenComponent c) {
    Object _parameters = c;
    if (Integer.valueOf(state().boundariesCrossed).equals(redefinesTokenComponent_TokenComponent_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.redefinesTokenComponent(TokenComponent).");
    }
    redefinesTokenComponent_TokenComponent_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    try {
        if (c.hostClass() != this) {
          // Inherited component.
          return false;
        }
        if (superClass() == null) {
          // No definition in superclass.
          return true;
        }
        for (Component d : superClass().components()) {
          if (d.name().equals(c.name()) && d instanceof TokenComponent && c.isNTA() == d.isNTA()) {
            return false;
          }
        }
        return true; // No equal definition in superclass.
      }
    finally {
      redefinesTokenComponent_TokenComponent_visited.remove(_parameters);
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isRegionRoot_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void isRegionRoot_reset() {
    isRegionRoot_computed = false;
    isRegionRoot_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean isRegionRoot_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean isRegionRoot_value;

  /**
   * @attribute syn
   * @aspect CoarseIncremental
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:135
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CoarseIncremental", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:135")
  @SideEffect.Pure(group="_ASTNode") public boolean isRegionRoot() {
    ASTState state = state();
    if (isRegionRoot_computed) {
      return isRegionRoot_value;
    }
    if (isRegionRoot_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.isRegionRoot().");
    }
    isRegionRoot_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    isRegionRoot_value = isRootNode() || lookupRegionDecl(name()) != null;
    if (isFinal && _boundaries == state().boundariesCrossed) {
    isRegionRoot_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    isRegionRoot_visited = -1;
    return isRegionRoot_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int hasRegionRootAsSuperClass_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void hasRegionRootAsSuperClass_reset() {
    hasRegionRootAsSuperClass_computed = false;
    hasRegionRootAsSuperClass_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean hasRegionRootAsSuperClass_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean hasRegionRootAsSuperClass_value;

  /**
   * @attribute syn
   * @aspect CoarseIncremental
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:148
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CoarseIncremental", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:148")
  @SideEffect.Pure(group="_ASTNode") public boolean hasRegionRootAsSuperClass() {
    ASTState state = state();
    if (hasRegionRootAsSuperClass_computed) {
      return hasRegionRootAsSuperClass_value;
    }
    if (hasRegionRootAsSuperClass_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.hasRegionRootAsSuperClass().");
    }
    hasRegionRootAsSuperClass_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    hasRegionRootAsSuperClass_value = hasRegionRootAsSuperClass_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    hasRegionRootAsSuperClass_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    hasRegionRootAsSuperClass_visited = -1;
    return hasRegionRootAsSuperClass_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean hasRegionRootAsSuperClass_compute() {
      ASTDecl superDecl = superClass();
      if (superDecl != null) {
        if (superDecl.isRegionRoot()) {
          return true;
        }
        return superDecl.hasRegionRootAsSuperClass();
      }
      return false;
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isRegionLeaf_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void isRegionLeaf_reset() {
    isRegionLeaf_computed = false;
    isRegionLeaf_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean isRegionLeaf_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean isRegionLeaf_value;

  /**
   * @attribute syn
   * @aspect CoarseIncremental
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:159
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CoarseIncremental", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:159")
  @SideEffect.Pure(group="_ASTNode") public boolean isRegionLeaf() {
    ASTState state = state();
    if (isRegionLeaf_computed) {
      return isRegionLeaf_value;
    }
    if (isRegionLeaf_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.isRegionLeaf().");
    }
    isRegionLeaf_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    isRegionLeaf_value = isRegionLeaf_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    isRegionLeaf_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    isRegionLeaf_visited = -1;
    return isRegionLeaf_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean isRegionLeaf_compute() {
      if (name().equals(config().listType()) || name().equals(config().optType())) {
        TypeDecl type = grammar().lookup(config().astNodeType());
        if (type != null && type instanceof ASTDecl && ((ASTDecl) type).isRegionRoot()) {
            return false;
        }
        return true;
      }
      for (int i = 0; i < getNumComponent(); i++) {
        Component comp = getComponent(i);
        TypeDecl type = grammar().lookup(comp.type());
        if (type != null && type instanceof ASTDecl && ((ASTDecl) type).isRegionRoot()) {
          return true;
        }
      }
      return false;
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map lookupInhDecl_String_visited = new java.util.HashMap(4);
  /** @apilevel internal */
  @SideEffect.Ignore private void lookupInhDecl_String_reset() {
    lookupInhDecl_String_values = new java.util.HashMap(4);
    lookupInhDecl_String_visited = new java.util.HashMap(4);
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map lookupInhDecl_String_values = new java.util.HashMap(4);

  /**
   * @attribute syn
   * @aspect InheritedAttributes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:86
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="InheritedAttributes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:86")
  @SideEffect.Pure(group="_ASTNode") public InhDecl lookupInhDecl(String signature) {
    Object _parameters = signature;
    ASTState state = state();
    if (lookupInhDecl_String_values.containsKey(_parameters)) {
      return (InhDecl) lookupInhDecl_String_values.get(_parameters);
    }
    if (Integer.valueOf(state().boundariesCrossed).equals(lookupInhDecl_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.lookupInhDecl(String).");
    }
    lookupInhDecl_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    InhDecl lookupInhDecl_String_value = lookupInhDecl_compute(signature);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    lookupInhDecl_String_values.put(_parameters, lookupInhDecl_String_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    lookupInhDecl_String_visited.remove(_parameters);
    return lookupInhDecl_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private InhDecl lookupInhDecl_compute(String signature) {
      InhDecl decl = super.lookupInhDecl(signature);
      if (decl != null || superClass() == null) {
        return decl;
      }
      return superClass().lookupInhDecl(signature);
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int defaultInhEqs_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void defaultInhEqs_reset() {
    defaultInhEqs_computed = false;
    
    defaultInhEqs_value = null;
    defaultInhEqs_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean defaultInhEqs_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Collection<String> defaultInhEqs_value;

  /**
   * @attribute syn
   * @aspect FindInheritedEquations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:135
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="FindInheritedEquations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:135")
  @SideEffect.Pure(group="_ASTNode") public Collection<String> defaultInhEqs() {
    ASTState state = state();
    if (defaultInhEqs_computed) {
      return defaultInhEqs_value;
    }
    if (defaultInhEqs_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.defaultInhEqs().");
    }
    defaultInhEqs_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    defaultInhEqs_value = defaultInhEqs_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    defaultInhEqs_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    defaultInhEqs_visited = -1;
    return defaultInhEqs_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Collection<String> defaultInhEqs_compute() {
      Collection<String> defaultEqs = new HashSet<String>();
      for (InhEq equ: getInhEqList()) {
        String childName = equ.childName();
        String signature = equ.signature();
        if (childName.equals("Child")) {
          defaultEqs.add(signature);
        }
      }
      if (superClass() != null) {
        defaultEqs.addAll(superClass().defaultInhEqs());
      }
      return defaultEqs;
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int componentInhEqs_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void componentInhEqs_reset() {
    componentInhEqs_computed = false;
    
    componentInhEqs_value = null;
    componentInhEqs_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean componentInhEqs_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Map<String,Collection<String>> componentInhEqs_value;

  /**
   * @attribute syn
   * @aspect FindInheritedEquations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:150
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="FindInheritedEquations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:150")
  @SideEffect.Pure(group="_ASTNode") public Map<String,Collection<String>> componentInhEqs() {
    ASTState state = state();
    if (componentInhEqs_computed) {
      return componentInhEqs_value;
    }
    if (componentInhEqs_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.componentInhEqs().");
    }
    componentInhEqs_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    componentInhEqs_value = componentInhEqs_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    componentInhEqs_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    componentInhEqs_visited = -1;
    return componentInhEqs_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Map<String,Collection<String>> componentInhEqs_compute() {
      Map<String,Collection<String>> componentEqs = new HashMap<String,Collection<String>>();
      for (InhEq equ: getInhEqList()) {
        String childName = equ.childName();
        String signature = equ.signature();
        if (!childName.equals("Child")) {
          Collection<String> eqs = componentEqs.get(childName);
          if (eqs == null) {
            eqs = new HashSet<String>();
            componentEqs.put(childName, eqs);
          }
          eqs.add(signature);
        }
      }
      if (superClass() != null) {
        componentEqs.putAll(superClass().componentInhEqs());
      }
      return componentEqs;
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map hasInhEqForDecl_String_String_visited = new java.util.HashMap(4);
  /**
   * Lookup matching equation for an inherited attribute declaration.
   * @param childName name of child component containing declaration
   * @param signature signature of the declaration
   * @return {@code true} if there is an equation matching the declaration
   * @attribute syn
   * @aspect FindInheritedEquations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:254
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="FindInheritedEquations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:254")
  @SideEffect.Pure(group="_ASTNode") public boolean hasInhEqForDecl(String childName, String signature) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(childName);
    _parameters.add(signature);
    if (Integer.valueOf(state().boundariesCrossed).equals(hasInhEqForDecl_String_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.hasInhEqForDecl(String,String).");
    }
    hasInhEqForDecl_String_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    try {
        if (defaultInhEqs().contains(signature)) {
          return true;
        }
        Collection<String> componentEqs = componentInhEqs().get(childName);
        return componentEqs != null && componentEqs.contains(signature);
      }
    finally {
      hasInhEqForDecl_String_String_visited.remove(_parameters);
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int numNonNTAComponent_visited = -1;
  /**
   * @attribute syn
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:72
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JaddCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:72")
  @SideEffect.Pure(group="_ASTNode") public int numNonNTAComponent() {
    if (numNonNTAComponent_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.numNonNTAComponent().");
    }
    numNonNTAComponent_visited = state().boundariesCrossed;
    try {
        int num = 0;
        for (Component c : components()) {
          if (!c.isNTA()) {
            num++;
          }
        }
        return num;
      }
    finally {
      numNonNTAComponent_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int childCount_visited = -1;
  /**
   * Number of children, including NTAs but excluding tokens
   * @attribute syn
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:85
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JaddCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:85")
  @SideEffect.Pure(group="_ASTNode") public int childCount() {
    if (childCount_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.childCount().");
    }
    childCount_visited = state().boundariesCrossed;
    try {
        int i = 0;
        for (Component c : components()) {
          if (!(c instanceof TokenComponent)) {
            i++;
          }
        }
        return i;
      }
    finally {
      childCount_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int numRegularChildren_visited = -1;
  /**
   * Number of children, excluding NTAs and tokens
   * @attribute syn
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:98
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JaddCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:98")
  @SideEffect.Pure(group="_ASTNode") public int numRegularChildren() {
    if (numRegularChildren_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.numRegularChildren().");
    }
    numRegularChildren_visited = state().boundariesCrossed;
    try {
        int i = 0;
        for (Component c : components()) {
          if (!c.isNTA() && !(c instanceof TokenComponent)) {
            i++;
          }
        }
        return i;
      }
    finally {
      numRegularChildren_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int initialChildArraySize_visited = -1;
  /**
   * String for calculating the initial child array size.
   * List nodes use the minimum list size as the initial child
   * array size.
   * @attribute syn
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:268
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JaddCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:268")
  @SideEffect.Pure(group="_ASTNode") public String initialChildArraySize() {
    if (initialChildArraySize_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.initialChildArraySize().");
    }
    initialChildArraySize_visited = state().boundariesCrossed;
    try {
        return String.format("(i + 1 > %d || !(this instanceof %s)) ? i + 1 : %d",
            config().minListSize(), config().listType(), config().minListSize());
      }
    finally {
      initialChildArraySize_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int debugASTNodeState_visited = -1;
  /**
   * Checks that ASTNode.state can be evaluated.
   * @attribute syn
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:276
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JaddCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:276")
  @SideEffect.Pure(group="_ASTNode") public String debugASTNodeState() {
    if (debugASTNodeState_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.debugASTNodeState().");
    }
    debugASTNodeState_visited = state().boundariesCrossed;
    try {
        if (config().debugMode() && !grammar().roots().isEmpty()) {
          // Check if a new state object is created for a node that is not a root node.
          StringBuilder buf = new StringBuilder();
          buf.append("if (");
          boolean first = true;
          for (Iterator iter = grammar().roots().iterator(); iter.hasNext(); ) {
            ASTDecl root = (ASTDecl)iter.next();
            if (!first) {
              buf.append(" && ");
            }
            first = false;
            buf.append("!(this instanceof " + root.name() + ")");
          }
          buf.append(")\n");
          buf.append(config().indent + "throw new RuntimeException(\"Trying to evaluate state in "
              + "a node which is not attached to the main tree\");");
          return buf.toString();
        } else {
          return "";
        }
      }
    finally {
      debugASTNodeState_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int needsListTouched_visited = -1;
  /**
   * @return <code>true</code> if the list$touched field is needed for this
   * ASTDecl.
   * @attribute syn
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:328
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JaddCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:328")
  @SideEffect.Pure(group="_ASTNode") public boolean needsListTouched() {
    if (needsListTouched_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.needsListTouched().");
    }
    needsListTouched_visited = state().boundariesCrossed;
    boolean needsListTouched_value = config().legacyRewrite();
    needsListTouched_visited = -1;
    return needsListTouched_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isASTNodeDecl_visited = -1;
  /**
   * @attribute syn
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:354
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JaddCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:354")
  @SideEffect.Pure(group="_ASTNode") public boolean isASTNodeDecl() {
    if (isASTNodeDecl_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.isASTNodeDecl().");
    }
    isASTNodeDecl_visited = state().boundariesCrossed;
    boolean isASTNodeDecl_value = name().equals(config().astNodeType());
    isASTNodeDecl_visited = -1;
    return isASTNodeDecl_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isOptDecl_visited = -1;
  /**
   * @attribute syn
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:356
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JaddCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:356")
  @SideEffect.Pure(group="_ASTNode") public boolean isOptDecl() {
    if (isOptDecl_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.isOptDecl().");
    }
    isOptDecl_visited = state().boundariesCrossed;
    boolean isOptDecl_value = name().equals(config().optType());
    isOptDecl_visited = -1;
    return isOptDecl_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isListDecl_visited = -1;
  /**
   * @attribute syn
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:358
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JaddCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:358")
  @SideEffect.Pure(group="_ASTNode") public boolean isListDecl() {
    if (isListDecl_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.isListDecl().");
    }
    isListDecl_visited = state().boundariesCrossed;
    boolean isListDecl_value = name().equals(config().listType());
    isListDecl_visited = -1;
    return isListDecl_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isOptSubtype_visited = -1;
  /**
   * @attribute syn
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:360
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JaddCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:360")
  @SideEffect.Pure(group="_ASTNode") public boolean isOptSubtype() {
    if (isOptSubtype_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.isOptSubtype().");
    }
    isOptSubtype_visited = state().boundariesCrossed;
    boolean isOptSubtype_value = isOptDecl() || (superClass() != null && superClass().isOptSubtype());
    isOptSubtype_visited = -1;
    return isOptSubtype_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isListSubtype_visited = -1;
  /**
   * @attribute syn
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:363
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JaddCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:363")
  @SideEffect.Pure(group="_ASTNode") public boolean isListSubtype() {
    if (isListSubtype_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.isListSubtype().");
    }
    isListSubtype_visited = state().boundariesCrossed;
    boolean isListSubtype_value = isListDecl() || (superClass() != null && superClass().isListSubtype());
    isListSubtype_visited = -1;
    return isListSubtype_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int checkRegularNodeStructure_visited = -1;
  /**
   * Check the node structure of a regular (non-Opt, non-List)
   * JJTree node.
   * @attribute syn
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:370
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JaddCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:370")
  @SideEffect.Pure(group="_ASTNode") public String checkRegularNodeStructure() {
    if (checkRegularNodeStructure_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.checkRegularNodeStructure().");
    }
    checkRegularNodeStructure_visited = state().boundariesCrossed;
    try {
        StringBuilder buf = new StringBuilder();
        int j = 0;
        for (Component c : components()) {
          buf.append(c.checkComponentStructure(j));
          if (!(c instanceof TokenComponent)) {
            j++;
          }
        }
        return buf.toString();
      }
    finally {
      checkRegularNodeStructure_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int jjtGenPrintChildren_visited = -1;
  /**
   * @attribute syn
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:408
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JaddCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:408")
  @SideEffect.Pure(group="_ASTNode") public String jjtGenPrintChildren() {
    if (jjtGenPrintChildren_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.jjtGenPrintChildren().");
    }
    jjtGenPrintChildren_visited = state().boundariesCrossed;
    try {
        StringBuilder buf = new StringBuilder();
        for (Component c : components()) {
          if (c instanceof TokenComponent) {
            TokenComponent t = (TokenComponent)c;
            String id = t.getTokenId().getID();
            buf.append("out.print(\"\\\"\" + get" + id + "() + \"\\\"\");\n");
          }
        }
        return buf.toString();
      }
    finally {
      jjtGenPrintChildren_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map hasInhEq_String_visited = new java.util.HashMap(4);
  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:668
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:668")
  @SideEffect.Pure(group="_ASTNode") public boolean hasInhEq(String attrName) {
    Object _parameters = attrName;
    if (Integer.valueOf(state().boundariesCrossed).equals(hasInhEq_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.hasInhEq(String).");
    }
    hasInhEq_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean hasInhEq_String_value = super.hasInhEq(attrName) || superClass() != null && superClass().hasInhEq(attrName);
    hasInhEq_String_visited.remove(_parameters);
    return hasInhEq_String_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map lookupSynDeclPrefix_String_visited = new java.util.HashMap(4);
  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:703
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:703")
  @SideEffect.Pure(group="_ASTNode") public SynDecl lookupSynDeclPrefix(String signature) {
    Object _parameters = signature;
    if (Integer.valueOf(state().boundariesCrossed).equals(lookupSynDeclPrefix_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.lookupSynDeclPrefix(String).");
    }
    lookupSynDeclPrefix_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    try {
        SynDecl decl = super.lookupSynDeclPrefix(signature);
        if (decl != null || superClass() == null) {
          return decl;
        }
        return superClass().lookupSynDeclPrefix(signature);
      }
    finally {
      lookupSynDeclPrefix_String_visited.remove(_parameters);
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map lookupInhDeclPrefix_String_visited = new java.util.HashMap(4);
  /**
   * @attribute syn
   * @aspect JragCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:731
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JragCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JragCodeGen.jrag:731")
  @SideEffect.Pure(group="_ASTNode") public InhDecl lookupInhDeclPrefix(String signature) {
    Object _parameters = signature;
    if (Integer.valueOf(state().boundariesCrossed).equals(lookupInhDeclPrefix_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.lookupInhDeclPrefix(String).");
    }
    lookupInhDeclPrefix_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    try {
        InhDecl decl = super.lookupInhDeclPrefix(signature);
        if (decl != null || superClass() == null) {
          return decl;
        }
        return superClass().lookupInhDeclPrefix(signature);
      }
    finally {
      lookupInhDeclPrefix_String_visited.remove(_parameters);
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int synEquations_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void synEquations_reset() {
    synEquations_computed = false;
    
    synEquations_value = null;
    synEquations_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean synEquations_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Set synEquations_value;

  /**
   * @attribute syn
   * @aspect AllEquations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:105
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AllEquations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:105")
  @SideEffect.Pure(group="_ASTNode") public Set synEquations() {
    ASTState state = state();
    if (synEquations_computed) {
      return synEquations_value;
    }
    if (synEquations_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.synEquations().");
    }
    synEquations_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    synEquations_value = synEquations_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    synEquations_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    synEquations_visited = -1;
    return synEquations_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Set synEquations_compute() {
      Set set = super.synEquations();
      if (superClass() != null)
        set.addAll(superClass().synEquations());
      return set;
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int synDeclarations_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void synDeclarations_reset() {
    synDeclarations_computed = false;
    
    synDeclarations_value = null;
    synDeclarations_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean synDeclarations_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Set synDeclarations_value;

  /**
   * @attribute syn
   * @aspect AllEquations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:120
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AllEquations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:120")
  @SideEffect.Pure(group="_ASTNode") public Set synDeclarations() {
    ASTState state = state();
    if (synDeclarations_computed) {
      return synDeclarations_value;
    }
    if (synDeclarations_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.synDeclarations().");
    }
    synDeclarations_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    synDeclarations_value = synDeclarations_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    synDeclarations_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    synDeclarations_visited = -1;
    return synDeclarations_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Set synDeclarations_compute() {
      Set set = super.synDeclarations();
      if (superClass() != null)
        set.addAll(superClass().synDeclarations());
      return set;
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int inhEquations_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void inhEquations_reset() {
    inhEquations_computed = false;
    
    inhEquations_value = null;
    inhEquations_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean inhEquations_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Set inhEquations_value;

  /**
   * @attribute syn
   * @aspect AllEquations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:135
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AllEquations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:135")
  @SideEffect.Pure(group="_ASTNode") public Set inhEquations() {
    ASTState state = state();
    if (inhEquations_computed) {
      return inhEquations_value;
    }
    if (inhEquations_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.inhEquations().");
    }
    inhEquations_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    inhEquations_value = inhEquations_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    inhEquations_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    inhEquations_visited = -1;
    return inhEquations_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Set inhEquations_compute() {
      Set set = super.inhEquations();
      if (superClass() != null)
        set.addAll(superClass().inhEquations());
      return set;
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int inhDeclarations_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void inhDeclarations_reset() {
    inhDeclarations_computed = false;
    
    inhDeclarations_value = null;
    inhDeclarations_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean inhDeclarations_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Set inhDeclarations_value;

  /**
   * @attribute syn
   * @aspect AllEquations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:150
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AllEquations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:150")
  @SideEffect.Pure(group="_ASTNode") public Set inhDeclarations() {
    ASTState state = state();
    if (inhDeclarations_computed) {
      return inhDeclarations_value;
    }
    if (inhDeclarations_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.inhDeclarations().");
    }
    inhDeclarations_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    inhDeclarations_value = inhDeclarations_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    inhDeclarations_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    inhDeclarations_visited = -1;
    return inhDeclarations_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Set inhDeclarations_compute() {
      Set set = super.inhDeclarations();
      if (superClass() != null)
        set.addAll(superClass().inhDeclarations());
      return set;
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map lookupSynDecl_String_visited = new java.util.HashMap(4);
  /** @apilevel internal */
  @SideEffect.Ignore private void lookupSynDecl_String_reset() {
    lookupSynDecl_String_values = new java.util.HashMap(4);
    lookupSynDecl_String_visited = new java.util.HashMap(4);
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map lookupSynDecl_String_values = new java.util.HashMap(4);

  /**
   * @attribute syn
   * @aspect BindSynEquations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:167
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="BindSynEquations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:167")
  @SideEffect.Pure(group="_ASTNode") public SynDecl lookupSynDecl(String signature) {
    Object _parameters = signature;
    ASTState state = state();
    if (lookupSynDecl_String_values.containsKey(_parameters)) {
      return (SynDecl) lookupSynDecl_String_values.get(_parameters);
    }
    if (Integer.valueOf(state().boundariesCrossed).equals(lookupSynDecl_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.lookupSynDecl(String).");
    }
    lookupSynDecl_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    SynDecl lookupSynDecl_String_value = lookupSynDecl_compute(signature);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    lookupSynDecl_String_values.put(_parameters, lookupSynDecl_String_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    lookupSynDecl_String_visited.remove(_parameters);
    return lookupSynDecl_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private SynDecl lookupSynDecl_compute(String signature) {
      SynDecl decl = super.lookupSynDecl(signature);
      if (decl != null || superClass() == null)
        return decl;
      return superClass().lookupSynDecl(signature);
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map lookupSynEq_String_visited = new java.util.HashMap(4);
  /** @apilevel internal */
  @SideEffect.Ignore private void lookupSynEq_String_reset() {
    lookupSynEq_String_values = new java.util.HashMap(4);
    lookupSynEq_String_visited = new java.util.HashMap(4);
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map lookupSynEq_String_values = new java.util.HashMap(4);

  /**
   * @attribute syn
   * @aspect BindSynEquations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:181
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="BindSynEquations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:181")
  @SideEffect.Pure(group="_ASTNode") public SynEq lookupSynEq(String signature) {
    Object _parameters = signature;
    ASTState state = state();
    if (lookupSynEq_String_values.containsKey(_parameters)) {
      return (SynEq) lookupSynEq_String_values.get(_parameters);
    }
    if (Integer.valueOf(state().boundariesCrossed).equals(lookupSynEq_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.lookupSynEq(String).");
    }
    lookupSynEq_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    SynEq lookupSynEq_String_value = lookupSynEq_compute(signature);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    lookupSynEq_String_values.put(_parameters, lookupSynEq_String_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    lookupSynEq_String_visited.remove(_parameters);
    return lookupSynEq_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private SynEq lookupSynEq_compute(String signature) {
      SynEq equations = super.lookupSynEq(signature);
      if (equations != null || superClass() == null) {
        return equations;
      }
      return superClass().lookupSynEq(signature);
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map lookupInhEq_String_String_visited = new java.util.HashMap(4);
  /** @apilevel internal */
  @SideEffect.Ignore private void lookupInhEq_String_String_reset() {
    lookupInhEq_String_String_values = new java.util.HashMap(4);
    lookupInhEq_String_String_visited = new java.util.HashMap(4);
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map lookupInhEq_String_String_values = new java.util.HashMap(4);

  /**
   * @attribute syn
   * @aspect BindSynEquations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:201
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="BindSynEquations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\NameBinding.jrag:201")
  @SideEffect.Pure(group="_ASTNode") public InhEq lookupInhEq(String signature, String childName) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(signature);
    _parameters.add(childName);
    ASTState state = state();
    if (lookupInhEq_String_String_values.containsKey(_parameters)) {
      return (InhEq) lookupInhEq_String_String_values.get(_parameters);
    }
    if (Integer.valueOf(state().boundariesCrossed).equals(lookupInhEq_String_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute TypeDecl.lookupInhEq(String,String).");
    }
    lookupInhEq_String_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    InhEq lookupInhEq_String_String_value = lookupInhEq_compute(signature, childName);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    lookupInhEq_String_String_values.put(_parameters, lookupInhEq_String_String_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    lookupInhEq_String_String_visited.remove(_parameters);
    return lookupInhEq_String_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private InhEq lookupInhEq_compute(String signature, String childName) {
      InhEq equation = super.lookupInhEq(signature, childName);
      if (equation != null || superClass() == null) {
        return equation;
      }
      return superClass().lookupInhEq(signature, childName);
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int hasRewrites_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void hasRewrites_reset() {
    hasRewrites_computed = false;
    hasRewrites_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean hasRewrites_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean hasRewrites_value;

  /**
   * @attribute syn
   * @aspect Rewrites
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Rewrites.jrag:149
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Rewrites", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Rewrites.jrag:149")
  @SideEffect.Pure(group="_ASTNode") public boolean hasRewrites() {
    ASTState state = state();
    if (hasRewrites_computed) {
      return hasRewrites_value;
    }
    if (hasRewrites_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.hasRewrites().");
    }
    hasRewrites_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    hasRewrites_value = getNumRewrite() > 0 || (superClass() != null && superClass().hasRewrites());
    if (isFinal && _boundaries == state().boundariesCrossed) {
    hasRewrites_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    hasRewrites_visited = -1;
    return hasRewrites_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int hasRewriteAttribute_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void hasRewriteAttribute_reset() {
    hasRewriteAttribute_computed = false;
    hasRewriteAttribute_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean hasRewriteAttribute_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean hasRewriteAttribute_value;

  /**
   * @return {@code true} if this AST node has a rewrittenNode attribute declaration
   * for circular NTA rewrites.
   * @attribute syn
   * @aspect Rewrites
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Rewrites.jrag:156
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Rewrites", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Rewrites.jrag:156")
  @SideEffect.Pure(group="_ASTNode") public boolean hasRewriteAttribute() {
    ASTState state = state();
    if (hasRewriteAttribute_computed) {
      return hasRewriteAttribute_value;
    }
    if (hasRewriteAttribute_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.hasRewriteAttribute().");
    }
    hasRewriteAttribute_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    hasRewriteAttribute_value = config().rewriteCircularNTA()
          && getNumRewrite() > 0
          && (superClass() == null || !superClass().hasRewrites());
    if (isFinal && _boundaries == state().boundariesCrossed) {
    hasRewriteAttribute_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    hasRewriteAttribute_visited = -1;
    return hasRewriteAttribute_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int getCircularRewriteDecl_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void getCircularRewriteDecl_reset() {
    getCircularRewriteDecl_computed = false;
    
    getCircularRewriteDecl_value = null;
    getCircularRewriteDecl_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean getCircularRewriteDecl_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected CircularRewriteDecl getCircularRewriteDecl_value;

  /**
   * @attribute syn nta
   * @aspect Rewrites
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Rewrites.jrag:192
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="Rewrites", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Rewrites.jrag:192")
  @SideEffect.Pure(group="_ASTNode") public CircularRewriteDecl getCircularRewriteDecl() {
    ASTState state = state();
    if (getCircularRewriteDecl_computed) {
      return (CircularRewriteDecl) getChild(getCircularRewriteDeclChildPosition());
    }
    if (getCircularRewriteDecl_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.getCircularRewriteDecl().");
    }
    getCircularRewriteDecl_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    getCircularRewriteDecl_value = new CircularRewriteDecl(
              new List<Parameter>(),
              "rewrittenNode",
              "ASTNode",
              CacheMode.DEFAULT,
              "",
              0,
              0,
              false,
              true,
              "",
              "",
              new List<Annotation>());
    setChild(getCircularRewriteDecl_value, getCircularRewriteDeclChildPosition());
    if (isFinal && _boundaries == state().boundariesCrossed) {
    getCircularRewriteDecl_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    getCircularRewriteDecl_visited = -1;
    CircularRewriteDecl node = (CircularRewriteDecl) this.getChild(getCircularRewriteDeclChildPosition());
    return node;
  }
  /**
   * @attribute inh
   * @aspect CoarseIncremental
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:137
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="CoarseIncremental", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:137")
  @SideEffect.Pure(group="_ASTNode") public RegionDecl lookupRegionDecl(String name) {
    Object _parameters = name;
    if (Integer.valueOf(state().boundariesCrossed).equals(lookupRegionDecl_String_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute ASTDecl.lookupRegionDecl(String).");
    }
    lookupRegionDecl_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    RegionDecl lookupRegionDecl_String_value = getParent().Define_lookupRegionDecl(this, null, name);
    lookupRegionDecl_String_visited.remove(_parameters);
    return lookupRegionDecl_String_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map lookupRegionDecl_String_visited = new java.util.HashMap(4);
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:128
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_hostClass(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getCollEqListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:422
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return this;
    }
    else if (_callerNode == getCollDeclListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\CollectionAttributes.jrag:420
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return this;
    }
    else {
      return super.Define_hostClass(_callerNode, _childNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_hostClass(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    // Declared at C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:33
    if (!hasSuperClass() && !isASTNodeDecl()) {
      return rewriteRule0();
    }
    return super.rewriteTo();
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ClassRelations.jrag:33
   * @apilevel internal
   */
  private ASTDecl rewriteRule0() {
{
      setSuperClass(new IdUse(config().astNodeType()));
      return this;
    }  }
  @SideEffect.Local(group="_ASTNode") protected void collect_contributors_Grammar_problems(Grammar _root, @SideEffect.Local java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTErrors.jrag:70
    if (grammar().lookup(name()) != this) {
      {
        Grammar target = (Grammar) (grammar());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTErrors.jrag:79
    if (isCircular()) {
      {
        Grammar target = (Grammar) (grammar());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTErrors.jrag:88
    if (hasSuperClass() && superClass() == null) {
      {
        Grammar target = (Grammar) (grammar());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_Grammar_problems(_root, _map);
  }
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_Grammar_problems(Collection<Problem> collection) {
    super.contributeTo_Grammar_problems(collection);
    if (grammar().lookup(name()) != this) {
      collection.add(Problem.builder()
                .message("multiple production rule for non-terminal %s", name())
                .sourceFile(getFileName())
                .sourceLine(getStartLine())
                .buildError());
    }
    if (isCircular()) {
      collection.add(Problem.builder()
                .message("%s causes circular inheritance", name())
                .sourceFile(getFileName())
                .sourceLine(getStartLine())
                .buildError());
    }
    if (hasSuperClass() && superClass() == null) {
      collection.add(Problem.builder()
              .message("%s inherits from undeclared class %s", name(), getSuperClass().name())
              .sourceFile(getFileName())
              .sourceLine(getStartLine())
              .buildError());
    }
  }
}
