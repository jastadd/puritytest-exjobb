/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package org.jastadd.ast.AST;
import org.jastadd.ast.AST.*;
import java.util.*;
import org.jastadd.Problem;
import org.jastadd.jrag.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jastadd.jrag.AST.ASTExpression;
import org.jastadd.jrag.Unparser;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.io.PrintStream;
import java.io.*;
import java.util.regex.*;
import org.jastadd.JastAdd;
import org.jastadd.jrag.AST.ASTAspectMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectRefineMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectFieldDeclaration;
import org.jastadd.jrag.AST.ASTBlock;
import org.jastadd.jrag.ClassBodyObject;
import org.jastadd.jrag.AST.ASTCompilationUnit;
import org.jastadd.ast.AST.Token;
import org.jastadd.ast.AST.SimpleNode;
import org.jastadd.Configuration;
import org.jastadd.tinytemplate.*;
/** A component of an AST node type declaration. 
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\Ast.ast:64
 * @production Component : {@link ASTNode};

 */
public abstract class Component extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect ASTErrors
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTErrors.jrag:117
   */
  public boolean declared() {
    return grammar().lookup(type()) != null;
  }
  /**
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:507
   */
  public abstract void jaddGen(int index, boolean publicModifier, ASTDecl decl);
  /**
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:563
   */
  protected void parse(String s) {
    org.jastadd.jrag.AST.JragParser jp =
        new org.jastadd.jrag.AST.JragParser(new java.io.StringReader(s));
    jp.root = grammar();

    // The file name is not meaningful, as the component is completely generated
    // from a node type specification
    jp.setFileName("");

    try {
      jp.AspectBodyDeclarationsEOF();
    } catch (Exception e) {
      System.err.println("Aspect parsing error: " + e.getMessage());
      e.printStackTrace(System.err);
    }
  }
  /**
   * @aspect JastAddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JastAddCodeGen.jadd:196
   */
  public abstract String componentString();
  /**
   * @declaredat ASTNode:1
   */
  public Component(int i) {
    super(i);
  }
  /**
   * @declaredat ASTNode:5
   */
  public Component(Ast p, int i) {
    this(i);
    parser = p;
  }
  /**
   * @declaredat ASTNode:10
   */
  public Component() {
    this(0);
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:19
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /**
   * @declaredat ASTNode:21
   */
  public void dumpTree(String indent, java.io.PrintStream out) {
    out.print(indent + "Component");
    String childIndent = indent + "  ";
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).dumpTree(childIndent, out);
    }
  }
  /**
   * @declaredat ASTNode:28
   */
  public Object jjtAccept(AstVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
  /**
   * @declaredat ASTNode:31
   */
  public void jjtAddChild(Node n, int i) {
    checkChild(n, i);
    super.jjtAddChild(n, i);
  }
  /**
   * @declaredat ASTNode:35
   */
  public void checkChild(Node n, int i) {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:38
   */
  @SideEffect.Pure public int getNumChild() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:44
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:48
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    isTokenComponent_reset();
    typeDecl_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:54
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:58
   */
  @SideEffect.Fresh public Component clone() throws CloneNotSupportedException {
    Component node = (Component) super.clone();
    return node;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:69
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public abstract Component fullCopy();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:77
   */
  @SideEffect.Fresh(group="_ASTNode") public abstract Component treeCopyNoTransform();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:85
   */
  @SideEffect.Fresh(group="_ASTNode") public abstract Component treeCopy();
  /**
   * @attribute syn
   * @aspect Names
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:106
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Names", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:106")
  @SideEffect.Pure(group="_ASTNode") public abstract String name();
  /**
   * @attribute syn
   * @aspect Types
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:123
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Types", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:123")
  @SideEffect.Pure(group="_ASTNode") public abstract String type();
  /**
   * @attribute syn
   * @aspect ConstructorParameterTypes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:120
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstructorParameterTypes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:120")
  @SideEffect.Pure(group="_ASTNode") public abstract String constrParmType();
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isTokenComponent_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void isTokenComponent_reset() {
    isTokenComponent_computed = false;
    isTokenComponent_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean isTokenComponent_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean isTokenComponent_value;

  /**
   * @attribute syn
   * @aspect ASTEqual
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTEqual.jrag:51
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ASTEqual", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTEqual.jrag:51")
  @SideEffect.Pure(group="_ASTNode") public boolean isTokenComponent() {
    ASTState state = state();
    if (isTokenComponent_computed) {
      return isTokenComponent_value;
    }
    if (isTokenComponent_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Component.isTokenComponent().");
    }
    isTokenComponent_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    isTokenComponent_value = false;
    if (isFinal && _boundaries == state().boundariesCrossed) {
    isTokenComponent_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    isTokenComponent_visited = -1;
    return isTokenComponent_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int className_visited = -1;
  /**
   * @attribute syn
   * @aspect ComponentHostClass
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:135
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ComponentHostClass", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTNameBinding.jrag:135")
  @SideEffect.Pure(group="_ASTNode") public String className() {
    if (className_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Component.className().");
    }
    className_visited = state().boundariesCrossed;
    String className_value = hostClass().name();
    className_visited = -1;
    return className_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int ntaShadowingNonNTA_visited = -1;
  /**
   * @return {@code true} if this attribute is an NTA shadowing a non-NTA child
   * of a supertype
   * @attribute syn
   * @aspect Comp
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:90
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Comp", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:90")
  @SideEffect.Pure(group="_ASTNode") public boolean ntaShadowingNonNTA() {
    if (ntaShadowingNonNTA_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Component.ntaShadowingNonNTA().");
    }
    ntaShadowingNonNTA_visited = state().boundariesCrossed;
    try {
        if (!isNTA()) {
          return false;
        }
        String name = name();
        TypeDecl hostClass = hostClass();
        while (true) {
          hostClass = hostClass instanceof ASTDecl ? ((ASTDecl) hostClass).superClass() : null;
          if (hostClass == null) {
            return false;
          }
          for (Component comp : hostClass.components()) {
            if (!comp.isNTA() && name.equals(comp.name())) {
              return true;
            }
          }
        }
      }
    finally {
      ntaShadowingNonNTA_visited = -1;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int isNTA_visited = -1;
  /**
   * @attribute syn
   * @aspect NTADetector
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:112
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NTADetector", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:112")
  @SideEffect.Pure(group="_ASTNode") public boolean isNTA() {
    if (isNTA_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Component.isNTA().");
    }
    isNTA_visited = state().boundariesCrossed;
    boolean isNTA_value = false;
    isNTA_visited = -1;
    return isNTA_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int typeDecl_visited = -1;
  /** @apilevel internal */
  @SideEffect.Ignore private void typeDecl_reset() {
    typeDecl_computed = false;
    
    typeDecl_value = null;
    typeDecl_visited = -1;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean typeDecl_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected TypeDecl typeDecl_value;

  /**
   * @attribute syn
   * @aspect FindInheritedEquations
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:346
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="FindInheritedEquations", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\InheritedAttributes.jrag:346")
  @SideEffect.Pure(group="_ASTNode") public TypeDecl typeDecl() {
    ASTState state = state();
    if (typeDecl_computed) {
      return typeDecl_value;
    }
    if (typeDecl_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Component.typeDecl().");
    }
    typeDecl_visited = state().boundariesCrossed;
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    typeDecl_value = grammar().lookup(type());
    if (isFinal && _boundaries == state().boundariesCrossed) {
    typeDecl_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    typeDecl_visited = -1;
    return typeDecl_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map checkComponentStructure_int_visited = new java.util.HashMap(4);
  /**
   * Check the structure of a single JJTree component.
   * @attribute syn
   * @aspect JaddCodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:385
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="JaddCodeGen", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\JaddCodeGen.jrag:385")
  @SideEffect.Pure(group="_ASTNode") public String checkComponentStructure(int childIndex) {
    Object _parameters = childIndex;
    if (Integer.valueOf(state().boundariesCrossed).equals(checkComponentStructure_int_visited.get(_parameters))) {
      throw new RuntimeException("Circular definition of attribute Component.checkComponentStructure(int).");
    }
    checkComponentStructure_int_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    String checkComponentStructure_int_value = "";
    checkComponentStructure_int_visited.remove(_parameters);
    return checkComponentStructure_int_value;
  }
  /**
   * @attribute inh
   * @aspect ComponentHostClass
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:128
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="ComponentHostClass", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ComponentsUtil.jrag:128")
  @SideEffect.Pure(group="_ASTNode") public TypeDecl hostClass() {
    if (hostClass_visited == state().boundariesCrossed) {
      throw new RuntimeException("Circular definition of attribute Component.hostClass().");
    }
    hostClass_visited = state().boundariesCrossed;
    TypeDecl hostClass_value = getParent().Define_hostClass(this, null);
    hostClass_visited = -1;
    return hostClass_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected int hostClass_visited = -1;
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  @SideEffect.Local(group="_ASTNode") protected void collect_contributors_Grammar_problems(Grammar _root, @SideEffect.Local java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTErrors.jrag:97
    if (hostClass().lookupComponent(name()) != this) {
      {
        Grammar target = (Grammar) (grammar());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\jastadd2\\src\\jastadd\\ast\\ASTErrors.jrag:107
    if (!declared()) {
      {
        Grammar target = (Grammar) (grammar());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_Grammar_problems(_root, _map);
  }
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_Grammar_problems(Collection<Problem> collection) {
    super.contributeTo_Grammar_problems(collection);
    if (hostClass().lookupComponent(name()) != this) {
      collection.add(Problem.builder()
                .message("Component name %s is not unique in production rule for %s (%s)",
                    name(), hostClass().name(), hostClass().lookupComponent(name()))
                .sourceFile(hostClass().getFileName())
                .sourceLine(hostClass().getStartLine())
                .buildError());
    }
    if (!declared()) {
      collection.add(Problem.builder()
                .message("Type %s of component %s in production %s is not declared",
                    type(), name(), hostClass().name())
                .sourceFile(hostClass().getFileName())
                .sourceLine(hostClass().getStartLine())
                .buildError());
    }
  }
}
