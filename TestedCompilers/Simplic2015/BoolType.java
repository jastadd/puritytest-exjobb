/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.3 */
package lang.ast;
import java.util.*;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
/**
 * @ast node
 * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Simplic.ast:20
 * @astdecl BoolType : Type;
 * @production BoolType : {@link Type};

 */
public class BoolType extends Type implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public BoolType() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:17
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:21
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:25
   */
  @SideEffect.Ignore @SideEffect.Fresh public BoolType clone() throws CloneNotSupportedException {
    BoolType node = (BoolType) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:30
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public BoolType copy() {
    try {
      BoolType node = (BoolType) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:49
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public BoolType fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:59
   */
  @SideEffect.Fresh(group="_ASTNode") public BoolType treeCopyNoTransform() {
    BoolType tree = (BoolType) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:79
   */
  @SideEffect.Fresh(group="_ASTNode") public BoolType treeCopy() {
    BoolType tree = (BoolType) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:93
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean isBool_visited = false;
  /**
   * @attribute syn
   * @aspect TypeAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:7
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="TypeAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:7")
  @SideEffect.Pure(group="_ASTNode") public boolean isBool() {
    if (isBool_visited) {
      throw new RuntimeException("Circular definition of attribute BoolType.isBool().");
    }
    isBool_visited = true;
    boolean isBool_value = true;
    isBool_visited = false;
    return isBool_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean toString_visited = false;
  /**
   * @attribute syn
   * @aspect TypeAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:53
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="TypeAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:53")
  @SideEffect.Pure(group="_ASTNode") public String toString() {
    if (toString_visited) {
      throw new RuntimeException("Circular definition of attribute BoolType.toString().");
    }
    toString_visited = true;
    String toString_value = "boolean type";
    toString_visited = false;
    return toString_value;
  }
}
