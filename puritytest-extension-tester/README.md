﻿# README #

Tester for the JastAdd purity annotator checker. 

### What is this project for? ###

Provide tests for the purity annotator where it can be confirmed that the annotations and the structure of the generated methods corresponds the original assumptions when the annotator was designed.

Does only test lines starting with “@” containing “@SideEffect” inorder to capture relevant method signatures and field declarations. Lines are also checked if starting with “public”,”private” and ”protected” to capture all other declarations. All other lines are ignored allowing for internal changes without failing the tests.


### Usage ###
 
<<<<<<< HEAD
Inside the folder tool/ the annotating JastAdd version should be placed. 
Everytime the annotatator is generated in jastAdd2 project the resulting file should be placed in this tool/ folder.

The tool can be build and run with the ant build script. 
ant jar : generates the tool but doesn’t run it.
ant test : tests the tool using no options
ant test -Dargs=’[arguments]’

Alternatively the tool “purityJastAddTester.jar” is run directly with 
Java -jar purityJastAddTester.jar [Options]

Options to provide
run without any option to preform all tests"
-noGen : skips running jastAdd2.jar and simply check previous generated code
-oneTest: to run one specific test provide as the last argument.
-fast: to run only a few overarching tests of all the primary constructions
-stopFail : stop on a failure.
-timeout x:timeout after x ms of jastAdd weaving (5000) standard
-h / -help: display this help information

Example is 
```
Java -jar purityJastAddTester.jar -noGen -stopFail -oneTest fastTests/ circNTA
```

Which only runs the test case circNTA without using the JastAdd to update the generated sources.

## Add a new test case ##

Provide the JastAdd source in a new folder under testfiles/source/ or it subfolder fastTests and JastAddtests. 


A intended result needs to be provide in the corresponding map under testfiles/goal. 

Before testing for new changes to the annotator you can generate a first verision of the result in the JastAdd and the move that folder from gen/ to goal/. 


### Current limitations ###

Methods must be generated in the same order in the resulting files and no new methods can be allowed without failing the tests. In these cases the goal files must be changed based on the modifications. This means that any structure changes of the generated Jastadd leads to incompatible results.
 
=======
Inside the folder tool the purity checker should be placed. 


### Current limitations ###

Methods must be generated in the same order in the resulting files and no new methods can be allowed without failing the tests. In these cases the goal files must be changed based on the modifications.
 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

>>>>>>> 6a053ff1fadced59e6cb972a15f1b86218e70167
### Who do I talk to? ###

Mikael Johnsson (dat12mj2@student.lu.se)
