// .options=rewrite noStatic
import static runtime.Test.*;

public class Test {
	public static void main(String[] args) {
		A a = new A(new B());
		B b = a.getB();
		b.state();
		
		testNotNull(a.testGetState());
		testSame(a.testGetState(), b.testGetState());
	}
}
