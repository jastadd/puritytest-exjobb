// .options=rewrite noStatic
import static runtime.Test.*;

public class Test {
	public static void main(String[] args) {
		A a = new A();
		B b = new B();
		a.state();
		b.state(); // 'b' is not in the same tree as 'a'
		a.setB(b);
		
		testNotNull(a.testGetState());
		testNotNull(b.testGetState());
		testNotSame(a.testGetState(), b.testGetState());
	}
}
