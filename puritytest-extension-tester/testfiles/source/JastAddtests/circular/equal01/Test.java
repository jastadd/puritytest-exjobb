// Tests equality check used for circular NTAs.
//
// .options:
import static runtime.Test.*;

public class Test {
  public static void main(String[] args) {
    A a_a1 = new A();
    a_a1.setId("a");
    A a_a2 = new A();
    a_a2.setId("a");
    A a_b1 = new A();
    a_b1.setId("b");

    B b_a1 = new B();
    b_a1.setId("a");

    // An AST value is null or a tuple of its type, tokens and children>

    // Two AST values are equal if they are both null.
    testTrue(a_a1.equal(null,null));

    // If their type, tokens and children are equal.

    // Same type, same token, no children.
    testTrue(a_a1.equal(a_a1,a_a2));

    // Different types, same token, no children.
    testFalse(a_a1.equal(a_a1,b_a1));

    // Same type, different token, no children.
    testFalse(a_a1.equal(a_a1,a_b1));

    C c = new C();
    C c2 = new C();
    D d = new D();
    D d2 = new D();
    D d3 = new D();
    A a_c = new A("a", new List().add(c));
    A a_c2 = new A("a", new List().add(c2));
    A a_d = new A("a", new List().add(d));
    A a_d_2 = new A("a", new List().add(d2).add(d3));

    // Same type, same token, same child.
    testTrue(a_a1.equal(a_c,a_c2));

    // Same type, same token, different child.
    testFalse(a_a2.equal(a_c,a_d));

    // Same type, same token, different number of children.
    testFalse(a_a2.equal(a_d,a_d_2));

    // Same type, same token, no children, different parent.
    testTrue(a_a2.equal(d,d2));
  }
}
