import static runtime.Test.*;

public class Test {
  public static void main(String[] args) {
    Node node = new Node();
    node.toBeRefined();
    testEqual("refined_A1_Node_toBeRefined", node.str[0]);
    testEqual("aTest.Node.toBeRefined", node.str[1]);
    testEqual("Test.Node.toBeRefinedd", node.str[2]);
    testEqual("refined_A1_Node_toBeRefined", node.str[3]);
    testEqual("unrefined", node.str[4]);
    testEqual("rrrefineddd", node.str[5]);
    testEqual("refinedrefined", node.str[6]);
  }
}
