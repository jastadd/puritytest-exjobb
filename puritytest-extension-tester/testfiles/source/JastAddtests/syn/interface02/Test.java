import static runtime.Test.*;

public class Test {
  public static void main(String[] args) {
    B1 b1 = new B1();
    B2 b2 = new B2();
    testFalse(b1.attr());
    testTrue(b2.attr());
  }
}
