aspect Visitor {
	/**
	 * Visitor interface for Calc language. Each concrete node type must
	 * have a visit method.
	 */
	public interface Visitor {
		public Object visit(List node, Object data);
		public Object visit(Program node, Object data);
		public Object visit(Function node, Object data);
		public Object visit(While node, Object data);
		public Object visit(If node, Object data);
		public Object visit(Return node, Object data);
		public Object visit(Decl node, Object data);
		public Object visit(Call node, Object data);
		public Object visit(Assign node, Object data);
		public Object visit(Block node, Object data);
		public Object visit(Mul node, Object data);
		public Object visit(Add node, Object data);
		public Object visit(Sub node, Object data);
		public Object visit(Div node, Object data);
		public Object visit(Mod node, Object data);
		public Object visit(Eq node, Object data);
		public Object visit(Neq node, Object data);
		public Object visit(Leq node, Object data);
		public Object visit(Less node, Object data);
		public Object visit(Geq node, Object data);
		public Object visit(Great node, Object data);
		public Object visit(Numeral node, Object data);
		public Object visit(IDExpr node, Object data);
		public Object visit(FunctionExpr node, Object data);
		public Object visit(Opt node, Object data);
		public Object visit(FunctionDecl node, Object data);
	}

	public Object ASTNode.accept(Visitor visitor, Object data) {
		throw new Error("Visitor: accept method not available for " + getClass().getName());
	}
	public Object Opt.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Program.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object List.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Function.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object While.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object If.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Return.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Decl.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Call.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Assign.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Block.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Mul.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Add.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Sub.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Div.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Mod.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Eq.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Neq.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Leq.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Less.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Geq.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Great.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Numeral.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object IDExpr.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object FunctionExpr.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object FunctionDecl.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
}
