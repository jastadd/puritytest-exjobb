/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\nta\parameterized01;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\parameterized01\\Test.ast:1
 * @astdecl X : ASTNode;
 * @production X : {@link ASTNode};

 */
public class X extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public X() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:19
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:23
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    myY_String_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:28
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:32
   */
  @SideEffect.Ignore @SideEffect.Fresh public X clone() throws CloneNotSupportedException {
    X node = (X) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public X copy() {
    try {
      X node = (X) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:56
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public X fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:66
   */
  @SideEffect.Fresh(group="_ASTNode") public X treeCopyNoTransform() {
    X tree = (X) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:86
   */
  @SideEffect.Fresh(group="_ASTNode") public X treeCopy() {
    X tree = (X) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:100
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Set myY_String_visited;
  /** @apilevel internal */
  @SideEffect.Ignore private void myY_String_reset() {
    myY_String_values = null;
    myY_String_proxy = null;
    myY_String_visited = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTNode myY_String_proxy;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map myY_String_values;

  /**
   * @attribute syn
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\parameterized01\\Test.jrag:2
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\parameterized01\\Test.jrag:2")
  @SideEffect.Pure(group="_ASTNode") public Y myY(String name) {
    Object _parameters = name;
    if (myY_String_visited == null) myY_String_visited = new java.util.HashSet(4);
    if (myY_String_values == null) myY_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (myY_String_values.containsKey(_parameters)) {
      return (Y) myY_String_values.get(_parameters);
    }
    if (myY_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute X.myY(String).");
    }
    myY_String_visited.add(_parameters);
    state().enterLazyAttribute();
    Y myY_String_value = myY_compute(name);
    if (myY_String_proxy == null) {
      myY_String_proxy = new ASTNode();
      myY_String_proxy.setParent(this);
    }
    if (myY_String_value != null) {
      myY_String_value.setParent(myY_String_proxy);
      if (myY_String_value.mayHaveRewrite()) {
        myY_String_value = (Y) myY_String_value.rewrittenNode();
        myY_String_value.setParent(myY_String_proxy);
      }
    }
    myY_String_values.put(_parameters, myY_String_value);
    state().leaveLazyAttribute();
    myY_String_visited.remove(_parameters);
    return myY_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Fresh private Y myY_compute(String name) {
      return new Y(name);
    }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
