/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\nta\flush01;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\flush01\\Test.ast:1
 * @astdecl Node : ASTNode ::= C;
 * @production Node : {@link ASTNode} ::= <span class="component">{@link C}</span>;

 */
public class Node extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public Node() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:14
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:20
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:24
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    getC_reset();
    c_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:30
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:34
   */
  @SideEffect.Ignore @SideEffect.Fresh public Node clone() throws CloneNotSupportedException {
    Node node = (Node) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:39
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public Node copy() {
    try {
      Node node = (Node) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:58
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Node fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:68
   */
  @SideEffect.Fresh(group="_ASTNode") public Node treeCopyNoTransform() {
    Node tree = (Node) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        switch (i) {
        case 0:
          tree.children[i] = null;
          continue;
        }
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:93
   */
  @SideEffect.Fresh(group="_ASTNode") public Node treeCopy() {
    Node tree = (Node) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        switch (i) {
        case 0:
          tree.children[i] = null;
          continue;
        }
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:112
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Retrieves the C child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the C child.
   * @apilevel low-level
   */
  @SideEffect.Pure public C getCNoTransform() {
    return (C) getChildNoTransform(0);
  }
  /**
   * Retrieves the child position of the optional child C.
   * @return The the child position of the optional child C.
   * @apilevel low-level
   */
  @SideEffect.Pure protected int getCChildPosition() {
    return 0;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean getC_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void getC_reset() {
    getC_computed = false;
    
    getC_value = null;
    getC_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean getC_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected C getC_value;

  /**
   * @attribute syn nta
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\flush01\\Test.jrag:2
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\flush01\\Test.jrag:2")
  @SideEffect.Pure(group="_ASTNode") public C getC() {
    ASTState state = state();
    if (getC_computed) {
      return (C) getChild(getCChildPosition());
    }
    if (getC_visited) {
      throw new RuntimeException("Circular definition of attribute Node.getC().");
    }
    getC_visited = true;
    state().enterLazyAttribute();
    getC_value = new C();
    setChild(getC_value, getCChildPosition());
    getC_computed = true;
    state().leaveLazyAttribute();
    getC_visited = false;
    C node = (C) this.getChild(getCChildPosition());
    return node;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean c_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void c_reset() {
    c_computed = false;
    
    c_value = null;
    c_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected C c_value;

  /**
   * @attribute syn
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\flush01\\Test.jrag:3
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\flush01\\Test.jrag:3")
  @SideEffect.Pure(group="_ASTNode") public C c() {
    ASTState state = state();
    if (c_computed) {
      return c_value;
    }
    if (c_visited) {
      throw new RuntimeException("Circular definition of attribute Node.c().");
    }
    c_visited = true;
    state().enterLazyAttribute();
    c_value = new C();
    c_value.setParent(this);
    c_computed = true;
    state().leaveLazyAttribute();
    c_visited = false;
    return c_value;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
