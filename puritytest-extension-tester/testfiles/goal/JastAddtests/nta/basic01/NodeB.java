/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\nta\basic01;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\basic01\\Test.ast:2
 * @astdecl NodeB : ASTNode ::= NtaB:B;
 * @production NodeB : {@link ASTNode} ::= <span class="component">NtaB:{@link B}</span>;

 */
public class NodeB extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\basic01\\Test.jrag:5
   */
  public B theB = new B();
  /**
   * @declaredat ASTNode:1
   */
  public NodeB() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:14
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:20
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:24
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    getNtaB_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:29
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  @SideEffect.Ignore @SideEffect.Fresh public NodeB clone() throws CloneNotSupportedException {
    NodeB node = (NodeB) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public NodeB copy() {
    try {
      NodeB node = (NodeB) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:57
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public NodeB fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:67
   */
  @SideEffect.Fresh(group="_ASTNode") public NodeB treeCopyNoTransform() {
    NodeB tree = (NodeB) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        switch (i) {
        case 0:
          tree.children[i] = null;
          continue;
        }
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:92
   */
  @SideEffect.Fresh(group="_ASTNode") public NodeB treeCopy() {
    NodeB tree = (NodeB) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        switch (i) {
        case 0:
          tree.children[i] = null;
          continue;
        }
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:111
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Retrieves the NtaB child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the NtaB child.
   * @apilevel low-level
   */
  @SideEffect.Pure public B getNtaBNoTransform() {
    return (B) getChildNoTransform(0);
  }
  /**
   * Retrieves the child position of the optional child NtaB.
   * @return The the child position of the optional child NtaB.
   * @apilevel low-level
   */
  @SideEffect.Pure protected int getNtaBChildPosition() {
    return 0;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean getNtaB_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void getNtaB_reset() {
    getNtaB_computed = false;
    
    getNtaB_value = null;
    getNtaB_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean getNtaB_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected B getNtaB_value;

  /**
   * @attribute syn nta
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\basic01\\Test.jrag:6
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\basic01\\Test.jrag:6")
  @SideEffect.Pure(group="_ASTNode") public B getNtaB() {
    ASTState state = state();
    if (getNtaB_computed) {
      return (B) getChild(getNtaBChildPosition());
    }
    if (getNtaB_visited) {
      throw new RuntimeException("Circular definition of attribute NodeB.getNtaB().");
    }
    getNtaB_visited = true;
    state().enterLazyAttribute();
    getNtaB_value = getNtaB_compute();
    setChild(getNtaB_value, getNtaBChildPosition());
    getNtaB_computed = true;
    state().leaveLazyAttribute();
    getNtaB_visited = false;
    B node = (B) this.getChild(getNtaBChildPosition());
    return node;
  }
  /** @apilevel internal */
  @SideEffect.Fresh private B getNtaB_compute() {
  		return theB;
  	}
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
