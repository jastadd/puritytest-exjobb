/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\nta\coll01;
import java.util.Collection;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\coll01\\Test.ast:1
 * @astdecl Node : ASTNode ::= A:NamedNode B:NamedNode;
 * @production Node : {@link ASTNode} ::= <span class="component">A:{@link NamedNode}</span> <span class="component">B:{@link NamedNode}</span>;

 */
public class Node extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public Node() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
  }
  /**
   * @declaredat ASTNode:13
   */
  @ASTNodeAnnotation.Constructor(
    name = {"B"},
    type = {"NamedNode"},
    kind = {"Child"}
  )
  public Node(NamedNode p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:22
   */
  @SideEffect.Pure protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:28
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:32
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    getA_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
    Node_childNames_visited = false;
    Node_childNames_computed = false;
    
    Node_childNames_value = null;
    contributorMap_Node_childNames = null;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:46
   */
  @SideEffect.Ignore @SideEffect.Fresh public Node clone() throws CloneNotSupportedException {
    Node node = (Node) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:51
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public Node copy() {
    try {
      Node node = (Node) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:70
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Node fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:80
   */
  @SideEffect.Fresh(group="_ASTNode") public Node treeCopyNoTransform() {
    Node tree = (Node) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        switch (i) {
        case 1:
          tree.children[i] = null;
          continue;
        }
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:105
   */
  @SideEffect.Fresh(group="_ASTNode") public Node treeCopy() {
    Node tree = (Node) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        switch (i) {
        case 1:
          tree.children[i] = null;
          continue;
        }
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:124
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the B child.
   * @param node The new node to replace the B child.
   * @apilevel high-level
   */
  public void setB(NamedNode node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the B child.
   * @return The current node used as the B child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="B")
  @SideEffect.Pure public NamedNode getB() {
    return (NamedNode) getChild(0);
  }
  /**
   * Retrieves the B child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the B child.
   * @apilevel low-level
   */
  @SideEffect.Pure public NamedNode getBNoTransform() {
    return (NamedNode) getChildNoTransform(0);
  }
  /**
   * Retrieves the A child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the A child.
   * @apilevel low-level
   */
  @SideEffect.Pure public NamedNode getANoTransform() {
    return (NamedNode) getChildNoTransform(1);
  }
  /**
   * Retrieves the child position of the optional child A.
   * @return The the child position of the optional child A.
   * @apilevel low-level
   */
  @SideEffect.Pure protected int getAChildPosition() {
    return 1;
  }
  /**
   * @aspect <NoAspect>
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\coll01\\Test.jrag:7
   */
  /** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map<ASTNode, java.util.Set<ASTNode>> contributorMap_Node_childNames = null;

  /** @apilevel internal */
  @SideEffect.Ignore protected void survey_Node_childNames() {
    if (contributorMap_Node_childNames == null) {
      contributorMap_Node_childNames = new java.util.IdentityHashMap<ASTNode, java.util.Set<ASTNode>>();
      collect_contributors_Node_childNames(this, contributorMap_Node_childNames);
    }
  }

/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean getA_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void getA_reset() {
    getA_computed = false;
    
    getA_value = null;
    getA_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean getA_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected NamedNode getA_value;

  /**
   * @attribute syn nta
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\coll01\\Test.jrag:5
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\coll01\\Test.jrag:5")
  @SideEffect.Pure(group="_ASTNode") public NamedNode getA() {
    ASTState state = state();
    if (getA_computed) {
      return (NamedNode) getChild(getAChildPosition());
    }
    if (getA_visited) {
      throw new RuntimeException("Circular definition of attribute Node.getA().");
    }
    getA_visited = true;
    state().enterLazyAttribute();
    getA_value = new NamedNode("nta");
    setChild(getA_value, getAChildPosition());
    getA_computed = true;
    state().leaveLazyAttribute();
    getA_visited = false;
    NamedNode node = (NamedNode) this.getChild(getAChildPosition());
    return node;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean Node_childNames_visited = false;
  /**
   * @attribute coll
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\coll01\\Test.jrag:7
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.COLL)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\coll01\\Test.jrag:7")
  @SideEffect.Pure(group="_ASTNode") public Collection<String> childNames() {
    ASTState state = state();
    if (Node_childNames_computed) {
      return Node_childNames_value;
    }
    if (Node_childNames_visited) {
      throw new RuntimeException("Circular definition of attribute Node.childNames().");
    }
    Node_childNames_visited = true;
    state().enterLazyAttribute();
    Node_childNames_value = childNames_compute();
    Node_childNames_computed = true;
    state().leaveLazyAttribute();
    Node_childNames_visited = false;
    return Node_childNames_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure(group="_ASTNode") private Collection<String> childNames_compute() {
    ASTNode node = this;
    while (node != null && !(node instanceof Node)) {
      node = node.getParent();
    }
    Node root = (Node) node;
    root.survey_Node_childNames();
    Collection<String> _computedValue = new HashSet<String>();
    if (root.contributorMap_Node_childNames.containsKey(this)) {
          for (ASTNode contributor : root.contributorMap_Node_childNames.get(this)) {
            contributor.contributeTo_Node_childNames(_computedValue);
          }
    }
    return _computedValue;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean Node_childNames_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Collection<String> Node_childNames_value;

}
