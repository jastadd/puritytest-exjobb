/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\incremental\node\Test184;
import java.util.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test184\\Test.ast:2
 * @astdecl Root : ASTNode ::= A;
 * @production Root : {@link ASTNode} ::= <span class="component">{@link A}</span>;

 */
public class Root extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public Root() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
  }
  /**
   * @declaredat ASTNode:13
   */
  @ASTNodeAnnotation.Constructor(
    name = {"A"},
    type = {"A"},
    kind = {"Child"}
  )
  public Root(A p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:22
   */
  @SideEffect.Pure protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:28
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:32
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    parRoot_String_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:41
   */
  @SideEffect.Ignore @SideEffect.Fresh public Root clone() throws CloneNotSupportedException {
    Root node = (Root) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:46
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public Root copy() {
    try {
      Root node = (Root) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:65
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Root fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:75
   */
  @SideEffect.Fresh(group="_ASTNode") public Root treeCopyNoTransform() {
    Root tree = (Root) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:95
   */
  @SideEffect.Fresh(group="_ASTNode") public Root treeCopy() {
    Root tree = (Root) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:109
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the A child.
   * @param node The new node to replace the A child.
   * @apilevel high-level
   */
  public void setA(A node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the A child.
   * @return The current node used as the A child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="A")
  @SideEffect.Pure public A getA() {
    return (A) getChild(0);
  }
  /**
   * Retrieves the A child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the A child.
   * @apilevel low-level
   */
  @SideEffect.Pure public A getANoTransform() {
    return (A) getChildNoTransform(0);
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Set parRoot_String_visited;
  /** @apilevel internal */
  @SideEffect.Ignore private void parRoot_String_reset() {
    parRoot_String_values = null;
    parRoot_String_proxy = null;
    parRoot_String_visited = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTNode parRoot_String_proxy;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map parRoot_String_values;

  /**
   * @attribute syn
   * @aspect Test184
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test184\\Test184.jrag:31
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="Test184", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test184\\Test184.jrag:31")
  @SideEffect.Pure(group="_ASTNode") public D parRoot(String id) {
    Object _parameters = id;
    if (parRoot_String_visited == null) parRoot_String_visited = new java.util.HashSet(4);
    if (parRoot_String_values == null) parRoot_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (parRoot_String_values.containsKey(_parameters)) {
      return (D) parRoot_String_values.get(_parameters);
    }
    if (parRoot_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute Root.parRoot(String).");
    }
    parRoot_String_visited.add(_parameters);
    state().enterLazyAttribute();
    D parRoot_String_value = parRoot_compute(id);
    if (parRoot_String_proxy == null) {
      parRoot_String_proxy = new ASTNode();
      parRoot_String_proxy.setParent(this);
    }
    if (parRoot_String_value != null) {
      parRoot_String_value.setParent(parRoot_String_proxy);
      if (parRoot_String_value.mayHaveRewrite()) {
        parRoot_String_value = (D) parRoot_String_value.rewrittenNode();
        parRoot_String_value.setParent(parRoot_String_proxy);
      }
    }
    parRoot_String_values.put(_parameters, parRoot_String_value);
    state().leaveLazyAttribute();
    parRoot_String_visited.remove(_parameters);
    return parRoot_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Fresh private D parRoot_compute(String id) {
      D d = new D();
      d.setName(id);
      return d;
    }
  /**
   * @attribute inh
   * @aspect Test184
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test184\\Test184.jrag:40
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="Test184", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test184\\Test184.jrag:40")
  @SideEffect.Pure(group="_ASTNode") public String name() {
    if (name_visited) {
      throw new RuntimeException("Circular definition of attribute Root.name().");
    }
    name_visited = true;
    String name_value = getParent().Define_name(this, null);
    name_visited = false;
    return name_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean name_visited = false;
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test184\\Test184.jrag:25
   * @apilevel internal
   */
 @SideEffect.Pure(group="_ASTNode") public D Define_lookupD(ASTNode _callerNode, ASTNode _childNode, String id) {
    if (getANoTransform() != null && _callerNode == getA()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test184\\Test184.jrag:27
      {
          return parRoot(id);
        }
    }
    else {
      return getParent().Define_lookupD(this, _callerNode, id);
    }
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test184\\Test184.jrag:25
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute lookupD
   */
  @SideEffect.Pure protected boolean canDefine_lookupD(ASTNode _callerNode, ASTNode _childNode, String id) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
