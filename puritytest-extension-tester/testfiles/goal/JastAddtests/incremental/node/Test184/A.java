/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\incremental\node\Test184;
import java.util.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test184\\Test.ast:3
 * @astdecl A : ASTNode ::= C;
 * @production A : {@link ASTNode} ::= <span class="component">{@link C}</span>;

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public A() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
  }
  /**
   * @declaredat ASTNode:13
   */
  @ASTNodeAnnotation.Constructor(
    name = {"C"},
    type = {"C"},
    kind = {"Child"}
  )
  public A(C p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:22
   */
  @SideEffect.Pure protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:28
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:32
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    parA_String_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:41
   */
  @SideEffect.Ignore @SideEffect.Fresh public A clone() throws CloneNotSupportedException {
    A node = (A) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:46
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public A copy() {
    try {
      A node = (A) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:65
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public A fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:75
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopyNoTransform() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:95
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopy() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:109
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the C child.
   * @param node The new node to replace the C child.
   * @apilevel high-level
   */
  public void setC(C node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the C child.
   * @return The current node used as the C child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="C")
  @SideEffect.Pure public C getC() {
    return (C) getChild(0);
  }
  /**
   * Retrieves the C child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the C child.
   * @apilevel low-level
   */
  @SideEffect.Pure public C getCNoTransform() {
    return (C) getChildNoTransform(0);
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Set parA_String_visited;
  /** @apilevel internal */
  @SideEffect.Ignore private void parA_String_reset() {
    parA_String_values = null;
    parA_String_proxy = null;
    parA_String_visited = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTNode parA_String_proxy;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map parA_String_values;

  /**
   * @attribute syn
   * @aspect Test184
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test184\\Test184.jrag:14
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="Test184", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test184\\Test184.jrag:14")
  @SideEffect.Pure(group="_ASTNode") public B parA(String id) {
    Object _parameters = id;
    if (parA_String_visited == null) parA_String_visited = new java.util.HashSet(4);
    if (parA_String_values == null) parA_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (parA_String_values.containsKey(_parameters)) {
      return (B) parA_String_values.get(_parameters);
    }
    if (parA_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute A.parA(String).");
    }
    parA_String_visited.add(_parameters);
    state().enterLazyAttribute();
    B parA_String_value = parA_compute(id);
    if (parA_String_proxy == null) {
      parA_String_proxy = new ASTNode();
      parA_String_proxy.setParent(this);
    }
    if (parA_String_value != null) {
      parA_String_value.setParent(parA_String_proxy);
      if (parA_String_value.mayHaveRewrite()) {
        parA_String_value = (B) parA_String_value.rewrittenNode();
        parA_String_value.setParent(parA_String_proxy);
      }
    }
    parA_String_values.put(_parameters, parA_String_value);
    state().leaveLazyAttribute();
    parA_String_visited.remove(_parameters);
    return parA_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Fresh private B parA_compute(String id) {
      D d = lookupD(id);
      B b = new B();
      b.setName(id);
      b.setD(d);
      d.name();
      return b;
    }
  /**
   * @attribute inh
   * @aspect Test184
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test184\\Test184.jrag:25
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="Test184", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test184\\Test184.jrag:25")
  @SideEffect.Pure(group="_ASTNode") public D lookupD(String id) {
    Object _parameters = id;
    if (lookupD_String_visited == null) lookupD_String_visited = new java.util.HashSet(4);
    if (lookupD_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute A.lookupD(String).");
    }
    lookupD_String_visited.add(_parameters);
    D lookupD_String_value = getParent().Define_lookupD(this, null, id);
    lookupD_String_visited.remove(_parameters);
    return lookupD_String_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Set lookupD_String_visited;
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test184\\Test184.jrag:8
   * @apilevel internal
   */
 @SideEffect.Pure(group="_ASTNode") public B Define_lookupB(ASTNode _callerNode, ASTNode _childNode, String id) {
    if (getCNoTransform() != null && _callerNode == getC()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test184\\Test184.jrag:10
      {
          return parA(id);
        }
    }
    else {
      return getParent().Define_lookupB(this, _callerNode, id);
    }
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test184\\Test184.jrag:8
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute lookupB
   */
  @SideEffect.Pure protected boolean canDefine_lookupB(ASTNode _callerNode, ASTNode _childNode, String id) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
