/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\incremental\node\Test151;
import java.util.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test151\\Test.ast:1
 * @astdecl A : ASTNode ::= <Name:String>;
 * @production A : {@link ASTNode} ::= <span class="component">&lt;Name:String&gt;</span>;

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public A() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /**
   * @declaredat ASTNode:12
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Name"},
    type = {"String"},
    kind = {"Token"}
  )
  public A(String p0) {
    setName(p0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:21
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:27
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:31
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    a0_reset();
    a1_reset();
    c_String_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:42
   */
  @SideEffect.Ignore @SideEffect.Fresh public A clone() throws CloneNotSupportedException {
    A node = (A) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:47
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public A copy() {
    try {
      A node = (A) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:66
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public A fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:76
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopyNoTransform() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:96
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopy() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:110
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_Name == ((A) node).tokenString_Name);    
  }
  /**
   * Replaces the lexeme Name.
   * @param value The new value for the lexeme Name.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setName(String value) {
    tokenString_Name = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_Name;
  /**
   * Retrieves the value for the lexeme Name.
   * @return The value for the lexeme Name.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Name")
  @SideEffect.Pure(group="_ASTNode") public String getName() {
    return tokenString_Name != null ? tokenString_Name : "";
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean a0_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void a0_reset() {
    a0_computed = false;
    a0_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a0_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a0_value;

  /**
   * @attribute syn
   * @aspect Test151
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test151\\Test151.jrag:5
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test151", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test151\\Test151.jrag:5")
  @SideEffect.Pure(group="_ASTNode") public boolean a0() {
    ASTState state = state();
    if (a0_computed) {
      return a0_value;
    }
    if (a0_visited) {
      throw new RuntimeException("Circular definition of attribute A.a0().");
    }
    a0_visited = true;
    state().enterLazyAttribute();
    a0_value = b0("a");
    a0_computed = true;
    state().leaveLazyAttribute();
    a0_visited = false;
    return a0_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Set b0_String_visited;
  /**
   * @attribute syn
   * @aspect Test151
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test151\\Test151.jrag:7
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test151", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test151\\Test151.jrag:7")
  @SideEffect.Pure(group="_ASTNode") public boolean b0(String s) {
    Object _parameters = s;
    if (b0_String_visited == null) b0_String_visited = new java.util.HashSet(4);
    if (b0_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute A.b0(String).");
    }
    b0_String_visited.add(_parameters);
    boolean b0_String_value = c(s);
    b0_String_visited.remove(_parameters);
    return b0_String_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean a1_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void a1_reset() {
    a1_computed = false;
    a1_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a1_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a1_value;

  /**
   * @attribute syn
   * @aspect Test151
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test151\\Test151.jrag:9
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test151", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test151\\Test151.jrag:9")
  @SideEffect.Pure(group="_ASTNode") public boolean a1() {
    ASTState state = state();
    if (a1_computed) {
      return a1_value;
    }
    if (a1_visited) {
      throw new RuntimeException("Circular definition of attribute A.a1().");
    }
    a1_visited = true;
    state().enterLazyAttribute();
    a1_value = b1("b");
    a1_computed = true;
    state().leaveLazyAttribute();
    a1_visited = false;
    return a1_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Set b1_String_visited;
  /**
   * @attribute syn
   * @aspect Test151
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test151\\Test151.jrag:11
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test151", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test151\\Test151.jrag:11")
  @SideEffect.Pure(group="_ASTNode") public boolean b1(String s) {
    Object _parameters = s;
    if (b1_String_visited == null) b1_String_visited = new java.util.HashSet(4);
    if (b1_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute A.b1(String).");
    }
    b1_String_visited.add(_parameters);
    boolean b1_String_value = c(s);
    b1_String_visited.remove(_parameters);
    return b1_String_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Set c_String_visited;
  /** @apilevel internal */
  @SideEffect.Ignore private void c_String_reset() {
    c_String_values = null;
    c_String_visited = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map c_String_values;

  /**
   * @attribute syn
   * @aspect Test151
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test151\\Test151.jrag:13
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test151", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test151\\Test151.jrag:13")
  @SideEffect.Pure(group="_ASTNode") public boolean c(String s) {
    Object _parameters = s;
    if (c_String_visited == null) c_String_visited = new java.util.HashSet(4);
    if (c_String_values == null) c_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (c_String_values.containsKey(_parameters)) {
      return (Boolean) c_String_values.get(_parameters);
    }
    if (c_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute A.c(String).");
    }
    c_String_visited.add(_parameters);
    state().enterLazyAttribute();
    boolean c_String_value = getName().equals(s);
    c_String_values.put(_parameters, c_String_value);
    state().leaveLazyAttribute();
    c_String_visited.remove(_parameters);
    return c_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
