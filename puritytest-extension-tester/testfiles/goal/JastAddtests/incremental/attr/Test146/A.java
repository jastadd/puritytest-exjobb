/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\incremental\attr\Test146;
import java.util.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\attr\\Test146\\Test.ast:1
 * @astdecl A : ASTNode ::= <Name:String> B;
 * @production A : {@link ASTNode} ::= <span class="component">&lt;Name:String&gt;</span> <span class="component">{@link B}</span>;

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public A() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
  }
  /**
   * @declaredat ASTNode:13
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Name", "B"},
    type = {"String", "B"},
    kind = {"Token", "Child"}
  )
  public A(String p0, B p1) {
    setName(p0);
    setChild(p1, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:23
   */
  @SideEffect.Pure protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:29
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    d_reset();
    makeB_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:39
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  @SideEffect.Ignore @SideEffect.Fresh public A clone() throws CloneNotSupportedException {
    A node = (A) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:48
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public A copy() {
    try {
      A node = (A) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:67
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public A fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:77
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopyNoTransform() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:97
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopy() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:111
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_Name == ((A) node).tokenString_Name);    
  }
  /**
   * Replaces the lexeme Name.
   * @param value The new value for the lexeme Name.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setName(String value) {
    tokenString_Name = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_Name;
  /**
   * Retrieves the value for the lexeme Name.
   * @return The value for the lexeme Name.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Name")
  @SideEffect.Pure(group="_ASTNode") public String getName() {
    return tokenString_Name != null ? tokenString_Name : "";
  }
  /**
   * Replaces the B child.
   * @param node The new node to replace the B child.
   * @apilevel high-level
   */
  public void setB(B node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the B child.
   * @return The current node used as the B child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="B")
  @SideEffect.Pure public B getB() {
    return (B) getChild(0);
  }
  /**
   * Retrieves the B child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the B child.
   * @apilevel low-level
   */
  @SideEffect.Pure public B getBNoTransform() {
    return (B) getChildNoTransform(0);
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean d_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void d_reset() {
    d_computed = false;
    d_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean d_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean d_value;

  /**
   * @attribute syn
   * @aspect Test146
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\attr\\Test146\\Test146.jrag:8
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test146", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\attr\\Test146\\Test146.jrag:8")
  @SideEffect.Pure(group="_ASTNode") public boolean d() {
    ASTState state = state();
    if (d_computed) {
      return d_value;
    }
    if (d_visited) {
      throw new RuntimeException("Circular definition of attribute A.d().");
    }
    d_visited = true;
    state().enterLazyAttribute();
    d_value = makeB() instanceof C;
    d_computed = true;
    state().leaveLazyAttribute();
    d_visited = false;
    return d_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean makeB_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void makeB_reset() {
    makeB_computed = false;
    
    makeB_value = null;
    makeB_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean makeB_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected B makeB_value;

  /**
   * @attribute syn
   * @aspect Test146
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\attr\\Test146\\Test146.jrag:9
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="Test146", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\attr\\Test146\\Test146.jrag:9")
  @SideEffect.Pure(group="_ASTNode") public B makeB() {
    ASTState state = state();
    if (makeB_computed) {
      return makeB_value;
    }
    if (makeB_visited) {
      throw new RuntimeException("Circular definition of attribute A.makeB().");
    }
    makeB_visited = true;
    state().enterLazyAttribute();
    makeB_value = getName().equals("a") ? new B() : new C();
    makeB_value.setParent(this);
    makeB_computed = true;
    state().leaveLazyAttribute();
    makeB_visited = false;
    return makeB_value;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\attr\\Test146\\Test146.jrag:5
   * @apilevel internal
   */
 @SideEffect.Pure(group="_ASTNode") public int Define_c(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return 5;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\attr\\Test146\\Test146.jrag:5
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute c
   */
  @SideEffect.Pure protected boolean canDefine_c(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
