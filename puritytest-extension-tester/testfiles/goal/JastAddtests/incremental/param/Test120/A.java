/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\incremental\param\Test120;
import java.util.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test.ast:1
 * @astdecl A : ASTNode ::= <Name:String>;
 * @production A : {@link ASTNode} ::= <span class="component">&lt;Name:String&gt;</span>;

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public A() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /**
   * @declaredat ASTNode:12
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Name"},
    type = {"String"},
    kind = {"Token"}
  )
  public A(String p0) {
    setName(p0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:21
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:27
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:31
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    a1_reset();
    b2_reset();
    a3_reset();
    b3_reset();
    a4_reset();
    c4_reset();
    b5_reset();
    c5_reset();
    a6_reset();
    b6_reset();
    a7_reset();
    b7_2_reset();
    c7_1_reset();
    a8_reset();
    c8_1_reset();
    c8_2_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:51
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:55
   */
  @SideEffect.Ignore @SideEffect.Fresh public A clone() throws CloneNotSupportedException {
    A node = (A) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:60
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public A copy() {
    try {
      A node = (A) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:79
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public A fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:89
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopyNoTransform() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:109
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopy() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:123
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_Name == ((A) node).tokenString_Name);    
  }
  /**
   * Replaces the lexeme Name.
   * @param value The new value for the lexeme Name.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setName(String value) {
    tokenString_Name = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_Name;
  /**
   * Retrieves the value for the lexeme Name.
   * @return The value for the lexeme Name.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Name")
  @SideEffect.Pure(group="_ASTNode") public String getName() {
    return tokenString_Name != null ? tokenString_Name : "";
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean a0_visited = false;
  /**
   * @attribute syn
   * @aspect Test120
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:5
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test120", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:5")
  @SideEffect.Pure(group="_ASTNode") public boolean a0() {
    if (a0_visited) {
      throw new RuntimeException("Circular definition of attribute A.a0().");
    }
    a0_visited = true;
    boolean a0_value = b0();
    a0_visited = false;
    return a0_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean b0_visited = false;
  /**
   * @attribute syn
   * @aspect Test120
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:6
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test120", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:6")
  @SideEffect.Pure(group="_ASTNode") public boolean b0() {
    if (b0_visited) {
      throw new RuntimeException("Circular definition of attribute A.b0().");
    }
    b0_visited = true;
    boolean b0_value = getName().equals("a");
    b0_visited = false;
    return b0_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean a1_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void a1_reset() {
    a1_computed = false;
    a1_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a1_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a1_value;

  /**
   * @attribute syn
   * @aspect Test120
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:8
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test120", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:8")
  @SideEffect.Pure(group="_ASTNode") public boolean a1() {
    ASTState state = state();
    if (a1_computed) {
      return a1_value;
    }
    if (a1_visited) {
      throw new RuntimeException("Circular definition of attribute A.a1().");
    }
    a1_visited = true;
    state().enterLazyAttribute();
    a1_value = b1();
    a1_computed = true;
    state().leaveLazyAttribute();
    a1_visited = false;
    return a1_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean b1_visited = false;
  /**
   * @attribute syn
   * @aspect Test120
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:9
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test120", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:9")
  @SideEffect.Pure(group="_ASTNode") public boolean b1() {
    if (b1_visited) {
      throw new RuntimeException("Circular definition of attribute A.b1().");
    }
    b1_visited = true;
    boolean b1_value = getName().equals("a");
    b1_visited = false;
    return b1_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean a2_visited = false;
  /**
   * @attribute syn
   * @aspect Test120
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:11
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test120", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:11")
  @SideEffect.Pure(group="_ASTNode") public boolean a2() {
    if (a2_visited) {
      throw new RuntimeException("Circular definition of attribute A.a2().");
    }
    a2_visited = true;
    boolean a2_value = b2();
    a2_visited = false;
    return a2_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean b2_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void b2_reset() {
    b2_computed = false;
    b2_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean b2_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean b2_value;

  /**
   * @attribute syn
   * @aspect Test120
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:12
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test120", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:12")
  @SideEffect.Pure(group="_ASTNode") public boolean b2() {
    ASTState state = state();
    if (b2_computed) {
      return b2_value;
    }
    if (b2_visited) {
      throw new RuntimeException("Circular definition of attribute A.b2().");
    }
    b2_visited = true;
    state().enterLazyAttribute();
    b2_value = getName().equals("a");
    b2_computed = true;
    state().leaveLazyAttribute();
    b2_visited = false;
    return b2_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean a3_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void a3_reset() {
    a3_computed = false;
    a3_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a3_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a3_value;

  /**
   * @attribute syn
   * @aspect Test120
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:14
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test120", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:14")
  @SideEffect.Pure(group="_ASTNode") public boolean a3() {
    ASTState state = state();
    if (a3_computed) {
      return a3_value;
    }
    if (a3_visited) {
      throw new RuntimeException("Circular definition of attribute A.a3().");
    }
    a3_visited = true;
    state().enterLazyAttribute();
    a3_value = b3();
    a3_computed = true;
    state().leaveLazyAttribute();
    a3_visited = false;
    return a3_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean b3_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void b3_reset() {
    b3_computed = false;
    b3_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean b3_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean b3_value;

  /**
   * @attribute syn
   * @aspect Test120
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:15
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test120", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:15")
  @SideEffect.Pure(group="_ASTNode") public boolean b3() {
    ASTState state = state();
    if (b3_computed) {
      return b3_value;
    }
    if (b3_visited) {
      throw new RuntimeException("Circular definition of attribute A.b3().");
    }
    b3_visited = true;
    state().enterLazyAttribute();
    b3_value = getName().equals("a");
    b3_computed = true;
    state().leaveLazyAttribute();
    b3_visited = false;
    return b3_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean a4_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void a4_reset() {
    a4_computed = false;
    a4_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a4_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a4_value;

  /**
   * @attribute syn
   * @aspect Test120
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:18
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test120", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:18")
  @SideEffect.Pure(group="_ASTNode") public boolean a4() {
    ASTState state = state();
    if (a4_computed) {
      return a4_value;
    }
    if (a4_visited) {
      throw new RuntimeException("Circular definition of attribute A.a4().");
    }
    a4_visited = true;
    state().enterLazyAttribute();
    a4_value = b4();
    a4_computed = true;
    state().leaveLazyAttribute();
    a4_visited = false;
    return a4_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean b4_visited = false;
  /**
   * @attribute syn
   * @aspect Test120
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:19
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test120", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:19")
  @SideEffect.Pure(group="_ASTNode") public boolean b4() {
    if (b4_visited) {
      throw new RuntimeException("Circular definition of attribute A.b4().");
    }
    b4_visited = true;
    boolean b4_value = c4();
    b4_visited = false;
    return b4_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean c4_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void c4_reset() {
    c4_computed = false;
    c4_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c4_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c4_value;

  /**
   * @attribute syn
   * @aspect Test120
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:20
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test120", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:20")
  @SideEffect.Pure(group="_ASTNode") public boolean c4() {
    ASTState state = state();
    if (c4_computed) {
      return c4_value;
    }
    if (c4_visited) {
      throw new RuntimeException("Circular definition of attribute A.c4().");
    }
    c4_visited = true;
    state().enterLazyAttribute();
    c4_value = getName().equals("a");
    c4_computed = true;
    state().leaveLazyAttribute();
    c4_visited = false;
    return c4_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean a5_visited = false;
  /**
   * @attribute syn
   * @aspect Test120
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:22
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test120", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:22")
  @SideEffect.Pure(group="_ASTNode") public boolean a5() {
    if (a5_visited) {
      throw new RuntimeException("Circular definition of attribute A.a5().");
    }
    a5_visited = true;
    boolean a5_value = b5();
    a5_visited = false;
    return a5_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean b5_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void b5_reset() {
    b5_computed = false;
    b5_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean b5_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean b5_value;

  /**
   * @attribute syn
   * @aspect Test120
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:23
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test120", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:23")
  @SideEffect.Pure(group="_ASTNode") public boolean b5() {
    ASTState state = state();
    if (b5_computed) {
      return b5_value;
    }
    if (b5_visited) {
      throw new RuntimeException("Circular definition of attribute A.b5().");
    }
    b5_visited = true;
    state().enterLazyAttribute();
    b5_value = c5();
    b5_computed = true;
    state().leaveLazyAttribute();
    b5_visited = false;
    return b5_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean c5_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void c5_reset() {
    c5_computed = false;
    c5_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c5_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c5_value;

  /**
   * @attribute syn
   * @aspect Test120
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:24
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test120", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:24")
  @SideEffect.Pure(group="_ASTNode") public boolean c5() {
    ASTState state = state();
    if (c5_computed) {
      return c5_value;
    }
    if (c5_visited) {
      throw new RuntimeException("Circular definition of attribute A.c5().");
    }
    c5_visited = true;
    state().enterLazyAttribute();
    c5_value = getName().equals("a");
    c5_computed = true;
    state().leaveLazyAttribute();
    c5_visited = false;
    return c5_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean a6_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void a6_reset() {
    a6_computed = false;
    a6_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a6_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a6_value;

  /**
   * @attribute syn
   * @aspect Test120
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:26
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test120", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:26")
  @SideEffect.Pure(group="_ASTNode") public boolean a6() {
    ASTState state = state();
    if (a6_computed) {
      return a6_value;
    }
    if (a6_visited) {
      throw new RuntimeException("Circular definition of attribute A.a6().");
    }
    a6_visited = true;
    state().enterLazyAttribute();
    a6_value = b6();
    a6_computed = true;
    state().leaveLazyAttribute();
    a6_visited = false;
    return a6_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean b6_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void b6_reset() {
    b6_computed = false;
    b6_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean b6_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean b6_value;

  /**
   * @attribute syn
   * @aspect Test120
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:27
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test120", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:27")
  @SideEffect.Pure(group="_ASTNode") public boolean b6() {
    ASTState state = state();
    if (b6_computed) {
      return b6_value;
    }
    if (b6_visited) {
      throw new RuntimeException("Circular definition of attribute A.b6().");
    }
    b6_visited = true;
    state().enterLazyAttribute();
    b6_value = c6();
    b6_computed = true;
    state().leaveLazyAttribute();
    b6_visited = false;
    return b6_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean c6_visited = false;
  /**
   * @attribute syn
   * @aspect Test120
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:28
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test120", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:28")
  @SideEffect.Pure(group="_ASTNode") public boolean c6() {
    if (c6_visited) {
      throw new RuntimeException("Circular definition of attribute A.c6().");
    }
    c6_visited = true;
    boolean c6_value = getName().equals("a");
    c6_visited = false;
    return c6_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean a7_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void a7_reset() {
    a7_computed = false;
    a7_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a7_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected int a7_value;

  /**
   * @attribute syn
   * @aspect Test120
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:31
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test120", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:31")
  @SideEffect.Pure(group="_ASTNode") public int a7() {
    ASTState state = state();
    if (a7_computed) {
      return a7_value;
    }
    if (a7_visited) {
      throw new RuntimeException("Circular definition of attribute A.a7().");
    }
    a7_visited = true;
    state().enterLazyAttribute();
    a7_value = b7_1() + b7_2();
    a7_computed = true;
    state().leaveLazyAttribute();
    a7_visited = false;
    return a7_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean b7_1_visited = false;
  /**
   * @attribute syn
   * @aspect Test120
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:32
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test120", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:32")
  @SideEffect.Pure(group="_ASTNode") public int b7_1() {
    if (b7_1_visited) {
      throw new RuntimeException("Circular definition of attribute A.b7_1().");
    }
    b7_1_visited = true;
    int b7_1_value = c7_1() + c7_2();
    b7_1_visited = false;
    return b7_1_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean b7_2_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void b7_2_reset() {
    b7_2_computed = false;
    b7_2_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean b7_2_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected int b7_2_value;

  /**
   * @attribute syn
   * @aspect Test120
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:33
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test120", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:33")
  @SideEffect.Pure(group="_ASTNode") public int b7_2() {
    ASTState state = state();
    if (b7_2_computed) {
      return b7_2_value;
    }
    if (b7_2_visited) {
      throw new RuntimeException("Circular definition of attribute A.b7_2().");
    }
    b7_2_visited = true;
    state().enterLazyAttribute();
    b7_2_value = 1;
    b7_2_computed = true;
    state().leaveLazyAttribute();
    b7_2_visited = false;
    return b7_2_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean c7_1_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void c7_1_reset() {
    c7_1_computed = false;
    c7_1_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c7_1_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected int c7_1_value;

  /**
   * @attribute syn
   * @aspect Test120
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:34
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test120", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:34")
  @SideEffect.Pure(group="_ASTNode") public int c7_1() {
    ASTState state = state();
    if (c7_1_computed) {
      return c7_1_value;
    }
    if (c7_1_visited) {
      throw new RuntimeException("Circular definition of attribute A.c7_1().");
    }
    c7_1_visited = true;
    state().enterLazyAttribute();
    c7_1_value = 1;
    c7_1_computed = true;
    state().leaveLazyAttribute();
    c7_1_visited = false;
    return c7_1_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean c7_2_visited = false;
  /**
   * @attribute syn
   * @aspect Test120
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:35
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test120", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:35")
  @SideEffect.Pure(group="_ASTNode") public int c7_2() {
    if (c7_2_visited) {
      throw new RuntimeException("Circular definition of attribute A.c7_2().");
    }
    c7_2_visited = true;
    int c7_2_value = 1;
    c7_2_visited = false;
    return c7_2_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean a8_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void a8_reset() {
    a8_computed = false;
    a8_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a8_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected int a8_value;

  /**
   * @attribute syn
   * @aspect Test120
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:37
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test120", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:37")
  @SideEffect.Pure(group="_ASTNode") public int a8() {
    ASTState state = state();
    if (a8_computed) {
      return a8_value;
    }
    if (a8_visited) {
      throw new RuntimeException("Circular definition of attribute A.a8().");
    }
    a8_visited = true;
    state().enterLazyAttribute();
    a8_value = b8_1() + b8_2();
    a8_computed = true;
    state().leaveLazyAttribute();
    a8_visited = false;
    return a8_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean b8_1_visited = false;
  /**
   * @attribute syn
   * @aspect Test120
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:38
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test120", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:38")
  @SideEffect.Pure(group="_ASTNode") public int b8_1() {
    if (b8_1_visited) {
      throw new RuntimeException("Circular definition of attribute A.b8_1().");
    }
    b8_1_visited = true;
    int b8_1_value = c8_1() + c8_2();
    b8_1_visited = false;
    return b8_1_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean b8_2_visited = false;
  /**
   * @attribute syn
   * @aspect Test120
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:39
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test120", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:39")
  @SideEffect.Pure(group="_ASTNode") public int b8_2() {
    if (b8_2_visited) {
      throw new RuntimeException("Circular definition of attribute A.b8_2().");
    }
    b8_2_visited = true;
    int b8_2_value = 1;
    b8_2_visited = false;
    return b8_2_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean c8_1_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void c8_1_reset() {
    c8_1_computed = false;
    c8_1_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c8_1_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected int c8_1_value;

  /**
   * @attribute syn
   * @aspect Test120
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:40
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test120", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:40")
  @SideEffect.Pure(group="_ASTNode") public int c8_1() {
    ASTState state = state();
    if (c8_1_computed) {
      return c8_1_value;
    }
    if (c8_1_visited) {
      throw new RuntimeException("Circular definition of attribute A.c8_1().");
    }
    c8_1_visited = true;
    state().enterLazyAttribute();
    c8_1_value = 1;
    c8_1_computed = true;
    state().leaveLazyAttribute();
    c8_1_visited = false;
    return c8_1_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean c8_2_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void c8_2_reset() {
    c8_2_computed = false;
    c8_2_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c8_2_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected int c8_2_value;

  /**
   * @attribute syn
   * @aspect Test120
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:41
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test120", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test120\\Test120.jrag:41")
  @SideEffect.Pure(group="_ASTNode") public int c8_2() {
    ASTState state = state();
    if (c8_2_computed) {
      return c8_2_value;
    }
    if (c8_2_visited) {
      throw new RuntimeException("Circular definition of attribute A.c8_2().");
    }
    c8_2_visited = true;
    state().enterLazyAttribute();
    c8_2_value = 1;
    c8_2_computed = true;
    state().leaveLazyAttribute();
    c8_2_visited = false;
    return c8_2_value;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
