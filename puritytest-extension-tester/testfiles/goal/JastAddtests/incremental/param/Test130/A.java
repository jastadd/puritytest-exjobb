/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\incremental\param\Test130;
import java.util.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test130\\Test.ast:1
 * @astdecl A : ASTNode ::= <Name:String>;
 * @production A : {@link ASTNode} ::= <span class="component">&lt;Name:String&gt;</span>;

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public A() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /**
   * @declaredat ASTNode:12
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Name"},
    type = {"String"},
    kind = {"Token"}
  )
  public A(String p0) {
    setName(p0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:21
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:27
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:31
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    a_reset();
    b_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:41
   */
  @SideEffect.Ignore @SideEffect.Fresh public A clone() throws CloneNotSupportedException {
    A node = (A) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:46
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public A copy() {
    try {
      A node = (A) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:65
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public A fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:75
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopyNoTransform() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:95
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopy() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:109
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_Name == ((A) node).tokenString_Name);    
  }
  /**
   * Replaces the lexeme Name.
   * @param value The new value for the lexeme Name.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setName(String value) {
    tokenString_Name = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_Name;
  /**
   * Retrieves the value for the lexeme Name.
   * @return The value for the lexeme Name.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Name")
  @SideEffect.Pure(group="_ASTNode") public String getName() {
    return tokenString_Name != null ? tokenString_Name : "";
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle a_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void a_reset() {
    a_computed = false;
    a_initialized = false;
    a_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected int a_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test130", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test130\\Test130.jrag:5")
  @SideEffect.Pure(group="_ASTNode") public int a() {
    if (a_computed) {
      return a_value;
    }
    ASTState state = state();
    if (!a_initialized) {
      a_initialized = true;
      a_value = 0;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        a_cycle = state.nextCycle();
        int new_a_value = b();
        if (a_value != new_a_value) {
          state.setChangeInCycle();
        }
        a_value = new_a_value;
      } while (state.testAndClearChangeInCycle());
      a_computed = true;
      state.startLastCycle();
      int $tmp = b();

      state.leaveCircle();
    } else if (a_cycle != state.cycle()) {
      a_cycle = state.cycle();
      if (state.lastCycle()) {
        a_computed = true;
        int new_a_value = b();
        return new_a_value;
      }
      int new_a_value = b();
      if (a_value != new_a_value) {
        state.setChangeInCycle();
      }
      a_value = new_a_value;
    } else {
    }
    return a_value;
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle b_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void b_reset() {
    b_computed = false;
    b_initialized = false;
    b_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean b_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected int b_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean b_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test130", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\param\\Test130\\Test130.jrag:6")
  @SideEffect.Pure(group="_ASTNode") public int b() {
    if (b_computed) {
      return b_value;
    }
    ASTState state = state();
    if (!b_initialized) {
      b_initialized = true;
      b_value = 0;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        b_cycle = state.nextCycle();
        int new_b_value = (a() == 0 && getName().equals("a")) ? 1 : 1;
        if (b_value != new_b_value) {
          state.setChangeInCycle();
        }
        b_value = new_b_value;
      } while (state.testAndClearChangeInCycle());
      b_computed = true;
      state.startLastCycle();
      int $tmp = (a() == 0 && getName().equals("a")) ? 1 : 1;

      state.leaveCircle();
    } else if (b_cycle != state.cycle()) {
      b_cycle = state.cycle();
      if (state.lastCycle()) {
        b_computed = true;
        int new_b_value = (a() == 0 && getName().equals("a")) ? 1 : 1;
        return new_b_value;
      }
      int new_b_value = (a() == 0 && getName().equals("a")) ? 1 : 1;
      if (b_value != new_b_value) {
        state.setChangeInCycle();
      }
      b_value = new_b_value;
    } else {
    }
    return b_value;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
