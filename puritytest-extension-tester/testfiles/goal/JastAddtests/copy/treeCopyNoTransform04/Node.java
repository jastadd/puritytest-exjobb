/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\copy\treeCopyNoTransform04;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\copy\\treeCopyNoTransform04\\Test.ast:1
 * @astdecl Node : ASTNode ::= X*;
 * @production Node : {@link ASTNode} ::= <span class="component">{@link X}*</span>;

 */
public class Node extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public Node() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
    setChild(new List(), 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:15
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:21
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:25
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    getXList_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:30
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:34
   */
  @SideEffect.Ignore @SideEffect.Fresh public Node clone() throws CloneNotSupportedException {
    Node node = (Node) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:39
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public Node copy() {
    try {
      Node node = (Node) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:58
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Node fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:68
   */
  @SideEffect.Fresh(group="_ASTNode") public Node treeCopyNoTransform() {
    Node tree = (Node) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        switch (i) {
        case 0:
          tree.children[i] = new List();
          continue;
        }
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:93
   */
  @SideEffect.Fresh(group="_ASTNode") public Node treeCopy() {
    Node tree = (Node) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        switch (i) {
        case 0:
          tree.children[i] = new List();
          continue;
        }
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:112
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Retrieves the number of children in the X list.
   * @return Number of children in the X list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumX() {
    return getXList().getNumChild();
  }
  /**
   * Retrieves the number of children in the X list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the X list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumXNoTransform() {
    return getXListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the X list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the X list.
   * @apilevel high-level
   */
  @SideEffect.Pure public X getX(int i) {
    return (X) getXList().getChild(i);
  }
  /**
   * Check whether the X list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasX() {
    return getXList().getNumChild() != 0;
  }
  /**
   * Append an element to the X list.
   * @param node The element to append to the X list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addX(X node) {
    List<X> list = (parent == null) ? getXListNoTransform() : getXList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addXNoTransform(X node) {
    List<X> list = getXListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the X list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setX(X node, int i) {
    List<X> list = getXList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the child position of the X list.
   * @return The the child position of the X list.
   * @apilevel low-level
   */
  @SideEffect.Pure protected int getXListChildPosition() {
    return 0;
  }
  /**
   * Retrieves the X list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the X list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<X> getXListNoTransform() {
    return (List<X>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the X list without
   * triggering rewrites.
   */
  @SideEffect.Pure public X getXNoTransform(int i) {
    return (X) getXListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the X list.
   * @return The node representing the X list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<X> getXs() {
    return getXList();
  }
  /**
   * Retrieves the X list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the X list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<X> getXsNoTransform() {
    return getXListNoTransform();
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean getXList_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void getXList_reset() {
    getXList_computed = false;
    
    getXList_value = null;
    getXList_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean getXList_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected List<X> getXList_value;

  /**
   * @attribute syn nta
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\copy\\treeCopyNoTransform04\\Test.jrag:2
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\copy\\treeCopyNoTransform04\\Test.jrag:2")
  @SideEffect.Pure(group="_ASTNode") public List<X> getXList() {
    ASTState state = state();
    if (getXList_computed) {
      return (List<X>) getChild(getXListChildPosition());
    }
    if (getXList_visited) {
      throw new RuntimeException("Circular definition of attribute Node.getXList().");
    }
    getXList_visited = true;
    state().enterLazyAttribute();
    getXList_value = new List<X>();
    setChild(getXList_value, getXListChildPosition());
    getXList_computed = true;
    state().leaveLazyAttribute();
    getXList_visited = false;
    List<X> node = (List<X>) this.getChild(getXListChildPosition());
    return node;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
