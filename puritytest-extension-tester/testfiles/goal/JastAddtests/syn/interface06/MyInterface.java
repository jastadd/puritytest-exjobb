package testfiles/gen/JastAddtests\syn\interface06;

/**
 * @ast interface
 * @aspect InterfaceOrder
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\syn\\interface06\\Test.jrag:4
 */
public interface MyInterface {
  /**
   * @attribute syn
   * @aspect InterfaceOrder
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\syn\\interface06\\Test.jrag:2
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="InterfaceOrder", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\syn\\interface06\\Test.jrag:2")
  @SideEffect.Pure(group="_ASTNode") public int myValue1();
  /**
   * @attribute syn
   * @aspect InterfaceOrder
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\syn\\interface06\\Test.jrag:7
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="InterfaceOrder", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\syn\\interface06\\Test.jrag:7")
  @SideEffect.Pure(group="_ASTNode") public int myValue2();
}
