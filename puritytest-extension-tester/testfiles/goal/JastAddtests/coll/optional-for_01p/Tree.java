/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\coll\optional-for_01p;
import java.util.LinkedList;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\optional-for_01p\\Test.ast:1
 * @astdecl Tree : ASTNode ::= Node;
 * @production Tree : {@link ASTNode} ::= <span class="component">{@link Node}</span>;

 */
public class Tree extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public Tree() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
  }
  /**
   * @declaredat ASTNode:13
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Node"},
    type = {"Node"},
    kind = {"Child"}
  )
  public Tree(Node p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:22
   */
  @SideEffect.Pure protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:28
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:32
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:36
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
    Tree_leaves_visited = false;
    Tree_leaves_computed = false;
    
    Tree_leaves_value = null;
    contributorMap_Tree_leaves = null;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:45
   */
  @SideEffect.Ignore @SideEffect.Fresh public Tree clone() throws CloneNotSupportedException {
    Tree node = (Tree) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:50
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public Tree copy() {
    try {
      Tree node = (Tree) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:69
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Tree fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:79
   */
  @SideEffect.Fresh(group="_ASTNode") public Tree treeCopyNoTransform() {
    Tree tree = (Tree) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:99
   */
  @SideEffect.Fresh(group="_ASTNode") public Tree treeCopy() {
    Tree tree = (Tree) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:113
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the Node child.
   * @param node The new node to replace the Node child.
   * @apilevel high-level
   */
  public void setNode(Node node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the Node child.
   * @return The current node used as the Node child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Node")
  @SideEffect.Pure public Node getNode() {
    return (Node) getChild(0);
  }
  /**
   * Retrieves the Node child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Node child.
   * @apilevel low-level
   */
  @SideEffect.Pure public Node getNodeNoTransform() {
    return (Node) getChildNoTransform(0);
  }
  /**
   * @aspect <NoAspect>
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\optional-for_01p\\Test.jrag:4
   */
  /** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map<ASTNode, java.util.Set<ASTNode>> contributorMap_Tree_leaves = null;

  /** @apilevel internal */
  @SideEffect.Ignore protected void survey_Tree_leaves() {
    if (contributorMap_Tree_leaves == null) {
      contributorMap_Tree_leaves = new java.util.IdentityHashMap<ASTNode, java.util.Set<ASTNode>>();
      collect_contributors_Tree_leaves(this, contributorMap_Tree_leaves);
    }
  }

  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean Tree_leaves_visited = false;
  /**
   * @attribute coll
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\optional-for_01p\\Test.jrag:4
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.COLL)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\optional-for_01p\\Test.jrag:4")
  @SideEffect.Pure(group="_ASTNode") public LinkedList<Leaf> leaves() {
    ASTState state = state();
    if (Tree_leaves_computed) {
      return Tree_leaves_value;
    }
    if (Tree_leaves_visited) {
      throw new RuntimeException("Circular definition of attribute Tree.leaves().");
    }
    Tree_leaves_visited = true;
    state().enterLazyAttribute();
    Tree_leaves_value = leaves_compute();
    Tree_leaves_computed = true;
    state().leaveLazyAttribute();
    Tree_leaves_visited = false;
    return Tree_leaves_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure(group="_ASTNode") private LinkedList<Leaf> leaves_compute() {
    ASTNode node = this;
    while (node != null && !(node instanceof Tree)) {
      node = node.getParent();
    }
    Tree root = (Tree) node;
    root.survey_Tree_leaves();
    LinkedList<Leaf> _computedValue = new LinkedList<Leaf>();
    if (root.contributorMap_Tree_leaves.containsKey(this)) {
          for (ASTNode contributor : root.contributorMap_Tree_leaves.get(this)) {
            contributor.contributeTo_Tree_leaves(_computedValue);
          }
    }
    return _computedValue;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean Tree_leaves_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected LinkedList<Leaf> Tree_leaves_value;

}
