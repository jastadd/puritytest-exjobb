/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\coll\cache_03p;
import java.util.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\cache_03p\\Test.ast:2
 * @astdecl B : ASTNode;
 * @production B : {@link ASTNode};

 */
public class B extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public B() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:19
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:23
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:27
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
    B_parent_visited = false;
    B_parent_computed = false;
    
    B_parent_value = null;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:35
   */
  @SideEffect.Ignore @SideEffect.Fresh public B clone() throws CloneNotSupportedException {
    B node = (B) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:40
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public B copy() {
    try {
      B node = (B) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:59
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public B fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:69
   */
  @SideEffect.Fresh(group="_ASTNode") public B treeCopyNoTransform() {
    B tree = (B) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:89
   */
  @SideEffect.Fresh(group="_ASTNode") public B treeCopy() {
    B tree = (B) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:103
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * @attribute inh
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\cache_03p\\Test.jrag:4
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\cache_03p\\Test.jrag:4")
  @SideEffect.Pure(group="_ASTNode") public A a() {
    if (a_visited) {
      throw new RuntimeException("Circular definition of attribute B.a().");
    }
    a_visited = true;
    A a_value = getParent().Define_a(this, null);
    a_visited = false;
    return a_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean a_visited = false;
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean B_parent_visited = false;
  /**
   * @attribute coll
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\cache_03p\\Test.jrag:8
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.COLL)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\cache_03p\\Test.jrag:8")
  @SideEffect.Pure(group="_ASTNode") public Collection<A> parent() {
    ASTState state = state();
    if (B_parent_computed) {
      return B_parent_value;
    }
    if (B_parent_visited) {
      throw new RuntimeException("Circular definition of attribute B.parent().");
    }
    B_parent_visited = true;
    state().enterLazyAttribute();
    B_parent_value = parent_compute();
    B_parent_computed = true;
    state().leaveLazyAttribute();
    B_parent_visited = false;
    return B_parent_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure(group="_ASTNode") private Collection<A> parent_compute() {
    ASTNode node = this;
    while (node != null && !(node instanceof A)) {
      node = node.getParent();
    }
    A root = (A) node;
    root.survey_B_parent();
    Collection<A> _computedValue = new HashSet<A>();
    if (root.contributorMap_B_parent.containsKey(this)) {
          for (ASTNode contributor : root.contributorMap_B_parent.get(this)) {
            contributor.contributeTo_B_parent(_computedValue);
          }
    }
    return _computedValue;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean B_parent_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Collection<A> B_parent_value;

}
