/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\coll\no-root02;
import java.util.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\no-root02\\Test.ast:1
 * @astdecl Node : ASTNode ::= Node;
 * @production Node : {@link ASTNode} ::= <span class="component">{@link Node}</span>;

 */
public class Node extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public Node() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
  }
  /**
   * @declaredat ASTNode:13
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Node"},
    type = {"Node"},
    kind = {"Child"}
  )
  public Node(Node p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:22
   */
  @SideEffect.Pure protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:28
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:32
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:36
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
    Node_self_visited = false;
    Node_self_computed = false;
    
    Node_self_value = null;
    contributorMap_Node_self = null;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:45
   */
  @SideEffect.Ignore @SideEffect.Fresh public Node clone() throws CloneNotSupportedException {
    Node node = (Node) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:50
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public Node copy() {
    try {
      Node node = (Node) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:69
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Node fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:79
   */
  @SideEffect.Fresh(group="_ASTNode") public Node treeCopyNoTransform() {
    Node tree = (Node) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:99
   */
  @SideEffect.Fresh(group="_ASTNode") public Node treeCopy() {
    Node tree = (Node) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:113
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the Node child.
   * @param node The new node to replace the Node child.
   * @apilevel high-level
   */
  public void setNode(Node node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the Node child.
   * @return The current node used as the Node child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Node")
  @SideEffect.Pure public Node getNode() {
    return (Node) getChild(0);
  }
  /**
   * Retrieves the Node child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Node child.
   * @apilevel low-level
   */
  @SideEffect.Pure public Node getNodeNoTransform() {
    return (Node) getChildNoTransform(0);
  }
  /**
   * @aspect <NoAspect>
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\no-root02\\Test.jrag:7
   */
  /** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map<ASTNode, java.util.Set<ASTNode>> contributorMap_Node_self = null;

  /** @apilevel internal */
  @SideEffect.Ignore protected void survey_Node_self() {
    if (contributorMap_Node_self == null) {
      contributorMap_Node_self = new java.util.IdentityHashMap<ASTNode, java.util.Set<ASTNode>>();
      collect_contributors_Node_self(this, contributorMap_Node_self);
    }
  }

  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean Node_self_visited = false;
  /**
   * Super simple collection attribute that only collects one node.
   * @attribute coll
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\no-root02\\Test.jrag:7
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.COLL)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\no-root02\\Test.jrag:7")
  @SideEffect.Pure(group="_ASTNode") public Collection<Node> self() {
    ASTState state = state();
    if (Node_self_computed) {
      return Node_self_value;
    }
    if (Node_self_visited) {
      throw new RuntimeException("Circular definition of attribute Node.self().");
    }
    Node_self_visited = true;
    state().enterLazyAttribute();
    Node_self_value = self_compute();
    Node_self_computed = true;
    state().leaveLazyAttribute();
    Node_self_visited = false;
    return Node_self_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure(group="_ASTNode") private Collection<Node> self_compute() {
    ASTNode node = this;
    while (node != null && !(node instanceof Node)) {
      node = node.getParent();
    }
    Node root = (Node) node;
    root.survey_Node_self();
    Collection<Node> _computedValue = new HashSet<Node>();
    if (root.contributorMap_Node_self.containsKey(this)) {
          for (ASTNode contributor : root.contributorMap_Node_self.get(this)) {
            contributor.contributeTo_Node_self(_computedValue);
          }
    }
    return _computedValue;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean Node_self_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Collection<Node> Node_self_value;

  /** @apilevel internal */
  @SideEffect.Local(group="_ASTNode") protected void collect_contributors_Node_self(Node _root, @SideEffect.Ignore java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\no-root02\\Test.jrag:9
    if (true) {
      {
        Node target = (Node) (this);
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_Node_self(_root, _map);
  }
  /** @apilevel internal */
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_Node_self(Collection<Node> collection) {
    super.contributeTo_Node_self(collection);
    if (true) {
      collection.add(this);
    }
  }
}
