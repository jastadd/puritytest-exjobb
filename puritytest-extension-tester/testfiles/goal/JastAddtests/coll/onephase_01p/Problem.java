package testfiles/gen/JastAddtests\coll\onephase_01p;

import java.util.Collection;
import java.util.LinkedList;
/**
 * @ast class
 * @aspect Test
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\onephase_01p\\Test.jrag:6
 */
 class Problem extends java.lang.Object {
  
    public final String message;

  
    public Problem(String message) {
      this.message = message;
    }


}
