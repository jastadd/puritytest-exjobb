/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\coll\multi-target_01p;
import java.util.LinkedList;
import java.util.Collection;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\multi-target_01p\\Test.ast:3
 * @astdecl Multi : Node ::= Node*;
 * @production Multi : {@link Node} ::= <span class="component">{@link Node}*</span>;

 */
public class Multi extends Node implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public Multi() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
    setChild(new List(), 0);
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Node"},
    type = {"List<Node>"},
    kind = {"List"}
  )
  public Multi(List<Node> p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:23
   */
  @SideEffect.Pure protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:29
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:41
   */
  @SideEffect.Ignore @SideEffect.Fresh public Multi clone() throws CloneNotSupportedException {
    Multi node = (Multi) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:46
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public Multi copy() {
    try {
      Multi node = (Multi) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:65
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Multi fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:75
   */
  @SideEffect.Fresh(group="_ASTNode") public Multi treeCopyNoTransform() {
    Multi tree = (Multi) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:95
   */
  @SideEffect.Fresh(group="_ASTNode") public Multi treeCopy() {
    Multi tree = (Multi) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:109
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the Node list.
   * @param list The new list node to be used as the Node list.
   * @apilevel high-level
   */
  public void setNodeList(List<Node> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the Node list.
   * @return Number of children in the Node list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumNode() {
    return getNodeList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Node list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Node list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumNodeNoTransform() {
    return getNodeListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Node list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Node list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Node getNode(int i) {
    return (Node) getNodeList().getChild(i);
  }
  /**
   * Check whether the Node list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasNode() {
    return getNodeList().getNumChild() != 0;
  }
  /**
   * Append an element to the Node list.
   * @param node The element to append to the Node list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addNode(Node node) {
    List<Node> list = (parent == null) ? getNodeListNoTransform() : getNodeList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addNodeNoTransform(Node node) {
    List<Node> list = getNodeListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Node list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setNode(Node node, int i) {
    List<Node> list = getNodeList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Node list.
   * @return The node representing the Node list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Node")
  @SideEffect.Pure(group="_ASTNode") @SideEffect.FreshIf public List<Node> getNodeList() {
    List<Node> list = (List<Node>) getChild(0);
    return list;
  }
  /**
   * Retrieves the Node list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Node list.
   * @apilevel low-level
   */
  @SideEffect.Pure @SideEffect.FreshIf public List<Node> getNodeListNoTransform() {
    return (List<Node>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the Node list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Node getNodeNoTransform(int i) {
    return (Node) getNodeListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Node list.
   * @return The node representing the Node list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Node> getNodes() {
    return getNodeList();
  }
  /**
   * Retrieves the Node list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Node list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Node> getNodesNoTransform() {
    return getNodeListNoTransform();
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
