package testfiles/gen/JastAddtests\coll\lazycondition_02p;

import java.util.Collection;
import java.util.LinkedList;
/**
 * @ast class
 * @aspect Test
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\lazycondition_02p\\Test.jrag:6
 */
 class Problem extends java.lang.Object {
  
    public final String message;

  
    public Problem(String message) {
      this.message = message;
    }


}
