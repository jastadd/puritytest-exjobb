/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\jjtree\init_children01;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\jjtree\\init_children01\\Test.ast:1
 * @astdecl Program : ASTNode ::= X*;
 * @production Program : {@link ASTNode} ::= <span class="component">{@link X}*</span>;

 */
public class Program extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public Program() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
    setChild(new List(), 0);
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"X"},
    type = {"List<X>"},
    kind = {"List"}
  )
  public Program(List<X> p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:23
   */
  @SideEffect.Pure protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:29
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:41
   */
  @SideEffect.Ignore @SideEffect.Fresh public Program clone() throws CloneNotSupportedException {
    Program node = (Program) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:46
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public Program copy() {
    try {
      Program node = (Program) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:65
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Program fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:75
   */
  @SideEffect.Fresh(group="_ASTNode") public Program treeCopyNoTransform() {
    Program tree = (Program) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:95
   */
  @SideEffect.Fresh(group="_ASTNode") public Program treeCopy() {
    Program tree = (Program) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:109
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the X list.
   * @param list The new list node to be used as the X list.
   * @apilevel high-level
   */
  public void setXList(List<X> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the X list.
   * @return Number of children in the X list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumX() {
    return getXList().getNumChild();
  }
  /**
   * Retrieves the number of children in the X list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the X list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumXNoTransform() {
    return getXListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the X list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the X list.
   * @apilevel high-level
   */
  @SideEffect.Pure public X getX(int i) {
    return (X) getXList().getChild(i);
  }
  /**
   * Check whether the X list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasX() {
    return getXList().getNumChild() != 0;
  }
  /**
   * Append an element to the X list.
   * @param node The element to append to the X list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addX(X node) {
    List<X> list = (parent == null) ? getXListNoTransform() : getXList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addXNoTransform(X node) {
    List<X> list = getXListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the X list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setX(X node, int i) {
    List<X> list = getXList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the X list.
   * @return The node representing the X list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="X")
  @SideEffect.Pure(group="_ASTNode") @SideEffect.FreshIf public List<X> getXList() {
    List<X> list = (List<X>) getChild(0);
    return list;
  }
  /**
   * Retrieves the X list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the X list.
   * @apilevel low-level
   */
  @SideEffect.Pure @SideEffect.FreshIf public List<X> getXListNoTransform() {
    return (List<X>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the X list without
   * triggering rewrites.
   */
  @SideEffect.Pure public X getXNoTransform(int i) {
    return (X) getXListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the X list.
   * @return The node representing the X list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<X> getXs() {
    return getXList();
  }
  /**
   * Retrieves the X list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the X list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<X> getXsNoTransform() {
    return getXListNoTransform();
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
