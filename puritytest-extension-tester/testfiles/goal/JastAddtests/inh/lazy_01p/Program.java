/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\inh\lazy_01p;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\inh\\lazy_01p\\Test.ast:1
 * @astdecl Program : ASTNode ::= Package*;
 * @production Program : {@link ASTNode} ::= <span class="component">{@link Package}*</span>;

 */
public class Program extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\inh\\lazy_01p\\Test.jrag:2
   */
  public int lazyEvalCount = 0;
  /**
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\inh\\lazy_01p\\Test.jrag:9
   */
  public int nonLazyEvalCount = 0;
  /**
   * @declaredat ASTNode:1
   */
  public Program() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
    setChild(new List(), 0);
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Package"},
    type = {"List<Package>"},
    kind = {"List"}
  )
  public Program(List<Package> p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:23
   */
  @SideEffect.Pure protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:29
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:41
   */
  @SideEffect.Ignore @SideEffect.Fresh public Program clone() throws CloneNotSupportedException {
    Program node = (Program) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:46
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public Program copy() {
    try {
      Program node = (Program) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:65
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Program fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:75
   */
  @SideEffect.Fresh(group="_ASTNode") public Program treeCopyNoTransform() {
    Program tree = (Program) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:95
   */
  @SideEffect.Fresh(group="_ASTNode") public Program treeCopy() {
    Program tree = (Program) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:109
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the Package list.
   * @param list The new list node to be used as the Package list.
   * @apilevel high-level
   */
  public void setPackageList(List<Package> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the Package list.
   * @return Number of children in the Package list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumPackage() {
    return getPackageList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Package list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Package list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumPackageNoTransform() {
    return getPackageListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Package list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Package list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Package getPackage(int i) {
    return (Package) getPackageList().getChild(i);
  }
  /**
   * Check whether the Package list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasPackage() {
    return getPackageList().getNumChild() != 0;
  }
  /**
   * Append an element to the Package list.
   * @param node The element to append to the Package list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addPackage(Package node) {
    List<Package> list = (parent == null) ? getPackageListNoTransform() : getPackageList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addPackageNoTransform(Package node) {
    List<Package> list = getPackageListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Package list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setPackage(Package node, int i) {
    List<Package> list = getPackageList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Package list.
   * @return The node representing the Package list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Package")
  @SideEffect.Pure(group="_ASTNode") @SideEffect.FreshIf public List<Package> getPackageList() {
    List<Package> list = (List<Package>) getChild(0);
    return list;
  }
  /**
   * Retrieves the Package list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Package list.
   * @apilevel low-level
   */
  @SideEffect.Pure @SideEffect.FreshIf public List<Package> getPackageListNoTransform() {
    return (List<Package>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the Package list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Package getPackageNoTransform(int i) {
    return (Package) getPackageListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Package list.
   * @return The node representing the Package list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Package> getPackages() {
    return getPackageList();
  }
  /**
   * Retrieves the Package list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Package list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Package> getPackagesNoTransform() {
    return getPackageListNoTransform();
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\inh\\lazy_01p\\Test.jrag:3
   * @apilevel internal
   */
 @SideEffect.Pure(group="_ASTNode") public int Define_lazyAttr(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getPackageListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\inh\\lazy_01p\\Test.jrag:4
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      {
              lazyEvalCount += 1;
              return 0;
          }
    }
    else {
      return getParent().Define_lazyAttr(this, _callerNode);
    }
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\inh\\lazy_01p\\Test.jrag:3
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute lazyAttr
   */
  @SideEffect.Pure protected boolean canDefine_lazyAttr(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\inh\\lazy_01p\\Test.jrag:10
   * @apilevel internal
   */
 @SideEffect.Pure(group="_ASTNode") public int Define_nonLazyAttr(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getPackageListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\inh\\lazy_01p\\Test.jrag:11
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      {
              nonLazyEvalCount += 1;
              return 0;
          }
    }
    else {
      return getParent().Define_nonLazyAttr(this, _callerNode);
    }
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\inh\\lazy_01p\\Test.jrag:10
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute nonLazyAttr
   */
  @SideEffect.Pure protected boolean canDefine_nonLazyAttr(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
