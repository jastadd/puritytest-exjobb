/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\circular\test04;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test04\\Test.ast:1
 * @astdecl A : ASTNode;
 * @production A : {@link ASTNode};

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public A() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:19
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:23
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    u_reset();
    v_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:29
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  @SideEffect.Ignore @SideEffect.Fresh public A clone() throws CloneNotSupportedException {
    A node = (A) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public A copy() {
    try {
      A node = (A) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:57
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public A fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:67
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopyNoTransform() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:87
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopy() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:101
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle u_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void u_reset() {
    u_computed = false;
    u_initialized = false;
    u_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean u_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean u_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean u_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test04\\Test.jrag:2")
  @SideEffect.Pure(group="_ASTNode") public boolean u() {
    if (u_computed) {
      return u_value;
    }
    ASTState state = state();
    if (!u_initialized) {
      u_initialized = true;
      u_value = false;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        u_cycle = state.nextCycle();
        boolean new_u_value = v();
        if (u_value != new_u_value) {
          state.setChangeInCycle();
        }
        u_value = new_u_value;
      } while (state.testAndClearChangeInCycle());
      u_computed = true;
      state.startLastCycle();
      boolean $tmp = v();

      state.leaveCircle();
    } else if (u_cycle != state.cycle()) {
      u_cycle = state.cycle();
      if (state.lastCycle()) {
        u_computed = true;
        boolean new_u_value = v();
        return new_u_value;
      }
      boolean new_u_value = v();
      if (u_value != new_u_value) {
        state.setChangeInCycle();
      }
      u_value = new_u_value;
    } else {
    }
    return u_value;
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle v_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void v_reset() {
    v_computed = false;
    v_initialized = false;
    v_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean v_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean v_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean v_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test04\\Test.jrag:3")
  @SideEffect.Pure(group="_ASTNode") public boolean v() {
    if (v_computed) {
      return v_value;
    }
    ASTState state = state();
    if (!v_initialized) {
      v_initialized = true;
      v_value = false;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        v_cycle = state.nextCycle();
        boolean new_v_value = u();
        if (v_value != new_v_value) {
          state.setChangeInCycle();
        }
        v_value = new_v_value;
      } while (state.testAndClearChangeInCycle());
      v_computed = true;
      state.startLastCycle();
      boolean $tmp = u();

      state.leaveCircle();
    } else if (v_cycle != state.cycle()) {
      v_cycle = state.cycle();
      if (state.lastCycle()) {
        v_computed = true;
        boolean new_v_value = u();
        return new_v_value;
      }
      boolean new_v_value = u();
      if (v_value != new_v_value) {
        state.setChangeInCycle();
      }
      v_value = new_v_value;
    } else {
    }
    return v_value;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
