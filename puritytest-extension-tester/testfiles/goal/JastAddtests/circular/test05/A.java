/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\circular\test05;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test05\\Test.ast:1
 * @astdecl A : ASTNode;
 * @production A : {@link ASTNode};

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public A() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:19
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:23
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    x_reset();
    y_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:29
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  @SideEffect.Ignore @SideEffect.Fresh public A clone() throws CloneNotSupportedException {
    A node = (A) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public A copy() {
    try {
      A node = (A) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:57
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public A fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:67
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopyNoTransform() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:87
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopy() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:101
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle x_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void x_reset() {
    x_computed = false;
    x_initialized = false;
    x_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean x_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean x_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean x_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test05\\Test.jrag:2")
  @SideEffect.Pure(group="_ASTNode") public boolean x() {
    if (x_computed) {
      return x_value;
    }
    ASTState state = state();
    if (!x_initialized) {
      x_initialized = true;
      x_value = false;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        x_cycle = state.nextCycle();
        boolean new_x_value = y();
        if (x_value != new_x_value) {
          state.setChangeInCycle();
        }
        x_value = new_x_value;
      } while (state.testAndClearChangeInCycle());
      x_computed = true;
      state.startLastCycle();
      boolean $tmp = y();

      state.leaveCircle();
    } else if (x_cycle != state.cycle()) {
      x_cycle = state.cycle();
      if (state.lastCycle()) {
        x_computed = true;
        boolean new_x_value = y();
        return new_x_value;
      }
      boolean new_x_value = y();
      if (x_value != new_x_value) {
        state.setChangeInCycle();
      }
      x_value = new_x_value;
    } else {
    }
    return x_value;
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle y_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void y_reset() {
    y_computed = false;
    y_initialized = false;
    y_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean y_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean y_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean y_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test05\\Test.jrag:3")
  @SideEffect.Pure(group="_ASTNode") public boolean y() {
    if (y_computed) {
      return y_value;
    }
    ASTState state = state();
    if (!y_initialized) {
      y_initialized = true;
      y_value = false;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        y_cycle = state.nextCycle();
        boolean new_y_value = x() || true;
        if (y_value != new_y_value) {
          state.setChangeInCycle();
        }
        y_value = new_y_value;
      } while (state.testAndClearChangeInCycle());
      y_computed = true;
      state.startLastCycle();
      boolean $tmp = x() || true;

      state.leaveCircle();
    } else if (y_cycle != state.cycle()) {
      y_cycle = state.cycle();
      if (state.lastCycle()) {
        y_computed = true;
        boolean new_y_value = x() || true;
        return new_y_value;
      }
      boolean new_y_value = x() || true;
      if (y_value != new_y_value) {
        state.setChangeInCycle();
      }
      y_value = new_y_value;
    } else {
    }
    return y_value;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
