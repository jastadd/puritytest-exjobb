/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\circular\test02;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test02\\Test.ast:1
 * @astdecl A : ASTNode ::= B;
 * @production A : {@link ASTNode} ::= <span class="component">{@link B}</span>;

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public A() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
  }
  /**
   * @declaredat ASTNode:13
   */
  @ASTNodeAnnotation.Constructor(
    name = {"B"},
    type = {"B"},
    kind = {"Child"}
  )
  public A(B p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:22
   */
  @SideEffect.Pure protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:28
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:32
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    a_reset();
    b_reset();
    c_reset();
    d_reset();
    e_reset();
    f_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:42
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:46
   */
  @SideEffect.Ignore @SideEffect.Fresh public A clone() throws CloneNotSupportedException {
    A node = (A) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:51
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public A copy() {
    try {
      A node = (A) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:70
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public A fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:80
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopyNoTransform() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:100
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopy() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:114
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the B child.
   * @param node The new node to replace the B child.
   * @apilevel high-level
   */
  public void setB(B node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the B child.
   * @return The current node used as the B child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="B")
  @SideEffect.Pure public B getB() {
    return (B) getChild(0);
  }
  /**
   * Retrieves the B child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the B child.
   * @apilevel low-level
   */
  @SideEffect.Pure public B getBNoTransform() {
    return (B) getChildNoTransform(0);
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle a_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void a_reset() {
    a_computed = false;
    a_initialized = false;
    a_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test02\\Test.jrag:2")
  @SideEffect.Pure(group="_ASTNode") public boolean a() {
    if (a_computed) {
      return a_value;
    }
    ASTState state = state();
    if (!a_initialized) {
      a_initialized = true;
      a_value = false;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        a_cycle = state.nextCycle();
        boolean new_a_value = true;
        if (a_value != new_a_value) {
          state.setChangeInCycle();
        }
        a_value = new_a_value;
      } while (state.testAndClearChangeInCycle());
      a_computed = true;
      state.startLastCycle();
      boolean $tmp = true;

      state.leaveCircle();
    } else if (a_cycle != state.cycle()) {
      a_cycle = state.cycle();
      if (state.lastCycle()) {
        a_computed = true;
        boolean new_a_value = true;
        return new_a_value;
      }
      boolean new_a_value = true;
      if (a_value != new_a_value) {
        state.setChangeInCycle();
      }
      a_value = new_a_value;
    } else {
    }
    return a_value;
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle b_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void b_reset() {
    b_computed = false;
    b_initialized = false;
    b_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean b_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean b_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean b_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test02\\Test.jrag:3")
  @SideEffect.Pure(group="_ASTNode") public boolean b() {
    if (b_computed) {
      return b_value;
    }
    ASTState state = state();
    if (!b_initialized) {
      b_initialized = true;
      b_value = false;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        b_cycle = state.nextCycle();
        boolean new_b_value = true;
        if (b_value != new_b_value) {
          state.setChangeInCycle();
        }
        b_value = new_b_value;
      } while (state.testAndClearChangeInCycle());
      b_computed = true;
      state.startLastCycle();
      boolean $tmp = true;

      state.leaveCircle();
    } else if (b_cycle != state.cycle()) {
      b_cycle = state.cycle();
      if (state.lastCycle()) {
        b_computed = true;
        boolean new_b_value = true;
        return new_b_value;
      }
      boolean new_b_value = true;
      if (b_value != new_b_value) {
        state.setChangeInCycle();
      }
      b_value = new_b_value;
    } else {
    }
    return b_value;
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle c_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void c_reset() {
    c_computed = false;
    c_initialized = false;
    c_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test02\\Test.jrag:4")
  @SideEffect.Pure(group="_ASTNode") public boolean c() {
    if (c_computed) {
      return c_value;
    }
    ASTState state = state();
    if (!c_initialized) {
      c_initialized = true;
      c_value = false;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        c_cycle = state.nextCycle();
        boolean new_c_value = c_compute();
        if (c_value != new_c_value) {
          state.setChangeInCycle();
        }
        c_value = new_c_value;
      } while (state.testAndClearChangeInCycle());
      c_computed = true;
      state.startLastCycle();
      boolean $tmp = c_compute();

      state.leaveCircle();
    } else if (c_cycle != state.cycle()) {
      c_cycle = state.cycle();
      if (state.lastCycle()) {
        c_computed = true;
        boolean new_c_value = c_compute();
        return new_c_value;
      }
      boolean new_c_value = c_compute();
      if (c_value != new_c_value) {
        state.setChangeInCycle();
      }
      c_value = new_c_value;
    } else {
    }
    return c_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean c_compute() { return true; }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle d_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void d_reset() {
    d_computed = false;
    d_initialized = false;
    d_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean d_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean d_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean d_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test02\\Test.jrag:5")
  @SideEffect.Pure(group="_ASTNode") public boolean d() {
    if (d_computed) {
      return d_value;
    }
    ASTState state = state();
    if (!d_initialized) {
      d_initialized = true;
      d_value = false;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        d_cycle = state.nextCycle();
        boolean new_d_value = d_compute();
        if (d_value != new_d_value) {
          state.setChangeInCycle();
        }
        d_value = new_d_value;
      } while (state.testAndClearChangeInCycle());
      d_computed = true;
      state.startLastCycle();
      boolean $tmp = d_compute();

      state.leaveCircle();
    } else if (d_cycle != state.cycle()) {
      d_cycle = state.cycle();
      if (state.lastCycle()) {
        d_computed = true;
        boolean new_d_value = d_compute();
        return new_d_value;
      }
      boolean new_d_value = d_compute();
      if (d_value != new_d_value) {
        state.setChangeInCycle();
      }
      d_value = new_d_value;
    } else {
    }
    return d_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean d_compute() { return true; }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle e_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void e_reset() {
    e_computed = false;
    e_initialized = false;
    e_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean e_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean e_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean e_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test02\\Test.jrag:6")
  @SideEffect.Pure(group="_ASTNode") public boolean e() {
    if (e_computed) {
      return e_value;
    }
    ASTState state = state();
    if (!e_initialized) {
      e_initialized = true;
      e_value = false;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        e_cycle = state.nextCycle();
        boolean new_e_value = true;
        if (e_value != new_e_value) {
          state.setChangeInCycle();
        }
        e_value = new_e_value;
      } while (state.testAndClearChangeInCycle());
      e_computed = true;
      state.startLastCycle();
      boolean $tmp = true;

      state.leaveCircle();
    } else if (e_cycle != state.cycle()) {
      e_cycle = state.cycle();
      if (state.lastCycle()) {
        e_computed = true;
        boolean new_e_value = true;
        return new_e_value;
      }
      boolean new_e_value = true;
      if (e_value != new_e_value) {
        state.setChangeInCycle();
      }
      e_value = new_e_value;
    } else {
    }
    return e_value;
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle f_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void f_reset() {
    f_computed = false;
    f_initialized = false;
    f_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean f_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean f_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean f_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test02\\Test.jrag:8")
  @SideEffect.Pure(group="_ASTNode") public boolean f() {
    if (f_computed) {
      return f_value;
    }
    ASTState state = state();
    if (!f_initialized) {
      f_initialized = true;
      f_value = false;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        f_cycle = state.nextCycle();
        boolean new_f_value = f_compute();
        if (f_value != new_f_value) {
          state.setChangeInCycle();
        }
        f_value = new_f_value;
      } while (state.testAndClearChangeInCycle());
      f_computed = true;
      state.startLastCycle();
      boolean $tmp = f_compute();

      state.leaveCircle();
    } else if (f_cycle != state.cycle()) {
      f_cycle = state.cycle();
      if (state.lastCycle()) {
        f_computed = true;
        boolean new_f_value = f_compute();
        return new_f_value;
      }
      boolean new_f_value = f_compute();
      if (f_value != new_f_value) {
        state.setChangeInCycle();
      }
      f_value = new_f_value;
    } else {
    }
    return f_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean f_compute() { return true; }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test02\\Test.jrag:13
   * @apilevel internal
   */
 @SideEffect.Pure(group="_ASTNode") public boolean Define_g(ASTNode _callerNode, ASTNode _childNode) {
    if (getBNoTransform() != null && _callerNode == getB()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test02\\Test.jrag:12
      return true;
    }
    else {
      return getParent().Define_g(this, _callerNode);
    }
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test02\\Test.jrag:13
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute g
   */
  @SideEffect.Pure protected boolean canDefine_g(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test02\\Test.jrag:15
   * @apilevel internal
   */
 @SideEffect.Pure(group="_ASTNode") public boolean Define_h(ASTNode _callerNode, ASTNode _childNode) {
    if (getBNoTransform() != null && _callerNode == getB()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test02\\Test.jrag:14
      return true;
    }
    else {
      return getParent().Define_h(this, _callerNode);
    }
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test02\\Test.jrag:15
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute h
   */
  @SideEffect.Pure protected boolean canDefine_h(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
