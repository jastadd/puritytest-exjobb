/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\circular\test02;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test02\\Test.ast:2
 * @astdecl B : ASTNode;
 * @production B : {@link ASTNode};

 */
public class B extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public B() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:19
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:23
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    g_reset();
    h_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:29
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  @SideEffect.Ignore @SideEffect.Fresh public B clone() throws CloneNotSupportedException {
    B node = (B) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public B copy() {
    try {
      B node = (B) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:57
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public B fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:67
   */
  @SideEffect.Fresh(group="_ASTNode") public B treeCopyNoTransform() {
    B tree = (B) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:87
   */
  @SideEffect.Fresh(group="_ASTNode") public B treeCopy() {
    B tree = (B) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:101
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test02\\Test.jrag:13")
  @SideEffect.Pure(group="_ASTNode") public boolean g() {
    if (g_computed) {
      return g_value;
    }
    ASTState state = state();
    if (!g_initialized) {
      g_initialized = true;
      g_value = false;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        g_cycle = state.nextCycle();
        boolean new_g_value = getParent().Define_g(this, null);
        if (g_value != new_g_value) {
          state.setChangeInCycle();
        }
        g_value = new_g_value;
      } while (state.testAndClearChangeInCycle());
      g_computed = true;
      state.startLastCycle();
      boolean $tmp = getParent().Define_g(this, null);

      state.leaveCircle();
    } else if (g_cycle != state.cycle()) {
      g_cycle = state.cycle();
      if (state.lastCycle()) {
        g_computed = true;
        boolean new_g_value = getParent().Define_g(this, null);
        return new_g_value;
      }
      boolean new_g_value = getParent().Define_g(this, null);
      if (g_value != new_g_value) {
        state.setChangeInCycle();
      }
      g_value = new_g_value;
    } else {
    }
    return g_value;
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle g_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void g_reset() {
    g_computed = false;
    g_initialized = false;
    g_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean g_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean g_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean g_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test02\\Test.jrag:15")
  @SideEffect.Pure(group="_ASTNode") public boolean h() {
    if (h_computed) {
      return h_value;
    }
    ASTState state = state();
    if (!h_initialized) {
      h_initialized = true;
      h_value = false;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        h_cycle = state.nextCycle();
        boolean new_h_value = getParent().Define_h(this, null);
        if (h_value != new_h_value) {
          state.setChangeInCycle();
        }
        h_value = new_h_value;
      } while (state.testAndClearChangeInCycle());
      h_computed = true;
      state.startLastCycle();
      boolean $tmp = getParent().Define_h(this, null);

      state.leaveCircle();
    } else if (h_cycle != state.cycle()) {
      h_cycle = state.cycle();
      if (state.lastCycle()) {
        h_computed = true;
        boolean new_h_value = getParent().Define_h(this, null);
        return new_h_value;
      }
      boolean new_h_value = getParent().Define_h(this, null);
      if (h_value != new_h_value) {
        state.setChangeInCycle();
      }
      h_value = new_h_value;
    } else {
    }
    return h_value;
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle h_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void h_reset() {
    h_computed = false;
    h_initialized = false;
    h_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean h_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean h_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean h_initialized = false;
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
