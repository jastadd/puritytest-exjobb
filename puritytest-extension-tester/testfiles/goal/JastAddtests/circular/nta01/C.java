/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\circular\nta01;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\nta01\\Test.ast:4
 * @astdecl C : A;
 * @production C : {@link A};

 */
public class C extends A implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public C() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:19
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:23
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:27
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:31
   */
  @SideEffect.Ignore @SideEffect.Fresh public C clone() throws CloneNotSupportedException {
    C node = (C) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:36
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public C copy() {
    try {
      C node = (C) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:55
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public C fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:65
   */
  @SideEffect.Fresh(group="_ASTNode") public C treeCopyNoTransform() {
    C tree = (C) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:85
   */
  @SideEffect.Fresh(group="_ASTNode") public C treeCopy() {
    C tree = (C) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:99
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean isA_visited = false;
  /**
   * @attribute syn
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\nta01\\Test.jrag:9
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\nta01\\Test.jrag:9")
  @SideEffect.Pure(group="_ASTNode") public boolean isA() {
    if (isA_visited) {
      throw new RuntimeException("Circular definition of attribute A.isA().");
    }
    isA_visited = true;
    boolean isA_value = false;
    isA_visited = false;
    return isA_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean isB_visited = false;
  /**
   * @attribute syn
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\nta01\\Test.jrag:13
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\nta01\\Test.jrag:13")
  @SideEffect.Pure(group="_ASTNode") public boolean isB() {
    if (isB_visited) {
      throw new RuntimeException("Circular definition of attribute A.isB().");
    }
    isB_visited = true;
    boolean isB_value = false;
    isB_visited = false;
    return isB_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean isC_visited = false;
  /**
   * @attribute syn
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\nta01\\Test.jrag:17
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\nta01\\Test.jrag:17")
  @SideEffect.Pure(group="_ASTNode") public boolean isC() {
    if (isC_visited) {
      throw new RuntimeException("Circular definition of attribute A.isC().");
    }
    isC_visited = true;
    boolean isC_value = true;
    isC_visited = false;
    return isC_value;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
