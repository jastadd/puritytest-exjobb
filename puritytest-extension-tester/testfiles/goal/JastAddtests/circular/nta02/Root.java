/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\circular\nta02;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\nta02\\Test.ast:1
 * @astdecl Root : ASTNode;
 * @production Root : {@link ASTNode};

 */
public class Root extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public Root() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:19
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:23
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    a_int_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:28
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:32
   */
  @SideEffect.Ignore @SideEffect.Fresh public Root clone() throws CloneNotSupportedException {
    Root node = (Root) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public Root copy() {
    try {
      Root node = (Root) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:56
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Root fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:66
   */
  @SideEffect.Fresh(group="_ASTNode") public Root treeCopyNoTransform() {
    Root tree = (Root) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:86
   */
  @SideEffect.Fresh(group="_ASTNode") public Root treeCopy() {
    Root tree = (Root) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:100
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void a_int_reset() {
    a_int_values = null;
    a_int_proxy = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTNode a_int_proxy;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map a_int_values;

  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\nta02\\Test.jrag:3")
  @SideEffect.Pure(group="_ASTNode") public A a(int i) {
    Object _parameters = i;
    if (a_int_values == null) a_int_values = new java.util.HashMap(4);
    ASTState.CircularValue _value;
    if (a_int_values.containsKey(_parameters)) {
      Object _cache = a_int_values.get(_parameters);
      if (!(_cache instanceof ASTState.CircularValue)) {
        return (A) _cache;
      } else {
        _value = (ASTState.CircularValue) _cache;
      }
    } else {
      _value = new ASTState.CircularValue();
      a_int_values.put(_parameters, _value);
      _value.value = new A();
       if (_value.value != null) {
         if (a_int_proxy == null) {
           a_int_proxy = new ASTNode();
           a_int_proxy.setParent(this);
         }
         ((ASTNode) _value.value).setParent(a_int_proxy);
      }
    }
    ASTState state = state();
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      A new_a_int_value;
      do {
        _value.cycle = state.nextCycle();
        new_a_int_value = a_compute(i);
        if (!AttributeValue.equals(((A)_value.value), new_a_int_value)) {
          state.setChangeInCycle();
          _value.value = new_a_int_value;
          if (_value.value != null) {
            if (a_int_proxy == null) {
              a_int_proxy = new ASTNode();
              a_int_proxy.setParent(this);
            }
            ((ASTNode) _value.value).setParent(a_int_proxy);
          }
        }
      } while (state.testAndClearChangeInCycle());
      a_int_values.put(_parameters, new_a_int_value);
      state.startLastCycle();
      A $tmp = a_compute(i);

      state.leaveCircle();
      return new_a_int_value;
    } else if (_value.cycle != state.cycle()) {
      _value.cycle = state.cycle();
      A new_a_int_value = a_compute(i);
      if (state.lastCycle()) {
        a_int_values.put(_parameters, new_a_int_value);
      }
      if (!AttributeValue.equals(((A)_value.value), new_a_int_value)) {
        state.setChangeInCycle();
        _value.value = new_a_int_value;
        if (_value.value != null) {
          if (a_int_proxy == null) {
            a_int_proxy = new ASTNode();
            a_int_proxy.setParent(this);
          }
          ((ASTNode) _value.value).setParent(a_int_proxy);
        }
      }
      return new_a_int_value;
    } else {
      return (A) _value.value;
    }
  }
  /** @apilevel internal */
  @SideEffect.Fresh private A a_compute(int i) {
      A a = a(i);
      if (a.isA()) return new B();
      if (a.isB()) return new C();
      return a;
    }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
