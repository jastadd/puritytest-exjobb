/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\circular\coll01;
import java.util.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\coll01\\Test.ast:7
 * @astdecl State : Declaration ::= <Label:String>;
 * @production State : {@link Declaration} ::= <span class="component">&lt;Label:String&gt;</span>;

 */
public class State extends Declaration implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public State() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /**
   * @declaredat ASTNode:12
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Label"},
    type = {"String"},
    kind = {"Token"}
  )
  public State(String p0) {
    setLabel(p0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:21
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:27
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:31
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:35
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
    State_transitions_visited = false;
    State_transitions_computed = false;
    
    State_transitions_value = null;
    State_predecessors_visited = false;
    State_predecessors_computed = false;
    
    State_predecessors_value = null;
    State_altReachable_cycle = null;
    State_altReachable_computed = false;
    State_altReachable_initialized = false;
    State_altReachable_value = null;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:51
   */
  @SideEffect.Ignore @SideEffect.Fresh public State clone() throws CloneNotSupportedException {
    State node = (State) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:56
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public State copy() {
    try {
      State node = (State) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:75
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public State fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:85
   */
  @SideEffect.Fresh(group="_ASTNode") public State treeCopyNoTransform() {
    State tree = (State) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:105
   */
  @SideEffect.Fresh(group="_ASTNode") public State treeCopy() {
    State tree = (State) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:119
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_Label == ((State) node).tokenString_Label);    
  }
  /**
   * Replaces the lexeme Label.
   * @param value The new value for the lexeme Label.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setLabel(String value) {
    tokenString_Label = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_Label;
  /**
   * Retrieves the value for the lexeme Label.
   * @return The value for the lexeme Label.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Label")
  @SideEffect.Pure(group="_ASTNode") public String getLabel() {
    return tokenString_Label != null ? tokenString_Label : "";
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Set localLookup_String_visited;
  /**
   * @attribute syn
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\coll01\\Test.jrag:20
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\coll01\\Test.jrag:20")
  @SideEffect.Pure(group="_ASTNode") public State localLookup(String label) {
    Object _parameters = label;
    if (localLookup_String_visited == null) localLookup_String_visited = new java.util.HashSet(4);
    if (localLookup_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute Declaration.localLookup(String).");
    }
    localLookup_String_visited.add(_parameters);
    State localLookup_String_value = // R6
        (label.equals(getLabel())) ? this : null;
    localLookup_String_visited.remove(_parameters);
    return localLookup_String_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean successors_visited = false;
  /**
   * @attribute syn
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\coll01\\Test.jrag:32
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\coll01\\Test.jrag:32")
  @SideEffect.Pure(group="_ASTNode") public Set<State> successors() {
    if (successors_visited) {
      throw new RuntimeException("Circular definition of attribute State.successors().");
    }
    successors_visited = true;
    try {
        Set<State> result = new HashSet<State>();
        for (Transition t : transitions()) {
          if (t.target() != null) {
            result.add(t.target());
          }
        }
        return result;
      }
    finally {
      successors_visited = false;
    }
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean State_transitions_visited = false;
  /**
   * @attribute coll
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\coll01\\Test.jrag:25
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.COLL)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\coll01\\Test.jrag:25")
  @SideEffect.Pure(group="_ASTNode") public Set<Transition> transitions() {
    ASTState state = state();
    if (State_transitions_computed) {
      return State_transitions_value;
    }
    if (State_transitions_visited) {
      throw new RuntimeException("Circular definition of attribute State.transitions().");
    }
    State_transitions_visited = true;
    state().enterLazyAttribute();
    State_transitions_value = transitions_compute();
    State_transitions_computed = true;
    state().leaveLazyAttribute();
    State_transitions_visited = false;
    return State_transitions_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure(group="_ASTNode") private Set<Transition> transitions_compute() {
    ASTNode node = this;
    while (node != null && !(node instanceof StateMachine)) {
      node = node.getParent();
    }
    StateMachine root = (StateMachine) node;
    root.survey_State_transitions();
    Set<Transition> _computedValue = new HashSet<Transition>();
    if (root.contributorMap_State_transitions.containsKey(this)) {
          for (ASTNode contributor : root.contributorMap_State_transitions.get(this)) {
            contributor.contributeTo_State_transitions(_computedValue);
          }
    }
    return _computedValue;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean State_transitions_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Set<Transition> State_transitions_value;

/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean State_predecessors_visited = false;
  /**
   * @attribute coll
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\coll01\\Test.jrag:42
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.COLL)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\coll01\\Test.jrag:42")
  @SideEffect.Pure(group="_ASTNode") public Set<State> predecessors() {
    ASTState state = state();
    if (State_predecessors_computed) {
      return State_predecessors_value;
    }
    if (State_predecessors_visited) {
      throw new RuntimeException("Circular definition of attribute State.predecessors().");
    }
    State_predecessors_visited = true;
    state().enterLazyAttribute();
    State_predecessors_value = predecessors_compute();
    State_predecessors_computed = true;
    state().leaveLazyAttribute();
    State_predecessors_visited = false;
    return State_predecessors_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure(group="_ASTNode") private Set<State> predecessors_compute() {
    ASTNode node = this;
    while (node != null && !(node instanceof StateMachine)) {
      node = node.getParent();
    }
    StateMachine root = (StateMachine) node;
    root.survey_State_predecessors();
    Set<State> _computedValue = new HashSet<State>();
    if (root.contributorMap_State_predecessors.containsKey(this)) {
          for (ASTNode contributor : root.contributorMap_State_predecessors.get(this)) {
            contributor.contributeTo_State_predecessors(_computedValue);
          }
    }
    return _computedValue;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean State_predecessors_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Set<State> State_predecessors_value;

/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle State_altReachable_cycle = null;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.COLL, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\coll01\\Test.jrag:48")
  @SideEffect.Pure(group="_ASTNode") public Set<State> altReachable() {
    if (State_altReachable_computed) {
      return State_altReachable_value;
    }
    ASTState state = state();
    if (!State_altReachable_initialized) {
      ASTNode _node = this;
      while (_node != null && !(_node instanceof StateMachine)) {
        _node = _node.getParent();
      }
      StateMachine root = (StateMachine) _node;
      if (root.collecting_contributors_State_altReachable) {
        throw new RuntimeException("Circularity during survey phase");
      }
      root.survey_State_altReachable();
      State_altReachable_initialized = true;
      State_altReachable_value = new HashSet<State>();
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        State_altReachable_cycle = state.nextCycle();
        Set<State> new_State_altReachable_value = altReachable_compute();
        if (!AttributeValue.equals(State_altReachable_value, new_State_altReachable_value)) {
          state.setChangeInCycle();
        }
        State_altReachable_value = new_State_altReachable_value;
      } while (state.testAndClearChangeInCycle());
      State_altReachable_computed = true;
      state.startLastCycle();
      Set<State> $tmp = altReachable_compute();

      state.leaveCircle();
    } else if (State_altReachable_cycle != state.cycle()) {
      State_altReachable_cycle = state.cycle();
      if (state.lastCycle()) {
        State_altReachable_computed = true;
        Set<State> new_State_altReachable_value = altReachable_compute();
        return new_State_altReachable_value;
      }
      Set<State> new_State_altReachable_value = altReachable_compute();
      if (!AttributeValue.equals(State_altReachable_value, new_State_altReachable_value)) {
        state.setChangeInCycle();
      }
      State_altReachable_value = new_State_altReachable_value;
    } else {
    }
    return State_altReachable_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure(group="_ASTNode") private Set<State> altReachable_compute() {
    ASTNode node = this;
    while (node != null && !(node instanceof StateMachine)) {
      node = node.getParent();
    }
    StateMachine root = (StateMachine) node;
    root.survey_State_altReachable();
    Set<State> _computedValue = new HashSet<State>();
    if (root.contributorMap_State_altReachable.containsKey(this)) {
          for (ASTNode contributor : root.contributorMap_State_altReachable.get(this)) {
            contributor.contributeTo_State_altReachable(_computedValue);
          }
    }
    return _computedValue;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean State_altReachable_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Set<State> State_altReachable_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean State_altReachable_initialized = false;
  /** @apilevel internal */
  @SideEffect.Local(group="_ASTNode") protected void collect_contributors_State_predecessors(StateMachine _root, @SideEffect.Ignore java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\coll01\\Test.jrag:44
    for (State target : (Iterable<? extends State>) (successors())) {
      java.util.Set<ASTNode> contributors = _map.get(target);
      if (contributors == null) {
        contributors = new java.util.LinkedHashSet<ASTNode>();
        _map.put((ASTNode) target, contributors);
      }
      contributors.add(this);
    }
    super.collect_contributors_State_predecessors(_root, _map);
  }
  /** @apilevel internal */
  @SideEffect.Local(group="_ASTNode") protected void collect_contributors_State_altReachable(StateMachine _root, @SideEffect.Ignore java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\coll01\\Test.jrag:50
    for (State target : (Iterable<? extends State>) (predecessors())) {
      java.util.Set<ASTNode> contributors = _map.get(target);
      if (contributors == null) {
        contributors = new java.util.LinkedHashSet<ASTNode>();
        _map.put((ASTNode) target, contributors);
      }
      contributors.add(this);
    }
    super.collect_contributors_State_altReachable(_root, _map);
  }
  /** @apilevel internal */
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_State_predecessors(Set<State> collection) {
    super.contributeTo_State_predecessors(collection);
    collection.add(this);
  }
  /** @apilevel internal */
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_State_altReachable(Set<State> collection) {
    super.contributeTo_State_altReachable(collection);
    collection.addAll(union(asSet(this), altReachable()));
  }
}
