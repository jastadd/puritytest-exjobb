/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\circular\coll01;
import java.util.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\coll01\\Test.ast:5
 * @astdecl StateMachine : ASTNode ::= Declaration*;
 * @production StateMachine : {@link ASTNode} ::= <span class="component">{@link Declaration}*</span>;

 */
public class StateMachine extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public StateMachine() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
    setChild(new List(), 0);
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Declaration"},
    type = {"List<Declaration>"},
    kind = {"List"}
  )
  public StateMachine(List<Declaration> p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:23
   */
  @SideEffect.Pure protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:29
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
    contributorMap_State_transitions = null;
    contributorMap_State_predecessors = null;
    contributorMap_State_altReachable = null;
    collecting_contributors_State_altReachable = false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:45
   */
  @SideEffect.Ignore @SideEffect.Fresh public StateMachine clone() throws CloneNotSupportedException {
    StateMachine node = (StateMachine) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:50
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public StateMachine copy() {
    try {
      StateMachine node = (StateMachine) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:69
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public StateMachine fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:79
   */
  @SideEffect.Fresh(group="_ASTNode") public StateMachine treeCopyNoTransform() {
    StateMachine tree = (StateMachine) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:99
   */
  @SideEffect.Fresh(group="_ASTNode") public StateMachine treeCopy() {
    StateMachine tree = (StateMachine) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:113
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the Declaration list.
   * @param list The new list node to be used as the Declaration list.
   * @apilevel high-level
   */
  public void setDeclarationList(List<Declaration> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the Declaration list.
   * @return Number of children in the Declaration list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumDeclaration() {
    return getDeclarationList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Declaration list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Declaration list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumDeclarationNoTransform() {
    return getDeclarationListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Declaration list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Declaration list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Declaration getDeclaration(int i) {
    return (Declaration) getDeclarationList().getChild(i);
  }
  /**
   * Check whether the Declaration list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasDeclaration() {
    return getDeclarationList().getNumChild() != 0;
  }
  /**
   * Append an element to the Declaration list.
   * @param node The element to append to the Declaration list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addDeclaration(Declaration node) {
    List<Declaration> list = (parent == null) ? getDeclarationListNoTransform() : getDeclarationList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addDeclarationNoTransform(Declaration node) {
    List<Declaration> list = getDeclarationListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Declaration list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setDeclaration(Declaration node, int i) {
    List<Declaration> list = getDeclarationList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Declaration list.
   * @return The node representing the Declaration list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Declaration")
  @SideEffect.Pure(group="_ASTNode") @SideEffect.FreshIf public List<Declaration> getDeclarationList() {
    List<Declaration> list = (List<Declaration>) getChild(0);
    return list;
  }
  /**
   * Retrieves the Declaration list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Declaration list.
   * @apilevel low-level
   */
  @SideEffect.Pure @SideEffect.FreshIf public List<Declaration> getDeclarationListNoTransform() {
    return (List<Declaration>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the Declaration list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Declaration getDeclarationNoTransform(int i) {
    return (Declaration) getDeclarationListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Declaration list.
   * @return The node representing the Declaration list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Declaration> getDeclarations() {
    return getDeclarationList();
  }
  /**
   * Retrieves the Declaration list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Declaration list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Declaration> getDeclarationsNoTransform() {
    return getDeclarationListNoTransform();
  }
  /**
   * @aspect <NoAspect>
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\coll01\\Test.jrag:25
   */
  /** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map<ASTNode, java.util.Set<ASTNode>> contributorMap_State_transitions = null;

  /** @apilevel internal */
  @SideEffect.Ignore protected void survey_State_transitions() {
    if (contributorMap_State_transitions == null) {
      contributorMap_State_transitions = new java.util.IdentityHashMap<ASTNode, java.util.Set<ASTNode>>();
      collect_contributors_State_transitions(this, contributorMap_State_transitions);
    }
  }

  /**
   * @aspect <NoAspect>
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\coll01\\Test.jrag:42
   */
  /** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map<ASTNode, java.util.Set<ASTNode>> contributorMap_State_predecessors = null;

  /** @apilevel internal */
  @SideEffect.Ignore protected void survey_State_predecessors() {
    if (contributorMap_State_predecessors == null) {
      contributorMap_State_predecessors = new java.util.IdentityHashMap<ASTNode, java.util.Set<ASTNode>>();
      collect_contributors_State_predecessors(this, contributorMap_State_predecessors);
    }
  }

  /**
   * @aspect <NoAspect>
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\coll01\\Test.jrag:48
   */
  /** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map<ASTNode, java.util.Set<ASTNode>> contributorMap_State_altReachable = null;

  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") public boolean collecting_contributors_State_altReachable = false;

  /** @apilevel internal */
  @SideEffect.Ignore protected void survey_State_altReachable() {
    if (contributorMap_State_altReachable == null) {
      contributorMap_State_altReachable = new java.util.IdentityHashMap<ASTNode, java.util.Set<ASTNode>>();
      collecting_contributors_State_altReachable = true;
      collect_contributors_State_altReachable(this, contributorMap_State_altReachable);
      collecting_contributors_State_altReachable = false;
    }
  }

  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\coll01\\Test.jrag:10
   * @apilevel internal
   */
 @SideEffect.Pure(group="_ASTNode") public State Define_lookup(ASTNode _callerNode, ASTNode _childNode, String label) {
    if (_callerNode == getDeclarationListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\coll01\\Test.jrag:12
      int i = _callerNode.getIndexOfChild(_childNode);
      { // R4
          for (Declaration d : getDeclarationList()) {
            State match = d.localLookup(label);
            if (match != null) return match;
          }
          return null;
        }
    }
    else {
      return getParent().Define_lookup(this, _callerNode, label);
    }
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\coll01\\Test.jrag:10
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute lookup
   */
  @SideEffect.Pure protected boolean canDefine_lookup(ASTNode _callerNode, ASTNode _childNode, String label) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
