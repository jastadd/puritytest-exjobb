/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\circular\coll01;
import java.util.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\coll01\\Test.ast:8
 * @astdecl Transition : Declaration ::= <Label:String> <SourceLabel:String> <TargetLabel:String>;
 * @production Transition : {@link Declaration} ::= <span class="component">&lt;Label:String&gt;</span> <span class="component">&lt;SourceLabel:String&gt;</span> <span class="component">&lt;TargetLabel:String&gt;</span>;

 */
public class Transition extends Declaration implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public Transition() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /**
   * @declaredat ASTNode:12
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Label", "SourceLabel", "TargetLabel"},
    type = {"String", "String", "String"},
    kind = {"Token", "Token", "Token"}
  )
  public Transition(String p0, String p1, String p2) {
    setLabel(p0);
    setSourceLabel(p1);
    setTargetLabel(p2);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:23
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:29
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:41
   */
  @SideEffect.Ignore @SideEffect.Fresh public Transition clone() throws CloneNotSupportedException {
    Transition node = (Transition) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:46
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public Transition copy() {
    try {
      Transition node = (Transition) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:65
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Transition fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:75
   */
  @SideEffect.Fresh(group="_ASTNode") public Transition treeCopyNoTransform() {
    Transition tree = (Transition) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:95
   */
  @SideEffect.Fresh(group="_ASTNode") public Transition treeCopy() {
    Transition tree = (Transition) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:109
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_Label == ((Transition) node).tokenString_Label) && (tokenString_SourceLabel == ((Transition) node).tokenString_SourceLabel) && (tokenString_TargetLabel == ((Transition) node).tokenString_TargetLabel);    
  }
  /**
   * Replaces the lexeme Label.
   * @param value The new value for the lexeme Label.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setLabel(String value) {
    tokenString_Label = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_Label;
  /**
   * Retrieves the value for the lexeme Label.
   * @return The value for the lexeme Label.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Label")
  @SideEffect.Pure(group="_ASTNode") public String getLabel() {
    return tokenString_Label != null ? tokenString_Label : "";
  }
  /**
   * Replaces the lexeme SourceLabel.
   * @param value The new value for the lexeme SourceLabel.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setSourceLabel(String value) {
    tokenString_SourceLabel = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_SourceLabel;
  /**
   * Retrieves the value for the lexeme SourceLabel.
   * @return The value for the lexeme SourceLabel.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="SourceLabel")
  @SideEffect.Pure(group="_ASTNode") public String getSourceLabel() {
    return tokenString_SourceLabel != null ? tokenString_SourceLabel : "";
  }
  /**
   * Replaces the lexeme TargetLabel.
   * @param value The new value for the lexeme TargetLabel.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setTargetLabel(String value) {
    tokenString_TargetLabel = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_TargetLabel;
  /**
   * Retrieves the value for the lexeme TargetLabel.
   * @return The value for the lexeme TargetLabel.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="TargetLabel")
  @SideEffect.Pure(group="_ASTNode") public String getTargetLabel() {
    return tokenString_TargetLabel != null ? tokenString_TargetLabel : "";
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean source_visited = false;
  /**
   * @attribute syn
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\coll01\\Test.jrag:7
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\coll01\\Test.jrag:7")
  @SideEffect.Pure(group="_ASTNode") public State source() {
    if (source_visited) {
      throw new RuntimeException("Circular definition of attribute Transition.source().");
    }
    source_visited = true;
    State source_value = lookup(getSourceLabel());
    source_visited = false;
    return source_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean target_visited = false;
  /**
   * @attribute syn
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\coll01\\Test.jrag:8
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\coll01\\Test.jrag:8")
  @SideEffect.Pure(group="_ASTNode") public State target() {
    if (target_visited) {
      throw new RuntimeException("Circular definition of attribute Transition.target().");
    }
    target_visited = true;
    State target_value = lookup(getTargetLabel());
    target_visited = false;
    return target_value;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
  /** @apilevel internal */
  @SideEffect.Local(group="_ASTNode") protected void collect_contributors_State_transitions(StateMachine _root, @SideEffect.Ignore java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\coll01\\Test.jrag:27
    if (source() != null) {
      {
        State target = (State) (source());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_State_transitions(_root, _map);
  }
  /** @apilevel internal */
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_State_transitions(Set<Transition> collection) {
    super.contributeTo_State_transitions(collection);
    if (source() != null) {
      collection.add(this);
    }
  }
}
