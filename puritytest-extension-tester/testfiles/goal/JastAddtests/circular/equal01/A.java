/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\circular\equal01;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\equal01\\Test.ast:1
 * @astdecl A : ASTNode ::= <Id:String> C*;
 * @production A : {@link ASTNode} ::= <span class="component">&lt;Id:String&gt;</span> <span class="component">{@link C}*</span>;

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public A() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
    setChild(new List(), 0);
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Id", "C"},
    type = {"String", "List<C>"},
    kind = {"Token", "List"}
  )
  public A(String p0, List<C> p1) {
    setId(p0);
    setChild(p1, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:24
   */
  @SideEffect.Pure protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:30
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:34
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:42
   */
  @SideEffect.Ignore @SideEffect.Fresh public A clone() throws CloneNotSupportedException {
    A node = (A) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:47
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public A copy() {
    try {
      A node = (A) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:66
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public A fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:76
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopyNoTransform() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:96
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopy() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:110
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_Id == ((A) node).tokenString_Id);    
  }
  /**
   * Replaces the lexeme Id.
   * @param value The new value for the lexeme Id.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setId(String value) {
    tokenString_Id = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_Id;
  /**
   * Retrieves the value for the lexeme Id.
   * @return The value for the lexeme Id.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Id")
  @SideEffect.Pure(group="_ASTNode") public String getId() {
    return tokenString_Id != null ? tokenString_Id : "";
  }
  /**
   * Replaces the C list.
   * @param list The new list node to be used as the C list.
   * @apilevel high-level
   */
  public void setCList(List<C> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the C list.
   * @return Number of children in the C list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumC() {
    return getCList().getNumChild();
  }
  /**
   * Retrieves the number of children in the C list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the C list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumCNoTransform() {
    return getCListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the C list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the C list.
   * @apilevel high-level
   */
  @SideEffect.Pure public C getC(int i) {
    return (C) getCList().getChild(i);
  }
  /**
   * Check whether the C list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasC() {
    return getCList().getNumChild() != 0;
  }
  /**
   * Append an element to the C list.
   * @param node The element to append to the C list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addC(C node) {
    List<C> list = (parent == null) ? getCListNoTransform() : getCList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addCNoTransform(C node) {
    List<C> list = getCListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the C list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setC(C node, int i) {
    List<C> list = getCList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the C list.
   * @return The node representing the C list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="C")
  @SideEffect.Pure(group="_ASTNode") @SideEffect.FreshIf public List<C> getCList() {
    List<C> list = (List<C>) getChild(0);
    return list;
  }
  /**
   * Retrieves the C list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the C list.
   * @apilevel low-level
   */
  @SideEffect.Pure @SideEffect.FreshIf public List<C> getCListNoTransform() {
    return (List<C>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the C list without
   * triggering rewrites.
   */
  @SideEffect.Pure public C getCNoTransform(int i) {
    return (C) getCListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the C list.
   * @return The node representing the C list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<C> getCs() {
    return getCList();
  }
  /**
   * Retrieves the C list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the C list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<C> getCsNoTransform() {
    return getCListNoTransform();
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
