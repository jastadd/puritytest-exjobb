/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\circular\test16;
import java.util.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test16\\Test.ast:1
 * @astdecl A : ASTNode;
 * @production A : {@link ASTNode};

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public A() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:19
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:23
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    m_reset();
    n_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:29
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  @SideEffect.Ignore @SideEffect.Fresh public A clone() throws CloneNotSupportedException {
    A node = (A) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public A copy() {
    try {
      A node = (A) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:57
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public A fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:67
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopyNoTransform() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:87
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopy() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:101
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle m_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void m_reset() {
    m_computed = false;
    m_initialized = false;
    m_value = null;
    m_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean m_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected HashSet m_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean m_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test16\\Test.jrag:6")
  @SideEffect.Pure(group="_ASTNode") public HashSet m() {
    if (m_computed) {
      return m_value;
    }
    ASTState state = state();
    if (!m_initialized) {
      m_initialized = true;
      m_value = emptyHashSet();
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        m_cycle = state.nextCycle();
        HashSet new_m_value = m_compute();
        if (!AttributeValue.equals(m_value, new_m_value)) {
          state.setChangeInCycle();
        }
        m_value = new_m_value;
      } while (state.testAndClearChangeInCycle());
      m_computed = true;
      state.startLastCycle();
      HashSet $tmp = m_compute();

      state.leaveCircle();
    } else if (m_cycle != state.cycle()) {
      m_cycle = state.cycle();
      if (state.lastCycle()) {
        m_computed = true;
        HashSet new_m_value = m_compute();
        return new_m_value;
      }
      HashSet new_m_value = m_compute();
      if (!AttributeValue.equals(m_value, new_m_value)) {
        state.setChangeInCycle();
      }
      m_value = new_m_value;
    } else {
    }
    return m_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private HashSet m_compute() {
      return this.m();
    }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle n_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void n_reset() {
    n_computed = false;
    n_initialized = false;
    n_value = null;
    n_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean n_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected HashSet n_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean n_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test16\\Test.jrag:10")
  @SideEffect.Pure(group="_ASTNode") public HashSet n() {
    if (n_computed) {
      return n_value;
    }
    ASTState state = state();
    if (!n_initialized) {
      n_initialized = true;
      n_value = this.emptyHashSet();
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        n_cycle = state.nextCycle();
        HashSet new_n_value = n_compute();
        if (!AttributeValue.equals(n_value, new_n_value)) {
          state.setChangeInCycle();
        }
        n_value = new_n_value;
      } while (state.testAndClearChangeInCycle());
      n_computed = true;
      state.startLastCycle();
      HashSet $tmp = n_compute();

      state.leaveCircle();
    } else if (n_cycle != state.cycle()) {
      n_cycle = state.cycle();
      if (state.lastCycle()) {
        n_computed = true;
        HashSet new_n_value = n_compute();
        return new_n_value;
      }
      HashSet new_n_value = n_compute();
      if (!AttributeValue.equals(n_value, new_n_value)) {
        state.setChangeInCycle();
      }
      n_value = new_n_value;
    } else {
    }
    return n_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private HashSet n_compute() {
      return this.n();
    }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
