/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/fastTests\collAttr;
import java.util.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\collAttr\\Simplic.ast:7
 * @astdecl D : C ::= Elist:Expr* <ID:String>;
 * @production D : {@link C} ::= <span class="component">Elist:{@link Expr}*</span> <span class="component">&lt;ID:String&gt;</span>;

 */
public class D extends C implements Cloneable {
  /**
   * @aspect Visitor
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\collAttr\\Visitor.jrag:41
   */
  public Object accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
  /**
   * @declaredat ASTNode:1
   */
  public D() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
    setChild(new List(), 1);
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Cond", "Elist", "ID"},
    type = {"Expr", "List<Expr>", "String"},
    kind = {"Child", "List", "Token"}
  )
  public D(Expr p0, List<Expr> p1, String p2) {
    setChild(p0, 0);
    setChild(p1, 1);
    setID(p2);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:25
   */
  @SideEffect.Pure protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:31
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:35
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:39
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  @SideEffect.Ignore @SideEffect.Fresh public D clone() throws CloneNotSupportedException {
    D node = (D) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:48
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public D copy() {
    try {
      D node = (D) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:67
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public D fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:77
   */
  @SideEffect.Fresh(group="_ASTNode") public D treeCopyNoTransform() {
    D tree = (D) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:97
   */
  @SideEffect.Fresh(group="_ASTNode") public D treeCopy() {
    D tree = (D) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:111
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_ID == ((D) node).tokenString_ID);    
  }
  /**
   * Replaces the Cond child.
   * @param node The new node to replace the Cond child.
   * @apilevel high-level
   */
  public void setCond(Expr node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the Cond child.
   * @return The current node used as the Cond child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Cond")
  @SideEffect.Pure public Expr getCond() {
    return (Expr) getChild(0);
  }
  /**
   * Retrieves the Cond child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Cond child.
   * @apilevel low-level
   */
  @SideEffect.Pure public Expr getCondNoTransform() {
    return (Expr) getChildNoTransform(0);
  }
  /**
   * Replaces the Elist list.
   * @param list The new list node to be used as the Elist list.
   * @apilevel high-level
   */
  public void setElistList(List<Expr> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the Elist list.
   * @return Number of children in the Elist list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumElist() {
    return getElistList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Elist list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Elist list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumElistNoTransform() {
    return getElistListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Elist list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Elist list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Expr getElist(int i) {
    return (Expr) getElistList().getChild(i);
  }
  /**
   * Check whether the Elist list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasElist() {
    return getElistList().getNumChild() != 0;
  }
  /**
   * Append an element to the Elist list.
   * @param node The element to append to the Elist list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addElist(Expr node) {
    List<Expr> list = (parent == null) ? getElistListNoTransform() : getElistList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addElistNoTransform(Expr node) {
    List<Expr> list = getElistListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Elist list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setElist(Expr node, int i) {
    List<Expr> list = getElistList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Elist list.
   * @return The node representing the Elist list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Elist")
  @SideEffect.Pure(group="_ASTNode") @SideEffect.FreshIf public List<Expr> getElistList() {
    List<Expr> list = (List<Expr>) getChild(1);
    return list;
  }
  /**
   * Retrieves the Elist list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Elist list.
   * @apilevel low-level
   */
  @SideEffect.Pure @SideEffect.FreshIf public List<Expr> getElistListNoTransform() {
    return (List<Expr>) getChildNoTransform(1);
  }
  /**
   * @return the element at index {@code i} in the Elist list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Expr getElistNoTransform(int i) {
    return (Expr) getElistListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Elist list.
   * @return The node representing the Elist list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Expr> getElists() {
    return getElistList();
  }
  /**
   * Retrieves the Elist list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Elist list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Expr> getElistsNoTransform() {
    return getElistListNoTransform();
  }
  /**
   * Replaces the lexeme ID.
   * @param value The new value for the lexeme ID.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setID(String value) {
    tokenString_ID = value;
  }
  /**
   * Retrieves the value for the lexeme ID.
   * @return The value for the lexeme ID.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="ID")
  @SideEffect.Pure(group="_ASTNode") public String getID() {
    return tokenString_ID != null ? tokenString_ID : "";
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
  /** @apilevel internal */
  @SideEffect.Local(group="_ASTNode") protected void collect_contributors_A_allchildren(A _root, @SideEffect.Ignore java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\collAttr\\collAttr.jrag:13
    if (true) {
      {
        java.util.Set<ASTNode> contributors = _map.get(_root);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) _root, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_A_allchildren(_root, _map);
  }
  /** @apilevel internal */
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_A_allchildren(ArrayList<String> collection) {
    super.contributeTo_A_allchildren(collection);
    if (true) {
      collection.add(getID());
    }
  }
}
