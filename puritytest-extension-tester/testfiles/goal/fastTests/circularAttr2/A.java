/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/fastTests\circularAttr2;
import java.util.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\circularAttr2\\Small.ast:3
 * @astdecl A : ASTNode ::= <ID:String> A* C* B;
 * @production A : {@link ASTNode} ::= <span class="component">&lt;ID:String&gt;</span> <span class="component">{@link A}*</span> <span class="component">{@link C}*</span> <span class="component">{@link B}</span>;

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public A() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[3];
    setChild(new List(), 0);
    setChild(new List(), 1);
  }
  /**
   * @declaredat ASTNode:15
   */
  @ASTNodeAnnotation.Constructor(
    name = {"ID", "A", "C", "B"},
    type = {"String", "List<A>", "List<C>", "B"},
    kind = {"Token", "List", "List", "Child"}
  )
  public A(String p0, List<A> p1, List<C> p2, B p3) {
    setID(p0);
    setChild(p1, 0);
    setChild(p2, 1);
    setChild(p3, 2);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:27
   */
  @SideEffect.Pure protected int numChildren() {
    return 3;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:33
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    reachable_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:42
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:46
   */
  @SideEffect.Ignore @SideEffect.Fresh public A clone() throws CloneNotSupportedException {
    A node = (A) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:51
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public A copy() {
    try {
      A node = (A) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:70
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public A fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:80
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopyNoTransform() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:100
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopy() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:114
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_ID == ((A) node).tokenString_ID);    
  }
  /**
   * Replaces the lexeme ID.
   * @param value The new value for the lexeme ID.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setID(String value) {
    tokenString_ID = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_ID;
  /**
   * Retrieves the value for the lexeme ID.
   * @return The value for the lexeme ID.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="ID")
  @SideEffect.Pure(group="_ASTNode") public String getID() {
    return tokenString_ID != null ? tokenString_ID : "";
  }
  /**
   * Replaces the A list.
   * @param list The new list node to be used as the A list.
   * @apilevel high-level
   */
  public void setAList(List<A> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the A list.
   * @return Number of children in the A list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumA() {
    return getAList().getNumChild();
  }
  /**
   * Retrieves the number of children in the A list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the A list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumANoTransform() {
    return getAListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the A list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the A list.
   * @apilevel high-level
   */
  @SideEffect.Pure public A getA(int i) {
    return (A) getAList().getChild(i);
  }
  /**
   * Check whether the A list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasA() {
    return getAList().getNumChild() != 0;
  }
  /**
   * Append an element to the A list.
   * @param node The element to append to the A list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addA(A node) {
    List<A> list = (parent == null) ? getAListNoTransform() : getAList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addANoTransform(A node) {
    List<A> list = getAListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the A list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setA(A node, int i) {
    List<A> list = getAList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the A list.
   * @return The node representing the A list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="A")
  @SideEffect.Pure(group="_ASTNode") @SideEffect.FreshIf public List<A> getAList() {
    List<A> list = (List<A>) getChild(0);
    return list;
  }
  /**
   * Retrieves the A list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the A list.
   * @apilevel low-level
   */
  @SideEffect.Pure @SideEffect.FreshIf public List<A> getAListNoTransform() {
    return (List<A>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the A list without
   * triggering rewrites.
   */
  @SideEffect.Pure public A getANoTransform(int i) {
    return (A) getAListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the A list.
   * @return The node representing the A list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<A> getAs() {
    return getAList();
  }
  /**
   * Retrieves the A list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the A list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<A> getAsNoTransform() {
    return getAListNoTransform();
  }
  /**
   * Replaces the C list.
   * @param list The new list node to be used as the C list.
   * @apilevel high-level
   */
  public void setCList(List<C> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the C list.
   * @return Number of children in the C list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumC() {
    return getCList().getNumChild();
  }
  /**
   * Retrieves the number of children in the C list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the C list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumCNoTransform() {
    return getCListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the C list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the C list.
   * @apilevel high-level
   */
  @SideEffect.Pure public C getC(int i) {
    return (C) getCList().getChild(i);
  }
  /**
   * Check whether the C list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasC() {
    return getCList().getNumChild() != 0;
  }
  /**
   * Append an element to the C list.
   * @param node The element to append to the C list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addC(C node) {
    List<C> list = (parent == null) ? getCListNoTransform() : getCList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addCNoTransform(C node) {
    List<C> list = getCListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the C list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setC(C node, int i) {
    List<C> list = getCList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the C list.
   * @return The node representing the C list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="C")
  @SideEffect.Pure(group="_ASTNode") @SideEffect.FreshIf public List<C> getCList() {
    List<C> list = (List<C>) getChild(1);
    return list;
  }
  /**
   * Retrieves the C list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the C list.
   * @apilevel low-level
   */
  @SideEffect.Pure @SideEffect.FreshIf public List<C> getCListNoTransform() {
    return (List<C>) getChildNoTransform(1);
  }
  /**
   * @return the element at index {@code i} in the C list without
   * triggering rewrites.
   */
  @SideEffect.Pure public C getCNoTransform(int i) {
    return (C) getCListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the C list.
   * @return The node representing the C list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<C> getCs() {
    return getCList();
  }
  /**
   * Retrieves the C list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the C list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<C> getCsNoTransform() {
    return getCListNoTransform();
  }
  /**
   * Replaces the B child.
   * @param node The new node to replace the B child.
   * @apilevel high-level
   */
  public void setB(B node) {
    setChild(node, 2);
  }
  /**
   * Retrieves the B child.
   * @return The current node used as the B child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="B")
  @SideEffect.Pure public B getB() {
    return (B) getChild(2);
  }
  /**
   * Retrieves the B child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the B child.
   * @apilevel low-level
   */
  @SideEffect.Pure public B getBNoTransform() {
    return (B) getChildNoTransform(2);
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle reachable_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void reachable_reset() {
    reachable_computed = false;
    reachable_initialized = false;
    reachable_value = null;
    reachable_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean reachable_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Set<B> reachable_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean reachable_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Purity", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\circularAttr2\\circular2.jrag:3")
  @SideEffect.Pure(group="_ASTNode") public Set<B> reachable() {
    if (reachable_computed) {
      return reachable_value;
    }
    ASTState state = state();
    if (!reachable_initialized) {
      reachable_initialized = true;
      reachable_value = new HashSet<State>();
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        reachable_cycle = state.nextCycle();
        Set<B> new_reachable_value = reachable_compute();
        if (!AttributeValue.equals(reachable_value, new_reachable_value)) {
          state.setChangeInCycle();
        }
        reachable_value = new_reachable_value;
      } while (state.testAndClearChangeInCycle());
      reachable_computed = true;
      state.startLastCycle();
      Set<B> $tmp = reachable_compute();

      state.leaveCircle();
    } else if (reachable_cycle != state.cycle()) {
      reachable_cycle = state.cycle();
      if (state.lastCycle()) {
        reachable_computed = true;
        Set<B> new_reachable_value = reachable_compute();
        return new_reachable_value;
      }
      Set<B> new_reachable_value = reachable_compute();
      if (!AttributeValue.equals(reachable_value, new_reachable_value)) {
        state.setChangeInCycle();
      }
      reachable_value = new_reachable_value;
    } else {
    }
    return reachable_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Set<B> reachable_compute() { // R2
  	    HashSet<B> result = new HashSet<B>();
  	    for (A s : getA()) {
  	      result.add(s.getB());
  	      result.addAll(s.reachable());
  	    }
  	    return result;
  	  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
