/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/fastTests\circNTA;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\circNTA\\Small.ast:3
 * @astdecl A : ASTNode ::= <ID:String> C* B;
 * @production A : {@link ASTNode} ::= <span class="component">&lt;ID:String&gt;</span> <span class="component">{@link C}*</span> <span class="component">{@link B}</span>;

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public A() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
    setChild(new List(), 0);
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"ID", "C", "B"},
    type = {"String", "List<C>", "B"},
    kind = {"Token", "List", "Child"}
  )
  public A(String p0, List<C> p1, B p2) {
    setID(p0);
    setChild(p1, 0);
    setChild(p2, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:25
   */
  @SideEffect.Pure protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:31
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:35
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    a_reset();
    b_reset();
    c_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:42
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:46
   */
  @SideEffect.Ignore @SideEffect.Fresh public A clone() throws CloneNotSupportedException {
    A node = (A) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:51
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public A copy() {
    try {
      A node = (A) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:70
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public A fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:80
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopyNoTransform() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:100
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopy() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:114
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_ID == ((A) node).tokenString_ID);    
  }
  /**
   * Replaces the lexeme ID.
   * @param value The new value for the lexeme ID.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setID(String value) {
    tokenString_ID = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_ID;
  /**
   * Retrieves the value for the lexeme ID.
   * @return The value for the lexeme ID.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="ID")
  @SideEffect.Pure(group="_ASTNode") public String getID() {
    return tokenString_ID != null ? tokenString_ID : "";
  }
  /**
   * Replaces the C list.
   * @param list The new list node to be used as the C list.
   * @apilevel high-level
   */
  public void setCList(List<C> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the C list.
   * @return Number of children in the C list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumC() {
    return getCList().getNumChild();
  }
  /**
   * Retrieves the number of children in the C list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the C list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumCNoTransform() {
    return getCListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the C list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the C list.
   * @apilevel high-level
   */
  @SideEffect.Pure public C getC(int i) {
    return (C) getCList().getChild(i);
  }
  /**
   * Check whether the C list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasC() {
    return getCList().getNumChild() != 0;
  }
  /**
   * Append an element to the C list.
   * @param node The element to append to the C list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addC(C node) {
    List<C> list = (parent == null) ? getCListNoTransform() : getCList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addCNoTransform(C node) {
    List<C> list = getCListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the C list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setC(C node, int i) {
    List<C> list = getCList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the C list.
   * @return The node representing the C list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="C")
  @SideEffect.Pure(group="_ASTNode") @SideEffect.FreshIf public List<C> getCList() {
    List<C> list = (List<C>) getChild(0);
    return list;
  }
  /**
   * Retrieves the C list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the C list.
   * @apilevel low-level
   */
  @SideEffect.Pure @SideEffect.FreshIf public List<C> getCListNoTransform() {
    return (List<C>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the C list without
   * triggering rewrites.
   */
  @SideEffect.Pure public C getCNoTransform(int i) {
    return (C) getCListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the C list.
   * @return The node representing the C list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<C> getCs() {
    return getCList();
  }
  /**
   * Retrieves the C list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the C list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<C> getCsNoTransform() {
    return getCListNoTransform();
  }
  /**
   * Replaces the B child.
   * @param node The new node to replace the B child.
   * @apilevel high-level
   */
  public void setB(B node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the B child.
   * @return The current node used as the B child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="B")
  @SideEffect.Pure public B getB() {
    return (B) getChild(1);
  }
  /**
   * Retrieves the B child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the B child.
   * @apilevel low-level
   */
  @SideEffect.Pure public B getBNoTransform() {
    return (B) getChildNoTransform(1);
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle a_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void a_reset() {
    a_computed = false;
    a_initialized = false;
    a_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\circNTA\\Test.jrag:2")
  @SideEffect.Pure(group="_ASTNode") public boolean a() {
    if (a_computed) {
      return a_value;
    }
    ASTState state = state();
    if (!a_initialized) {
      a_initialized = true;
      a_value = false;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        a_cycle = state.nextCycle();
        boolean new_a_value = a_compute();
        if (a_value != new_a_value) {
          state.setChangeInCycle();
        }
        a_value = new_a_value;
      } while (state.testAndClearChangeInCycle());
      a_computed = true;
      state.startLastCycle();
      boolean $tmp = a_compute();

      state.leaveCircle();
    } else if (a_cycle != state.cycle()) {
      a_cycle = state.cycle();
      if (state.lastCycle()) {
        a_computed = true;
        boolean new_a_value = a_compute();
        return new_a_value;
      }
      boolean new_a_value = a_compute();
      if (a_value != new_a_value) {
        state.setChangeInCycle();
      }
      a_value = new_a_value;
    } else {
    }
    return a_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean a_compute() {
      System.out.println("Eval a()");
      return b();
    }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle b_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void b_reset() {
    b_computed = false;
    b_initialized = false;
    b_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean b_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean b_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean b_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\circNTA\\Test.jrag:6")
  @SideEffect.Pure(group="_ASTNode") public boolean b() {
    if (b_computed) {
      return b_value;
    }
    ASTState state = state();
    if (!b_initialized) {
      b_initialized = true;
      b_value = false;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        b_cycle = state.nextCycle();
        boolean new_b_value = b_compute();
        if (b_value != new_b_value) {
          state.setChangeInCycle();
        }
        b_value = new_b_value;
      } while (state.testAndClearChangeInCycle());
      b_computed = true;
      state.startLastCycle();
      boolean $tmp = b_compute();

      state.leaveCircle();
    } else if (b_cycle != state.cycle()) {
      b_cycle = state.cycle();
      if (state.lastCycle()) {
        b_computed = true;
        boolean new_b_value = b_compute();
        return new_b_value;
      }
      boolean new_b_value = b_compute();
      if (b_value != new_b_value) {
        state.setChangeInCycle();
      }
      b_value = new_b_value;
    } else {
    }
    return b_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean b_compute() {
      System.out.println("Eval b()");
      return c();
    }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle c_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void c_reset() {
    c_computed = false;
    c_initialized = false;
    c_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\circNTA\\Test.jrag:10")
  @SideEffect.Pure(group="_ASTNode") public boolean c() {
    if (c_computed) {
      return c_value;
    }
    ASTState state = state();
    if (!c_initialized) {
      c_initialized = true;
      c_value = false;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        c_cycle = state.nextCycle();
        boolean new_c_value = c_compute();
        if (c_value != new_c_value) {
          state.setChangeInCycle();
        }
        c_value = new_c_value;
      } while (state.testAndClearChangeInCycle());
      c_computed = true;
      state.startLastCycle();
      boolean $tmp = c_compute();

      state.leaveCircle();
    } else if (c_cycle != state.cycle()) {
      c_cycle = state.cycle();
      if (state.lastCycle()) {
        c_computed = true;
        boolean new_c_value = c_compute();
        return new_c_value;
      }
      boolean new_c_value = c_compute();
      if (c_value != new_c_value) {
        state.setChangeInCycle();
      }
      c_value = new_c_value;
    } else {
    }
    return c_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean c_compute() {
      System.out.println("Eval c()");
      return a() || true;
    }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
