/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/fastTests\Simplic2015;
import java.util.*;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\Simplic.ast:13
 * @astdecl Call : Stmt ::= FunctionExpr;
 * @production Call : {@link Stmt} ::= <span class="component">{@link FunctionExpr}</span>;

 */
public class Call extends Stmt implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\CodeGen.jrag:290
   */
  public void genCode(PrintStream out){
	getFunctionExpr().genCode(out);
}
  /**
   * @aspect Interpreter
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\Interpreter.jrag:98
   */
  public void eval(ActivationRecord actrec)
{
	getFunctionExpr().eval(actrec);
}
  /**
   * @aspect PrettyPrint
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\PrettyPrint.jrag:143
   */
  public void prettyPrint(PrintStream out, String ind) {
		out.print(ind);
		getFunctionExpr().prettyPrint(out, ind);
		out.print(";");	
	}
  /**
   * @aspect Visitor
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\Visitor.jrag:62
   */
  public Object accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
  /**
   * @declaredat ASTNode:1
   */
  public Call() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
  }
  /**
   * @declaredat ASTNode:13
   */
  @ASTNodeAnnotation.Constructor(
    name = {"FunctionExpr"},
    type = {"FunctionExpr"},
    kind = {"Child"}
  )
  public Call(FunctionExpr p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:22
   */
  @SideEffect.Pure protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:28
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:32
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:36
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:40
   */
  @SideEffect.Ignore @SideEffect.Fresh public Call clone() throws CloneNotSupportedException {
    Call node = (Call) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:45
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public Call copy() {
    try {
      Call node = (Call) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:64
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Call fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:74
   */
  @SideEffect.Fresh(group="_ASTNode") public Call treeCopyNoTransform() {
    Call tree = (Call) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:94
   */
  @SideEffect.Fresh(group="_ASTNode") public Call treeCopy() {
    Call tree = (Call) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:108
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the FunctionExpr child.
   * @param node The new node to replace the FunctionExpr child.
   * @apilevel high-level
   */
  public void setFunctionExpr(FunctionExpr node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the FunctionExpr child.
   * @return The current node used as the FunctionExpr child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="FunctionExpr")
  @SideEffect.Pure public FunctionExpr getFunctionExpr() {
    return (FunctionExpr) getChild(0);
  }
  /**
   * Retrieves the FunctionExpr child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the FunctionExpr child.
   * @apilevel low-level
   */
  @SideEffect.Pure public FunctionExpr getFunctionExprNoTransform() {
    return (FunctionExpr) getChildNoTransform(0);
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Set collectFunctionCalls_Set_Function__visited;
  /**
   * @attribute syn
   * @aspect FunctionCallsAnalysis
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\FunctionCallsAnalysis.jrag:75
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="FunctionCallsAnalysis", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\FunctionCallsAnalysis.jrag:75")
  @SideEffect.Pure(group="_ASTNode") public Set<Function> collectFunctionCalls(Set<Function> functions) {
    Object _parameters = functions;
    if (collectFunctionCalls_Set_Function__visited == null) collectFunctionCalls_Set_Function__visited = new java.util.HashSet(4);
    if (collectFunctionCalls_Set_Function__visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute Call.collectFunctionCalls(Set_Function_).");
    }
    collectFunctionCalls_Set_Function__visited.add(_parameters);
    try {
    		return getFunctionExpr().collectFunctionCalls(functions);
    	}
    finally {
      collectFunctionCalls_Set_Function__visited.remove(_parameters);
    }
  }
  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\NameAnalysis.jrag:11
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\NameAnalysis.jrag:11")
  @SideEffect.Pure(group="_ASTNode") public ASTNode lookup(String name) {
    Object _parameters = name;
    if (lookup_String_visited == null) lookup_String_visited = new java.util.HashSet(4);
    if (lookup_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute Call.lookup(String).");
    }
    lookup_String_visited.add(_parameters);
    ASTNode lookup_String_value = getParent().Define_lookup(this, null, name);
    lookup_String_visited.remove(_parameters);
    return lookup_String_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Set lookup_String_visited;
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\TypeAnalysis.jrag:33
   * @apilevel internal
   */
 @SideEffect.Pure(group="_ASTNode") public Type Define_expectedType(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return IntType();
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\TypeAnalysis.jrag:33
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute expectedType
   */
  @SideEffect.Pure protected boolean canDefine_expectedType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
