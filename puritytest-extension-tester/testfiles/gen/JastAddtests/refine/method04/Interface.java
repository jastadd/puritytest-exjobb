package testfiles/gen/JastAddtests\refine\method04;

/**
 * @ast interface
 * @aspect A1
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\refine\\method04\\Test.jrag:2
 */
 interface Interface {
  public void toBeRefined();

}
