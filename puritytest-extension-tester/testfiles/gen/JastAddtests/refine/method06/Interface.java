package testfiles/gen/JastAddtests\refine\method06;

/**
 * @ast interface
 * @aspect A1
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\refine\\method06\\Test.jrag:2
 */
 interface Interface {
  public ASTNode returnSomething(final ASTNode n);

}
