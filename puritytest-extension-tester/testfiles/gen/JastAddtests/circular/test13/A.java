/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\circular\test13;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test13\\Test.ast:2
 * @astdecl A : ASTNode;
 * @production A : {@link ASTNode};

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public A() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:19
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:23
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    c2_a_reset();
    c2_b_reset();
    c1_a_reset();
    c1_b_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:31
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:35
   */
  @SideEffect.Ignore @SideEffect.Fresh public A clone() throws CloneNotSupportedException {
    A node = (A) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:40
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public A copy() {
    try {
      A node = (A) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:59
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public A fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:69
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopyNoTransform() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:89
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopy() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:103
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test61", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test13\\Test.jrag:2")
  @SideEffect.Pure(group="_ASTNode") public int c2_a() {
    if (c2_a_computed) {
      return c2_a_value;
    }
    ASTState state = state();
    if (!c2_a_initialized) {
      c2_a_initialized = true;
      c2_a_value = 1;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        c2_a_cycle = state.nextCycle();
        int new_c2_a_value = getParent().Define_c2_a(this, null);
        if (c2_a_value != new_c2_a_value) {
          state.setChangeInCycle();
        }
        c2_a_value = new_c2_a_value;
      } while (state.testAndClearChangeInCycle());
      c2_a_computed = true;
      state.startLastCycle();
      int $tmp = getParent().Define_c2_a(this, null);

      state.leaveCircle();
    } else if (c2_a_cycle != state.cycle()) {
      c2_a_cycle = state.cycle();
      if (state.lastCycle()) {
        c2_a_computed = true;
        int new_c2_a_value = getParent().Define_c2_a(this, null);
        return new_c2_a_value;
      }
      int new_c2_a_value = getParent().Define_c2_a(this, null);
      if (c2_a_value != new_c2_a_value) {
        state.setChangeInCycle();
      }
      c2_a_value = new_c2_a_value;
    } else {
    }
    return c2_a_value;
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle c2_a_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void c2_a_reset() {
    c2_a_computed = false;
    c2_a_initialized = false;
    c2_a_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c2_a_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected int c2_a_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c2_a_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test61", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test13\\Test.jrag:7")
  @SideEffect.Pure(group="_ASTNode") public int c2_b() {
    if (c2_b_computed) {
      return c2_b_value;
    }
    ASTState state = state();
    if (!c2_b_initialized) {
      c2_b_initialized = true;
      c2_b_value = 1;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        c2_b_cycle = state.nextCycle();
        int new_c2_b_value = getParent().Define_c2_b(this, null);
        if (c2_b_value != new_c2_b_value) {
          state.setChangeInCycle();
        }
        c2_b_value = new_c2_b_value;
      } while (state.testAndClearChangeInCycle());
      c2_b_computed = true;
      state.startLastCycle();
      int $tmp = getParent().Define_c2_b(this, null);

      state.leaveCircle();
    } else if (c2_b_cycle != state.cycle()) {
      c2_b_cycle = state.cycle();
      if (state.lastCycle()) {
        c2_b_computed = true;
        int new_c2_b_value = getParent().Define_c2_b(this, null);
        return new_c2_b_value;
      }
      int new_c2_b_value = getParent().Define_c2_b(this, null);
      if (c2_b_value != new_c2_b_value) {
        state.setChangeInCycle();
      }
      c2_b_value = new_c2_b_value;
    } else {
    }
    return c2_b_value;
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle c2_b_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void c2_b_reset() {
    c2_b_computed = false;
    c2_b_initialized = false;
    c2_b_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c2_b_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected int c2_b_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c2_b_initialized = false;
  /**
   * @attribute inh
   * @aspect Test61
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test13\\Test.jrag:13
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="Test61", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test13\\Test.jrag:13")
  @SideEffect.Pure(group="_ASTNode") public int c3() {
    if (c3_visited) {
      throw new RuntimeException("Circular definition of attribute A.c3().");
    }
    c3_visited = true;
    int c3_value = getParent().Define_c3(this, null);
    c3_visited = false;
    return c3_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean c3_visited = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test61", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test13\\Test.jrag:18")
  @SideEffect.Pure(group="_ASTNode") public int c1_a() {
    if (c1_a_computed) {
      return c1_a_value;
    }
    ASTState state = state();
    if (!c1_a_initialized) {
      c1_a_initialized = true;
      c1_a_value = 1;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        c1_a_cycle = state.nextCycle();
        int new_c1_a_value = getParent().Define_c1_a(this, null);
        if (c1_a_value != new_c1_a_value) {
          state.setChangeInCycle();
        }
        c1_a_value = new_c1_a_value;
      } while (state.testAndClearChangeInCycle());
      c1_a_computed = true;
      state.startLastCycle();
      int $tmp = getParent().Define_c1_a(this, null);

      state.leaveCircle();
    } else if (c1_a_cycle != state.cycle()) {
      c1_a_cycle = state.cycle();
      if (state.lastCycle()) {
        c1_a_computed = true;
        int new_c1_a_value = getParent().Define_c1_a(this, null);
        return new_c1_a_value;
      }
      int new_c1_a_value = getParent().Define_c1_a(this, null);
      if (c1_a_value != new_c1_a_value) {
        state.setChangeInCycle();
      }
      c1_a_value = new_c1_a_value;
    } else {
    }
    return c1_a_value;
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle c1_a_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void c1_a_reset() {
    c1_a_computed = false;
    c1_a_initialized = false;
    c1_a_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c1_a_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected int c1_a_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c1_a_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test61", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test13\\Test.jrag:24")
  @SideEffect.Pure(group="_ASTNode") public int c1_b() {
    if (c1_b_computed) {
      return c1_b_value;
    }
    ASTState state = state();
    if (!c1_b_initialized) {
      c1_b_initialized = true;
      c1_b_value = 1;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        c1_b_cycle = state.nextCycle();
        int new_c1_b_value = getParent().Define_c1_b(this, null);
        if (c1_b_value != new_c1_b_value) {
          state.setChangeInCycle();
        }
        c1_b_value = new_c1_b_value;
      } while (state.testAndClearChangeInCycle());
      c1_b_computed = true;
      state.startLastCycle();
      int $tmp = getParent().Define_c1_b(this, null);

      state.leaveCircle();
    } else if (c1_b_cycle != state.cycle()) {
      c1_b_cycle = state.cycle();
      if (state.lastCycle()) {
        c1_b_computed = true;
        int new_c1_b_value = getParent().Define_c1_b(this, null);
        return new_c1_b_value;
      }
      int new_c1_b_value = getParent().Define_c1_b(this, null);
      if (c1_b_value != new_c1_b_value) {
        state.setChangeInCycle();
      }
      c1_b_value = new_c1_b_value;
    } else {
    }
    return c1_b_value;
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle c1_b_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void c1_b_reset() {
    c1_b_computed = false;
    c1_b_initialized = false;
    c1_b_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c1_b_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected int c1_b_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c1_b_initialized = false;
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
