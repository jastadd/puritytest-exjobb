/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\circular\test14;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test14\\Test.ast:1
 * @astdecl A : ASTNode ::= B;
 * @production A : {@link ASTNode} ::= <span class="component">{@link B}</span>;

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public A() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
  }
  /**
   * @declaredat ASTNode:13
   */
  @ASTNodeAnnotation.Constructor(
    name = {"B"},
    type = {"B"},
    kind = {"Child"}
  )
  public A(B p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:22
   */
  @SideEffect.Pure protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:28
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:32
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    a_String_reset();
    a_int_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:42
   */
  @SideEffect.Ignore @SideEffect.Fresh public A clone() throws CloneNotSupportedException {
    A node = (A) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:47
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public A copy() {
    try {
      A node = (A) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:66
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public A fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:76
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopyNoTransform() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:96
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopy() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:110
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the B child.
   * @param node The new node to replace the B child.
   * @apilevel high-level
   */
  public void setB(B node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the B child.
   * @return The current node used as the B child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="B")
  @SideEffect.Pure public B getB() {
    return (B) getChild(0);
  }
  /**
   * Retrieves the B child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the B child.
   * @apilevel low-level
   */
  @SideEffect.Pure public B getBNoTransform() {
    return (B) getChildNoTransform(0);
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void a_String_reset() {
    a_String_values = null;
  }
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map a_String_values;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test62", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test14\\Test.jrag:2")
  @SideEffect.Pure(group="_ASTNode") public boolean a(String name) {
    Object _parameters = name;
    if (a_String_values == null) a_String_values = new java.util.HashMap(4);
    ASTState.CircularValue _value;
    if (a_String_values.containsKey(_parameters)) {
      Object _cache = a_String_values.get(_parameters);
      if (!(_cache instanceof ASTState.CircularValue)) {
        return (Boolean) _cache;
      } else {
        _value = (ASTState.CircularValue) _cache;
      }
    } else {
      _value = new ASTState.CircularValue();
      a_String_values.put(_parameters, _value);
      _value.value = false;
    }
    ASTState state = state();
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      boolean new_a_String_value;
      do {
        _value.cycle = state.nextCycle();
        new_a_String_value = a_compute(name);
        if (((Boolean)_value.value) != new_a_String_value) {
          state.setChangeInCycle();
          _value.value = new_a_String_value;
        }
      } while (state.testAndClearChangeInCycle());
      a_String_values.put(_parameters, new_a_String_value);
      state.startLastCycle();
      boolean $tmp = a_compute(name);

      state.leaveCircle();
      return new_a_String_value;
    } else if (_value.cycle != state.cycle()) {
      _value.cycle = state.cycle();
      boolean new_a_String_value = a_compute(name);
      if (state.lastCycle()) {
        a_String_values.put(_parameters, new_a_String_value);
      }
      if (((Boolean)_value.value) != new_a_String_value) {
        state.setChangeInCycle();
        _value.value = new_a_String_value;
      }
      return new_a_String_value;
    } else {
      return (Boolean) _value.value;
    }
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean a_compute(String name) {
  	  	return (a(1) == 1);
  	  }
  /** @apilevel internal */
  @SideEffect.Ignore private void a_int_reset() {
    a_int_values = null;
  }
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map a_int_values;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test62", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test14\\Test.jrag:8")
  @SideEffect.Pure(group="_ASTNode") public int a(int i) {
    Object _parameters = i;
    if (a_int_values == null) a_int_values = new java.util.HashMap(4);
    ASTState.CircularValue _value;
    if (a_int_values.containsKey(_parameters)) {
      Object _cache = a_int_values.get(_parameters);
      if (!(_cache instanceof ASTState.CircularValue)) {
        return (Integer) _cache;
      } else {
        _value = (ASTState.CircularValue) _cache;
      }
    } else {
      _value = new ASTState.CircularValue();
      a_int_values.put(_parameters, _value);
      _value.value = 1;
    }
    ASTState state = state();
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      int new_a_int_value;
      do {
        _value.cycle = state.nextCycle();
        new_a_int_value = 1;
        if (((Integer)_value.value) != new_a_int_value) {
          state.setChangeInCycle();
          _value.value = new_a_int_value;
        }
      } while (state.testAndClearChangeInCycle());
      a_int_values.put(_parameters, new_a_int_value);
      state.startLastCycle();
      int $tmp = 1;

      state.leaveCircle();
      return new_a_int_value;
    } else if (_value.cycle != state.cycle()) {
      _value.cycle = state.cycle();
      int new_a_int_value = 1;
      if (state.lastCycle()) {
        a_int_values.put(_parameters, new_a_int_value);
      }
      if (((Integer)_value.value) != new_a_int_value) {
        state.setChangeInCycle();
        _value.value = new_a_int_value;
      }
      return new_a_int_value;
    } else {
      return (Integer) _value.value;
    }
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
