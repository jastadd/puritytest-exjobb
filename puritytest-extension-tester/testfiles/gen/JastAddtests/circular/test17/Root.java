/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\circular\test17;
import java.util.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test17\\Test.ast:1
 * @astdecl Root : ASTNode;
 * @production Root : {@link ASTNode};

 */
public class Root extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public Root() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:19
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:23
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    a1_reset();
    a2_reset();
    a3_reset();
    b_reset();
    c1_reset();
    c2_reset();
    c3_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:34
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  @SideEffect.Ignore @SideEffect.Fresh public Root clone() throws CloneNotSupportedException {
    Root node = (Root) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public Root copy() {
    try {
      Root node = (Root) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:62
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Root fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:72
   */
  @SideEffect.Fresh(group="_ASTNode") public Root treeCopyNoTransform() {
    Root tree = (Root) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:92
   */
  @SideEffect.Fresh(group="_ASTNode") public Root treeCopy() {
    Root tree = (Root) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:106
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle a1_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void a1_reset() {
    a1_computed = false;
    a1_initialized = false;
    a1_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a1_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a1_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a1_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test17\\Test.jrag:4")
  @SideEffect.Pure(group="_ASTNode") public boolean a1() {
    if (a1_computed) {
      return a1_value;
    }
    ASTState state = state();
    if (!a1_initialized) {
      a1_initialized = true;
      a1_value = true;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        a1_cycle = state.nextCycle();
        boolean new_a1_value = a2();
        if (a1_value != new_a1_value) {
          state.setChangeInCycle();
        }
        a1_value = new_a1_value;
      } while (state.testAndClearChangeInCycle());
      a1_computed = true;
      state.startLastCycle();
      boolean $tmp = a2();

      state.leaveCircle();
    } else if (a1_cycle != state.cycle()) {
      a1_cycle = state.cycle();
      if (state.lastCycle()) {
        a1_computed = true;
        boolean new_a1_value = a2();
        return new_a1_value;
      }
      boolean new_a1_value = a2();
      if (a1_value != new_a1_value) {
        state.setChangeInCycle();
      }
      a1_value = new_a1_value;
    } else {
    }
    return a1_value;
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle a2_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void a2_reset() {
    a2_computed = false;
    a2_initialized = false;
    a2_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a2_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a2_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a2_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test17\\Test.jrag:5")
  @SideEffect.Pure(group="_ASTNode") public boolean a2() {
    if (a2_computed) {
      return a2_value;
    }
    ASTState state = state();
    if (!a2_initialized) {
      a2_initialized = true;
      a2_value = true;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        a2_cycle = state.nextCycle();
        boolean new_a2_value = a3();
        if (a2_value != new_a2_value) {
          state.setChangeInCycle();
        }
        a2_value = new_a2_value;
      } while (state.testAndClearChangeInCycle());
      a2_computed = true;
      state.startLastCycle();
      boolean $tmp = a3();

      state.leaveCircle();
    } else if (a2_cycle != state.cycle()) {
      a2_cycle = state.cycle();
      if (state.lastCycle()) {
        a2_computed = true;
        boolean new_a2_value = a3();
        return new_a2_value;
      }
      boolean new_a2_value = a3();
      if (a2_value != new_a2_value) {
        state.setChangeInCycle();
      }
      a2_value = new_a2_value;
    } else {
    }
    return a2_value;
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle a3_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void a3_reset() {
    a3_computed = false;
    a3_initialized = false;
    a3_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a3_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a3_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a3_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test17\\Test.jrag:6")
  @SideEffect.Pure(group="_ASTNode") public boolean a3() {
    if (a3_computed) {
      return a3_value;
    }
    ASTState state = state();
    if (!a3_initialized) {
      a3_initialized = true;
      a3_value = true;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        a3_cycle = state.nextCycle();
        boolean new_a3_value = a3_compute();
        if (a3_value != new_a3_value) {
          state.setChangeInCycle();
        }
        a3_value = new_a3_value;
      } while (state.testAndClearChangeInCycle());
      a3_computed = true;
      state.startLastCycle();
      boolean $tmp = a3_compute();

      state.leaveCircle();
    } else if (a3_cycle != state.cycle()) {
      a3_cycle = state.cycle();
      if (state.lastCycle()) {
        a3_computed = true;
        boolean new_a3_value = a3_compute();
        return new_a3_value;
      }
      boolean new_a3_value = a3_compute();
      if (a3_value != new_a3_value) {
        state.setChangeInCycle();
      }
      a3_value = new_a3_value;
    } else {
    }
    return a3_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean a3_compute() {
  		b();
  		return a1();
  	}
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean b_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void b_reset() {
    b_computed = false;
    b_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean b_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected int b_value;

  /**
   * @attribute syn
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test17\\Test.jrag:11
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test17\\Test.jrag:11")
  @SideEffect.Pure(group="_ASTNode") public int b() {
    ASTState state = state();
    if (b_computed) {
      return b_value;
    }
    if (b_visited) {
      throw new RuntimeException("Circular definition of attribute Root.b().");
    }
    b_visited = true;
    state().enterLazyAttribute();
    b_value = c1();
    b_computed = true;
    state().leaveLazyAttribute();
    b_visited = false;
    return b_value;
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle c1_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void c1_reset() {
    c1_computed = false;
    c1_initialized = false;
    c1_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c1_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected int c1_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c1_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test17\\Test.jrag:13")
  @SideEffect.Pure(group="_ASTNode") public int c1() {
    if (c1_computed) {
      return c1_value;
    }
    ASTState state = state();
    if (!c1_initialized) {
      c1_initialized = true;
      c1_value = 0;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        c1_cycle = state.nextCycle();
        int new_c1_value = Math.min(5, c2() + 1);
        if (c1_value != new_c1_value) {
          state.setChangeInCycle();
        }
        c1_value = new_c1_value;
      } while (state.testAndClearChangeInCycle());
      c1_computed = true;
      state.startLastCycle();
      int $tmp = Math.min(5, c2() + 1);

      state.leaveCircle();
    } else if (c1_cycle != state.cycle()) {
      c1_cycle = state.cycle();
      if (state.lastCycle()) {
        c1_computed = true;
        int new_c1_value = Math.min(5, c2() + 1);
        return new_c1_value;
      }
      int new_c1_value = Math.min(5, c2() + 1);
      if (c1_value != new_c1_value) {
        state.setChangeInCycle();
      }
      c1_value = new_c1_value;
    } else {
    }
    return c1_value;
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle c2_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void c2_reset() {
    c2_computed = false;
    c2_initialized = false;
    c2_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c2_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected int c2_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c2_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test17\\Test.jrag:15")
  @SideEffect.Pure(group="_ASTNode") public int c2() {
    if (c2_computed) {
      return c2_value;
    }
    ASTState state = state();
    if (!c2_initialized) {
      c2_initialized = true;
      c2_value = 0;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        c2_cycle = state.nextCycle();
        int new_c2_value = c3();
        if (c2_value != new_c2_value) {
          state.setChangeInCycle();
        }
        c2_value = new_c2_value;
      } while (state.testAndClearChangeInCycle());
      c2_computed = true;
      state.startLastCycle();
      int $tmp = c3();

      state.leaveCircle();
    } else if (c2_cycle != state.cycle()) {
      c2_cycle = state.cycle();
      if (state.lastCycle()) {
        c2_computed = true;
        int new_c2_value = c3();
        return new_c2_value;
      }
      int new_c2_value = c3();
      if (c2_value != new_c2_value) {
        state.setChangeInCycle();
      }
      c2_value = new_c2_value;
    } else {
    }
    return c2_value;
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle c3_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void c3_reset() {
    c3_computed = false;
    c3_initialized = false;
    c3_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c3_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected int c3_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c3_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test17\\Test.jrag:17")
  @SideEffect.Pure(group="_ASTNode") public int c3() {
    if (c3_computed) {
      return c3_value;
    }
    ASTState state = state();
    if (!c3_initialized) {
      c3_initialized = true;
      c3_value = 0;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        c3_cycle = state.nextCycle();
        int new_c3_value = c1();
        if (c3_value != new_c3_value) {
          state.setChangeInCycle();
        }
        c3_value = new_c3_value;
      } while (state.testAndClearChangeInCycle());
      c3_computed = true;
      state.startLastCycle();
      int $tmp = c1();

      state.leaveCircle();
    } else if (c3_cycle != state.cycle()) {
      c3_cycle = state.cycle();
      if (state.lastCycle()) {
        c3_computed = true;
        int new_c3_value = c1();
        return new_c3_value;
      }
      int new_c3_value = c1();
      if (c3_value != new_c3_value) {
        state.setChangeInCycle();
      }
      c3_value = new_c3_value;
    } else {
    }
    return c3_value;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
