/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\circular\runtime-check_05p;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\runtime-check_05p\\Test.ast:1
 * @astdecl Node : ASTNode;
 * @production Node : {@link ASTNode};

 */
public class Node extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public Node() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:19
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:23
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    circ_reset();
    limit_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:29
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  @SideEffect.Ignore @SideEffect.Fresh public Node clone() throws CloneNotSupportedException {
    Node node = (Node) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public Node copy() {
    try {
      Node node = (Node) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:57
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Node fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:67
   */
  @SideEffect.Fresh(group="_ASTNode") public Node treeCopyNoTransform() {
    Node tree = (Node) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:87
   */
  @SideEffect.Fresh(group="_ASTNode") public Node treeCopy() {
    Node tree = (Node) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:101
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle circ_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void circ_reset() {
    circ_computed = false;
    circ_initialized = false;
    circ_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean circ_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected int circ_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean circ_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\runtime-check_05p\\Test.jrag:2")
  @SideEffect.Pure(group="_ASTNode") public int circ() {
    if (circ_computed) {
      return circ_value;
    }
    ASTState state = state();
    if (!circ_initialized) {
      circ_initialized = true;
      circ_value = 0;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        circ_cycle = state.nextCycle();
        int new_circ_value = min(circ() + 1, limit());
        if (circ_value != new_circ_value) {
          state.setChangeInCycle();
        }
        circ_value = new_circ_value;
      } while (state.testAndClearChangeInCycle());
      circ_computed = true;
      state.startLastCycle();
      int $tmp = min(circ() + 1, limit());

      state.leaveCircle();
    } else if (circ_cycle != state.cycle()) {
      circ_cycle = state.cycle();
      if (state.lastCycle()) {
        circ_computed = true;
        int new_circ_value = min(circ() + 1, limit());
        return new_circ_value;
      }
      int new_circ_value = min(circ() + 1, limit());
      if (circ_value != new_circ_value) {
        state.setChangeInCycle();
      }
      circ_value = new_circ_value;
    } else {
    }
    return circ_value;
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle limit_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void limit_reset() {
    limit_computed = false;
    limit_initialized = false;
    limit_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean limit_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected int limit_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean limit_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\runtime-check_05p\\Test.jrag:4")
  @SideEffect.Pure(group="_ASTNode") public int limit() {
    if (limit_computed) {
      return limit_value;
    }
    ASTState state = state();
    if (!limit_initialized) {
      limit_initialized = true;
      limit_value = 0;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        limit_cycle = state.nextCycle();
        int new_limit_value = min(limit() + 1, 12);
        if (limit_value != new_limit_value) {
          state.setChangeInCycle();
        }
        limit_value = new_limit_value;
      } while (state.testAndClearChangeInCycle());
      limit_computed = true;
      state.startLastCycle();
      int $tmp = min(limit() + 1, 12);

      state.leaveCircle();
    } else if (limit_cycle != state.cycle()) {
      limit_cycle = state.cycle();
      if (state.lastCycle()) {
        limit_computed = true;
        int new_limit_value = min(limit() + 1, 12);
        return new_limit_value;
      }
      int new_limit_value = min(limit() + 1, 12);
      if (limit_value != new_limit_value) {
        state.setChangeInCycle();
      }
      limit_value = new_limit_value;
    } else {
    }
    return limit_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Set min_int_int_visited;
  /**
   * @attribute syn
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\runtime-check_05p\\Test.jrag:6
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\runtime-check_05p\\Test.jrag:6")
  @SideEffect.Pure(group="_ASTNode") public int min(int a, int b) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(a);
    _parameters.add(b);
    if (min_int_int_visited == null) min_int_int_visited = new java.util.HashSet(4);
    if (min_int_int_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute Node.min(int,int).");
    }
    min_int_int_visited.add(_parameters);
    try {
        if (a < b) {
          return a;
        } else {
          return b;
        }
      }
    finally {
      min_int_int_visited.remove(_parameters);
    }
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
