/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\circular\test09;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test09\\Test.ast:1
 * @astdecl A : ASTNode;
 * @production A : {@link ASTNode};

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public A() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:19
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:23
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    a_reset();
    b_reset();
    c_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:30
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:34
   */
  @SideEffect.Ignore @SideEffect.Fresh public A clone() throws CloneNotSupportedException {
    A node = (A) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:39
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public A copy() {
    try {
      A node = (A) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:58
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public A fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:68
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopyNoTransform() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:88
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopy() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:102
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle a_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void a_reset() {
    a_computed = false;
    a_initialized = false;
    a_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test09\\Test.jrag:2")
  @SideEffect.Pure(group="_ASTNode") public boolean a() {
    if (a_computed) {
      return a_value;
    }
    ASTState state = state();
    if (!a_initialized) {
      a_initialized = true;
      a_value = false;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        a_cycle = state.nextCycle();
        boolean new_a_value = a_compute();
        if (a_value != new_a_value) {
          state.setChangeInCycle();
        }
        a_value = new_a_value;
      } while (state.testAndClearChangeInCycle());
      a_computed = true;
      state.startLastCycle();
      boolean $tmp = a_compute();

      state.leaveCircle();
    } else if (a_cycle != state.cycle()) {
      a_cycle = state.cycle();
      if (state.lastCycle()) {
        a_computed = true;
        boolean new_a_value = a_compute();
        return new_a_value;
      }
      boolean new_a_value = a_compute();
      if (a_value != new_a_value) {
        state.setChangeInCycle();
      }
      a_value = new_a_value;
    } else {
    }
    return a_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean a_compute() {
      System.out.println("Eval a()");
      return b();
    }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle b_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void b_reset() {
    b_computed = false;
    b_initialized = false;
    b_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean b_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean b_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean b_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test09\\Test.jrag:6")
  @SideEffect.Pure(group="_ASTNode") public boolean b() {
    if (b_computed) {
      return b_value;
    }
    ASTState state = state();
    if (!b_initialized) {
      b_initialized = true;
      b_value = false;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        b_cycle = state.nextCycle();
        boolean new_b_value = b_compute();
        if (b_value != new_b_value) {
          state.setChangeInCycle();
        }
        b_value = new_b_value;
      } while (state.testAndClearChangeInCycle());
      b_computed = true;
      state.startLastCycle();
      boolean $tmp = b_compute();

      state.leaveCircle();
    } else if (b_cycle != state.cycle()) {
      b_cycle = state.cycle();
      if (state.lastCycle()) {
        b_computed = true;
        boolean new_b_value = b_compute();
        return new_b_value;
      }
      boolean new_b_value = b_compute();
      if (b_value != new_b_value) {
        state.setChangeInCycle();
      }
      b_value = new_b_value;
    } else {
    }
    return b_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean b_compute() {
      System.out.println("Eval b()");
      return c();
    }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle c_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void c_reset() {
    c_computed = false;
    c_initialized = false;
    c_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\test09\\Test.jrag:10")
  @SideEffect.Pure(group="_ASTNode") public boolean c() {
    if (c_computed) {
      return c_value;
    }
    ASTState state = state();
    if (!c_initialized) {
      c_initialized = true;
      c_value = false;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        c_cycle = state.nextCycle();
        boolean new_c_value = c_compute();
        if (c_value != new_c_value) {
          state.setChangeInCycle();
        }
        c_value = new_c_value;
      } while (state.testAndClearChangeInCycle());
      c_computed = true;
      state.startLastCycle();
      boolean $tmp = c_compute();

      state.leaveCircle();
    } else if (c_cycle != state.cycle()) {
      c_cycle = state.cycle();
      if (state.lastCycle()) {
        c_computed = true;
        boolean new_c_value = c_compute();
        return new_c_value;
      }
      boolean new_c_value = c_compute();
      if (c_value != new_c_value) {
        state.setChangeInCycle();
      }
      c_value = new_c_value;
    } else {
    }
    return c_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean c_compute() {
      System.out.println("Eval c()");
      return a() || true;
    }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
