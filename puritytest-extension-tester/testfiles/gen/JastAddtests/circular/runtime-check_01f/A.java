/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\circular\runtime-check_01f;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\runtime-check_01f\\Test.ast:1
 * @astdecl A : ASTNode;
 * @production A : {@link ASTNode};

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public A() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:19
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:23
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    a_String_reset();
    a_int_reset();
    c1_reset();
    c2_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:31
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:35
   */
  @SideEffect.Ignore @SideEffect.Fresh public A clone() throws CloneNotSupportedException {
    A node = (A) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:40
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public A copy() {
    try {
      A node = (A) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:59
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public A fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:69
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopyNoTransform() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:89
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopy() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:103
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void a_String_reset() {
    a_String_values = null;
  }
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map a_String_values;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test63", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\runtime-check_01f\\Test.jrag:11")
  @SideEffect.Pure(group="_ASTNode") public int a(String name) {
    Object _parameters = name;
    if (a_String_values == null) a_String_values = new java.util.HashMap(4);
    ASTState.CircularValue _value;
    if (a_String_values.containsKey(_parameters)) {
      Object _cache = a_String_values.get(_parameters);
      if (!(_cache instanceof ASTState.CircularValue)) {
        return (Integer) _cache;
      } else {
        _value = (ASTState.CircularValue) _cache;
      }
    } else {
      _value = new ASTState.CircularValue();
      a_String_values.put(_parameters, _value);
      _value.value = 1;
    }
    ASTState state = state();
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      int new_a_String_value;
      do {
        _value.cycle = state.nextCycle();
        new_a_String_value = a_compute(name);
        if (((Integer)_value.value) != new_a_String_value) {
          state.setChangeInCycle();
          _value.value = new_a_String_value;
        }
      } while (state.testAndClearChangeInCycle());
      a_String_values.put(_parameters, new_a_String_value);
      state.startLastCycle();
      int $tmp = a_compute(name);

      state.leaveCircle();
      return new_a_String_value;
    } else if (_value.cycle != state.cycle()) {
      _value.cycle = state.cycle();
      int new_a_String_value = a_compute(name);
      if (state.lastCycle()) {
        a_String_values.put(_parameters, new_a_String_value);
      }
      if (((Integer)_value.value) != new_a_String_value) {
        state.setChangeInCycle();
        _value.value = new_a_String_value;
      }
      return new_a_String_value;
    } else {
      return (Integer) _value.value;
    }
  }
  /** @apilevel internal */
  @SideEffect.Pure private int a_compute(String name) {
      if (a(1) >= 2) {
        return 3;
      } else {
        return a(1) + 1;
      }
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void a_int_reset() {
    a_int_values = null;
  }
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map a_int_values;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test63", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\runtime-check_01f\\Test.jrag:19")
  @SideEffect.Pure(group="_ASTNode") public int a(int i) {
    Object _parameters = i;
    if (a_int_values == null) a_int_values = new java.util.HashMap(4);
    ASTState.CircularValue _value;
    if (a_int_values.containsKey(_parameters)) {
      Object _cache = a_int_values.get(_parameters);
      if (!(_cache instanceof ASTState.CircularValue)) {
        return (Integer) _cache;
      } else {
        _value = (ASTState.CircularValue) _cache;
      }
    } else {
      _value = new ASTState.CircularValue();
      a_int_values.put(_parameters, _value);
      _value.value = 1;
    }
    ASTState state = state();
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      int new_a_int_value;
      do {
        _value.cycle = state.nextCycle();
        new_a_int_value = a_compute(i);
        if (((Integer)_value.value) != new_a_int_value) {
          state.setChangeInCycle();
          _value.value = new_a_int_value;
        }
      } while (state.testAndClearChangeInCycle());
      a_int_values.put(_parameters, new_a_int_value);
      state.startLastCycle();
      int $tmp = a_compute(i);

      state.leaveCircle();
      return new_a_int_value;
    } else if (_value.cycle != state.cycle()) {
      _value.cycle = state.cycle();
      int new_a_int_value = a_compute(i);
      if (state.lastCycle()) {
        a_int_values.put(_parameters, new_a_int_value);
      }
      if (((Integer)_value.value) != new_a_int_value) {
        state.setChangeInCycle();
        _value.value = new_a_int_value;
      }
      return new_a_int_value;
    } else {
      return (Integer) _value.value;
    }
  }
  /** @apilevel internal */
  @SideEffect.Pure private int a_compute(int i) {
      return a("x") + b();
    }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean b_visited = false;
  /**
   * @attribute syn
   * @aspect Test63
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\runtime-check_01f\\Test.jrag:23
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test63", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\runtime-check_01f\\Test.jrag:23")
  @SideEffect.Pure(group="_ASTNode") public int b() {
    if (b_visited) {
      throw new RuntimeException("Circular definition of attribute A.b().");
    }
    b_visited = true;
    int b_value = c1();
    b_visited = false;
    return b_value;
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle c1_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void c1_reset() {
    c1_computed = false;
    c1_initialized = false;
    c1_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c1_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected int c1_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c1_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test63", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\runtime-check_01f\\Test.jrag:25")
  @SideEffect.Pure(group="_ASTNode") public int c1() {
    if (c1_computed) {
      return c1_value;
    }
    ASTState state = state();
    if (!c1_initialized) {
      c1_initialized = true;
      c1_value = 0;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        c1_cycle = state.nextCycle();
        int new_c1_value = c2();
        if (c1_value != new_c1_value) {
          state.setChangeInCycle();
        }
        c1_value = new_c1_value;
      } while (state.testAndClearChangeInCycle());
      c1_computed = true;
      state.startLastCycle();
      int $tmp = c2();

      state.leaveCircle();
    } else if (c1_cycle != state.cycle()) {
      c1_cycle = state.cycle();
      if (state.lastCycle()) {
        c1_computed = true;
        int new_c1_value = c2();
        return new_c1_value;
      }
      int new_c1_value = c2();
      if (c1_value != new_c1_value) {
        state.setChangeInCycle();
      }
      c1_value = new_c1_value;
    } else {
    }
    return c1_value;
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle c2_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void c2_reset() {
    c2_computed = false;
    c2_initialized = false;
    c2_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c2_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected int c2_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean c2_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test63", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\circular\\runtime-check_01f\\Test.jrag:26")
  @SideEffect.Pure(group="_ASTNode") public int c2() {
    if (c2_computed) {
      return c2_value;
    }
    ASTState state = state();
    if (!c2_initialized) {
      c2_initialized = true;
      c2_value = 0;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        c2_cycle = state.nextCycle();
        int new_c2_value = c1() + a(1);
        if (c2_value != new_c2_value) {
          state.setChangeInCycle();
        }
        c2_value = new_c2_value;
      } while (state.testAndClearChangeInCycle());
      c2_computed = true;
      state.startLastCycle();
      int $tmp = c1() + a(1);

      state.leaveCircle();
    } else if (c2_cycle != state.cycle()) {
      c2_cycle = state.cycle();
      if (state.lastCycle()) {
        c2_computed = true;
        int new_c2_value = c1() + a(1);
        return new_c2_value;
      }
      int new_c2_value = c1() + a(1);
      if (c2_value != new_c2_value) {
        state.setChangeInCycle();
      }
      c2_value = new_c2_value;
    } else {
    }
    return c2_value;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
