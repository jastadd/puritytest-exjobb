/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\inh\opt_01p;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\inh\\opt_01p\\Test.ast:1
 * @astdecl Node : ASTNode ::= [A];
 * @production Node : {@link ASTNode} ::= <span class="component">[{@link A}]</span>;

 */
public class Node extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public Node() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
    setChild(new Opt(), 0);
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"A"},
    type = {"Opt<A>"},
    kind = {"Opt"}
  )
  public Node(Opt<A> p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:23
   */
  @SideEffect.Pure protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:29
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:41
   */
  @SideEffect.Ignore @SideEffect.Fresh public Node clone() throws CloneNotSupportedException {
    Node node = (Node) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:46
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public Node copy() {
    try {
      Node node = (Node) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:65
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Node fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:75
   */
  @SideEffect.Fresh(group="_ASTNode") public Node treeCopyNoTransform() {
    Node tree = (Node) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:95
   */
  @SideEffect.Fresh(group="_ASTNode") public Node treeCopy() {
    Node tree = (Node) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:109
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the optional node for the A child. This is the <code>Opt</code>
   * node containing the child A, not the actual child!
   * @param opt The new node to be used as the optional node for the A child.
   * @apilevel low-level
   */
  public void setAOpt(Opt<A> opt) {
    setChild(opt, 0);
  }
  /**
   * Replaces the (optional) A child.
   * @param node The new node to be used as the A child.
   * @apilevel high-level
   */
  public void setA(A node) {
    getAOpt().setChild(node, 0);
  }
  /**
   * Check whether the optional A child exists.
   * @return {@code true} if the optional A child exists, {@code false} if it does not.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasA() {
    return getAOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) A child.
   * @return The A child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   */
  @SideEffect.Pure public A getA() {
    return (A) getAOpt().getChild(0);
  }
  /**
   * Retrieves the optional node for the A child. This is the <code>Opt</code> node containing the child A, not the actual child!
   * @return The optional node for child the A child.
   * @apilevel low-level
   */
  @ASTNodeAnnotation.OptChild(name="A")
  @SideEffect.Pure public Opt<A> getAOpt() {
    return (Opt<A>) getChild(0);
  }
  /**
   * Retrieves the optional node for child A. This is the <code>Opt</code> node containing the child A, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child A.
   * @apilevel low-level
   */
  @SideEffect.Pure public Opt<A> getAOptNoTransform() {
    return (Opt<A>) getChildNoTransform(0);
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\inh\\opt_01p\\Test.jrag:2
   * @apilevel internal
   */
 @SideEffect.Pure(group="_ASTNode") public int Define_value(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getAOptNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\inh\\opt_01p\\Test.jrag:3
      return 177;
    }
    else {
      return getParent().Define_value(this, _callerNode);
    }
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\inh\\opt_01p\\Test.jrag:2
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute value
   */
  @SideEffect.Pure protected boolean canDefine_value(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
