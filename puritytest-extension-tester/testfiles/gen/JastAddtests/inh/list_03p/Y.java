/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\inh\list_03p;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\inh\\list_03p\\Test.ast:2
 * @astdecl Y : X;
 * @production Y : {@link X};

 */
public class Y extends X implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public Y() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
    setChild(new List(), 0);
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"X", "Y"},
    type = {"List<X>", "Y"},
    kind = {"List", "Child"}
  )
  public Y(List<X> p0, Y p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:24
   */
  @SideEffect.Pure protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:30
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:34
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:42
   */
  @SideEffect.Ignore @SideEffect.Fresh public Y clone() throws CloneNotSupportedException {
    Y node = (Y) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:47
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public Y copy() {
    try {
      Y node = (Y) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:66
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Y fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:76
   */
  @SideEffect.Fresh(group="_ASTNode") public Y treeCopyNoTransform() {
    Y tree = (Y) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:96
   */
  @SideEffect.Fresh(group="_ASTNode") public Y treeCopy() {
    Y tree = (Y) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:110
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the X list.
   * @param list The new list node to be used as the X list.
   * @apilevel high-level
   */
  public void setXList(List<X> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the X list.
   * @return Number of children in the X list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumX() {
    return getXList().getNumChild();
  }
  /**
   * Retrieves the number of children in the X list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the X list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumXNoTransform() {
    return getXListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the X list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the X list.
   * @apilevel high-level
   */
  @SideEffect.Pure public X getX(int i) {
    return (X) getXList().getChild(i);
  }
  /**
   * Check whether the X list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasX() {
    return getXList().getNumChild() != 0;
  }
  /**
   * Append an element to the X list.
   * @param node The element to append to the X list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addX(X node) {
    List<X> list = (parent == null) ? getXListNoTransform() : getXList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addXNoTransform(X node) {
    List<X> list = getXListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the X list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setX(X node, int i) {
    List<X> list = getXList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the X list.
   * @return The node representing the X list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="X")
  @SideEffect.Pure(group="_ASTNode") @SideEffect.FreshIf public List<X> getXList() {
    List<X> list = (List<X>) getChild(0);
    return list;
  }
  /**
   * Retrieves the X list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the X list.
   * @apilevel low-level
   */
  @SideEffect.Pure @SideEffect.FreshIf public List<X> getXListNoTransform() {
    return (List<X>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the X list without
   * triggering rewrites.
   */
  @SideEffect.Pure public X getXNoTransform(int i) {
    return (X) getXListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the X list.
   * @return The node representing the X list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<X> getXs() {
    return getXList();
  }
  /**
   * Retrieves the X list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the X list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<X> getXsNoTransform() {
    return getXListNoTransform();
  }
  /**
   * Replaces the Y child.
   * @param node The new node to replace the Y child.
   * @apilevel high-level
   */
  public void setY(Y node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the Y child.
   * @return The current node used as the Y child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Y")
  @SideEffect.Pure public Y getY() {
    return (Y) getChild(1);
  }
  /**
   * Retrieves the Y child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Y child.
   * @apilevel low-level
   */
  @SideEffect.Pure public Y getYNoTransform() {
    return (Y) getChildNoTransform(1);
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\inh\\list_03p\\Test.jrag:2
   * @apilevel internal
   */
 @SideEffect.Pure(group="_ASTNode") public boolean Define_childOfY(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\inh\\list_03p\\Test.jrag:2
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute childOfY
   */
  @SideEffect.Pure protected boolean canDefine_childOfY(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
