/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\inh\list_01p;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\inh\\list_01p\\Test.ast:1
 * @astdecl Node : ASTNode ::= A*;
 * @production Node : {@link ASTNode} ::= <span class="component">{@link A}*</span>;

 */
public class Node extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public Node() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
    setChild(new List(), 0);
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"A"},
    type = {"List<A>"},
    kind = {"List"}
  )
  public Node(List<A> p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:23
   */
  @SideEffect.Pure protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:29
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:41
   */
  @SideEffect.Ignore @SideEffect.Fresh public Node clone() throws CloneNotSupportedException {
    Node node = (Node) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:46
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public Node copy() {
    try {
      Node node = (Node) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:65
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Node fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:75
   */
  @SideEffect.Fresh(group="_ASTNode") public Node treeCopyNoTransform() {
    Node tree = (Node) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:95
   */
  @SideEffect.Fresh(group="_ASTNode") public Node treeCopy() {
    Node tree = (Node) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:109
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the A list.
   * @param list The new list node to be used as the A list.
   * @apilevel high-level
   */
  public void setAList(List<A> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the A list.
   * @return Number of children in the A list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumA() {
    return getAList().getNumChild();
  }
  /**
   * Retrieves the number of children in the A list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the A list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumANoTransform() {
    return getAListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the A list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the A list.
   * @apilevel high-level
   */
  @SideEffect.Pure public A getA(int i) {
    return (A) getAList().getChild(i);
  }
  /**
   * Check whether the A list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasA() {
    return getAList().getNumChild() != 0;
  }
  /**
   * Append an element to the A list.
   * @param node The element to append to the A list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addA(A node) {
    List<A> list = (parent == null) ? getAListNoTransform() : getAList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addANoTransform(A node) {
    List<A> list = getAListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the A list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setA(A node, int i) {
    List<A> list = getAList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the A list.
   * @return The node representing the A list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="A")
  @SideEffect.Pure(group="_ASTNode") @SideEffect.FreshIf public List<A> getAList() {
    List<A> list = (List<A>) getChild(0);
    return list;
  }
  /**
   * Retrieves the A list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the A list.
   * @apilevel low-level
   */
  @SideEffect.Pure @SideEffect.FreshIf public List<A> getAListNoTransform() {
    return (List<A>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the A list without
   * triggering rewrites.
   */
  @SideEffect.Pure public A getANoTransform(int i) {
    return (A) getAListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the A list.
   * @return The node representing the A list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<A> getAs() {
    return getAList();
  }
  /**
   * Retrieves the A list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the A list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<A> getAsNoTransform() {
    return getAListNoTransform();
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\inh\\list_01p\\Test.jrag:2
   * @apilevel internal
   */
 @SideEffect.Pure(group="_ASTNode") public int Define_value(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getAListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\inh\\list_01p\\Test.jrag:3
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return 13;
    }
    else {
      int i = this.getIndexOfChild(_callerNode);
      return 0;
    }
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\inh\\list_01p\\Test.jrag:2
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute value
   */
  @SideEffect.Pure protected boolean canDefine_value(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
