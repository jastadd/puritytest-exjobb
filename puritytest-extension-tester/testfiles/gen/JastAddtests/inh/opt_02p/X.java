/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\inh\opt_02p;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\inh\\opt_02p\\Test.ast:1
 * @astdecl X : ASTNode ::= [X] Y;
 * @production X : {@link ASTNode} ::= <span class="component">[{@link X}]</span> <span class="component">{@link Y}</span>;

 */
public class X extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public X() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
    setChild(new Opt(), 0);
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"X", "Y"},
    type = {"Opt<X>", "Y"},
    kind = {"Opt", "Child"}
  )
  public X(Opt<X> p0, Y p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:24
   */
  @SideEffect.Pure protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:30
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:34
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:42
   */
  @SideEffect.Ignore @SideEffect.Fresh public X clone() throws CloneNotSupportedException {
    X node = (X) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:47
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public X copy() {
    try {
      X node = (X) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:66
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public X fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:76
   */
  @SideEffect.Fresh(group="_ASTNode") public X treeCopyNoTransform() {
    X tree = (X) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:96
   */
  @SideEffect.Fresh(group="_ASTNode") public X treeCopy() {
    X tree = (X) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:110
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the optional node for the X child. This is the <code>Opt</code>
   * node containing the child X, not the actual child!
   * @param opt The new node to be used as the optional node for the X child.
   * @apilevel low-level
   */
  public void setXOpt(Opt<X> opt) {
    setChild(opt, 0);
  }
  /**
   * Replaces the (optional) X child.
   * @param node The new node to be used as the X child.
   * @apilevel high-level
   */
  public void setX(X node) {
    getXOpt().setChild(node, 0);
  }
  /**
   * Check whether the optional X child exists.
   * @return {@code true} if the optional X child exists, {@code false} if it does not.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasX() {
    return getXOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) X child.
   * @return The X child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   */
  @SideEffect.Pure public X getX() {
    return (X) getXOpt().getChild(0);
  }
  /**
   * Retrieves the optional node for the X child. This is the <code>Opt</code> node containing the child X, not the actual child!
   * @return The optional node for child the X child.
   * @apilevel low-level
   */
  @ASTNodeAnnotation.OptChild(name="X")
  @SideEffect.Pure public Opt<X> getXOpt() {
    return (Opt<X>) getChild(0);
  }
  /**
   * Retrieves the optional node for child X. This is the <code>Opt</code> node containing the child X, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child X.
   * @apilevel low-level
   */
  @SideEffect.Pure public Opt<X> getXOptNoTransform() {
    return (Opt<X>) getChildNoTransform(0);
  }
  /**
   * Replaces the Y child.
   * @param node The new node to replace the Y child.
   * @apilevel high-level
   */
  public void setY(Y node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the Y child.
   * @return The current node used as the Y child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Y")
  @SideEffect.Pure public Y getY() {
    return (Y) getChild(1);
  }
  /**
   * Retrieves the Y child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Y child.
   * @apilevel low-level
   */
  @SideEffect.Pure public Y getYNoTransform() {
    return (Y) getChildNoTransform(1);
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\inh\\opt_02p\\Test.jrag:2
   * @apilevel internal
   */
 @SideEffect.Pure(group="_ASTNode") public boolean Define_childOfY(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\inh\\opt_02p\\Test.jrag:2
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute childOfY
   */
  @SideEffect.Pure protected boolean canDefine_childOfY(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
