/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\coll\multi-target_01p;
import java.util.LinkedList;
import java.util.Collection;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\multi-target_01p\\Test.ast:4
 * @astdecl Decl : Node ::= <ID:String>;
 * @production Decl : {@link Node} ::= <span class="component">&lt;ID:String&gt;</span>;

 */
public class Decl extends Node implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public Decl() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /**
   * @declaredat ASTNode:12
   */
  @ASTNodeAnnotation.Constructor(
    name = {"ID"},
    type = {"String"},
    kind = {"Token"}
  )
  public Decl(String p0) {
    setID(p0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:21
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:27
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:31
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:35
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:39
   */
  @SideEffect.Ignore @SideEffect.Fresh public Decl clone() throws CloneNotSupportedException {
    Decl node = (Decl) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:44
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public Decl copy() {
    try {
      Decl node = (Decl) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:63
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Decl fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:73
   */
  @SideEffect.Fresh(group="_ASTNode") public Decl treeCopyNoTransform() {
    Decl tree = (Decl) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:93
   */
  @SideEffect.Fresh(group="_ASTNode") public Decl treeCopy() {
    Decl tree = (Decl) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:107
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_ID == ((Decl) node).tokenString_ID);    
  }
  /**
   * Replaces the lexeme ID.
   * @param value The new value for the lexeme ID.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setID(String value) {
    tokenString_ID = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_ID;
  /**
   * Retrieves the value for the lexeme ID.
   * @return The value for the lexeme ID.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="ID")
  @SideEffect.Pure(group="_ASTNode") public String getID() {
    return tokenString_ID != null ? tokenString_ID : "";
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean myUses_visited = false;
  /**
   * @attribute syn
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\multi-target_01p\\Test.jrag:17
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\multi-target_01p\\Test.jrag:17")
  @SideEffect.Pure(group="_ASTNode") public Collection<Use> myUses() {
    if (myUses_visited) {
      throw new RuntimeException("Circular definition of attribute Decl.myUses().");
    }
    myUses_visited = true;
    try {
        Collection<Use> uses = new LinkedList<Use>();
        for (Use use : root().uses()) {
          if (use.getID().equals(getID())) {
            uses.add(use);
          }
        }
        return uses;
      }
    finally {
      myUses_visited = false;
    }
  }
  /**
   * @attribute inh
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\multi-target_01p\\Test.jrag:5
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\multi-target_01p\\Test.jrag:5")
  @SideEffect.Pure(group="_ASTNode") public Root root() {
    if (root_visited) {
      throw new RuntimeException("Circular definition of attribute Decl.root().");
    }
    root_visited = true;
    Root root_value = getParent().Define_root(this, null);
    root_visited = false;
    return root_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean root_visited = false;
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
  /** @apilevel internal */
  @SideEffect.Local(group="_ASTNode") protected void collect_contributors_Use_decls(Root _root, @SideEffect.Ignore java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\multi-target_01p\\Test.jrag:15
    for (Use target : (Iterable<? extends Use>) (myUses())) {
      java.util.Set<ASTNode> contributors = _map.get(target);
      if (contributors == null) {
        contributors = new java.util.LinkedHashSet<ASTNode>();
        _map.put((ASTNode) target, contributors);
      }
      contributors.add(this);
    }
    super.collect_contributors_Use_decls(_root, _map);
  }
  /** @apilevel internal */
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_Use_decls(LinkedList<Decl> collection) {
    super.contributeTo_Use_decls(collection);
    collection.add(this);
  }
}
