/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\coll\subtype_02p;
import java.util.LinkedList;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\subtype_02p\\Test.ast:3
 * @astdecl B : A ::= [A];
 * @production B : {@link A} ::= <span class="component">[{@link A}]</span>;

 */
public class B extends A implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public B() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
    setChild(new Opt(), 0);
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"A"},
    type = {"Opt<A>"},
    kind = {"Opt"}
  )
  public B(Opt<A> p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:23
   */
  @SideEffect.Pure protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:29
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:41
   */
  @SideEffect.Ignore @SideEffect.Fresh public B clone() throws CloneNotSupportedException {
    B node = (B) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:46
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public B copy() {
    try {
      B node = (B) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:65
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public B fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:75
   */
  @SideEffect.Fresh(group="_ASTNode") public B treeCopyNoTransform() {
    B tree = (B) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:95
   */
  @SideEffect.Fresh(group="_ASTNode") public B treeCopy() {
    B tree = (B) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:109
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the optional node for the A child. This is the <code>Opt</code>
   * node containing the child A, not the actual child!
   * @param opt The new node to be used as the optional node for the A child.
   * @apilevel low-level
   */
  public void setAOpt(Opt<A> opt) {
    setChild(opt, 0);
  }
  /**
   * Replaces the (optional) A child.
   * @param node The new node to be used as the A child.
   * @apilevel high-level
   */
  public void setA(A node) {
    getAOpt().setChild(node, 0);
  }
  /**
   * Check whether the optional A child exists.
   * @return {@code true} if the optional A child exists, {@code false} if it does not.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasA() {
    return getAOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) A child.
   * @return The A child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   */
  @SideEffect.Pure public A getA() {
    return (A) getAOpt().getChild(0);
  }
  /**
   * Retrieves the optional node for the A child. This is the <code>Opt</code> node containing the child A, not the actual child!
   * @return The optional node for child the A child.
   * @apilevel low-level
   */
  @ASTNodeAnnotation.OptChild(name="A")
  @SideEffect.Pure public Opt<A> getAOpt() {
    return (Opt<A>) getChild(0);
  }
  /**
   * Retrieves the optional node for child A. This is the <code>Opt</code> node containing the child A, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child A.
   * @apilevel low-level
   */
  @SideEffect.Pure public Opt<A> getAOptNoTransform() {
    return (Opt<A>) getChildNoTransform(0);
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
  /** @apilevel internal */
  @SideEffect.Local(group="_ASTNode") protected void collect_contributors_Root_names(Root _root, @SideEffect.Ignore java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\subtype_02p\\Test.jrag:12
    if (hasA()) {
      {
        Root target = (Root) (root());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_Root_names(_root, _map);
  }
  /** @apilevel internal */
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_Root_names(LinkedList<String> collection) {
    super.contributeTo_Root_names(collection);
    if (hasA()) {
      collection.add("B");
    }
  }
}
