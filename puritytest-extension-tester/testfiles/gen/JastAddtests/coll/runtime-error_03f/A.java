/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\coll\runtime-error_03f;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\runtime-error_03f\\Test.ast:2
 * @astdecl A : ASTNode ::= B*;
 * @production A : {@link ASTNode} ::= <span class="component">{@link B}*</span>;

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public A() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
    setChild(new List(), 0);
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"B"},
    type = {"List<B>"},
    kind = {"List"}
  )
  public A(List<B> p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:23
   */
  @SideEffect.Pure protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:29
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
    A_set_visited = false;
    A_set_computed = false;
    
    A_set_value = null;
    A_set2_visited = false;
    A_set2_computed = false;
    
    A_set2_value = null;
    contributorMap_A_set2 = null;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:50
   */
  @SideEffect.Ignore @SideEffect.Fresh public A clone() throws CloneNotSupportedException {
    A node = (A) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:55
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public A copy() {
    try {
      A node = (A) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:74
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public A fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:84
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopyNoTransform() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:104
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopy() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:118
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the B list.
   * @param list The new list node to be used as the B list.
   * @apilevel high-level
   */
  public void setBList(List<B> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the B list.
   * @return Number of children in the B list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumB() {
    return getBList().getNumChild();
  }
  /**
   * Retrieves the number of children in the B list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the B list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumBNoTransform() {
    return getBListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the B list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the B list.
   * @apilevel high-level
   */
  @SideEffect.Pure public B getB(int i) {
    return (B) getBList().getChild(i);
  }
  /**
   * Check whether the B list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasB() {
    return getBList().getNumChild() != 0;
  }
  /**
   * Append an element to the B list.
   * @param node The element to append to the B list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addB(B node) {
    List<B> list = (parent == null) ? getBListNoTransform() : getBList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addBNoTransform(B node) {
    List<B> list = getBListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the B list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setB(B node, int i) {
    List<B> list = getBList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the B list.
   * @return The node representing the B list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="B")
  @SideEffect.Pure(group="_ASTNode") @SideEffect.FreshIf public List<B> getBList() {
    List<B> list = (List<B>) getChild(0);
    return list;
  }
  /**
   * Retrieves the B list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the B list.
   * @apilevel low-level
   */
  @SideEffect.Pure @SideEffect.FreshIf public List<B> getBListNoTransform() {
    return (List<B>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the B list without
   * triggering rewrites.
   */
  @SideEffect.Pure public B getBNoTransform(int i) {
    return (B) getBListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the B list.
   * @return The node representing the B list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<B> getBs() {
    return getBList();
  }
  /**
   * Retrieves the B list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the B list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<B> getBsNoTransform() {
    return getBListNoTransform();
  }
  /**
   * @aspect <NoAspect>
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\runtime-error_03f\\Test.jrag:12
   */
  /** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map<ASTNode, java.util.Set<ASTNode>> contributorMap_A_set2 = null;

  /** @apilevel internal */
  @SideEffect.Ignore protected void survey_A_set2() {
    if (contributorMap_A_set2 == null) {
      contributorMap_A_set2 = new java.util.IdentityHashMap<ASTNode, java.util.Set<ASTNode>>();
      collect_contributors_A_set2(this, contributorMap_A_set2);
    }
  }

  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean A_set_visited = false;
  /**
   * @attribute coll
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\runtime-error_03f\\Test.jrag:8
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.COLL)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\runtime-error_03f\\Test.jrag:8")
  @SideEffect.Pure(group="_ASTNode") public HashSet<B> set() {
    ASTState state = state();
    if (A_set_computed) {
      return A_set_value;
    }
    if (A_set_visited) {
      throw new RuntimeException("Circular definition of attribute A.set().");
    }
    A_set_visited = true;
    state().enterLazyAttribute();
    A_set_value = set_compute();
    A_set_computed = true;
    state().leaveLazyAttribute();
    A_set_visited = false;
    return A_set_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure(group="_ASTNode") private HashSet<B> set_compute() {
    ASTNode node = this;
    while (node != null && !(node instanceof Root)) {
      node = node.getParent();
    }
    Root root = (Root) node;
    root.survey_A_set();
    HashSet<B> _computedValue = new HashSet<B>();
    if (root.contributorMap_A_set.containsKey(this)) {
          for (ASTNode contributor : root.contributorMap_A_set.get(this)) {
            contributor.contributeTo_A_set(_computedValue);
          }
    }
    return _computedValue;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean A_set_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected HashSet<B> A_set_value;

/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean A_set2_visited = false;
  /**
   * @attribute coll
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\runtime-error_03f\\Test.jrag:12
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.COLL)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\coll\\runtime-error_03f\\Test.jrag:12")
  @SideEffect.Pure(group="_ASTNode") public HashSet<B> set2() {
    ASTState state = state();
    if (A_set2_computed) {
      return A_set2_value;
    }
    if (A_set2_visited) {
      throw new RuntimeException("Circular definition of attribute A.set2().");
    }
    A_set2_visited = true;
    state().enterLazyAttribute();
    A_set2_value = set2_compute();
    A_set2_computed = true;
    state().leaveLazyAttribute();
    A_set2_visited = false;
    return A_set2_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure(group="_ASTNode") private HashSet<B> set2_compute() {
    ASTNode node = this;
    while (node != null && !(node instanceof A)) {
      node = node.getParent();
    }
    A root = (A) node;
    root.survey_A_set2();
    HashSet<B> _computedValue = new HashSet<B>();
    if (root.contributorMap_A_set2.containsKey(this)) {
          for (ASTNode contributor : root.contributorMap_A_set2.get(this)) {
            contributor.contributeTo_A_set2(_computedValue);
          }
    }
    return _computedValue;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean A_set2_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected HashSet<B> A_set2_value;

}
