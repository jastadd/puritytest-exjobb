package testfiles/gen/JastAddtests\syn\interface03;

/**
 * @ast interface
 * @aspect Test1
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\syn\\interface03\\Test.jrag:2
 */
 interface I {
  /**
   * @attribute syn
   * @aspect Test1
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\syn\\interface03\\Test.jrag:5
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test1", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\syn\\interface03\\Test.jrag:5")
  @SideEffect.Pure(group="_ASTNode") public boolean attr();
}
