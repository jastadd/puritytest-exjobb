/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\nta\mutator_02f;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\mutator_02f\\Test.ast:1
 * @astdecl A : ASTNode ::= B*;
 * @production A : {@link ASTNode} ::= <span class="component">{@link B}*</span>;

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\mutator_02f\\Test.jrag:7
   */
  public void error() {
		// ERROR: no set method generated for the NTA B
		setBList(new List<B>());
	}
  /**
   * @declaredat ASTNode:1
   */
  public A() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
    setChild(new List(), 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:15
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:21
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:25
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    getBList_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:30
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:34
   */
  @SideEffect.Ignore @SideEffect.Fresh public A clone() throws CloneNotSupportedException {
    A node = (A) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:39
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public A copy() {
    try {
      A node = (A) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:58
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public A fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:68
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopyNoTransform() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        switch (i) {
        case 0:
          tree.children[i] = new List();
          continue;
        }
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:93
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopy() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        switch (i) {
        case 0:
          tree.children[i] = new List();
          continue;
        }
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:112
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Retrieves the number of children in the B list.
   * @return Number of children in the B list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumB() {
    return getBList().getNumChild();
  }
  /**
   * Retrieves the number of children in the B list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the B list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumBNoTransform() {
    return getBListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the B list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the B list.
   * @apilevel high-level
   */
  @SideEffect.Pure public B getB(int i) {
    return (B) getBList().getChild(i);
  }
  /**
   * Check whether the B list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasB() {
    return getBList().getNumChild() != 0;
  }
  /**
   * Append an element to the B list.
   * @param node The element to append to the B list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addB(B node) {
    List<B> list = (parent == null) ? getBListNoTransform() : getBList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addBNoTransform(B node) {
    List<B> list = getBListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the B list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setB(B node, int i) {
    List<B> list = getBList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the child position of the B list.
   * @return The the child position of the B list.
   * @apilevel low-level
   */
  @SideEffect.Pure protected int getBListChildPosition() {
    return 0;
  }
  /**
   * Retrieves the B list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the B list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<B> getBListNoTransform() {
    return (List<B>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the B list without
   * triggering rewrites.
   */
  @SideEffect.Pure public B getBNoTransform(int i) {
    return (B) getBListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the B list.
   * @return The node representing the B list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<B> getBs() {
    return getBList();
  }
  /**
   * Retrieves the B list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the B list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<B> getBsNoTransform() {
    return getBListNoTransform();
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean getBList_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void getBList_reset() {
    getBList_computed = false;
    
    getBList_value = null;
    getBList_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean getBList_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected List<B> getBList_value;

  /**
   * @attribute syn nta
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\mutator_02f\\Test.jrag:5
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\mutator_02f\\Test.jrag:5")
  @SideEffect.Pure(group="_ASTNode") public List<B> getBList() {
    ASTState state = state();
    if (getBList_computed) {
      return (List<B>) getChild(getBListChildPosition());
    }
    if (getBList_visited) {
      throw new RuntimeException("Circular definition of attribute A.getBList().");
    }
    getBList_visited = true;
    state().enterLazyAttribute();
    getBList_value = new List<B>();
    setChild(getBList_value, getBListChildPosition());
    getBList_computed = true;
    state().leaveLazyAttribute();
    getBList_visited = false;
    List<B> node = (List<B>) this.getChild(getBListChildPosition());
    return node;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
