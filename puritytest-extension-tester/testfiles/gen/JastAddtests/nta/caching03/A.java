/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\nta\caching03;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\caching03\\Test.ast:1
 * @astdecl A : ASTNode ::= [B];
 * @production A : {@link ASTNode} ::= <span class="component">[{@link B}]</span>;

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public A() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
    setChild(new Opt(), 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:15
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:21
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:25
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    getBOpt_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:30
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:34
   */
  @SideEffect.Ignore @SideEffect.Fresh public A clone() throws CloneNotSupportedException {
    A node = (A) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:39
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public A copy() {
    try {
      A node = (A) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:58
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public A fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:68
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopyNoTransform() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        switch (i) {
        case 0:
          tree.children[i] = new Opt();
          continue;
        }
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:93
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopy() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        switch (i) {
        case 0:
          tree.children[i] = new Opt();
          continue;
        }
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:112
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the (optional) B child.
   * @param node The new node to be used as the B child.
   * @apilevel high-level
   */
  public void setB(B node) {
    getBOpt().setChild(node, 0);
  }
  /**
   * Check whether the optional B child exists.
   * @return {@code true} if the optional B child exists, {@code false} if it does not.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasB() {
    return getBOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) B child.
   * @return The B child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   */
  @SideEffect.Pure public B getB() {
    return (B) getBOpt().getChild(0);
  }
  /**
   * Retrieves the optional node for child B. This is the <code>Opt</code> node containing the child B, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child B.
   * @apilevel low-level
   */
  @SideEffect.Pure public Opt<B> getBOptNoTransform() {
    return (Opt<B>) getChildNoTransform(0);
  }
  /**
   * Retrieves the child position of the optional child B.
   * @return The the child position of the optional child B.
   * @apilevel low-level
   */
  @SideEffect.Pure protected int getBOptChildPosition() {
    return 0;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean getBOpt_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void getBOpt_reset() {
    getBOpt_computed = false;
    
    getBOpt_value = null;
    getBOpt_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean getBOpt_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Opt<B> getBOpt_value;

  /**
   * @attribute syn nta
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\caching03\\Test.jrag:2
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\caching03\\Test.jrag:2")
  @SideEffect.Pure(group="_ASTNode") public Opt<B> getBOpt() {
    ASTState state = state();
    if (getBOpt_computed) {
      return (Opt<B>) getChild(getBOptChildPosition());
    }
    if (getBOpt_visited) {
      throw new RuntimeException("Circular definition of attribute A.getBOpt().");
    }
    getBOpt_visited = true;
    state().enterLazyAttribute();
    getBOpt_value = new Opt(new B());
    setChild(getBOpt_value, getBOptChildPosition());
    getBOpt_computed = true;
    state().leaveLazyAttribute();
    getBOpt_visited = false;
    Opt<B> node = (Opt<B>) this.getChild(getBOptChildPosition());
    return node;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
