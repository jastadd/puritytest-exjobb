/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\nta\caching02;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\caching02\\Test.ast:1
 * @astdecl A : ASTNode ::= B BFinal:B;
 * @production A : {@link ASTNode} ::= <span class="component">{@link B}</span> <span class="component">BFinal:{@link B}</span>;

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public A() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:14
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:20
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:24
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    getB_reset();
    getBFinal_reset();
    b_reset();
    selectB_int_reset();
    object_reset();
    objectFinal_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:34
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  @SideEffect.Ignore @SideEffect.Fresh public A clone() throws CloneNotSupportedException {
    A node = (A) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public A copy() {
    try {
      A node = (A) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:62
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public A fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:72
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopyNoTransform() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        switch (i) {
        case 0:
        case 1:
          tree.children[i] = null;
          continue;
        }
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:98
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopy() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        switch (i) {
        case 0:
        case 1:
          tree.children[i] = null;
          continue;
        }
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:118
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Retrieves the B child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the B child.
   * @apilevel low-level
   */
  @SideEffect.Pure public B getBNoTransform() {
    return (B) getChildNoTransform(0);
  }
  /**
   * Retrieves the child position of the optional child B.
   * @return The the child position of the optional child B.
   * @apilevel low-level
   */
  @SideEffect.Pure protected int getBChildPosition() {
    return 0;
  }
  /**
   * Retrieves the BFinal child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the BFinal child.
   * @apilevel low-level
   */
  @SideEffect.Pure public B getBFinalNoTransform() {
    return (B) getChildNoTransform(1);
  }
  /**
   * Retrieves the child position of the optional child BFinal.
   * @return The the child position of the optional child BFinal.
   * @apilevel low-level
   */
  @SideEffect.Pure protected int getBFinalChildPosition() {
    return 1;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean getB_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void getB_reset() {
    getB_computed = false;
    
    getB_value = null;
    getB_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean getB_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected B getB_value;

  /**
   * @attribute syn nta
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\caching02\\Test.jrag:2
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\caching02\\Test.jrag:2")
  @SideEffect.Pure(group="_ASTNode") public B getB() {
    ASTState state = state();
    if (getB_computed) {
      return (B) getChild(getBChildPosition());
    }
    if (getB_visited) {
      throw new RuntimeException("Circular definition of attribute A.getB().");
    }
    getB_visited = true;
    state().enterLazyAttribute();
    getB_value = new B();
    setChild(getB_value, getBChildPosition());
    getB_computed = true;
    state().leaveLazyAttribute();
    getB_visited = false;
    B node = (B) this.getChild(getBChildPosition());
    return node;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean getBFinal_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void getBFinal_reset() {
    getBFinal_computed = false;
    
    getBFinal_value = null;
    getBFinal_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean getBFinal_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected B getBFinal_value;

  /**
   * @attribute syn nta
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\caching02\\Test.jrag:3
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\caching02\\Test.jrag:3")
  @SideEffect.Pure(group="_ASTNode") public B getBFinal() {
    ASTState state = state();
    if (getBFinal_computed) {
      return (B) getChild(getBFinalChildPosition());
    }
    if (getBFinal_visited) {
      throw new RuntimeException("Circular definition of attribute A.getBFinal().");
    }
    getBFinal_visited = true;
    state().enterLazyAttribute();
    getBFinal_value = new B();
    setChild(getBFinal_value, getBFinalChildPosition());
    getBFinal_computed = true;
    state().leaveLazyAttribute();
    getBFinal_visited = false;
    B node = (B) this.getChild(getBFinalChildPosition());
    return node;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean b_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void b_reset() {
    b_computed = false;
    
    b_value = null;
    b_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean b_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected B b_value;

  /**
   * @attribute syn
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\caching02\\Test.jrag:5
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\caching02\\Test.jrag:5")
  @SideEffect.Pure(group="_ASTNode") public B b() {
    ASTState state = state();
    if (b_computed) {
      return b_value;
    }
    if (b_visited) {
      throw new RuntimeException("Circular definition of attribute A.b().");
    }
    b_visited = true;
    state().enterLazyAttribute();
    b_value = new B();
    b_value.setParent(this);
    b_computed = true;
    state().leaveLazyAttribute();
    b_visited = false;
    return b_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Set selectB_int_visited;
  /** @apilevel internal */
  @SideEffect.Ignore private void selectB_int_reset() {
    selectB_int_values = null;
    selectB_int_proxy = null;
    selectB_int_visited = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTNode selectB_int_proxy;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map selectB_int_values;

  /**
   * @attribute syn
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\caching02\\Test.jrag:6
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\caching02\\Test.jrag:6")
  @SideEffect.Pure(group="_ASTNode") public B selectB(int i) {
    Object _parameters = i;
    if (selectB_int_visited == null) selectB_int_visited = new java.util.HashSet(4);
    if (selectB_int_values == null) selectB_int_values = new java.util.HashMap(4);
    ASTState state = state();
    if (selectB_int_values.containsKey(_parameters)) {
      return (B) selectB_int_values.get(_parameters);
    }
    if (selectB_int_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute A.selectB(int).");
    }
    selectB_int_visited.add(_parameters);
    state().enterLazyAttribute();
    B selectB_int_value = new B();
    if (selectB_int_proxy == null) {
      selectB_int_proxy = new ASTNode();
      selectB_int_proxy.setParent(this);
    }
    if (selectB_int_value != null) {
      selectB_int_value.setParent(selectB_int_proxy);
      if (selectB_int_value.mayHaveRewrite()) {
        selectB_int_value = (B) selectB_int_value.rewrittenNode();
        selectB_int_value.setParent(selectB_int_proxy);
      }
    }
    selectB_int_values.put(_parameters, selectB_int_value);
    state().leaveLazyAttribute();
    selectB_int_visited.remove(_parameters);
    return selectB_int_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean object_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void object_reset() {
    object_computed = false;
    
    object_value = null;
    object_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean object_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Object object_value;

  /**
   * @attribute syn
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\caching02\\Test.jrag:8
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\caching02\\Test.jrag:8")
  @SideEffect.Pure(group="_ASTNode") public Object object() {
    ASTState state = state();
    if (object_computed) {
      return object_value;
    }
    if (object_visited) {
      throw new RuntimeException("Circular definition of attribute A.object().");
    }
    object_visited = true;
    state().enterLazyAttribute();
    object_value = new Object();
    object_computed = true;
    state().leaveLazyAttribute();
    object_visited = false;
    return object_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean objectFinal_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void objectFinal_reset() {
    objectFinal_computed = false;
    
    objectFinal_value = null;
    objectFinal_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean objectFinal_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Object objectFinal_value;

  /**
   * @attribute syn
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\caching02\\Test.jrag:9
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\caching02\\Test.jrag:9")
  @SideEffect.Pure(group="_ASTNode") public Object objectFinal() {
    ASTState state = state();
    if (objectFinal_computed) {
      return objectFinal_value;
    }
    if (objectFinal_visited) {
      throw new RuntimeException("Circular definition of attribute A.objectFinal().");
    }
    objectFinal_visited = true;
    state().enterLazyAttribute();
    objectFinal_value = new Object();
    objectFinal_computed = true;
    state().leaveLazyAttribute();
    objectFinal_visited = false;
    return objectFinal_value;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\caching02\\Test.jrag:11
   * @apilevel internal
   */
 @SideEffect.Pure(group="_ASTNode") public int Define_value(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == selectB_int_proxy) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\caching02\\Test.jrag:13
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return -2;
    }
    else {
      int childIndex = this.getIndexOfChild(_callerNode);
      return -1;
    }
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\caching02\\Test.jrag:11
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute value
   */
  @SideEffect.Pure protected boolean canDefine_value(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
