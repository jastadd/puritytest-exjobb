/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\nta\parameterized02;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\parameterized02\\Test.ast:1
 * @astdecl A : ASTNode;
 * @production A : {@link ASTNode};

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public A() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:19
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:23
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    b_reset();
    b_int_reset();
    b_int_String_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:30
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:34
   */
  @SideEffect.Ignore @SideEffect.Fresh public A clone() throws CloneNotSupportedException {
    A node = (A) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:39
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public A copy() {
    try {
      A node = (A) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:58
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public A fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:68
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopyNoTransform() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:88
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopy() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:102
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean b_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void b_reset() {
    b_computed = false;
    
    b_value = null;
    b_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean b_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected B b_value;

  /**
   * @attribute syn
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\parameterized02\\Test.jrag:2
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\parameterized02\\Test.jrag:2")
  @SideEffect.Pure(group="_ASTNode") public B b() {
    ASTState state = state();
    if (b_computed) {
      return b_value;
    }
    if (b_visited) {
      throw new RuntimeException("Circular definition of attribute A.b().");
    }
    b_visited = true;
    state().enterLazyAttribute();
    b_value = new B(0, "");
    b_value.setParent(this);
    b_computed = true;
    state().leaveLazyAttribute();
    b_visited = false;
    return b_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Set b_int_visited;
  /** @apilevel internal */
  @SideEffect.Ignore private void b_int_reset() {
    b_int_values = null;
    b_int_proxy = null;
    b_int_visited = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTNode b_int_proxy;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map b_int_values;

  /**
   * @attribute syn
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\parameterized02\\Test.jrag:3
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\parameterized02\\Test.jrag:3")
  @SideEffect.Pure(group="_ASTNode") public B b(int n) {
    Object _parameters = n;
    if (b_int_visited == null) b_int_visited = new java.util.HashSet(4);
    if (b_int_values == null) b_int_values = new java.util.HashMap(4);
    ASTState state = state();
    if (b_int_values.containsKey(_parameters)) {
      return (B) b_int_values.get(_parameters);
    }
    if (b_int_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute A.b(int).");
    }
    b_int_visited.add(_parameters);
    state().enterLazyAttribute();
    B b_int_value = new B(n, "");
    if (b_int_proxy == null) {
      b_int_proxy = new ASTNode();
      b_int_proxy.setParent(this);
    }
    if (b_int_value != null) {
      b_int_value.setParent(b_int_proxy);
      if (b_int_value.mayHaveRewrite()) {
        b_int_value = (B) b_int_value.rewrittenNode();
        b_int_value.setParent(b_int_proxy);
      }
    }
    b_int_values.put(_parameters, b_int_value);
    state().leaveLazyAttribute();
    b_int_visited.remove(_parameters);
    return b_int_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Set b_int_String_visited;
  /** @apilevel internal */
  @SideEffect.Ignore private void b_int_String_reset() {
    b_int_String_values = null;
    b_int_String_proxy = null;
    b_int_String_visited = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTNode b_int_String_proxy;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map b_int_String_values;

  /**
   * @attribute syn
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\parameterized02\\Test.jrag:4
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\nta\\parameterized02\\Test.jrag:4")
  @SideEffect.Pure(group="_ASTNode") public B b(int n, String s) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(n);
    _parameters.add(s);
    if (b_int_String_visited == null) b_int_String_visited = new java.util.HashSet(4);
    if (b_int_String_values == null) b_int_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (b_int_String_values.containsKey(_parameters)) {
      return (B) b_int_String_values.get(_parameters);
    }
    if (b_int_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute A.b(int,String).");
    }
    b_int_String_visited.add(_parameters);
    state().enterLazyAttribute();
    B b_int_String_value = new B(n, s);
    if (b_int_String_proxy == null) {
      b_int_String_proxy = new ASTNode();
      b_int_String_proxy.setParent(this);
    }
    if (b_int_String_value != null) {
      b_int_String_value.setParent(b_int_String_proxy);
      if (b_int_String_value.mayHaveRewrite()) {
        b_int_String_value = (B) b_int_String_value.rewrittenNode();
        b_int_String_value.setParent(b_int_String_proxy);
      }
    }
    b_int_String_values.put(_parameters, b_int_String_value);
    state().leaveLazyAttribute();
    b_int_String_visited.remove(_parameters);
    return b_int_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
