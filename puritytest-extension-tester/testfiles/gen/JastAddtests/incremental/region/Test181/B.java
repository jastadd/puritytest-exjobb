/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\incremental\region\Test181;
import java.util.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\region\\Test181\\Test.ast:2
 * @astdecl B : ASTNode;
 * @production B : {@link ASTNode};

 */
public class B extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public B() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:19
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:23
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    a_reset();
    b_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:29
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  @SideEffect.Ignore @SideEffect.Fresh public B clone() throws CloneNotSupportedException {
    B node = (B) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public B copy() {
    try {
      B node = (B) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:57
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public B fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:67
   */
  @SideEffect.Fresh(group="_ASTNode") public B treeCopyNoTransform() {
    B tree = (B) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:87
   */
  @SideEffect.Fresh(group="_ASTNode") public B treeCopy() {
    B tree = (B) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:101
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean a_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void a_reset() {
    a_computed = false;
    a_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean a_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected int a_value;

  /**
   * @attribute syn
   * @aspect Test181
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\region\\Test181\\Test181.jrag:7
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test181", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\region\\Test181\\Test181.jrag:7")
  @SideEffect.Pure(group="_ASTNode") public int a() {
    ASTState state = state();
    if (a_computed) {
      return a_value;
    }
    if (a_visited) {
      throw new RuntimeException("Circular definition of attribute B.a().");
    }
    a_visited = true;
    state().enterLazyAttribute();
    a_value = b();
    a_computed = true;
    state().leaveLazyAttribute();
    a_visited = false;
    return a_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean b_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void b_reset() {
    b_computed = false;
    b_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean b_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected int b_value;

  /**
   * @attribute syn
   * @aspect Test181
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\region\\Test181\\Test181.jrag:8
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Test181", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\region\\Test181\\Test181.jrag:8")
  @SideEffect.Pure(group="_ASTNode") public int b() {
    ASTState state = state();
    if (b_computed) {
      return b_value;
    }
    if (b_visited) {
      throw new RuntimeException("Circular definition of attribute B.b().");
    }
    b_visited = true;
    state().enterLazyAttribute();
    b_value = 5;
    b_computed = true;
    state().leaveLazyAttribute();
    b_visited = false;
    return b_value;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
