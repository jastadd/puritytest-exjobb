package testfiles/gen/JastAddtests\incremental\node\Test184;

import java.util.*;
/**
 * @ast class
 * @declaredat ASTNode:277
 */
public class SideEffect extends java.lang.Object {
  
 @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
  @java.lang.annotation.Target({java.lang.annotation.ElementType.METHOD,java.lang.annotation.ElementType.CONSTRUCTOR,
  java.lang.annotation.ElementType.PARAMETER,java.lang.annotation.ElementType.LOCAL_VARIABLE,java.lang.annotation.ElementType.FIELD})
  @java.lang.annotation.Documented
  public @interface FreshEx {
	String group() default "";
  }

  
  
  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
  @java.lang.annotation.Target({java.lang.annotation.ElementType.METHOD,java.lang.annotation.ElementType.CONSTRUCTOR,
  java.lang.annotation.ElementType.PARAMETER})
  @java.lang.annotation.Documented
  public @interface FreshIf {
  }

  
  
  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
  @java.lang.annotation.Target({java.lang.annotation.ElementType.METHOD,java.lang.annotation.ElementType.CONSTRUCTOR,
  java.lang.annotation.ElementType.PARAMETER})
  @java.lang.annotation.Documented
  public @interface Inner {
  }

  
    
  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
  @java.lang.annotation.Target({java.lang.annotation.ElementType.METHOD,java.lang.annotation.ElementType.CONSTRUCTOR})
  @java.lang.annotation.Documented
  public @interface Impure {
  }

  
  
  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
  @java.lang.annotation.Target({java.lang.annotation.ElementType.METHOD,java.lang.annotation.ElementType.CONSTRUCTOR,
  java.lang.annotation.ElementType.PARAMETER,java.lang.annotation.ElementType.LOCAL_VARIABLE,java.lang.annotation.ElementType.FIELD})
  @java.lang.annotation.Documented
  public @interface Fresh {
	String group() default "";
	boolean self() default false; 
  }

  

  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
  @java.lang.annotation.Target({java.lang.annotation.ElementType.METHOD,java.lang.annotation.ElementType.CONSTRUCTOR,
  java.lang.annotation.ElementType.PARAMETER,java.lang.annotation.ElementType.LOCAL_VARIABLE,java.lang.annotation.ElementType.FIELD})
  @java.lang.annotation.Documented
  public @interface NonFresh {
	String group() default "";
  }

  

  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
  @java.lang.annotation.Target({java.lang.annotation.ElementType.FIELD,java.lang.annotation.ElementType.METHOD})
  @java.lang.annotation.Documented
  public @interface Secret {
	String group() default "";
  }

  
  
  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
  @java.lang.annotation.Target({java.lang.annotation.ElementType.TYPE})
  @java.lang.annotation.Documented
  public @interface Entity {
  }

  
  
  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
  @java.lang.annotation.Target({java.lang.annotation.ElementType.METHOD,java.lang.annotation.ElementType.CONSTRUCTOR,
  java.lang.annotation.ElementType.PARAMETER,java.lang.annotation.ElementType.LOCAL_VARIABLE,java.lang.annotation.ElementType.FIELD})
  @java.lang.annotation.Documented
  public @interface Ignore {
  }

  

  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
  @java.lang.annotation.Target({java.lang.annotation.ElementType.METHOD,java.lang.annotation.ElementType.CONSTRUCTOR,
  java.lang.annotation.ElementType.FIELD,java.lang.annotation.ElementType.PARAMETER})
  @java.lang.annotation.Documented
  public @interface Local {
	String group() default "";
  }

  

  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
  @java.lang.annotation.Target(java.lang.annotation.ElementType.METHOD)
  @java.lang.annotation.Documented
  public @interface Pure {
	String group() default "";
  }


}
