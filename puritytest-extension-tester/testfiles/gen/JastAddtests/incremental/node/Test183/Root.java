/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\incremental\node\Test183;
import java.util.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test183\\Test.ast:2
 * @astdecl Root : ASTNode ::= A D*;
 * @production Root : {@link ASTNode} ::= <span class="component">{@link A}</span> <span class="component">{@link D}*</span>;

 */
public class Root extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public Root() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
    setChild(new List(), 1);
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"A"},
    type = {"A"},
    kind = {"Child"}
  )
  public Root(A p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:23
   */
  @SideEffect.Pure protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:29
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    getDList_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:42
   */
  @SideEffect.Ignore @SideEffect.Fresh public Root clone() throws CloneNotSupportedException {
    Root node = (Root) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:47
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public Root copy() {
    try {
      Root node = (Root) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:66
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Root fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:76
   */
  @SideEffect.Fresh(group="_ASTNode") public Root treeCopyNoTransform() {
    Root tree = (Root) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        switch (i) {
        case 1:
          tree.children[i] = new List();
          continue;
        }
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:101
   */
  @SideEffect.Fresh(group="_ASTNode") public Root treeCopy() {
    Root tree = (Root) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        switch (i) {
        case 1:
          tree.children[i] = new List();
          continue;
        }
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:120
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the A child.
   * @param node The new node to replace the A child.
   * @apilevel high-level
   */
  public void setA(A node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the A child.
   * @return The current node used as the A child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="A")
  @SideEffect.Pure public A getA() {
    return (A) getChild(0);
  }
  /**
   * Retrieves the A child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the A child.
   * @apilevel low-level
   */
  @SideEffect.Pure public A getANoTransform() {
    return (A) getChildNoTransform(0);
  }
  /**
   * Retrieves the number of children in the D list.
   * @return Number of children in the D list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumD() {
    return getDList().getNumChild();
  }
  /**
   * Retrieves the number of children in the D list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the D list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumDNoTransform() {
    return getDListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the D list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the D list.
   * @apilevel high-level
   */
  @SideEffect.Pure public D getD(int i) {
    return (D) getDList().getChild(i);
  }
  /**
   * Check whether the D list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasD() {
    return getDList().getNumChild() != 0;
  }
  /**
   * Append an element to the D list.
   * @param node The element to append to the D list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addD(D node) {
    List<D> list = (parent == null) ? getDListNoTransform() : getDList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addDNoTransform(D node) {
    List<D> list = getDListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the D list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setD(D node, int i) {
    List<D> list = getDList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the child position of the D list.
   * @return The the child position of the D list.
   * @apilevel low-level
   */
  @SideEffect.Pure protected int getDListChildPosition() {
    return 1;
  }
  /**
   * Retrieves the D list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the D list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<D> getDListNoTransform() {
    return (List<D>) getChildNoTransform(1);
  }
  /**
   * @return the element at index {@code i} in the D list without
   * triggering rewrites.
   */
  @SideEffect.Pure public D getDNoTransform(int i) {
    return (D) getDListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the D list.
   * @return The node representing the D list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<D> getDs() {
    return getDList();
  }
  /**
   * Retrieves the D list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the D list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<D> getDsNoTransform() {
    return getDListNoTransform();
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean getDList_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void getDList_reset() {
    getDList_computed = false;
    
    getDList_value = null;
    getDList_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean getDList_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected List getDList_value;

  /**
   * @attribute syn nta
   * @aspect Test183
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test183\\Test183.jrag:5
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="Test183", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test183\\Test183.jrag:5")
  @SideEffect.Pure(group="_ASTNode") public List getDList() {
    ASTState state = state();
    if (getDList_computed) {
      return (List) getChild(getDListChildPosition());
    }
    if (getDList_visited) {
      throw new RuntimeException("Circular definition of attribute Root.getDList().");
    }
    getDList_visited = true;
    state().enterLazyAttribute();
    getDList_value = new List();
    setChild(getDList_value, getDListChildPosition());
    getDList_computed = true;
    state().leaveLazyAttribute();
    getDList_visited = false;
    List node = (List) this.getChild(getDListChildPosition());
    return node;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test183\\Test183.jrag:26
   * @apilevel internal
   */
 @SideEffect.Pure(group="_ASTNode") public D Define_lookupD(ASTNode _callerNode, ASTNode _childNode) {
    if (getANoTransform() != null && _callerNode == getA()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test183\\Test183.jrag:28
      {
          if (getNumD() == 0) 
           addD(new D("a"));
          return getD(getNumD()-1);
        }
    }
    else {
      return getParent().Define_lookupD(this, _callerNode);
    }
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test183\\Test183.jrag:26
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute lookupD
   */
  @SideEffect.Pure protected boolean canDefine_lookupD(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test183\\Test183.jrag:34
   * @apilevel internal
   */
 @SideEffect.Pure(group="_ASTNode") public String Define_name(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getDListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test183\\Test183.jrag:35
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return "name";
    }
    else {
      return getParent().Define_name(this, _callerNode);
    }
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\incremental\\node\\Test183\\Test183.jrag:34
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute name
   */
  @SideEffect.Pure protected boolean canDefine_name(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
