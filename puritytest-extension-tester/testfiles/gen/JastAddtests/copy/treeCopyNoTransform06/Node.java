/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/JastAddtests\copy\treeCopyNoTransform06;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\copy\\treeCopyNoTransform06\\Test.ast:1
 * @astdecl Node : ASTNode ::= [X];
 * @production Node : {@link ASTNode} ::= <span class="component">[{@link X}]</span>;

 */
public class Node extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public Node() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
    setChild(new Opt(), 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:15
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:21
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:25
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    getXOpt_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:30
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:34
   */
  @SideEffect.Ignore @SideEffect.Fresh public Node clone() throws CloneNotSupportedException {
    Node node = (Node) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:39
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public Node copy() {
    try {
      Node node = (Node) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:58
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Node fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:68
   */
  @SideEffect.Fresh(group="_ASTNode") public Node treeCopyNoTransform() {
    Node tree = (Node) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        switch (i) {
        case 0:
          tree.children[i] = new Opt();
          continue;
        }
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:93
   */
  @SideEffect.Fresh(group="_ASTNode") public Node treeCopy() {
    Node tree = (Node) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        switch (i) {
        case 0:
          tree.children[i] = new Opt();
          continue;
        }
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:112
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the (optional) X child.
   * @param node The new node to be used as the X child.
   * @apilevel high-level
   */
  public void setX(X node) {
    getXOpt().setChild(node, 0);
  }
  /**
   * Check whether the optional X child exists.
   * @return {@code true} if the optional X child exists, {@code false} if it does not.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasX() {
    return getXOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) X child.
   * @return The X child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   */
  @SideEffect.Pure public X getX() {
    return (X) getXOpt().getChild(0);
  }
  /**
   * Retrieves the optional node for child X. This is the <code>Opt</code> node containing the child X, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child X.
   * @apilevel low-level
   */
  @SideEffect.Pure public Opt<X> getXOptNoTransform() {
    return (Opt<X>) getChildNoTransform(0);
  }
  /**
   * Retrieves the child position of the optional child X.
   * @return The the child position of the optional child X.
   * @apilevel low-level
   */
  @SideEffect.Pure protected int getXOptChildPosition() {
    return 0;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean getXOpt_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void getXOpt_reset() {
    getXOpt_computed = false;
    
    getXOpt_value = null;
    getXOpt_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean getXOpt_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected Opt<X> getXOpt_value;

  /**
   * @attribute syn nta
   * @aspect Test
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\copy\\treeCopyNoTransform06\\Test.jrag:2
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\JastAddtests\\copy\\treeCopyNoTransform06\\Test.jrag:2")
  @SideEffect.Pure(group="_ASTNode") public Opt<X> getXOpt() {
    ASTState state = state();
    if (getXOpt_computed) {
      return (Opt<X>) getChild(getXOptChildPosition());
    }
    if (getXOpt_visited) {
      throw new RuntimeException("Circular definition of attribute Node.getXOpt().");
    }
    getXOpt_visited = true;
    state().enterLazyAttribute();
    getXOpt_value = new Opt<X>();
    setChild(getXOpt_value, getXOptChildPosition());
    getXOpt_computed = true;
    state().leaveLazyAttribute();
    getXOpt_visited = false;
    Opt<X> node = (Opt<X>) this.getChild(getXOptChildPosition());
    return node;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
