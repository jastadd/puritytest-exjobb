package testfiles/gen/fastTests\Simplic2015;

import java.util.*;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
/**
 * @ast class
 * @aspect Interpreter
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\Interpreter.jrag:30
 */
 class ReturnException extends Exception {
  

	private int value;

  
	
	public ReturnException(int i, String s)
	{
		super(s);
		value= i;
	}

  

	public int getValue(){
		return value;
	}


}
