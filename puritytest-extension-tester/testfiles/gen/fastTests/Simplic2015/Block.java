/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/fastTests\Simplic2015;
import java.util.*;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\Simplic.ast:15
 * @astdecl Block : Stmt ::= Stmt*;
 * @production Block : {@link Stmt} ::= <span class="component">{@link Stmt}*</span>;

 */
public class Block extends Stmt implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\CodeGen.jrag:128
   */
  public void genCode(PrintStream out) {
	for(int i=0;i<getNumStmt();i++)
	{
		getStmt(i).genCode(out);
	}
}
  /**
   * @aspect Interpreter
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\Interpreter.jrag:75
   */
  public void eval(ActivationRecord actrec) throws ReturnException{
	for(int i=0;i<getNumStmt();i++){
		getStmt(i).eval(actrec);
	}
}
  /**
   * @aspect PrettyPrint
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\PrettyPrint.jrag:155
   */
  public void prettyPrint(PrintStream out, String ind) {
		for(int i=0;i<getNumStmt();i++){
		getStmt(i).prettyPrint(out, ind);
		out.print("\n");
		}
	}
  /**
   * @aspect Visitor
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\Visitor.jrag:68
   */
  public Object accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
  /**
   * @declaredat ASTNode:1
   */
  public Block() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
    setChild(new List(), 0);
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Stmt"},
    type = {"List<Stmt>"},
    kind = {"List"}
  )
  public Block(List<Stmt> p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:23
   */
  @SideEffect.Pure protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:29
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:41
   */
  @SideEffect.Ignore @SideEffect.Fresh public Block clone() throws CloneNotSupportedException {
    Block node = (Block) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:46
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public Block copy() {
    try {
      Block node = (Block) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:65
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Block fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:75
   */
  @SideEffect.Fresh(group="_ASTNode") public Block treeCopyNoTransform() {
    Block tree = (Block) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:95
   */
  @SideEffect.Fresh(group="_ASTNode") public Block treeCopy() {
    Block tree = (Block) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:109
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the Stmt list.
   * @param list The new list node to be used as the Stmt list.
   * @apilevel high-level
   */
  public void setStmtList(List<Stmt> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the Stmt list.
   * @return Number of children in the Stmt list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumStmt() {
    return getStmtList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Stmt list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Stmt list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumStmtNoTransform() {
    return getStmtListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Stmt list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Stmt list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Stmt getStmt(int i) {
    return (Stmt) getStmtList().getChild(i);
  }
  /**
   * Check whether the Stmt list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasStmt() {
    return getStmtList().getNumChild() != 0;
  }
  /**
   * Append an element to the Stmt list.
   * @param node The element to append to the Stmt list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addStmt(Stmt node) {
    List<Stmt> list = (parent == null) ? getStmtListNoTransform() : getStmtList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addStmtNoTransform(Stmt node) {
    List<Stmt> list = getStmtListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Stmt list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setStmt(Stmt node, int i) {
    List<Stmt> list = getStmtList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Stmt list.
   * @return The node representing the Stmt list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Stmt")
  @SideEffect.Pure(group="_ASTNode") @SideEffect.FreshIf public List<Stmt> getStmtList() {
    List<Stmt> list = (List<Stmt>) getChild(0);
    return list;
  }
  /**
   * Retrieves the Stmt list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Stmt list.
   * @apilevel low-level
   */
  @SideEffect.Pure @SideEffect.FreshIf public List<Stmt> getStmtListNoTransform() {
    return (List<Stmt>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the Stmt list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Stmt getStmtNoTransform(int i) {
    return (Stmt) getStmtListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Stmt list.
   * @return The node representing the Stmt list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Stmt> getStmts() {
    return getStmtList();
  }
  /**
   * Retrieves the Stmt list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Stmt list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Stmt> getStmtsNoTransform() {
    return getStmtListNoTransform();
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Set collectFunctionCalls_Set_Function__visited;
  /**
   * @attribute syn
   * @aspect FunctionCallsAnalysis
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\FunctionCallsAnalysis.jrag:26
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="FunctionCallsAnalysis", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\FunctionCallsAnalysis.jrag:26")
  @SideEffect.Pure(group="_ASTNode") public Set<Function> collectFunctionCalls(Set<Function> functions) {
    Object _parameters = functions;
    if (collectFunctionCalls_Set_Function__visited == null) collectFunctionCalls_Set_Function__visited = new java.util.HashSet(4);
    if (collectFunctionCalls_Set_Function__visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute Block.collectFunctionCalls(Set_Function_).");
    }
    collectFunctionCalls_Set_Function__visited.add(_parameters);
    try {
    		for(int i=0;i<getNumStmt();i++)
    		{
    			getStmt(i).collectFunctionCalls(functions);
    		}
    		return functions;
    	}
    finally {
      collectFunctionCalls_Set_Function__visited.remove(_parameters);
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Set localLookup_String_int_visited;
  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\NameAnalysis.jrag:92
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\NameAnalysis.jrag:92")
  @SideEffect.Pure(group="_ASTNode") public AbstractDecl localLookup(String name, int until) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(name);
    _parameters.add(until);
    if (localLookup_String_int_visited == null) localLookup_String_int_visited = new java.util.HashSet(4);
    if (localLookup_String_int_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute Block.localLookup(String,int).");
    }
    localLookup_String_int_visited.add(_parameters);
    try {
    		AbstractDecl temp;
    		for (int i = 0; i <= until; i++) {
    			temp = getStmt(i).localLookup(name, until);
    			if (!temp.isUnknown()) {
    				return temp;
    			}
    		}
    		return new UnknownDecl();
    	}
    finally {
      localLookup_String_int_visited.remove(_parameters);
    }
  }
  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\NameAnalysis.jrag:8
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\NameAnalysis.jrag:8")
  @SideEffect.Pure(group="_ASTNode") public ASTNode lookup(String name) {
    Object _parameters = name;
    if (lookup_String_visited == null) lookup_String_visited = new java.util.HashSet(4);
    if (lookup_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute Block.lookup(String).");
    }
    lookup_String_visited.add(_parameters);
    ASTNode lookup_String_value = getParent().Define_lookup(this, null, name);
    lookup_String_visited.remove(_parameters);
    return lookup_String_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Set lookup_String_visited;
  /**
   * @attribute inh
   * @aspect UniqueName
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\uniqueName.jrag:9
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="UniqueName", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\uniqueName.jrag:9")
  @SideEffect.Pure(group="_ASTNode") public String nameLookup(String name) {
    Object _parameters = name;
    if (nameLookup_String_visited == null) nameLookup_String_visited = new java.util.HashSet(4);
    if (nameLookup_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute Block.nameLookup(String).");
    }
    nameLookup_String_visited.add(_parameters);
    String nameLookup_String_value = getParent().Define_nameLookup(this, null, name);
    nameLookup_String_visited.remove(_parameters);
    return nameLookup_String_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Set nameLookup_String_visited;
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\NameAnalysis.jrag:6
   * @apilevel internal
   */
 @SideEffect.Pure(group="_ASTNode") public ASTNode Define_lookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    if (_callerNode == getStmtListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\NameAnalysis.jrag:42
      int index = _callerNode.getIndexOfChild(_childNode);
      {
      		AbstractDecl decl = localLookup(name, index);
      		if (!decl.isUnknown())
      			return decl; 
      		return lookup(name);
      	}
    }
    else {
      return getParent().Define_lookup(this, _callerNode, name);
    }
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\NameAnalysis.jrag:6
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute lookup
   */
  @SideEffect.Pure protected boolean canDefine_lookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\NameAnalysis.jrag:12
   * @apilevel internal
   */
 @SideEffect.Pure(group="_ASTNode") public AbstractDecl Define_Scopelookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    if (_callerNode == getStmtListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\NameAnalysis.jrag:51
      int index = _callerNode.getIndexOfChild(_childNode);
      {
      		return localLookup(name, index); 
      	}
    }
    else {
      return getParent().Define_Scopelookup(this, _callerNode, name);
    }
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\NameAnalysis.jrag:12
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute Scopelookup
   */
  @SideEffect.Pure protected boolean canDefine_Scopelookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\uniqueName.jrag:9
   * @apilevel internal
   */
 @SideEffect.Pure(group="_ASTNode") public String Define_nameLookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    if (_callerNode == getStmtListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\uniqueName.jrag:16
      int index = _callerNode.getIndexOfChild(_childNode);
      {
      		return nameLookup(index+"_"+name);
      	}
    }
    else {
      return getParent().Define_nameLookup(this, _callerNode, name);
    }
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\uniqueName.jrag:9
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute nameLookup
   */
  @SideEffect.Pure protected boolean canDefine_nameLookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
}
