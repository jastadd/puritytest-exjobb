package testfiles/gen/fastTests\Simplic2015;

import java.util.*;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
/**
 * @ast class
 * @aspect Interpreter
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\Interpreter.jrag:8
 */
 class ActivationRecord extends java.lang.Object {
  
	HashMap<String,Integer> map=new HashMap<String,Integer>();

  
	
	public ActivationRecord()
	{
	}

  

	public int get(String s)
	{
		if (map.containsKey(s))
			return map.get(s);
		return 0; 
	}

   

	public void set(String s,int i)
	{
		map.put(s,i);
	}


}
