/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/fastTests\collAttr;
import java.util.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\collAttr\\Simplic.ast:3
 * @astdecl A : ASTNode ::= <ID:String> C* B;
 * @production A : {@link ASTNode} ::= <span class="component">&lt;ID:String&gt;</span> <span class="component">{@link C}*</span> <span class="component">{@link B}</span>;

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect Visitor
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\collAttr\\Visitor.jrag:32
   */
  public Object accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
  /**
   * @declaredat ASTNode:1
   */
  public A() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
    setChild(new List(), 0);
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"ID", "C", "B"},
    type = {"String", "List<C>", "B"},
    kind = {"Token", "List", "Child"}
  )
  public A(String p0, List<C> p1, B p2) {
    setID(p0);
    setChild(p1, 0);
    setChild(p2, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:25
   */
  @SideEffect.Pure protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:31
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:35
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:39
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
    A_allchildren_visited = false;
    A_allchildren_computed = false;
    
    A_allchildren_value = null;
    contributorMap_A_allchildren = null;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:48
   */
  @SideEffect.Ignore @SideEffect.Fresh public A clone() throws CloneNotSupportedException {
    A node = (A) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:53
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public A copy() {
    try {
      A node = (A) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:72
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public A fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:82
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopyNoTransform() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:102
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopy() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:116
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_ID == ((A) node).tokenString_ID);    
  }
  /**
   * Replaces the lexeme ID.
   * @param value The new value for the lexeme ID.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setID(String value) {
    tokenString_ID = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_ID;
  /**
   * Retrieves the value for the lexeme ID.
   * @return The value for the lexeme ID.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="ID")
  @SideEffect.Pure(group="_ASTNode") public String getID() {
    return tokenString_ID != null ? tokenString_ID : "";
  }
  /**
   * Replaces the C list.
   * @param list The new list node to be used as the C list.
   * @apilevel high-level
   */
  public void setCList(List<C> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the C list.
   * @return Number of children in the C list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumC() {
    return getCList().getNumChild();
  }
  /**
   * Retrieves the number of children in the C list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the C list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumCNoTransform() {
    return getCListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the C list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the C list.
   * @apilevel high-level
   */
  @SideEffect.Pure public C getC(int i) {
    return (C) getCList().getChild(i);
  }
  /**
   * Check whether the C list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasC() {
    return getCList().getNumChild() != 0;
  }
  /**
   * Append an element to the C list.
   * @param node The element to append to the C list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addC(C node) {
    List<C> list = (parent == null) ? getCListNoTransform() : getCList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addCNoTransform(C node) {
    List<C> list = getCListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the C list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setC(C node, int i) {
    List<C> list = getCList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the C list.
   * @return The node representing the C list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="C")
  @SideEffect.Pure(group="_ASTNode") @SideEffect.FreshIf public List<C> getCList() {
    List<C> list = (List<C>) getChild(0);
    return list;
  }
  /**
   * Retrieves the C list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the C list.
   * @apilevel low-level
   */
  @SideEffect.Pure @SideEffect.FreshIf public List<C> getCListNoTransform() {
    return (List<C>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the C list without
   * triggering rewrites.
   */
  @SideEffect.Pure public C getCNoTransform(int i) {
    return (C) getCListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the C list.
   * @return The node representing the C list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<C> getCs() {
    return getCList();
  }
  /**
   * Retrieves the C list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the C list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<C> getCsNoTransform() {
    return getCListNoTransform();
  }
  /**
   * Replaces the B child.
   * @param node The new node to replace the B child.
   * @apilevel high-level
   */
  public void setB(B node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the B child.
   * @return The current node used as the B child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="B")
  @SideEffect.Pure public B getB() {
    return (B) getChild(1);
  }
  /**
   * Retrieves the B child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the B child.
   * @apilevel low-level
   */
  @SideEffect.Pure public B getBNoTransform() {
    return (B) getChildNoTransform(1);
  }
  /**
   * @aspect <NoAspect>
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\collAttr\\collAttr.jrag:6
   */
  /** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Map<ASTNode, java.util.Set<ASTNode>> contributorMap_A_allchildren = null;

  /** @apilevel internal */
  @SideEffect.Ignore protected void survey_A_allchildren() {
    if (contributorMap_A_allchildren == null) {
      contributorMap_A_allchildren = new java.util.IdentityHashMap<ASTNode, java.util.Set<ASTNode>>();
      collect_contributors_A_allchildren(this, contributorMap_A_allchildren);
    }
  }

  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean A_allchildren_visited = false;
  /**
   * @attribute coll
   * @aspect a
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\collAttr\\collAttr.jrag:6
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.COLL)
  @ASTNodeAnnotation.Source(aspect="a", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\collAttr\\collAttr.jrag:6")
  @SideEffect.Pure(group="_ASTNode") public ArrayList<String> allchildren() {
    ASTState state = state();
    if (A_allchildren_computed) {
      return A_allchildren_value;
    }
    if (A_allchildren_visited) {
      throw new RuntimeException("Circular definition of attribute A.allchildren().");
    }
    A_allchildren_visited = true;
    state().enterLazyAttribute();
    A_allchildren_value = allchildren_compute();
    A_allchildren_computed = true;
    state().leaveLazyAttribute();
    A_allchildren_visited = false;
    return A_allchildren_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure(group="_ASTNode") private ArrayList<String> allchildren_compute() {
    ASTNode node = this;
    while (node != null && !(node instanceof A)) {
      node = node.getParent();
    }
    A root = (A) node;
    root.survey_A_allchildren();
    ArrayList<String> _computedValue = new ArrayList<String>();
    if (root.contributorMap_A_allchildren.containsKey(this)) {
          for (ASTNode contributor : root.contributorMap_A_allchildren.get(this)) {
            contributor.contributeTo_A_allchildren(_computedValue);
          }
    }
    return _computedValue;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean A_allchildren_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ArrayList<String> A_allchildren_value;

}
