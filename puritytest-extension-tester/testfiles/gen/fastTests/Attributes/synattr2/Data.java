package testfiles/gen/fastTests\Attributes\synattr2;

/**
 * @ast class
 * @aspect inhattr1
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Attributes\\synattr2\\synattr2.jrag:13
 */
 class Data extends java.lang.Object {
  
		public int number;

  
		public String id;

  
		public ASTNode creator;

  
		public Data(int i,String id,ASTNode creator){
			number = i;
			this.id = id;
			this.creator=creator;
		}


}
