package testfiles/gen/fastTests\Attributes\inhattr1;

/**
 * @ast class
 * @aspect inhattr1
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Attributes\\inhattr1\\inhattr1.jrag:10
 */
 class Data extends java.lang.Object {
  
		public int number;

  
		public String id;

  
		public ASTNode creator;

  
		public Data(int i,String id,ASTNode creator){
			number = i;
			this.id = id;
			this.creator=creator;
		}


}
