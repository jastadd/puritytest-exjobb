package src;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Scanner;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.io.FilenameFilter;
import java.util.Collection;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.FileNotFoundException;


public class AnnotationTester {
	
	/**
	 * Helper data class for moving around options settings
	 */
	public class OptionData{
		boolean noGen;
		boolean oneTest;
		boolean fastTest;
		boolean exit;
		boolean stopFail;
		String testName;
	}
	
	/**
	 * Miliseconds to timeout per test check.
	 */
	static int timeout = 5000; 
	static final String source="testfiles"+File.separator+"source";
	
	/**
	 * Main method for the tester. The options are provided via command line.
	 * Valid flags :-noGen , -oneTest , -timeout, -fast and -help.
	 * -noGen : Disable regeneration with JastAdd.
	 * -oneTest : Only preform one test specified by folder name
	 * -timeout : milliseconds on generation. 
	 * -fast : do only the tests in the fast check folder.
	 * -stopFail : stop on failure.
	 * 
	 *  @param args - command line options
	 */
	public static void main(String[] args){
		AnnotationTester tester = new AnnotationTester();
		tester.process(args);
	}
	
	/**
	 * The tester process .
	 * 1. First is the options processesed.
	 * 2. Collecting folders with test cases from source/... alternatively more specified
	 * 3. Generate with JastAdd.jar in tools/ if -noGen is not specified
	 * 4. match result in gen/ with orginals in goal/
	 *
	 * @param args 
	 * 			The command line options to use.
	 */
	public void process(String[] args){
		OptionData oData=setupOptions(args);
		if (oData.exit)
			System.exit(-1);
		Collection<File> tests = new LinkedList<File>();
		File start = new File(source+(oData.fastTest ? File.separator+"fastTests" : ""));
		LinkedList<File> uncheckedDirs=new LinkedList<File>();
		uncheckedDirs.add(start);
		while (!uncheckedDirs.isEmpty()){
			File searched = uncheckedDirs.pop();
			File[] allFiles = searched.listFiles();
			File[] Files =  searched.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.toLowerCase().endsWith(".ast") ||  name.toLowerCase().endsWith(".jreg") ||  name.toLowerCase().endsWith(".jrag") ||  name.toLowerCase().endsWith(".java");
				}
			});
			if (Files.length!=0)
				tests.add(searched);
			for (File f : allFiles){
				if (f.isDirectory())
					uncheckedDirs.add(f);
			}
		}
		int passedTest=0;
		if (oData.oneTest){
			System.out.println("Preform single test of "+oData.testName);
		}else{
		System.out.println("There are "+tests.size()+" tests.");
		}
		int testOK;
		for (File test:tests){
			if (oData.oneTest && !test.getName().equals(oData.testName))
					continue;
			try{
				System.out.println("Start "+test.getName());
				if (!oData.noGen){
					testOK=compileJastAdd(test);
					if(oData.stopFail && testOK==0)
						System.exit(-1);
				}
				testOK=preform(test);
				passedTest+=testOK;
			}catch(IOException e){
				System.out.println("Couldn't compile "+test.getName());
			}	
		}
		System.out.println("Results:"+passedTest+"/"+tests.size());
	}
	
	/**
	 * Process all command line arguments to options.
	 *  @param args 
	 *  		The command line options to use.
	 *  @return 
	 *  		The Option information stored in a container object.
	 */
	private OptionData setupOptions(String[] args){
		int i=0;
		OptionData oData = new OptionData();
		boolean oneTest = false;
		while(i<args.length){
			if (args[i].equals("-noGen")){
				oData.noGen=true;
			} else if (args[i].equals("-oneTest")){
				oData.oneTest=true;
				if (i==args.length){
					displayHelp("missing [testfolder]"); oData.exit=true;
				}
				oData.testName=args[++i];
			} else if (args[i].equals("-timeout")){
				timeout = Integer.parseInt(args[i++]);
			} else if (args[i].equals("-fast")){
				oData.fastTest=true;
			} else if (args[i].equals("-stopFail")){
				oData.stopFail=true;
			} else if (args[i].equals("-h") || args[i].equals("-help")){
				displayHelp(""); oData.exit=true;
			} else if ( args[i].startsWith("-")){
				displayHelp(args[i]); oData.exit=true;
			} else{
				return oData;
			}
		i++;
		}
		return oData;
	}
	
	/**
	 * Display a help message with instructions for how to use the tester.
	 * @param faultyOption
	 * 			The command option that cause intization to fail.
	 */
	private void displayHelp(String faultyOption){
		System.out.println("JastAdd Purityannotations tester");
		if (!faultyOption.equals("")){
			System.out.println("");
			System.out.println("The tool failed because of the unexpected "+faultyOption);
			System.out.println("");
		}
		System.out.println("Usage instructions:");
		System.out.println("1. place the jastAdd2.jar which is purityAnnotating in tools/");
		System.out.println("2. run a command on the form [Options] [testfolders].");
		System.out.println("Not providing a [testfolder] option imply test all testfolders.");
		System.out.println("");
		System.out.println("Options to provide");
		System.out.println("run without any option to preform all tests");
		System.out.println("-noGen : skips running jastAdd2.jar and simply check previous generated code");
		System.out.println("-oneTest: to run one specific test provide as the last argument");
		System.out.println("-fast: to run only a few overarching tests of all the primary constructions");
		System.out.println("-stopFail : stop on a failure.");
		System.out.println("-timeout x:timeout after x ms of jastAdd weaving (5000) standard");
		System.out.println("-h / -help: display this help information");
	}
	
	/**
	 * Compiles Java source from the JastAdd sources in the given folder.
	 * The resulting Java code is placed in a folder under gen/ with same tree structure
	 * as where the JastAdd source is placed under source/.
	 * @param folder
	 * 			The folder containing the JastAdd sources.
	 * @return 
	 * 			If Compilation worked or not in form of a number (1 or 0).
	 */
	private int compileJastAdd(File folder) throws IOException{
		File JastAdd = new File("tool"+File.separatorChar+"jastadd2.jar");
		if (!JastAdd.exists()){
			System.err.println("jastadd2.jar is missing from tool folder");
			System.exit(-1);
		}
		File[] Files = folder.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".ast") || name.toLowerCase().endsWith(".jrag") || name.toLowerCase().endsWith(".jreg");
			}
		});
		String[] files = new String[Files.length];
		for (int i = 0; i < Files.length; i++) {
			files[i] = Files[i].getCanonicalPath();
		}
		String commandString = Arrays.toString(files);
		String folderLocation =Paths.get(source).relativize(folder.toPath()).toString();
		commandString = commandString.substring(1, commandString.length()-1).replace(',', ' ');
		//Step1: Empty Folder!!
		folder.delete();
		//Step2 : Generate!!
		ProcessThread JastAddproc = new ProcessThread("java -jar tool"+File.separatorChar+
				"jastadd2.jar --rewrite=cnta "+"--package=testfiles/gen/"+folderLocation+" "+commandString,System.out,System.err);
		JastAddproc.start();
		try{
		JastAddproc.join(5000l);
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Failed JastAdd generation");
			System.exit(-1);
		}
		int exitValue = JastAddproc.exitCode;
		if (exitValue!=0){
			System.out.println("Failed Test");
			return 0;
		}
		return 1;
	}
	
	/** 
	 *  Tests the result after generating the files from folder f (a source folder).
	 *  The generated files in the corresponding gen folder
	 *  needs to be checked against the files in corresponding goal folder.
	 *  @param f
	 *  		The folder with the JastAdd sources for this test.
	 *  @return 
	 * 			If the generated and target code is similar.
	 * 			The result is given in form of a number (1 or 0).
	 */
	private int preform(File f){
		try{
			File gen=new File(f.getCanonicalPath().replace(source,
					"testfiles"+File.separatorChar+"gen"));
			File goal=new File(f.getCanonicalPath().replace(source,
					"testfiles"+File.separatorChar+"goal")); 
			if (gen.exists() && goal.exists()){
				File[] genFiles = gen.listFiles(new FilenameFilter() {
					public boolean accept(File dir, String name) {
						return name.toLowerCase().endsWith(".java");
					}
				});
				File[] goalFiles = goal.listFiles(new FilenameFilter() {
					public boolean accept(File dir, String name) {
						return name.toLowerCase().endsWith(".java");
					}
				});
				int errors=0;
				for (int i=0;i<genFiles.length;i++){
					errors+=compareFile(genFiles[i],goalFiles[i]);
				}
				if (errors!=0)
					System.out.println(f.getCanonicalPath()+" failed due to errors");
				return errors == 0 ? 1 : 0;
			}
			System.out.println(f.getCanonicalPath()+" failed due to not generated");
		}catch(IOException e){
			System.out.println(f.getName()+"failed due to IOException");
			e.printStackTrace();
		}
		
		return 0;
	}
	
	/**
	 * Compares JastAdd generated sources only (signatures relevant)
	 * Only lines with a @-interface are checked. 
	 * @param gen
	 * 			The latest generated file.
	 * @param goal 
	 * 			The target generated file.
	 * @return 
	 * 			If any annotations where changed or not.
	 * 			
	 */
	private int compareFile(File gen,File goal){
		try{
			boolean hasProblem=false;
			if (gen.getName().equals(goal.getName())){
				boolean errors = false;
				Scanner scanA=new Scanner(gen);
				Scanner scanB=new Scanner(goal);
				while(scanB.hasNextLine() && scanA.hasNextLine()){
					String lineA = scanA.nextLine().trim();
					String lineB = scanB.nextLine().trim();
					while(scanB.hasNextLine() && !(lineB.startsWith("@") &&!lineB.contains("@return") && !lineB.contains("@declaredat") && !lineB.contains("@ASTNodeAnnotation")))
						lineB = scanB.nextLine().trim();
					while(scanA.hasNextLine() && !(lineA.startsWith("@") &&!lineA.contains("@return") && !lineA.contains("@declaredat") && !lineA.contains("@ASTNodeAnnotation")))
						lineA = scanA.nextLine().trim();
					String Anorm = lineA.replace("\r", "");
					String Bnorm = lineB.replace("\r", "");
					if (!Anorm.equals(Bnorm)){
						System.out.println("File:"+goal.getName());
						System.out.println(lineA+"differers form the default of "+lineB); 
						int diff = differenceString(Anorm,Bnorm);
						System.out.println("Difference form :"+lineA.substring(diff)+" diff=="+diff);
						errors=true;
						hasProblem=true;
						System.out.println("");
					}
				}
				if (hasProblem)
					System.out.println("");
				return errors ? 1: 0;
			}
			System.out.println("Comparing files with different names "+gen.getName()+" and "+goal.getName());
			System.exit(1);
			return 1;
		}catch (FileNotFoundException e){
			e.printStackTrace();
			if (!gen.exists())
				System.out.println("Can't find file "+gen.getName()+".");
			if (!goal.exists())
				System.out.println("Can't find file "+goal.getName()+".");
		}
		return 1;
	}
	
	/**
	 * Find the first difference between a pair of strings.
	 * @param A
	 * 			The first string to check.
	 * @param B
	 * 			The second string to check. 
	 * @return
	 * 			The index of the first difference of the length differencial 
	 * 			in the case of A.length != B.length.
	 */
	public int differenceString(String A, String B){
		if (A.length()!=B.length()){
			System.out.println("Length difference "+(A.length()-B.length()));
			return 0;
		}
		int i=0;
		while(i<A.length()){
			if(A.charAt(i)==B.charAt(i)){
				i++;
			}else{
				return i;
			}
		}
		return A.length();
	}
}
