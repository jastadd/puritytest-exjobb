public class A{ 
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.COLL)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="X.jrag:10")
	@Pure(group="coll") public ArrayList<String> coll() {
		...
		A_coll_value = coll_compute();
	}

	@Fresh(group="coll") private ArrayList<String> coll_compute() {
		ASTNode node = this;
		while (node != null && !(node instanceof A)) {
			node = node.getParent();
		}
		A root = (A) node;
		root.survey_A_coll();
		ArrayList<String> _computedValue = new ArrayList<String>();
		if (root.contributorMap_A_coll.containsKey(this)) {
		for (ASTNode contributor : root.contributorMap_A_coll.get(this)) {
			contributor.contributeTo_A_coll(_computedValue);
			}
		}
		return _computedValue;
	}
	// Helper method
	@Pure(group="coll") protected void survey_A_coll() {
	    if (contributorMap_A_coll == null) {
	      contributorMap_A_coll = new java.util.IdentityHashMap<ASTNode, java.util.Set<ASTNode>>();
	      collect_contributors_A_coll(this, contributorMap_A_coll);
	    }
	  }
	
	@Pure protected void collect_contributers_A_coll(A _root, @Local _map){
		// Collect contribution
		if (condition()){
			A target = (A) (a());
			java.util.Set<ASTNode> contributors = _map.get(target);
			if (contributors == null) {
			contributors = new java.util.LinkedHashSet<ASTNode>();
			_map.put((ASTNode) target, contributors);
			}
		}
		super.collect_contributors_A_set(_root, _map);
	}
	@Secret(group="coll") protected java.util.Map<ASTNode, java.util.Set<ASTNode>> contributorMap_A_coll = null;
	
	@Pure protected void contributeTo_A_coll(@Local ArrayList<String> collection) {
	    super.contributeTo_A_coll(collection);
	    if (condition()) 
	      collection.add(getID());
	  }	
}

