public class A{
	@SideEffect.Local public ASTNode rewriteTo() {
		if (RewriteCondition0())) 
			return rewriteRule0();
		return super.rewriteTo();
	}
	// Rewrites should allow itself to be rewriteable
	@Fresh(rewrite=true) private D rewriteRule0() {
		// Create new C node
		D newD= new D(...);
		newD.children[1]=this.children[2];
		newD.children[0]=this.children[1];
		newD.setB(getB());
		return D;
	}
	/** @apilevel internal */
	@Pure public boolean canRewrite() {
		if (RewriteCondition0()) {
			return true;
		return false;
	}
	
	@Pure public boolean mayHaveRewrite() {
		return true;
	}
	
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, 
			isCircular=true, isNTA=true)
	@ASTNodeAnnotation.Source(aspect="", declaredAt=":0")
	@SideEffect.Local(group="_ASTNode") public ASTNode rewrittenNode() {
		// ... circular NTA more or less ...
	}
	// Circular NTA fields ....
}
