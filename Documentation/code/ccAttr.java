public class A{
    /** @apilevel internal */
  @Secret(group="_ASTNode")
  protected boolean circAttr_computed = false;
  ------- protected boolean circAttr_visited = false;
  ------- protected String circAttr_value;
  ------- protected boolean circAttr_initialized = false;
  ------- protected ASTState.Cycle circAttr_cycle = null;

  @Pure private String synAttr_compute() {
	 // somthing pure
  }
  @Pure private String initial() {
	 // A inital value (user provided)
  }
  @SideEffect.Ignore private void synAttr_String_reset() {}
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, 
		  isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="X.jrag:3")
  @Pure(group="_ASTNode") public String ASTNode.synAttr(){
		if (circAttr_computed) {
			return circAttr_value;
		}
		ASTState state = state();
		if (!circAttr_initialized) {
			circAttr_initialized = true;
			circAttr_value = inital(); //  bottom value 
		}
		if (!state.inCircle() || state.calledByLazyAttribute()) {
			state.enterCircle();
			. . .
			circAttr_computed = true;
			. . .
			state.leaveCircle();
		} else if (circAttr_cycle != state.cycle()) {
			circAttr_cycle = state.cycle();
			if (state.lastCycle()) {
				circAttr_computed = true;
				boolean new_circAttr_value = synAttr_compute();
				return new_circAttr_value;
			}
			boolean new_circAttr_value = synAttr_compute();
			if (new_circAttr_value != circAttr_value) {
				state.setChangeInCycle();
			}
			circAttr_value = new_circAttr_value;
		}
		return circAttr_value;
  } 
}
