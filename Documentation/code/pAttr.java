public class A{
    /** @apilevel internal */
  @Secret(group="_ASTNode") protected boolean synAttr_computed = false;
  @Secret(group="_ASTNode") protected boolean synAttr_visited = false;
  @Secret(group="_ASTNode") protected String synAttr_value;

  @Pure private String synAttr_compute() {
	 // somthing pure
  }
  
  @SideEffect.Ignore private void synAttr_String_reset() {
	synAttr_value = null;
	synAttr_visited = false;
  }
  
  @Pure(group="_ASTNode") public Object ASTNode.synAttr(){
   . . .
  if (synAttr_computed) {
    return synAttr_value;
  }
   . . .
  synAttr_value = synAttr_compute();
  synAttr_computed = true;
  synAttr_visited = false;
  return synAttr_value;
  } 
}
