
public class ASTNode{
	/** @apilevel internal */
	@SideEffect.Secret(group="_ASTNode") 
	protected boolean getA_visited = false;
	@SideEffect.Secret(group="_ASTNode")
	protected boolean getA_computed = false;
	@SideEffect.Secret(group="_ASTNode") 
	protected A getA_value;
	
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN,
			isNTA=true)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="X.jrag:2")
	@SideEffect.Pure(group="_ASTNode") public A getA() {
		. . . 
		A getA_value = getA_compute();
		setChild(getA_value, getAChildPosition());
		. . .
		A node = (A) this.getChild(getAChildPosition());
		return node;
	}
	
	@SideEffect.Fresh private A getA_compute() {
		// Pure effects
		A node = new A(new B(ID), new B(ID));
		return node;
	}
	
}
