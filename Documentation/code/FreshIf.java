import java.util.Set;

public class Test{
   int y = 9;
	@FreshIf @Local public Test freshIf(){
		return this;
	}
	@Fresh public Test freshIf2(){
		return this;
	}
	@Local public Test freshIf3(){
		return this;
	}
	
	@Fresh public void freshIf3(){ 
		Map<Test, Set<Test>> x= new ...();
		x.put(this,new HashSet<Test>());
		modSet(x.get(this)); 
		// Can only work under
		// @Local Map<...,...>.put(@FreshIf x)
		// @FreshIf Map<...,...>.get(this);
	}
	
	@Local public void modSet(@Local Set<Test> t){ 
		t.add(this);
	}
}
