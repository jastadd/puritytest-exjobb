package test;
 
public class A{
	@SideEffect.Ignore public void flushAttrCache() {
		super.flushAttrCache();
		attr_String_reset();
	}	
	@SideEffect.Ignore public void flushCollectionCache() {
		super.flushCollectionCache();
		A_b_visited = false;
		A_b_computed = false;
		A_b_value = null;
		contributorMap_Node_b = null;
		. . .
	}
}
