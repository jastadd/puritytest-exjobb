
/**
 * @ast class
 * @declaredat ASTNode:259
 */
public class SideEffect {
	@java.lang.annotation.Retention(
		java.lang.annotation.RetentionPolicy.RUNTIME)
	@java.lang.annotation.Target({METHOD,
		CONSTRUCTOR, PARAMETER   })
	public @interface FreshIf {
	}

	@java.lang.annotation.Retention(
		java.lang.annotation.RetentionPolicy.RUNTIME)
	@java.lang.annotation.Target({METHOD,
		CONSTRUCTOR, PARAMETER,
		LOCAL_VARIABLE,FIELD   })
	public @interface Fresh {
		String group() default "";
		boolean rewrite() default false;
	}

	@java.lang.annotation.Retention(
		java.lang.annotation.RetentionPolicy.RUNTIME)
	@java.lang.annotation.Target({METHOD,
		CONSTRUCTOR,PARAMETER,
		LOCAL_VARIABLE,FIELD   })
	public @interface NonFresh {
		String group() default "";
	}


	@java.lang.annotation.Retention(
		java.lang.annotation.RetentionPolicy.RUNTIME)
	@java.lang.annotation.Target({FIELD,
		METHOD   })
	public @interface Secret {
		String group() default "";
	}


	@java.lang.annotation.Retention(
		java.lang.annotation.RetentionPolicy.RUNTIME)
	@java.lang.annotation.Target({TYPE   })
	public @interface Entity {
	}



	@java.lang.annotation.Retention(
		java.lang.annotation.RetentionPolicy.RUNTIME)
	@java.lang.annotation.Target({METHOD,
		CONSTRUCTOR,PARAMETER,
		LOCAL_VARIABLE,FIELD   })
	public @interface Ignore {
	}



	@java.lang.annotation.Retention(
		java.lang.annotation.RetentionPolicy.RUNTIME)
	@java.lang.annotation.Target({METHOD,
		CONSTRUCTOR,FIELD,
		PARAMETER   })
	public @interface Local {
		String group() default "";
	}

	@java.lang.annotation.Retention(
		java.lang.annotation.RetentionPolicy.RUNTIME)
	@java.lang.annotation.Target(METHOD,
		java.lang.annotation.ElementType.CONSTRUCTOR)
	public @interface Pure {
		String group() default "";
	}
}
