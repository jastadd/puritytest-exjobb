// Simplified overview of Freshness analysis
// Freshness=local information if knownlocally otherwise
// Freshness=heapStatus(Access chain) (cfg lookup)
// heapStatus(Access chain)=
//		min(currentDefinitions((Access chain).Freshness()),

// Ignore>Fresh>Local>NonFresh>Maybe
public class Test {
	@Local Test LT;
	Test T;
	int x;
	@Fresh public test FreshAnalysis(int a){
		Test test = new Test(); //*1 // Freshness=Fresh 
		test.T = new Test(); //*2 // Freshness=Fresh
		test.LT.T = new test(); //*3
		if (x>a){
		test.T =  FreshAnalysis(4); //*4 // Freshness=Fresh
		}else{
		test.LT.T = T; } //*5 	
		// Freshness=NonFresh
		if (x==10)
		test.T=test.LT.T; //*6 
		// Freshness=heapStatus(test.LT.T)  ->
		// 			 currentDefinitions={*2,*4,*5}
		test.LT.LT.T = test(); //*7
		return test.T.LT.LT;  
		// Freshness=heapStatus(test.T)-> 
		//	min(currentDefinitions(test.T).Freshness()),
		// with currentDefinitions(test.T) = {*6,*4,*3} 
		// Freshness=min(heapStatus(test.LT.T),Fresh,Fresh)
		// Freshness=min(min(Fresh,Maybe,NonFresh),Fresh)
		// Freshness=Maybe -> wrong because *4
	}
}

 