public class Test{
   @Local Test Lt;
   Test t;
   int i;
   
   @Pure public void AliasDemo(){
		Test tester = new Test(); 
		test1.t= new Test(); 
		Test aliaser = tester; 
		aliaser.t = t; // tester.t no longer fresh
		tester.t.i++; //Wrong !
		// The same is not possible for Local field due to Locals rules
		
		aliaser.Lt = t; // Not allowed due to t not fresh.
		// Any aliasing variable can only place fresh objects
		// in which case the freshness is preserved
		aliaser.Lt = new Test(); 
		// tester.Lt consequently always fresh invariant of any aliasing;	
		tester.Lt.t = t; // always acceptable
   }
}
