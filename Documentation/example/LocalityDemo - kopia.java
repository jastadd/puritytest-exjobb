public class Test{
   int x;
   Test old;
   @Local Test locals;
   @Pure public void Locality(){
	   Test t=new test();
	   t.t = new test();
	   t.t.t = old; 
	   // Loop updating t.i ,t.t.i ,t.t.t.i ;
	   // Generally problematic to check
	   for (int i=0;i<3,i++){
		   t.i++; // wrong because of t.t.t;
		   t = t.t;
   		}
	   // @Local provides an infinite change of
	   // locations that are either null or also fresh
	   // which is whats general wanted if access chains 
	   // longer than t.t should be modifiable.
	   for (int i=0;i<3,i++){
		   t.i++; 
		   t = t.locals;
   		}
   }
}
