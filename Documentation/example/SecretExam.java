public class Secret{
    @Secret(group="group1") boolean b;
    @Secret(group="group1") List<String> secretlist;
    @Secret(group="group2") int a;
    private int open;
    
    // May not change the Secret fields
    public Secret(int x){
    open=x;
    }
    // May change group2 fields and un annotated
    @Pure(group="group2") Secret(int x,int y){
    open=x;
    a=y;
    }
    
    @Pure(group="group1") 
    public List<String> worker(){
    	if (b)
    	return secretlist;
    	secretlist=calcSecret();
    	b=true;
    	return secretlist;
    }
}