import lang.ast.SideEffect.*;
public class Main{
	@Pure public static void main(String[] args){
		A p=new A();
		A x=new B();
		p.getOpen(); //Wrong!!
		x.getOpen(); // (Type dataflow = Okey, declared type= Wrong)
	}
}

public class A{
    public int getOpen(){
    	return 5;}
}

public class B extends A{
	@Pure public int getOpen(){
		return 5;}
}