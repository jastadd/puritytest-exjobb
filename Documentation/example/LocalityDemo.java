public class Test{
   int x;
   Test t;
   @Local Test Lt;
   
   @Local public Test FreshnessDemo1(){
		Test tester = new Test(); // test1 = [Fresh]
		tester.t= new Test(); // tester.t = [Fresh]
		tester.t.Lt.Lt=tester;
		// The locality top for tester.t.Lt.Lt is tester.t
		//tester.t is deduced to fresh of "new Test()" and assign valid.
		
		tester.FreshnessDemo1().Lt.Lt = tester; 
		// Locality top is tester.FreshnessDemo1() which has not Fresh
		// assignment is thus wrong!!

		tester.t.Lt.t = new Test(); 
		Lt = tester.t.Lt.t.Lt;
		// The locality top "tester.t.Lt.t" is a 
		// to long access chain. It unlikely that it should be fresh and
		// expencive to keep track of field information so deep in objects.
		
		tester.Lt.t = tester;
		Lt = tester.Lt.t.Lt.Lt 
		// Locality Top = tester.Lt.t which is Fresh okay
				
		// Loop updating tester.i , ... , tester.t.t.t.i .
		// .Verifing all individually would be expensive way
		// of finding the the error at tester.t.t.t.i especially if 
		// the loop is long. 
		tester.t = new tester();
		tester.t.t = new tester();
		for (int i=0;i<4,i++){
			tester.i=0; 
			tester = tester.t;
	   	}
		
		// @Local provides an infinite chain of
		// locations that are either null or fresh.
		  while(tester!=null){
			   tester.i=0; 
			   tester = tester.Lt;
	   		}
		return g()>3 ? t : tester;
   }
   
   
}
