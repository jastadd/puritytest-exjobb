import lang.ast.SideEffect.*;
public class ExtendandImpl03 extends
  Parent implements Interface01{
   //Wrong missing @Fresh!
  public Integer freshMethod(){return 1;} 
  //Wrong needed @Pure!
  @Local public Integer pureMethod(){return 1;} 
}

public class Parent{
  @Pure public Integer freshMethod(){return 1;}
  @Pure public Integer pureMethod(){return 1;}
}

public interface Interface01{
  @Fresh public int freshMethod();
  public int pureMethod();
}